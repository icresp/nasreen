package com.vansale.icresp.nasreen.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.SimpleFragmentPagerAdapter;
import com.vansale.icresp.nasreen.model.Shop;

import static com.vansale.icresp.nasreen.config.ConfigKey.SHOP_KEY;


public class ReceiptHistoryActivity extends AppCompatActivity {


    private TextView tvToolBarShopName;
    private ImageButton ibBack;
    private Shop SELECTED_SHOP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_history);

        ibBack = findViewById(R.id.imageButton_toolbar_back);
        tvToolBarShopName = findViewById(R.id.textView_toolbar_shopNameAndCode);

        // Find the view pager that will allow the user to swipe between fragments
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_receipt_history);

        // Create an adapter that knows which fragment should be shown on each page
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_receipt_history);
        tabLayout.setupWithViewPager(viewPager);

        try {
            SELECTED_SHOP = (Shop) getIntent().getSerializableExtra(SHOP_KEY);
        } catch (Exception e) {
            e.getStackTrace();
        }

        if (SELECTED_SHOP == null) {
            finish();
            return;
        }


        tvToolBarShopName.setText(String.valueOf(SELECTED_SHOP.getShopName() + "\t" + SELECTED_SHOP.getShopCode()));
        tvToolBarShopName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvToolBarShopName.setSelected(true);


        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
