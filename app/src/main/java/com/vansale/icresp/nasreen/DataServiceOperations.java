package com.vansale.icresp.nasreen;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.NoSale;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.webservice.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigKey.CUSTOMER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.DAY_REGISTER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_ANY_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_CUSTOMER_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_QUOTATION_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RECEIPT_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.webservice.WebService.webDayClose;
import static com.vansale.icresp.nasreen.webservice.WebService.webNoSale;
import static com.vansale.icresp.nasreen.webservice.WebService.webPaidReceipt;
import static com.vansale.icresp.nasreen.webservice.WebService.webPlaceOrder;
import static com.vansale.icresp.nasreen.webservice.WebService.webPlaceQuotation;

/**
 * Created by mentor on 24/11/17.
 */

public class DataServiceOperations {

    OnNotifyListener listener;
    MyDatabase myDatabase;
    private Context context;
    private String TAG = "DataServiceOperations";
    private String EXECUTIVE_ID = "";

    private SessionValue sessionValue;
    private SessionAuth sessionAuth;
    private String dayRegId;

    ArrayList<Sales> saleList = new ArrayList<>();
    ArrayList<Sales> quotationList = new ArrayList<>();

    ArrayList<Receipt> receipts = new ArrayList<>();
    ArrayList<OpeningBalanceTransaction> openingBalances = new ArrayList<>();

    ArrayList<NoSale> noSales = new ArrayList<>();

    public DataServiceOperations(Context context, OnNotifyListener mListener) {
        this.context = context;
        this.sessionAuth = new SessionAuth(context);
        this.EXECUTIVE_ID = sessionAuth.getExecutiveId();
        this.myDatabase = new MyDatabase(context);
        this.sessionValue = new SessionValue(context);

        this.listener = mListener;
    }

    public void dayClose() {

        this.dayRegId = sessionValue.getDayRegisterId();

        this.saleList = myDatabase.getAllSales();
        this.quotationList = myDatabase.getAllQuotations();

        this.receipts = myDatabase.getAllTransactions();
        this.openingBalances = myDatabase.getOpeningBalancesTransactions();

        this.noSales = myDatabase.getAllNoSaleReasons();
        if (!saleList.isEmpty())
            placeOrder();
        else if (!quotationList.isEmpty())
            placeQuotation();
        else if (!receipts.isEmpty() || !openingBalances.isEmpty())
            paidReceipt();
        else if (!noSales.isEmpty())
            noSaleRequest();
        else
            dayCloseRequest();
//        placeOrder_();
    }

    private void placeOrder_() {


        final JSONObject object = new JSONObject();

        final JSONArray saleArray = new JSONArray();


        try {
            for (Sales s : saleList) {

                final JSONObject saleObj = new JSONObject();

                saleObj.put(CUSTOMER_KEY, s.getCustomerId());
                saleObj.put("bill_date", s.getDate());
                saleObj.put("total_amount", s.getTotal()-s.getCollectionDiscount());
                saleObj.put("sale_type", s.getSaleType());
                saleObj.put("paid_amount", s.getPaid());
                saleObj.put("invoice_no", s.getInvoiceCode());

                saleObj.put("purchase_order_no", s.getSalePo());
                saleObj.put("total_discount", s.getDiscountTotal());

                if (s.getPaid() == s.getTotal())
                    saleObj.put("invoice_type", "CashSale");
                else
                    saleObj.put("invoice_type", "CreditSale");

                JSONArray cartArray = new JSONArray();

                for (CartItem c : s.getCartItems()) {

                    JSONObject obj = new JSONObject();

                    obj.put("product_id", c.getProductId());
                    obj.put("unit_price", c.getProductPrice());
                    obj.put("net_price", c.getNetPrice());
                    obj.put("sale_price", c.getSalePrice());
                    obj.put("disc", c.getDiscountPerce());
                    obj.put("product_quantity", c.getPieceQuantity());
                    obj.put("product_total", c.getTotalPrice());
                    cartArray.put(obj);

                }

                saleObj.put("ordered_products", cartArray);

                saleArray.put(saleObj);

            }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("Sale", saleArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        printLog(TAG, "placeOrder  object   " + object);

            }

    //    place order
    private void placeOrder() {

        final ProgressDialog pd = ProgressDialog.show(context, null, "Please wait...", false, false);

        final JSONObject object = new JSONObject();

        final JSONArray saleArray = new JSONArray();

        try {
            for (Sales s : saleList) {

                final JSONObject saleObj = new JSONObject();

                saleObj.put(CUSTOMER_KEY, s.getCustomerId());
                saleObj.put("bill_date", s.getDate());
                saleObj.put("total_amount", s.getTotal()-s.getCollectionDiscount());
                saleObj.put("sale_type", s.getSaleType());
                saleObj.put("paid_amount", s.getPaid());
                saleObj.put("invoice_no", s.getInvoiceCode());

                saleObj.put("purchase_order_no", s.getSalePo());
                saleObj.put("total_discount", s.getDiscountTotal());

                saleObj.put("latitude", s.getSaleLatitude());
                saleObj.put("longitude", s.getSaleLongitude());

                if (s.getPaid() == s.getTotal())
                    saleObj.put("invoice_type", "CashSale");
                else
                    saleObj.put("invoice_type", "CreditSale");

                JSONArray cartArray = new JSONArray();

                for (CartItem c : s.getCartItems()) {

                    JSONObject obj = new JSONObject();

                    obj.put("product_id", c.getProductId());
                    obj.put("unit_price", c.getProductPrice());
                    obj.put("product_quantity", c.getPieceQuantity());
                    obj.put("product_unit", c.getProductUnit());
                    obj.put("net_price", c.getNetPrice());
                    obj.put("product_total", c.getTotalPrice());

                    cartArray.put(obj);
                }

                saleObj.put("ordered_products", cartArray);
                saleArray.put(saleObj);
            }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("Sale", saleArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        printLog(TAG, "placeOrder  object   " + object);
        Log.e(TAG, "placeOrder  object   " + object);

        webPlaceOrder(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                printLog(TAG, "placeOrder  response   " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_SALE_TYPE);

                        Log.v(TAG, "placeOrder  deleteStatus   " + deleteStatus);

                        if (!quotationList.isEmpty())
                            placeQuotation();
                        else {

                            if (!receipts.isEmpty() || !openingBalances.isEmpty())
                                paidReceipt();
                            else {

                                if (!noSales.isEmpty())
                                    noSaleRequest();
                                else
                                    dayCloseRequest();
                            }
                        }

                    } else
                        Toast.makeText(context, "Orders " + response.getString("status"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        }, object);

    }

    //    place quotation
    private void placeQuotation() {

        final ProgressDialog pd = ProgressDialog.show(context, null, "Please wait...", false, false);

        final JSONObject object = new JSONObject();

        final JSONArray quotationArray = new JSONArray();

        try {
            for (Sales s : quotationList) {

                final JSONObject saleObj = new JSONObject();

                saleObj.put(CUSTOMER_KEY, s.getCustomerId());
                saleObj.put("bill_date", s.getDate());
                saleObj.put("total_amount", s.getTotal());
                saleObj.put("sale_type", s.getSaleType());
                JSONArray cartArray = new JSONArray();

                for (CartItem c : s.getCartItems()) {

                    JSONObject obj = new JSONObject();

                    obj.put("product_id", c.getProductId());
                    obj.put("unit_price", c.getProductPrice());
                    obj.put("product_quantity", c.getPieceQuantity());
                    obj.put("product_total", c.getTotalPrice());
                    cartArray.put(obj);

                }

                saleObj.put("ordered_products", cartArray);

                quotationArray.put(saleObj);

            }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("Sale", quotationArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.v(TAG, "placeQuotation  object   " + object);


        webPlaceQuotation(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                Log.v(TAG, "placeQuotation  response   " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_QUOTATION_TYPE);
                        Log.v(TAG, "placeQuotation  deleteStatus   " + deleteStatus);


                        if (!receipts.isEmpty() || !openingBalances.isEmpty())
                            paidReceipt();
                        else if (!noSales.isEmpty())
                            noSaleRequest();
                        else
                            dayCloseRequest();


                    } else
                        Toast.makeText(context, "Quotation " + response.getString("status"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        }, object);


    }


    //    paid receipt
    private void paidReceipt() {

        final ProgressDialog pd = ProgressDialog.show(context, null, "Please wait...", false, false);

        JSONObject object = new JSONObject();
        JSONArray invArray = new JSONArray();
        JSONArray openBalArray = new JSONArray();
        try {

            if (!receipts.isEmpty())
//                for (R invoice : invoiceList) {
                for (Receipt receipt : receipts) {

//                    for (Transaction t : invoice.getTransactions()) {

                    final String invoiceNo = receipt.getInvoiceNo();
                    final double receivableAmount = receipt.getReceivedAmount();
                    final String receiptNo = receipt.getReceiptNo();

                    String paymentType = receipt.getTransactionType();
                    String cheque = receipt.getChequeNumber();
                    String bankName = receipt.getBankName();
                    String issueDate = receipt.getIssueDate();
                    String clearDate = receipt.getClearDate();
                    String remark = receipt.getRemark();

                    JSONObject obj = new JSONObject();
                    obj.put("invoice_no", invoiceNo);
                    obj.put("amount", receivableAmount);
                    obj.put("receipt_no", receiptNo);

                    obj.put("receipt_type", paymentType);
                    obj.put("cheque_no", cheque);
                    obj.put("bank_name", bankName);
                    obj.put("issue_date", issueDate);
                    obj.put("clear_date", clearDate);
                    obj.put("remark", remark);
                    if (receivableAmount != 0)
                        invArray.put(obj);

//                    }
                }

            if (!openingBalances.isEmpty())
                for (OpeningBalanceTransaction openingBalance : openingBalances) {

                    int customerId = openingBalance.getCustomerId();
                    double receiveBalAmount = openingBalance.getReceivedAmount();
                    String paymentType = openingBalance.getTransactionType();
                    String cheque = openingBalance.getChequeNumber();
                    String bankName = openingBalance.getBankName();
                    String remark = openingBalance.getRemark();
                    String receiptNo = openingBalance.getReceiptNo();
                    String issueDate = openingBalance.getIssueDate();
                    String clearDate = openingBalance.getClearDate();

                    JSONObject openObj = new JSONObject();
                    openObj.put(CUSTOMER_KEY, customerId);
                    openObj.put("amount", receiveBalAmount);
                    openObj.put("receipt_no", receiptNo);
                    openObj.put("receipt_type", paymentType);
                    openObj.put("cheque_no", cheque);
                    openObj.put("bank_name", bankName);
                    openObj.put("issue_date", issueDate);
                    openObj.put("clear_date", clearDate);
                    openObj.put("remark", remark);
                    if (receiveBalAmount != 0)
                        openBalArray.put(openObj);
                }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("PaidList", invArray);
            object.put("openList", openBalArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.v(TAG, "paidReceipt  object  " + object);

        webPaidReceipt(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                Log.v(TAG, "paidReceipt  response  " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_RECEIPT_TYPE);

                        Log.v(TAG, "paidReceipt  deleteStatus   " + deleteStatus);

                        if (!noSales.isEmpty())
                            noSaleRequest();
                        else
                            dayCloseRequest();

                    } else
                        Toast.makeText(context, "Receipt " + response.getString("status"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();
            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        }, object);
    }

    //    no sale
    private void noSaleRequest() {


        final ProgressDialog pd = ProgressDialog.show(context, null, "Please wait...", false, false);


        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();

        try {

            for (NoSale noSale : noSales) {

                JSONObject obj = new JSONObject();
                obj.put(CUSTOMER_KEY, noSale.getCustomerId());
                obj.put("reason", noSale.getReason());
                obj.put("latitude", noSale.getLatitude());
                obj.put("longitude", noSale.getLongitude());

                array.put(obj);

            }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("Visits", array);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "noSaleRequest  object  " + object);

        webNoSale(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                Log.v(TAG, "noSaleRequest  response  " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_CUSTOMER_TYPE);

                        Log.v(TAG, "paidReceipt  deleteStatus   " + deleteStatus);

                        if (deleteStatus)
                            dayCloseRequest();
                        else
                            Toast.makeText(context, "No Sale  failed", Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(context, "No Sale " + response.getString("status"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                pd.dismiss();
            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        }, object);
    }


    //    day close request
    private void dayCloseRequest() {

        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put("closing_stock", myDatabase.getStockAmount());
            object.put(DAY_REGISTER_KEY, dayRegId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        printLog(TAG, "dayCloseRequest  object   " + object);

        final ProgressDialog pd = ProgressDialog.show(context, null, "Please wait...", false, false);

        webDayClose(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                printLog(TAG, "dayCloseRequest  response   " + response);

                try {
                    if (response.getString("status").equals("success")) {
                        // change status
                        sessionValue.storeGroupAndRoute(null, null, 0, "");
                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_ANY_TYPE);
                        printLog(TAG, "dayCloseRequest  deleteStatus   " + deleteStatus);

                        if (listener != null)
                            listener.onNotified();

                        Toast.makeText(context, "Day Close Successful", Toast.LENGTH_SHORT).show();

                    } else if (response.getString("status").equals("Already Closed")) {
                        boolean deleteStatus = myDatabase.deleteTableRequest(REQ_ANY_TYPE);
                        printLog(TAG, "dayCloseRequest  deleteStatus   " + deleteStatus);

                        // change status
                        sessionValue.storeGroupAndRoute(null, null, 0, "");

                        if (listener != null)
                            listener.onNotified();
                        Toast.makeText(context, response.getString("status"), Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(context, "Day Close " + response.getString("status"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        }, object);
    }
}
