package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.model.CartItem;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


/**
 * Created by mentor on 30/10/17.
 */

public class RvReturnInvoiceAdapter extends RecyclerView.Adapter<RvReturnInvoiceAdapter.RvReturnProductHolder> {

    private ArrayList<CartItem> cartItems;

    private Context context;

    String TAG = "RvReturnInvoiceAdapter";


    public OnNotifyListener listener;

    public RvReturnInvoiceAdapter(ArrayList<CartItem> cartItems, OnNotifyListener listener) {
        this.cartItems = cartItems;

        try {
            this.listener = listener;
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    @Override
    public RvReturnProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_return_invoice, parent, false);


        return new RvReturnProductHolder(view);
    }

    @Override
    public void onBindViewHolder(RvReturnProductHolder holder, int position) {


        final CartItem cartItem = cartItems.get(position);
        int s = position + 1;

        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvProductCode.setText(cartItem.getProductCode());
        holder.tvProductName.setText(cartItem.getProductName());

        holder.tvActualQty.setText(String.valueOf(cartItem.getPieceQuantity()));

        holder.tvReturnQty.setText(String.valueOf(cartItem.getReturnQuantity()));


        holder.tvNetPrice.setText(getAmount(cartItem.getNetPrice()));

        holder.tvTotal.setText(String.valueOf(getAmount(cartItem.getNetPrice() * cartItem.getReturnQuantity()) + " " + context.getString(R.string.currency)));


        if (listener != null)
            listener.onNotified();


    }


    public ArrayList<CartItem> getReturnItems() {
        return cartItems;
    }


//total without tax amount


    public double getNetTotal() {

        double netTotal = 0;

        for (CartItem cartItem : cartItems) {
            if (cartItem.getNetPrice() != 0.0) {
                double d = cartItem.getNetPrice() * cartItem.getReturnQuantity();
                netTotal += d;
            }
        }
        return netTotal;
    }


    //total with tax amount
    public double getGrandTotal() {

        double grandTotal = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem cartItem : cartItems) {
                if (cartItem.getTotalPrice() != 0.0) {
                    double f = cartItem.getTotalPrice();
                    grandTotal += f;
                }
            }
        }
        return grandTotal;
    }


    public double getTaxTotal() {
        double totalTax = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem c : cartItems) {
                if (c.getTaxValue() != 0.0) {
                    double f = c.getTaxValue() * c.getReturnQuantity();
                    totalTax += f;
                }
            }
        }
        return totalTax;
    }


    @Override
    public int getItemCount() {
        return cartItems.size();
    }


    private void delete(int position) { //removes the row

        cartItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartItems.size());
    }

    private void addItem(CartItem item) {
        cartItems.add(item);
//        notifyItemInserted(cartItems.size() - 1);
        notifyDataSetChanged();


    }


    private void updateItem(CartItem item, int pos) {
        cartItems.set(pos, item);
        notifyItemChanged(pos);
    }

    public void returnItem(CartItem item) {

        boolean isAdd = false;
        if (cartItems.isEmpty()) {
            addItem(item);
            Log.d(TAG, "returnItem  empty " + item.getProductName());
        } else {
            for (int i = 0; i < cartItems.size(); i++) {

                if (item.equals(cartItems.get(i))) {
                    updateItem(item, i);
                    isAdd = true;
                    Log.d(TAG, "returnItem  update " + item.getProductName());
                }
            }
            if (!isAdd) {
                addItem(item);
                Log.d(TAG, "returnItem  add " + item.getProductName());
                isAdd = true;
            }
        }
    }


    class RvReturnProductHolder extends RecyclerView.ViewHolder {


        TextView tvSlNo, tvProductCode, tvProductName, tvNetPrice, tvActualQty, tvReturnQty, tvTotal;

        RvReturnProductHolder(View itemView) {
            super(itemView);

            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_invoice_slNo);
            tvProductCode = (TextView) itemView.findViewById(R.id.textView_item_invoice_productCode);
            tvProductName = (TextView) itemView.findViewById(R.id.textView_item_invoice_productName);
            tvNetPrice = (TextView) itemView.findViewById(R.id.textView_item_invoice_netPrice);
            tvActualQty = (TextView) itemView.findViewById(R.id.textView_item_invoice_actualQty);
            tvReturnQty = (TextView) itemView.findViewById(R.id.textView_item_invoice_returnQty);
            tvTotal = (TextView) itemView.findViewById(R.id.textView_item_invoice_totalPrice);


        }
    }
}
