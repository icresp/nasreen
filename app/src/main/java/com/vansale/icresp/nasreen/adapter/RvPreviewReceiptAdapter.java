package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.model.Receipt;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


/**
 * Created by mentor on 6/1/18.
 */

public class RvPreviewReceiptAdapter extends RecyclerView.Adapter<RvPreviewReceiptAdapter.RvPreviewReceiptHolder> {


    private Context context;

    private String TAG = "RvPreviewReceiptAdapter";


    private ArrayList<Receipt> receipts;

    public RvPreviewReceiptAdapter(ArrayList<Receipt> list) {
        this.receipts = list;

    }

    @Override
    public RvPreviewReceiptHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_receipt_preview, viewGroup, false);

        return new RvPreviewReceiptHolder(view);
    }

    @Override
    public void onBindViewHolder(RvPreviewReceiptHolder holder, int i) {

        final Receipt receipt = receipts.get(i);


        int s = i + 1;
        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvInvoiceNo.setText(String.valueOf(receipt.getInvoiceNo()));
        holder.tvPaid.setText(getAmount(receipt.getReceivedAmount()));
        holder.tvBalance.setText(getAmount(receipt.getBalanceAmount()));


    }

    @Override
    public int getItemCount() {
        return (null != receipts ? receipts.size() : 0);
    }

    public class RvPreviewReceiptHolder extends RecyclerView.ViewHolder {
        TextView tvSlNo, tvInvoiceNo, tvBalance, tvPaid;

        public RvPreviewReceiptHolder(View itemView) {
            super(itemView);
            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_preview_slNo);
            tvInvoiceNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_preview_invoiceNo);
            tvPaid = (TextView) itemView.findViewById(R.id.textView_item_receipt_preview_paid);
            tvBalance = (TextView) itemView.findViewById(R.id.textView_item_receipt_preview_balanceAmount);


        }
    }
}
