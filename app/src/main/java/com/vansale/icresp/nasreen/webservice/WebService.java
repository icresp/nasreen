package com.vansale.icresp.nasreen.webservice;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.vansale.icresp.nasreen.config.ConfigURL;
import com.vansale.icresp.nasreen.controller.ApplicationController;

import org.json.JSONException;
import org.json.JSONObject;

import static com.vansale.icresp.nasreen.config.ConfigURL.URL_DAY_CLOSE;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_GET_ALL_RECEIPT;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_GET_GROUP_BYROUTE;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_GET_VERSION_CHECK;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_GROUPS;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_GROUPS_REGISTER;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_LOGIN;
import static com.vansale.icresp.nasreen.config.ConfigURL.URL_NO_SALE;

/**
 * Created by mentor on 1/6/17.
 */

public class WebService {


    public WebService() {
    }

    /**
     * Login Web Server using Volley
     */
    public static void webLogin(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_login_tag";

        JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL_LOGIN, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                String errorMessage = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }


                callback.onErrorResponse(errorMessage);


            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(loginRequest, REQ_TAG);


    }


    /**
     * GroupList Web Server using Volley
     */
    public static void webGroupList(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_login_tag";

        JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL_GROUPS, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(loginRequest, REQ_TAG);
    }


    /**
     * Group Register Server using Volley
     */
    public static void webGroupRegister(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_group_register_tag";

        JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL_GROUPS_REGISTER, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(loginRequest, REQ_TAG);


    }


    /**
     * Day Close Server using Volley
     */
    public static void webDayClose(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_day_close_tag";

        JsonObjectRequest dayCloseRequest = new JsonObjectRequest(Request.Method.POST, URL_DAY_CLOSE, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(dayCloseRequest, REQ_TAG);


    }


    /**
     * GetProductTypes from Web Server using Volley
     */
    public static void webGetProductTypes(final webObjectCallback callback) {

        final String REQ_TAG = "volley_get_product_type_tag";

        StringRequest getProductTypeRequest = new StringRequest(Request.Method.GET, ConfigURL.URL_PRODUCT_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject object = new JSONObject();
                        try {
                            object = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        callback.onResponse(object);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);


            }
        });


        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(getProductTypeRequest, REQ_TAG);


    }

    /**
     * Get Van Stock by types from Web Server using Volley
     */
    public static void webGetVanStock(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_get_stock_tag";

        JsonObjectRequest getVanStockRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_VAN_STOCK, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });


        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(getVanStockRequest, REQ_TAG);


    }


    /**
     * Group By Shops Web Server using Volley
     */
    public static void webGroupByShop(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_group_by_shop_tag";

        JsonObjectRequest groupByShopRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_GROUPSBYCUSTOMERLIST, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                String errorMessage = "Unknown error!";

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(groupByShopRequest, REQ_TAG);


    }


    /**
     * Registered Shops Web Server using Volley
     */
    public static void webRegisteredShop(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_registered_shop_tag";

        JsonObjectRequest registeredShopRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_REGISTERED_SHOP, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                String errorMessage = "Unknown error!";

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(registeredShopRequest, REQ_TAG);


    }


    /**
     * All route Shops Web Server using Volley
     */
    public static void webAllRouteShop(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_route_shop_tag";

        JsonObjectRequest routeShopRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_ALLROUTE_SHOP, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                String errorMessage = "Unknown error!";

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(routeShopRequest, REQ_TAG);


    }


    /**
     * GetInvoice Details Web Server using Volley
     */
    public static void webGetReturnInvoiceDetails(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_return_invoice_details_tag";

        JsonObjectRequest invoiceDetailsRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_INVOICE_DETAILS, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(invoiceDetailsRequest, REQ_TAG);


    }

    /**
     * Get Receipt Web Server using Volley
     */
    public static void webGetReceipt(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_getReceipt_tag";

        JsonObjectRequest receiptRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_GET_PENDING_RECEIPT, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(receiptRequest, REQ_TAG);
    }



    /**
     * Get Receipt Web Server using Volley
     */
    public static void webGetAllRefund(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_getReceipt_tag";

        JsonObjectRequest receiptRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_GET_REFUNDS, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(receiptRequest, REQ_TAG);
    }




    /**
     * Get All Receipt Web Server using Volley
     */
    public static void webGetAllReceipt(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_get_all_Receipt_tag";

        JsonObjectRequest allReceiptRequest = new JsonObjectRequest(Request.Method.POST, URL_GET_ALL_RECEIPT, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(allReceiptRequest, REQ_TAG);


    }


    /**
     * Paid Receipt Web Server using Volley
     */
    public static void webPaidReceipt(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_paid_receipt_tag";

        JsonObjectRequest paidReceiptRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_PAID_RECEIPT, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(paidReceiptRequest, REQ_TAG);
    }


    /**
     * Paid Refund Web Server using Volley
     */
    public static void webPaidRefunds(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_paid_receipt_tag";

        JsonObjectRequest paidRefundtRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_PAID_REFUNDS, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(paidRefundtRequest, REQ_TAG);
    }

    /**
     * sales return Invoice Web Server using Volley
     */
    public static void webSaleReturn(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_sale_return_tag";

        JsonObjectRequest salesReturnRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_SALE_RETURN, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(salesReturnRequest, REQ_TAG);
    }

    /**
     * Get Group by Route using Volley
     */
    public static void webGetGroupByRoute(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_getGroup_tag";

            JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL_GET_GROUP_BYROUTE, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(loginRequest, REQ_TAG);
    }

    /**
     * Order Place Web Server using Volley
     */
    public static void webPlaceOrder(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_place_order_tag";

        JsonObjectRequest placeOrderRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_PLACE_ORDER, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(placeOrderRequest, REQ_TAG);


    }


    /**
     * Order Quotation Web Server using Volley
     */
    public static void webPlaceQuotation(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_quotation_order_tag";

        JsonObjectRequest quotationOrderRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_QUOTATION_ORDER, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(quotationOrderRequest, REQ_TAG);


    }


    /**
     * Order Place Web Server using Volley
     */
    public static void webNoSale(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_no_sale_tag";

        JsonObjectRequest noSaleRequest = new JsonObjectRequest(Request.Method.POST, URL_NO_SALE, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(noSaleRequest, REQ_TAG);


    }


    /**
     * Add Customer Web Server using Volley
     */
    public static void webAddCustomer(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_addCustomer_tag";

        JsonObjectRequest addCustomerRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_ADD_CUSTOMER, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(addCustomerRequest, REQ_TAG);


    }


    /**
     * Add Customer Web Server using Volley
     */

    /**
     * GetProductTypes from Web Server using Volley
     */
    public static void webGetCustomerDivisionanType(final webObjectCallback callback) {

        final String REQ_TAG = "volley_customer_divi_tag";

        StringRequest getCustomerDivRequest = new StringRequest(Request.Method.GET, ConfigURL.URL_GET_CUSTOMER_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject object = new JSONObject();
                        try {
                            object = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        callback.onResponse(object);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);

            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(getCustomerDivRequest, REQ_TAG);

    }


    /**
     * change password Web Server using Volley
     */
    public static void webChangePassword(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_change_password_tag";

        JsonObjectRequest changePasswordRequest = new JsonObjectRequest(Request.Method.POST, ConfigURL.URL_CHANGE_PASSWORD, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);
            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(changePasswordRequest, REQ_TAG);

    }


    /**
     * Get Banks from Web Server using Volley
     */
    public static void webGetBanks(final webObjectCallback callback) {

        final String REQ_TAG = "volley_get_bank_tag";
        //http://13.233.131.201/alhudaibah_test/Settings/api_get_bank_list/

        StringRequest getCustomerBank = new StringRequest(Request.Method.GET, ConfigURL.URL_GET_BANK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject object = new JSONObject();
                        try {
                            object = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        callback.onResponse(object);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }
                callback.onErrorResponse(errorMessage);

            }
        });


        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(getCustomerBank, REQ_TAG);
    }



    /**
     * Version check Web Server using Volley
     */
    public static void webVersionCheck(final webObjectCallback callback, JSONObject object) {

        final String REQ_TAG = "volley_login_tag";

        JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL_GET_VERSION_CHECK, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    if (error.getClass().equals(TimeoutError.class)) {
                        // Show timeout error message
                        errorMessage = "Oops. Timeout error!";

                    } else {
                        errorMessage = "Oops. NoConnectionError!";
                    }

                } else if (error instanceof AuthFailureError) {

                    errorMessage = "Oops. AuthFailureError!";
                } else if (error instanceof ServerError) {

                    errorMessage = "Oops. ServerError!";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Oops. NetworkError!";
                } else if (error instanceof ParseError) {
                    errorMessage = "Oops. ParseError!";
                }

                callback.onErrorResponse(errorMessage);

            }
        });

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(loginRequest, REQ_TAG);


    }




    public interface webObjectCallback {
        void onResponse(JSONObject response);

        void onErrorResponse(String error);
    }


}
