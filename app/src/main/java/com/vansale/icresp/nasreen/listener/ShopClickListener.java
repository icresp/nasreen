package com.vansale.icresp.nasreen.listener;


import com.vansale.icresp.nasreen.model.Shop;

/**
 * Created by mentor on 10/1/18.
 */

public interface ShopClickListener {
    void onShopClick(Shop shop);

}
