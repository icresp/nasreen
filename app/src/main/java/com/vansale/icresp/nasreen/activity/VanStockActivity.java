package com.vansale.icresp.nasreen.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvVanStockAdapter;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.Product;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetVanStock;


public class VanStockActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    String TAG = "VanStockActivity";
    private String EXECUTIVE_ID = "";
    private RecyclerView recyclerView;

    private ImageButton ibBack;
    private TextView tvToolBarTitle;

    private RvVanStockAdapter adapter;
    private LinearLayout layout;
    private ErrorView errorView;
    private TextView tvTotalStock, tvTotalQuantity;

    private SessionAuth sessionAuth;


    final ArrayList<Product> products = new ArrayList<>();

    private MyDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_van_stock);

        ibBack = (ImageButton) findViewById(R.id.imageButton_toolbar_back);
        tvToolBarTitle = (TextView) findViewById(R.id.textView_toolbar_shopNameAndCode);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_vanStock_product);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        layout = (LinearLayout) findViewById(R.id.layout_vanStock);
        errorView = (ErrorView) findViewById(R.id.errorView);


        tvTotalStock = (TextView) findViewById(R.id.textView_vanStock_totalStockAmount);
        tvTotalQuantity = (TextView) findViewById(R.id.textView_vanStock_totalStockQuantity);


        sessionAuth = new SessionAuth(VanStockActivity.this);


        myDatabase = new MyDatabase(VanStockActivity.this);
        adapter = new RvVanStockAdapter(products);

        EXECUTIVE_ID = sessionAuth.getExecutiveId();


//         Recycler View
        recyclerView.setHasFixedSize(true);
        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
                .build());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);


        tvToolBarTitle.setCompoundDrawables(null, null, null, null);
        tvToolBarTitle.setText("Van Stock");


        products.addAll(myDatabase.getAllStock());

        if (!products.isEmpty()) {
            setRecyclerView();

            tvTotalStock.setText(String.valueOf("Total Stock :" + getAmount(myDatabase.getStockAmount()) + " " + getString(R.string.currency)));
            tvTotalQuantity.setText(String.valueOf("Total Quantity : " + String.valueOf(myDatabase.getStockQuantity())));

        } else

            setErrorView("No Stock for Offline", "", true);


        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }


    private void setRecyclerView() {

        adapter.notifyDataSetChanged();
        layout.setVisibility(View.VISIBLE);
        setProgressBar(false);
        errorView.setVisibility(View.GONE);

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        layout.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);

        setProgressBar(false);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                storeVanStockFromServer();

            }
        });
    }


    //    Load Stock All List from Server
    private void storeVanStockFromServer() {


        if (!checkConnection()) {
            setErrorView(getString(R.string.no_internet), "", false);
            return;
        }


        if (myDatabase.isExistProducts()) {
            return;
        }


        setProgressBar(true);


        final JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        products.clear();

        webGetVanStock(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    if (response.getString("status").equals("success") && !response.isNull("Stocks")) {
                        JSONArray stockArray = response.getJSONArray("Stocks");
                        for (int i = 0; i < stockArray.length(); i++) {

                            JSONObject stockObject = stockArray.getJSONObject(i);
                            Product product = new Product();


                            product.setProductId(stockObject.getInt("product_id"));
                            product.setProductCode(stockObject.getString("product_code"));
                            product.setProductName(stockObject.getString("product_name"));
                            product.setArabicName(stockObject.getString("arabic_name"));

                            product.setProductType(stockObject.getString("product_type_name"));
                            product.setBrandName(stockObject.getString("brand_name"));

                            product.setWholeSalePrice(stockObject.getLong("product_wholesale_price"));
                            product.setRetailPrice(stockObject.getLong("product_mrp"));


                            product.setPiecepercart(stockObject.getInt("piece_per_cart"));

                            product.setStockQuantity(stockObject.getInt("stock_quantity"));

                            products.add(product);

                        }


                        float toatalStock = response.getLong("total_stock");
                        int stockQuantity = response.getInt("total_quantity");

                        tvTotalStock.setText(String.valueOf("Total Stock :" + getAmount(toatalStock) + " " + getString(R.string.currency)));
                        tvTotalQuantity.setText("Total Quantity : " + String.valueOf(stockQuantity));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (products.isEmpty())
                    setErrorView("No Stock", "", false);
                else {
                    setRecyclerView();

                }


            }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, getString(R.string.error_view_retry), true);

            }
        }, object);


    }


    //  ProgressBar
    private void setProgressBar(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            layout.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);


        }

    }


    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

}
