package com.vansale.icresp.nasreen.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.ReceiptTypeClickListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.vansale.icresp.nasreen.config.Generic.dateFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;

@SuppressLint("ValidFragment")
public class ReceiptTypeDialogFrag extends DialogFragment implements View.OnClickListener {

    String TAG = "ReceiptTypeDialogFrag";
    private ReceiptTypeClickListener listener;
    private Button btnOk, btnCancel;
    private TextView tvDate, tvTotalAmount, tvIssueDate, tvClearDate;
    private EditText etCheque, etBankName, etRemarks;
    private double totalAmount;
    private ViewGroup chequeView;

    private RadioGroup rgType;


    private Calendar issueDateCalendar;
    private Calendar clearingDateCalendar;

    private DatePickerDialog issueDatePicker;
    private DatePickerDialog clearingDatePicker;


    @SuppressLint("ValidFragment")
    public ReceiptTypeDialogFrag(double total, ReceiptTypeClickListener listener) {
        this.totalAmount = total;
        this.listener = listener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_receipt_type, container, false);


    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        // Do all the stuff to initialize your custom view


        rgType = (RadioGroup) v.findViewById(R.id.radioGroup_receiptTypeDialog);

        btnOk = (Button) v.findViewById(R.id.button_receiptTypeDialog_ok);
        btnCancel = (Button) v.findViewById(R.id.button_receiptTypeDialog_cancel);

        tvDate = (TextView) v.findViewById(R.id.textView_receiptTypeDialog_date);
        tvTotalAmount = (TextView) v.findViewById(R.id.textView_receiptTypeDialog_TotalAmount);
        tvIssueDate = (TextView) v.findViewById(R.id.textView_receiptTypeDialog_issue_date);
        tvClearDate = (TextView) v.findViewById(R.id.textView_receiptTypeDialog_clear_date);

        etCheque = (EditText) v.findViewById(R.id.editText_receiptTypeDialog_chequeNumber);
        etBankName = (EditText) v.findViewById(R.id.editText_receiptTypeDialog_bankName);
        etRemarks = (EditText) v.findViewById(R.id.editText_receiptTypeDialog_remarks);
        chequeView = (ViewGroup) v.findViewById(R.id.view_receiptTypeDialog_chequeView);


        issueDateCalendar = Calendar.getInstance();
        clearingDateCalendar = Calendar.getInstance();

        tvTotalAmount.setText(getAmount(totalAmount) + " " + getContext().getString(R.string.currency));


        chequeView.setVisibility(View.GONE);

        final String strDate = getTodayDate();
        tvDate.setText(strDate);

        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());


                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    // Changes the textview's text to "Checked: example radiobutton text"

                    int rbTypeId = checkedRadioButton.getId();
                    viewEnable(rbTypeId == R.id.radioButton_receiptTypeDialog_cheque);
                }


            }
        });


        initialise();
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        tvIssueDate.setOnClickListener(this);
        tvClearDate.setOnClickListener(this);


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    private void initialise() {


        issueDatePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        issueDateCalendar.set(year, monthOfYear, dayOfMonth);
                        tvIssueDate.setText(dateFormat.format(issueDateCalendar.getTime()));


                        if (issueDateCalendar.getTime().after(clearingDateCalendar.getTime())) {
                            clearingDateCalendar.set(year, monthOfYear, dayOfMonth);
                            tvClearDate.setText(dateFormat.format(clearingDateCalendar.getTime()));
                        }
                    }

                },
                issueDateCalendar.get(Calendar.YEAR), // Initial year selection
                issueDateCalendar.get(Calendar.MONTH), // Initial month selection
                issueDateCalendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
//////

        clearingDatePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        clearingDateCalendar.set(year, monthOfYear, dayOfMonth);

                        tvClearDate.setText(dateFormat.format(clearingDateCalendar.getTime()));
                    }

                },
                clearingDateCalendar.get(Calendar.YEAR), // Initial year selection
                clearingDateCalendar.get(Calendar.MONTH), // Initial month selection
                clearingDateCalendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );


        /****/


//        toDatePicker.setMaxDate( Calendar.getInstance());
        issueDatePicker.setMaxDate(Calendar.getInstance());

    }


    private void viewEnable(boolean isEnable) {
        etRemarks.setEnabled(isEnable);
        etBankName.setEnabled(isEnable);
        etCheque.setEnabled(isEnable);

        chequeView.setVisibility(isEnable ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.textView_receiptTypeDialog_issue_date:
                issueDatePicker.show(getActivity().getFragmentManager(), "issueDatePickerDialog");
                break;
            case R.id.textView_receiptTypeDialog_clear_date:
//                clearingDatePicker.setMinDate(issueDateCalendar);
                clearingDatePicker.setMinDate(Calendar.getInstance());
                clearingDatePicker.show(getActivity().getFragmentManager(), "clearingDatePickerDatePickerDialog");

                break;

            case R.id.button_receiptTypeDialog_ok:
                if (isValidate()) {
                    RadioButton rbType = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());

                    String cheque = "", bank = "", remarks = "", issueDate = "", clearDate = "";
                    if (rbType.getText().toString().trim().equals(getActivity().getString(R.string.cheque))) {

                        cheque = TextUtils.isEmpty(etCheque.getText().toString().trim()) ? "" : etCheque.getText().toString().trim();
                        bank = TextUtils.isEmpty(etBankName.getText().toString().trim()) ? "" : etBankName.getText().toString().trim();
                        remarks = TextUtils.isEmpty(etRemarks.getText().toString().trim()) ? "" : etRemarks.getText().toString().trim();
                        issueDate = TextUtils.isEmpty(tvIssueDate.getText().toString().trim()) ? "" : tvIssueDate.getText().toString().trim();
                        clearDate = TextUtils.isEmpty(tvClearDate.getText().toString().trim()) ? "" : tvClearDate.getText().toString().trim();
                    }


                    if (listener != null)
                        listener.onReceiptClick(rbType.getText().toString().trim(), cheque, bank, remarks, issueDate, clearDate);

                    dismiss();
                }

                break;
            case R.id.button_receiptTypeDialog_cancel:

                dismiss();
                break;

        }


    }

    boolean isValidate() {

        boolean status = false;

        etCheque.setError(null);
        etBankName.setError(null);

        RadioButton checkedRadioButton = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());


        if (checkedRadioButton == null) {
            Toast.makeText(getContext(), "Please select type", Toast.LENGTH_SHORT).show();
        } else {
            int rbTypeId = checkedRadioButton.getId();


            if (rbTypeId == R.id.radioButton_receiptTypeDialog_cash)
                status = true;
            else if (TextUtils.isEmpty(tvIssueDate.getText().toString().trim()))
                Toast.makeText(getContext(), "Please select Issue Date", Toast.LENGTH_SHORT).show();

            else if (TextUtils.isEmpty(tvClearDate.getText().toString().trim()))
                Toast.makeText(getContext(), "Please select ClearingDate", Toast.LENGTH_SHORT).show();
            else if (TextUtils.isEmpty(etCheque.getText().toString().trim())) {
                etCheque.setError("Cheque is empty");
            } else if (TextUtils.isEmpty(etBankName.getText().toString().trim())) {
                etBankName.setError("Bank name is empty");
            } else
                status = true;

        }
        return status;
    }

    @Override
    public void onStop() {
        super.onStop();

        hideSoftKeyboard();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    public interface paymentClickListener {

        void onPaymentClick(float paid);
    }

    private static String getTodayDate() {

        return dateFormat.format(Calendar.getInstance().getTime());
    }

}
