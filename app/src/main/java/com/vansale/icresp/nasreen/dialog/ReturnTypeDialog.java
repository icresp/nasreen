package com.vansale.icresp.nasreen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;

import java.util.Calendar;

import static com.vansale.icresp.nasreen.config.Generic.dateFormat;

public class ReturnTypeDialog extends Dialog implements View.OnClickListener {
    String TAG = "ReturnTypeDialog";
    private ReturnTypeClickListener  listener;
    private Button btnOk, btnCancel;
    private TextView tvDate;


    private RadioGroup rgType;

    private RadioButton rbType;


    public ReturnTypeDialog(@NonNull Context context,  ReturnTypeClickListener listener) {
        super(context);

        this.listener = listener;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_return_type);

        btnOk = (Button) findViewById(R.id.button_returnTypeDialog_ok);
        btnCancel = (Button) findViewById(R.id.button_returnTypeDialog_cancel);
        tvDate = (TextView) findViewById(R.id.textView_returnTypeDialog_date);

        rgType = (RadioGroup) findViewById(R.id.radioGroup_returnTypeDialog);

        rgType.clearCheck();

        final String strDate = getTodayDate();
        tvDate.setText(strDate);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_returnTypeDialog_ok:

                rbType = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());

                if (rbType == null) {
                    Toast.makeText(getContext(), "Please select type", Toast.LENGTH_SHORT).show();
                    break;
                }

                if (listener != null)
                    listener.onReturnTypeClick(rbType.getText().toString());

                dismiss();

                break;
            case R.id.button_returnTypeDialog_cancel:

                dismiss();
                break;
        }
    }

    private static String getTodayDate() {

        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public interface ReturnTypeClickListener {

        void onReturnTypeClick(String type);
    }
}
