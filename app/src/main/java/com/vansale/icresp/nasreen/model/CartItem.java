package com.vansale.icresp.nasreen.model;


/**
 * Created by sadiquekolakkal on 09-05-2017.
 */

public class CartItem extends Product {

    private int cartId;
    private int typeQuantity;
    private int pieceQuantity;
    private int stockQuantity;
    private int returnQuantity;
    private double productPrice;
    private double netPrice;
    private double salePrice;
    private double totalPrice;
    private double taxValue;
    private float discountPerce;
    private String orderType;

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    private String productUnit;


    public CartItem() {
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getTypeQuantity() {
        return typeQuantity;
    }

    public void setTypeQuantity(int typeQuantity) {
        this.typeQuantity = typeQuantity;
    }

    public int getPieceQuantity() {
        return pieceQuantity;
    }

    public void setPieceQuantity(int pieceQuantity) {
        this.pieceQuantity = pieceQuantity;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(int returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }




    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }


    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }


    public double getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(double taxValue) {
        this.taxValue = taxValue;
    }

    public float getDiscountPerce() {
        return discountPerce;
    }

    public void setDiscountPerce(float discountPerce) {
        this.discountPerce = discountPerce;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }


    @Override
    public String toString() {
        return getProductName();
    }

}
