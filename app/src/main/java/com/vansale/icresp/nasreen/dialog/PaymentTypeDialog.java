package com.vansale.icresp.nasreen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;

import java.util.Calendar;

import static com.vansale.icresp.nasreen.config.Generic.dateFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;

/**
 * Created by mentor on 15/1/18.
 */

public class PaymentTypeDialog extends Dialog implements View.OnClickListener {


    String TAG = "PaymentTypeDialog";
    private paymentTypeClickListener listener;
    private com.rey.material.widget.Button btnOk, btnCancel;
    private TextView tvDate, tvTotalAmount, tvCreditLimit;

    private double totalAmount, creditLimit;
    private RadioGroup rgType;

    private RadioButton rbType, rbCredit, rbCash;
    String type = "";

    public PaymentTypeDialog(@NonNull Context context, double limit, double total, String type, paymentTypeClickListener listener) {
        super(context);

        this.totalAmount = total;
        this.creditLimit = limit;

        this.listener = listener;
        this.type = type;


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_payment_type);


        btnOk = (Button) findViewById(R.id.button_paymentTypeDialog_ok);
        btnCancel = (Button) findViewById(R.id.button_paymentTypeDialog_cancel);
        tvDate = (TextView) findViewById(R.id.textView_paymentTypeDialog_date);
        tvCreditLimit = (TextView) findViewById(R.id.textView_paymentTypeDialog_creditLimit);
        tvTotalAmount = (TextView) findViewById(R.id.textView_paymentTypeDialog_TotalAmount);
        rgType = (RadioGroup) findViewById(R.id.radioGroup_paymentTypeDialog);
        rbCash = (RadioButton) findViewById(R.id.radioButton_paymentTypeDialog_cash);
        rbCredit = (RadioButton) findViewById(R.id.radioButton_paymentTypeDialog_credit);
        rgType.clearCheck();


        tvTotalAmount.setText(String.valueOf(getAmount(totalAmount) + " " + getContext().getString(R.string.currency)));
        tvCreditLimit.setText(String.valueOf("Credit up to " + getAmount(creditLimit) + " " + getContext().getString(R.string.currency)));


        if (type.equals("Credit"))
            rbCredit.setChecked(true);
        else if (type.equals("Cash"))
            rbCash.setChecked(true);


        final String strDate = getTodayDate();
        tvDate.setText(strDate);


        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_paymentTypeDialog_ok:


                rbType = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());

                if (rbType == null) {
                    Toast.makeText(getContext(), "Please select type", Toast.LENGTH_SHORT).show();
                    break;
                }

                if (listener != null)
                    listener.onPaymentTypeClick(rbType.getText().toString());

                dismiss();


                break;
            case R.id.button_paymentTypeDialog_cancel:

                dismiss();
                break;

        }

    }


    private static String getTodayDate() {

        return dateFormat.format(Calendar.getInstance().getTime());
    }


    public interface paymentTypeClickListener {

        void onPaymentTypeClick(String type);
    }
}
