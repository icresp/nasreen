package com.vansale.icresp.nasreen.config;

/**
 * Created by mentor on 1/6/17.
 *
 * Last edited on 26/07/2019 by HK
 * Last edited on 27/11/2019 by HK ( Print Article code for lulu)
 * Last edited on 10/12/2019 by HK ( 2nd print alignment issue)
 *
 */

public class ConfigURL {

   // private static final String URL_NASREEN = "http://54.188.28.103/nasreen_test1/Settings/";  //demo test  Changed on 07/07/2020

    private static final String URL_NASREEN = "http://54.189.43.214/nasreen/Settings/";  //live

    public static final String URL_LOGIN = URL_NASREEN + "executive_login";

    public static final String URL_GROUPS = URL_NASREEN + "api_get_route_group_list";

    public static final String URL_GROUPS_REGISTER = URL_NASREEN + "api_executive_day_register";

    public static final String URL_GROUPSBYCUSTOMERLIST = URL_NASREEN + "api_get_customer_info_by_search";

    public static final String URL_DAY_CLOSE = URL_NASREEN + "api_executive_day_close";

    public static final String URL_PRODUCT_TYPE = URL_NASREEN + "api_get_product_type";

    public static final String URL_VAN_STOCK = URL_NASREEN + "api_get_van_stock_search";

    public static final String URL_REGISTERED_SHOP = URL_NASREEN + "api_get_customer_info";

    public static final String URL_ALLROUTE_SHOP = URL_NASREEN + "api_get_route_customer_search";

    public static final String URL_PLACE_ORDER = URL_NASREEN + "api_multiple_order_place";

    public static final String URL_QUOTATION_ORDER = URL_NASREEN + "api_multiple_quotation";

    public static final String URL_GET_PENDING_RECEIPT = URL_NASREEN + "api_get_pending_reciept";

    public static final String URL_GET_ALL_RECEIPT = URL_NASREEN + "api_get_reciept";

    public static final String URL_PAID_RECEIPT = URL_NASREEN + "api_receipt_payment";

    public static final String URL_INVOICE_DETAILS = URL_NASREEN + "api_get_invoice_item_details";

    public static final String URL_SALE_RETURN = URL_NASREEN + "api_sale_return";

    public static final String URL_GET_CUSTOMER_TYPE = URL_NASREEN + "api_get_division_type_list";

    public static final String URL_ADD_CUSTOMER = URL_NASREEN + "api_customer_creation";

    public static final String URL_CHANGE_PASSWORD = URL_NASREEN + "executive_reset_password";

    public static final String URL_NO_SALE = URL_NASREEN + "api_multiple_no_sale";

    public static final String URL_GET_REFUNDS = URL_NASREEN + "api_get_refund_details";

    public static final String URL_PAID_REFUNDS = URL_NASREEN + "api_refund_details";  // api to save refund details

    public static final String URL_GET_GROUP_BYROUTE = URL_NASREEN + "api_get_group_by_route_list";

    public static final String URL_GET_BANK = URL_NASREEN + "api_get_bank_list";

    public static final String URL_GET_VERSION_CHECK = URL_NASREEN + "check_version_code";

}
