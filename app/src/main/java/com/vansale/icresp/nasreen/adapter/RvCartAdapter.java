package com.vansale.icresp.nasreen.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.textwatcher.TextValidator;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentageValue;
import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_CASE;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;

/**
 * Created by mentor on 6/5/17.
 */

public class RvCartAdapter extends RecyclerView.Adapter<RvCartAdapter.RvCartHolder> {
    private Context context;


    private String TAG = "RvCartAdapter";


    private ArrayList<CartItem> cartItems;


    OnNotifyListener listener;

    public RvCartAdapter(ArrayList<CartItem> cartItems, OnNotifyListener listener) {
        this.cartItems = cartItems;
        this.listener = listener;
    }

    @Override
    public RvCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);

        this.context = parent.getContext();

        return new RvCartHolder(view);
    }

    @Override
    public void onBindViewHolder(RvCartHolder holder, final int position) {

        final CartItem cartItem = cartItems.get(position);

        int s = position + 1;


        holder.tvCode.setText(cartItem.getProductCode());

        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvProductName.setText(cartItem.getProductName());

        holder.tvQty.setText(String.valueOf(cartItem.getTypeQuantity()));

        holder.tvType.setText(cartItem.getOrderType());

        double netPrice = cartItem.getNetPrice();
        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE))
            netPrice = netPrice * cartItem.getPiecepercart();

        holder.tvUnitPrice.setText(getAmount(netPrice));

        holder.tvTotal.setText(getAmount(cartItem.getNetPrice() * cartItem.getPieceQuantity()));

        holder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(position);
            }
        });


        holder.ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.anim_alpha);
                v.setAnimation(anim);
                showEditDialog(cartItem, position);

            }
        });


        if (listener != null)
            listener.onNotified();


    }


    private void delete(int position) { //removes the row


        try {
            cartItems.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, cartItems.size());


            if (cartItems.size() == position) {
                if (listener != null)
                    listener.onNotified();
            }
        } catch (IndexOutOfBoundsException e) {
            Log.v(TAG, "delete   Exception " + e.getMessage());
        }
    }

    private void addItem(CartItem item) {
        cartItems.add(item);
        notifyItemInserted(cartItems.size() - 1);
        notifyDataSetChanged();
    }

    public void updateItem(CartItem item, int pos) {
        cartItems.set(pos, item);
        notifyItemChanged(pos);

    }

    private void showEditDialog(final CartItem cart, final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

//        LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_edit_cart, null);
        dialogBuilder.setView(dialogView);

        final TextView tvProductName, tvNetPrice, tvTotalPrice;
        final EditText etQuantity;
        final ImageButton ibQuantityDown, ibQuantityUp;

        tvProductName = (TextView) dialogView.findViewById(R.id.textView_editCart_product);
        tvNetPrice = (TextView) dialogView.findViewById(R.id.textView_editCart_unitPrice);
        tvTotalPrice = (TextView) dialogView.findViewById(R.id.textView_editCart_total);

        etQuantity = (EditText) dialogView.findViewById(R.id.EditText_editCart_Qty);

        ibQuantityDown = (ImageButton) dialogView.findViewById(R.id.imageButton_editCart_QtyDown);

        ibQuantityUp = (ImageButton) dialogView.findViewById(R.id.imageButton_editCart_QtyUp);


        tvProductName.setText(cart.getProductName());
        tvNetPrice.setText(getAmount(cart.getNetPrice()));
        tvTotalPrice.setText(getAmount(cart.getNetPrice() * cart.getTypeQuantity()));
        etQuantity.setText(String.valueOf(cart.getTypeQuantity()));


        etQuantity.addTextChangedListener(new TextValidator(etQuantity) {
            @Override
            public void validate(TextView textView, String qtyString) {


                if (!TextUtils.isEmpty(qtyString)) {

                    int quantity = TextUtils.isEmpty(qtyString) ? 0 : Integer.valueOf(qtyString);

                    if (cart.getOrderType().equals(PRODUCT_UNIT_CASE))
                        quantity = quantity * cart.getPiecepercart();


                    if (quantity != 0) {
                        tvTotalPrice.setText(getAmount(cart.getNetPrice() * quantity));
                    } else {
                        tvTotalPrice.setText(getAmount(cart.getNetPrice() * 0));

                    }
                } else {
                    tvTotalPrice.setText(getAmount(cart.getNetPrice() * 0));
                }


            }
        });


        ibQuantityDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    String text = etQuantity.getText().toString().trim();
                    int previousSize = 0;

                    if (!text.isEmpty())
                        previousSize = Integer.parseInt(text);

                    /**decrement numbers up to 1*/
                    if (previousSize > 1) {
                        previousSize--;
                        etQuantity.setText(String.valueOf(previousSize));
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });

        ibQuantityUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String text = etQuantity.getText().toString().trim();

                    int nextSize = 0;

                    if (!text.isEmpty())
                        nextSize = Integer.parseInt(text);

                    /**increment numbers */

                    nextSize++;
                    etQuantity.setText(String.valueOf(nextSize));


                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });


        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String str = etQuantity.getText().toString().trim();

                int quantity = TextUtils.isEmpty(str) ? 0 : Integer.valueOf(str);

                cart.setTypeQuantity(quantity);


                if (cart.getOrderType().equals(PRODUCT_UNIT_CASE))
                    quantity = quantity * cart.getPiecepercart();

                cart.setPieceQuantity(quantity);
                cart.setTotalPrice(cart.getSalePrice() * quantity);
                updateItem(cart, position);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    public void changeItem(CartItem item) {

        boolean isAdd = false;
        if (cartItems.isEmpty()) {
            addItem(item);
        } else {
            for (int i = 0; i < cartItems.size(); i++) {

                if (item.getProductId() == cartItems.get(i).getProductId()) {

                    updateItem(item, i);
                    isAdd = true;

                }
            }
            if (!isAdd) {
                addItem(item);
            }
        }
    }


    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }


    public double getNetTotal() {

        double netTotal = 0;

        for (CartItem cartItem : cartItems) {
            if (cartItem.getNetPrice() != 0.0) {
                double d = cartItem.getNetPrice() * cartItem.getPieceQuantity();
                netTotal += d;
            }
        }
        return netTotal;
    }


    public double getGrandTotal() {

        double grandTotal = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem cartItem : cartItems) {
                if (cartItem.getTotalPrice() != 0.0) {
                    double f = cartItem.getTotalPrice();
                    grandTotal += f;
                }
            }
        }


        return grandTotal;
    }


    public double getDiscountTotal() {
        double totalDisc = 0;

        if (!cartItems.isEmpty()) {
            for (CartItem c : cartItems) {
                if (c.getTaxValue() != 0.0) {

                    double discountedPrice=  getPercentageValue(c.getNetPrice(),c.getDiscountPerce());
                    discountedPrice = discountedPrice * c.getPieceQuantity();
                    totalDisc += discountedPrice;
                }
            }
        }
        return totalDisc;
    }

    public double getTaxTotal() {
        double totalTax = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem c : cartItems) {
                if (c.getTaxValue() != 0.0) {
                    double f = c.getTaxValue() * c.getPieceQuantity();
                    totalTax += f;
                }
            }
        }
        return totalTax;
    }


    /******/

    @Override
    public int getItemCount() {
        return cartItems.size();
    }


    public class RvCartHolder extends RecyclerView.ViewHolder {


        TextView tvSlNo, tvProductName, tvQty, tvUnitPrice, tvTotal, tvCode, tvType;
        ImageButton ibEdit, ibDelete;


        public RvCartHolder(View itemView) {
            super(itemView);

            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_cart_no);
            tvProductName = (TextView) itemView.findViewById(R.id.textView_item_cart_productName);
            tvQty = (TextView) itemView.findViewById(R.id.textView_item_cart_qty);
            tvUnitPrice = (TextView) itemView.findViewById(R.id.textView_item_cart_unitPrice);
            tvTotal = (TextView) itemView.findViewById(R.id.textView_item_cart_total);
            tvType = (TextView) itemView.findViewById(R.id.textView_item_cart_type);

            tvCode = (TextView) itemView.findViewById(R.id.textView_item_cart_code);

            ibDelete = (ImageButton) itemView.findViewById(R.id.imageButton_item_cart_delete);
            ibEdit = (ImageButton) itemView.findViewById(R.id.imageButton_item_cart_edit);

        }
    }

}
