package com.vansale.icresp.nasreen.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.ReceiptActivity;
import com.vansale.icresp.nasreen.activity.ReceiptHistoryActivity;
import com.vansale.icresp.nasreen.activity.SaleHistoryActivity;
import com.vansale.icresp.nasreen.activity.SalesActivity;
import com.vansale.icresp.nasreen.activity.SalesReturnActivity;
import com.vansale.icresp.nasreen.animation.MyBounceInterpolator;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.Shop;

import static android.content.Context.LOCATION_SERVICE;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentage;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentageValue;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_ANY_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_NO_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_QUOTATION_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RECEIPT_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RETURN_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.SHOP_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;

public class ShopDashBoardFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    private String TAG = "ShopDashBoardFragment", str_Latitude = "0", str_Longitude = "0";

    private String provider;
    LocationManager locationManager;

    private static final String ARG_SHOP = "shop_key_arg";

    private Shop SELECTED_SHOP = null;


    private CardView cvSales, cvReciept, cvSalesReturn, cvNoSale, cvQuotation, cvFinish;

    private ImageView ivSales, ivReceipt, ivSalesReturn, ivNoSale, ivQuotation, ivFinish;

    private Animation myAnim;
    private MyBounceInterpolator interpolator;
    private ImageButton ibBack;
    private MyDatabase myDatabase;

    private int ANIM_TIME_OUT = 200;

    LocationManager lm ;
    boolean gps_enabled = false;
    boolean network_enabled = false;


    public static ShopDashBoardFragment newInstance(Shop shop) {
        ShopDashBoardFragment fragment = new ShopDashBoardFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOP, shop);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SELECTED_SHOP = (Shop) getArguments().getSerializable(ARG_SHOP);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shop_dash_board, container, false);


//        initialise container
        cvSales = (CardView) view.findViewById(R.id.cardView_sales);
        cvReciept = (CardView) view.findViewById(R.id.cardView_receipt);
        cvSalesReturn = (CardView) view.findViewById(R.id.cardView_return);
        cvNoSale = (CardView) view.findViewById(R.id.cardView_noSale);

        cvQuotation = (CardView) view.findViewById(R.id.cardView_quotation);
        cvFinish = (CardView) view.findViewById(R.id.cardView_finish);

        ibBack = (ImageButton) view.findViewById(R.id.imageButton_toolbar_back);

        ivSales = (ImageView) view.findViewById(R.id.imageView_badge_sales);
        ivReceipt = (ImageView) view.findViewById(R.id.imageView_badge_receipt);
        ivSalesReturn = (ImageView) view.findViewById(R.id.imageView_badge_return);
        ivNoSale = (ImageView) view.findViewById(R.id.imageView_badge_noSale);
        ivQuotation = (ImageView) view.findViewById(R.id.imageView_badge_quotation);
        ivFinish = (ImageView) view.findViewById(R.id.imageView_badge_finish);


        TextView tvToolBarShopName = (TextView) view.findViewById(R.id.textView_toolbar_shopNameAndCode);

        this.myDatabase = new MyDatabase(getActivity());


        if (SELECTED_SHOP == null) {
            getActivity().onBackPressed();
            return view;
        }

        Log.e("Shop Type", ""+SELECTED_SHOP.getTypeId());

        tvToolBarShopName.setText(String.valueOf(SELECTED_SHOP.getShopName() + "\t" + SELECTED_SHOP.getShopCode()));
        tvToolBarShopName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvToolBarShopName.setSelected(true);

        myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);



        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Requires Permission", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                LocationListener mlocListener = new MyLocationListener();
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                provider = locationManager.getBestProvider(criteria, true);
                locationManager.requestLocationUpdates(provider, 61000, 250,
                        mlocListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Error with location manager", Toast.LENGTH_SHORT).show();
        }


//        set ClickAction
        cvSales.setOnClickListener(this);
        cvReciept.setOnClickListener(this);
        cvSalesReturn.setOnClickListener(this);
        cvNoSale.setOnClickListener(this);
        cvQuotation.setOnClickListener(this);
        cvFinish.setOnClickListener(this);
        ibBack.setOnClickListener(this);


//        set long ClickAction
        cvSales.setOnLongClickListener(this);

        cvReciept.setOnLongClickListener(this);


        return view;
    }

    private void updateView() {


        boolean _sale = myDatabase.getVisitingStatus(REQ_SALE_TYPE, SELECTED_SHOP.getShopId());

        boolean _quotation = myDatabase.getVisitingStatus(REQ_QUOTATION_TYPE, SELECTED_SHOP.getShopId());

        boolean _return = myDatabase.getVisitingStatus(REQ_RETURN_TYPE, SELECTED_SHOP.getShopId());

        boolean _receipt = myDatabase.getVisitingStatus(REQ_RECEIPT_TYPE, SELECTED_SHOP.getShopId());

        boolean _no_sale = myDatabase.getVisitingStatus(REQ_NO_SALE_TYPE, SELECTED_SHOP.getShopId());

        boolean _finish = myDatabase.getVisitingStatus(REQ_ANY_TYPE, SELECTED_SHOP.getShopId());


        cvSales.setCardBackgroundColor(_sale ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));

        cvQuotation.setCardBackgroundColor(_quotation ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));
        cvSalesReturn.setCardBackgroundColor(_return ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));
        cvReciept.setCardBackgroundColor(_receipt ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));

        cvNoSale.setCardBackgroundColor(_no_sale ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));

        cvFinish.setEnabled(_finish);
        cvFinish.setCardBackgroundColor(!_finish ? getActivity().getResources().getColor(R.color.colorGrayLight) : getActivity().getResources().getColor(R.color.colorWhite));


    }


    @Override
    public void onClick(View v) {

        final boolean _finish = myDatabase.getVisitingStatus(REQ_ANY_TYPE, SELECTED_SHOP.getShopId());

        clearAnimations(); //animation stop another click time
        switch (v.getId()) {


            case R.id.cardView_sales:
                ivSales.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            return;
                        }else{

                            try {
                                   lm = (LocationManager)getActivity().getSystemService(LOCATION_SERVICE);
                                   gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                // network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                                   Log.e("Gps Enable", "result : "+gps_enabled);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("Gps Error", ""+e.getMessage());
                            }

                            //  && !network_enabled
                            if (!gps_enabled) {
                                Log.e("Gps Enable 2", "result : "+gps_enabled);
                                new AlertDialog.Builder(getActivity() )
                                        .setMessage( "GPS Enable" )
                                        .setPositiveButton( "Settings" , new
                                                DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick (DialogInterface paramDialogInterface , int paramInt) {
                                                        startActivity( new Intent(Settings. ACTION_LOCATION_SOURCE_SETTINGS )) ;
                                                    }
                                                })
                                        .setNegativeButton( "Cancel" , null )
                                        .show() ;
                            }else {

                                Intent intent = new Intent(getActivity(), SalesActivity.class);
                                intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_SALES);
                                intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                                startActivity(intent);

                            }

                        }

                       /* Intent intent = new Intent(getActivity(), SalesActivity.class);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_SALES);
                        intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                        startActivity(intent);*/
                    }
                }, ANIM_TIME_OUT);

                break;

            case R.id.cardView_quotation:

                ivQuotation.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(getActivity(), SalesActivity.class);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_QUOTATION);
                        intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                        startActivity(intent);
                    }
                }, ANIM_TIME_OUT);

                break;


            case R.id.cardView_return:

                ivSalesReturn.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            return;
                        } else {

                            try {
                                lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                // network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                                Log.e("Gps Enable", "result : " + gps_enabled);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("Gps Error", "" + e.getMessage());
                            }

                            //  && !network_enabled
                            if (!gps_enabled) {
                                Log.e("Gps Enable 2", "result : " + gps_enabled);
                                new AlertDialog.Builder(getActivity())
                                        .setMessage("GPS Enable")
                                        .setPositiveButton("Settings", new
                                                DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                                    }
                                                })
                                        .setNegativeButton("Cancel", null)
                                        .show();
                            } else {

                                Intent intent = new Intent(getActivity(), SalesReturnActivity.class);
                                intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                                startActivity(intent);
                            }
                        }
                    }
                }, ANIM_TIME_OUT);


                break;
            case R.id.cardView_receipt:

                ivReceipt.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(getActivity(), ReceiptActivity.class);
                        intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                        startActivity(intent);
                    }
                }, ANIM_TIME_OUT);


                break;


            case R.id.cardView_noSale:


                ivNoSale.startAnimation(myAnim);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!_finish) {

                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                                return;
                            } else {

                                try {
                                    lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                    // network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                                    Log.e("Gps Enable", "result : " + gps_enabled);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("Gps Error", "" + e.getMessage());
                                }

                                //  && !network_enabled
                                if (!gps_enabled) {
                                    Log.e("Gps Enable 2", "result : " + gps_enabled);
                                    new AlertDialog.Builder(getActivity())
                                            .setMessage("GPS Enable")
                                            .setPositiveButton("Settings", new
                                                    DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                                        }
                                                    })
                                            .setNegativeButton("Cancel", null)
                                            .show();
                                } else {

                                    noSaleDialog();

                                }
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), "No sale Can't work", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, ANIM_TIME_OUT);


                break;


            case R.id.cardView_finish:


                ivFinish.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (_finish)
                            finishConfirmationDialogue();


                    }
                }, ANIM_TIME_OUT);


                break;

            case R.id.imageButton_toolbar_back:

                getActivity().onBackPressed();

                break;

        }
    }


    //    Visit close
    private void finishConfirmationDialogue() {

        new android.app.AlertDialog.Builder(getActivity())
                .setMessage("Confirm finish ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean updateStatus = myDatabase.updateVisitFinish(SELECTED_SHOP.getShopId());  // update no sales reason status to local db

                        if (updateStatus)
                            getActivity().onBackPressed();
                        else
                            Toast.makeText(getActivity(), "Finish failed", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to register
                    }
                })
                .show();
    }


    private void noSaleDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("No Need");
        arrayAdapter.add("Not Open");
        arrayAdapter.add("Stock Available");
        arrayAdapter.add("Others...");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String strReason = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(getActivity());
                builderInner.setMessage(strReason);
                builderInner.setTitle("Your Selected Reason is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                     //   Toast.makeText(getContext(), "Lat : "+str_Latitude+"\nLong : "+str_Longitude, Toast.LENGTH_SHORT).show();

                        if (myDatabase.updateVisitStatus(SELECTED_SHOP.getShopId(), REQ_NO_SALE_TYPE, strReason, ""+str_Latitude, ""+str_Longitude))  // update no sales reason status to local db
                            updateView();


                        dialog.dismiss();
                    }
                });
                builderInner.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();
    }


    @Override
    public void onStart() {
        super.onStart();

        clearAnimations();


        updateView();

    }

    private void clearAnimations() {
        ivSales.clearAnimation();
        ivReceipt.clearAnimation();
        ivSalesReturn.clearAnimation();
        ivNoSale.clearAnimation();

        ivQuotation.clearAnimation();
        ivFinish.clearAnimation();

    }

    @Override
    public boolean onLongClick(View v) {


        clearAnimations(); //animation stop another click time
        switch (v.getId()) {


            case R.id.cardView_sales: {
                ivSales.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getActivity(), SaleHistoryActivity.class);
                        intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                        startActivity(intent);
                    }
                }, ANIM_TIME_OUT);

                return true;

            }
            case R.id.cardView_receipt: {
                ivReceipt.startAnimation(myAnim);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getActivity(), ReceiptHistoryActivity.class);
                        intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                        startActivity(intent);
                    }
                }, ANIM_TIME_OUT);

                return true;

            }


        }
        return false;
    }



    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();
            String Text = "My current location is: " + "Latitude = "
                    + loc.getLatitude() + "\nLongitude = " + loc.getLongitude();

            str_Latitude = "" + loc.getLatitude();
            str_Longitude = "" + loc.getLongitude();

            //  tvNetTotal.setText("Lat : "+str_Latitude+" / Long : "+str_Longitude);
            /*Toast.makeText(getActivity(), Text, Toast.LENGTH_SHORT)
                    .show();*/
            Log.d("TAG", "Starting..");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }



}
