package com.vansale.icresp.nasreen.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.AddShopActivity;
import com.vansale.icresp.nasreen.activity.OtherCustomerActivity;
import com.vansale.icresp.nasreen.activity.VanStockActivity;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.session.SessionValue;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


public class HomeDashBoardLeftFragment extends Fragment implements View.OnClickListener {


    private Button btnSearchCustomer, btnNewShop, btnVanStock;
    private TextView tvVanStock, tvTodaySale, tvCollection, tvSaleTarget;


    private SessionValue sessionValue;

    private MyDatabase myDatabase;

    private String TAG = "HomeDashBoardLeftFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_dash_board_left, container, false);

        btnSearchCustomer = (Button) view.findViewById(R.id.button_search_customer);
        btnNewShop = (Button) view.findViewById(R.id.button_createShop);
        btnVanStock = (Button) view.findViewById(R.id.button_vanStock);

        tvVanStock = (TextView) view.findViewById(R.id.textView_stock_dash);
        tvTodaySale = (TextView) view.findViewById(R.id.textView_todaySale_dash);
        tvCollection = (TextView) view.findViewById(R.id.textView_collection_dash);
        tvSaleTarget = (TextView) view.findViewById(R.id.textView_target_dash);

        sessionValue = new SessionValue(getActivity());

        myDatabase = new MyDatabase(getActivity());

        btnSearchCustomer.setOnClickListener(this);
        btnNewShop.setOnClickListener(this);
        btnVanStock.setOnClickListener(this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (sessionValue != null && myDatabase != null) {
            tvSaleTarget.setText(getAmount(sessionValue.getSaleTarget()));
            tvVanStock.setText(getAmount(myDatabase.getStockAmount()));
            tvTodaySale.setText(getAmount(myDatabase.getTodaySaleAmount()));
            tvCollection.setText(getAmount(myDatabase.getCollectionAmount() + myDatabase.getSalePaidAmount() + myDatabase.getTotalReceivableOpeningBalance()));  // cash sales + received invoice amount + receivable opening balance amount
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.button_search_customer:

                if (new SessionValue(getActivity()).isGroupRegister())
                    startActivity(new Intent(getActivity(), OtherCustomerActivity.class));
                else
                    Toast.makeText(getActivity(), "Please Register", Toast.LENGTH_SHORT).show();

                break;

            case R.id.button_createShop:
                if (new SessionValue(getActivity()).isGroupRegister())
                    startActivity(new Intent(getActivity(), AddShopActivity.class));
                else
                    Toast.makeText(getActivity(), "Please Register", Toast.LENGTH_SHORT).show();

                break;
            case R.id.button_vanStock:
                startActivity(new Intent(getActivity(), VanStockActivity.class));

                break;
        }

    }


}
