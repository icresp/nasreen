package com.vansale.icresp.nasreen.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mentor on 24/11/17.
 */

public class Sales implements Serializable {

    private String invoiceCode;
    private int locId;
    private int customerId;
    private double total;
    private double paid;
    private String date;
    private String saleType;
    private String salePo;
    private double discountTotal;
    private double collectionDiscount;

    private String saleLatitude;
    private String saleLongitude;

    public String getSaleLatitude() {
        return saleLatitude;
    }

    public void setSaleLatitude(String saleLatitude) {
        this.saleLatitude = saleLatitude;
    }

    public String getSaleLongitude() {
        return saleLongitude;
    }

    public void setSaleLongitude(String saleLongitude) {
        this.saleLongitude = saleLongitude;
    }

    private ArrayList<CartItem> cartItems = new ArrayList<>();


    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(ArrayList<CartItem> cartItems) {
        this.cartItems = cartItems;
    }




    public String getSalePo() {
        return salePo;
    }

    public void setSalePo(String salePo) {
        this.salePo = salePo;
    }

    public double getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(double discountTotal) {
        this.discountTotal = discountTotal;
    }

    public double getCollectionDiscount() {
        return collectionDiscount;
    }

    public void setCollectionDiscount(double collectionDiscount) {
        this.collectionDiscount = collectionDiscount;
    }
}
