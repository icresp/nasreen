package com.vansale.icresp.nasreen.config;


import static com.vansale.icresp.nasreen.config.ConfigValue.TAX_EXCLUSIVE;
import static com.vansale.icresp.nasreen.config.ConfigValue.TAX_INCLUSIVE;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;

public abstract class AmountCalculator {

    private static String TAX_TYPE = TAX_EXCLUSIVE;


    public static double getTaxPrice(double price, float percentage) {
        double taxPrice = 0;
        try {
            if (TAX_TYPE.equals(TAX_INCLUSIVE))
                taxPrice = getInclusiveTaxAmount(price, percentage);
            else
                taxPrice = getExclusiveTaxAmount(price, percentage);
        } catch (NumberFormatException e) {
            printLog("AmountCalculator", "getTaxPrice  Exception " + e.getMessage());
            e.printStackTrace();
        }
        return taxPrice;
    }


    public static double getWithoutTaxPrice(double price, float percentage) {
        double netPrice = 0;
        try {
            if (TAX_TYPE.equals(TAX_INCLUSIVE))
                netPrice = getInclusiveNetPrice(price, percentage);
            else
                netPrice = price;
        } catch (Exception e) {
            printLog("getWithoutTaxPrice", "getTaxPrice  Exception " + e.getMessage());
            e.printStackTrace();
        }
        return netPrice;
    }


    public static double getSalePrice(double price, float percentage) {
        double salePrice = 0;
        try {
            if (TAX_TYPE.equals(TAX_INCLUSIVE))
                salePrice = price;
            else
                salePrice = getExclusiveSalePrice(price, percentage);
        } catch (Exception e) {
            printLog("AmountCalculator", "getSalePrice  Exception " + e.getMessage());
            e.printStackTrace();
        }
        return salePrice;
    }


    //    find  percentage  calculate
    public static double getPercentage(double total, double percentageValue) {
        double percentage = 0;
        try {
            percentage = (percentageValue / total) * 100;
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return percentage;
    }


    //    find  percentage value calculate
    public static double getPercentageValue(double value, float percentage) {
        double percentageAmount = 0;
        try {
            percentageAmount = (value * percentage) / 100;//         percentage  calculate
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return percentageAmount;
    }


/**
 * this method using for conversion of   exclusiveNetPrice to sale price
 * */
    //    find  net price from discounted value
    public static double getNetValueFromDiscountedAmount(double discountedPrice, float discountedPercentage) {
        float netAmount = 0;
        try {
            netAmount =(float) discountedPrice/(1-  discountedPercentage/100);    // add percentage   for exclusive calculate
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return netAmount;
    }


    /***
     * tax calculation
     * */

    //    find from mrp amount for tax amount(exclusive)
    private static double getExclusiveTaxAmount(double withTaxPrice, float percentage) {
        double taxAmount = 0;
        try {
            taxAmount = (withTaxPrice * percentage) / 100;//         percentage  calculate total amount
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return taxAmount;
    }

    //    find for sale price from price amount (exclusive)
    private static double getExclusiveSalePrice(double price, float percentage) {
        double salePrice = 0;
        try {
            salePrice = (price * percentage) / 100;//        Percentage  calculate total amount
            salePrice = price + salePrice; //added tax value to price
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return salePrice;
    }

    //     find  for tax amount from price(Inclusive)
    private static double getInclusiveTaxAmount(double price, float percentage) {
        double taxAmount = 0;
        try {
            taxAmount = (price * percentage) / (100 + percentage);  //getting tax value
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return taxAmount;
    }

    //    find  for without tax amount from sale price(Inclusive)
    private static double getInclusiveNetPrice(double price, float percentage) {
        double unitPrice = 0;
        try {
            unitPrice = (price * percentage) / (100 + percentage);  //getting tax value
            unitPrice = price - unitPrice;  //get unit price
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return unitPrice;
    }


}
