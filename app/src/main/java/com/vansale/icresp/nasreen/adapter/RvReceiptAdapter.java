package com.vansale.icresp.nasreen.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.model.Receipt;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


/**
 * Created by mentor on 27/10/17.
 */

public class RvReceiptAdapter extends RecyclerView.Adapter<RvReceiptAdapter.RvReceiptHolder> {

    private Context context;

    private String TAG = "RvReceiptAdapter";


    private ArrayList<Receipt> receipts;
    private OnNotifyListener listener;


    public RvReceiptAdapter(ArrayList<Receipt> receipts) {
        this.receipts = receipts;

    }


    @Override
    public RvReceiptHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receipt, parent, false);

        this.listener = (OnNotifyListener) (parent.getContext());
        return new RvReceiptHolder(view);
    }


    @Override
    public void onBindViewHolder(final RvReceiptHolder holder, @SuppressLint("RecyclerView") final int position) {

        final Receipt receipt = receipts.get(position);


        try {
            int s = position + 1;


            holder.tvSlNo.setText(String.valueOf(s));
            holder.tvTotalAmount.setText(getAmount(receipt.getTotalAmount()));
            holder.tvInvoiceNo.setText(String.valueOf(receipt.getInvoiceNo()));

            double displayBalance = receipt.getBalanceAmount() - receipt.getReceivedAmount();

            holder.tvBalance.setText(getAmount(displayBalance));


            holder.tvReceivable.setText(getAmount(receipt.getReceivedAmount()));


            if (receipt.getReceivedAmount() == 0.0f)
                holder.tvReceivable.setText("");


        } catch (NullPointerException e) {

            Log.d(TAG, "onBindViewHolder  Exception  " + e.getMessage());
        }

    }


    public void updateItem(final Receipt item, final int pos) {

        this.receipts.set(pos, item);
        notifyItemChanged(pos);
    }


    private void delete(int position) { //removes the row

        receipts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, receipts.size());
    }

    private void addItem(Receipt item) {
        receipts.add(item);
        notifyItemInserted(receipts.size() - 1);
        notifyDataSetChanged();
    }


    public double getReceivedTotal() {
        double netTotal = 0.0f;
        try {

            for (Receipt receipt : receipts) {
                if (receipt.getReceivedAmount() != 0) {
                    double d = receipt.getReceivedAmount();
                    netTotal += d;
                }
            }

        } catch (NullPointerException e) {
            Log.d(TAG, "getReceivedTotal Exception  " + e.getMessage());

        }
        return netTotal;
    }


    public ArrayList<Receipt> getPaymentList() {

        return receipts;
    }

    public ArrayList<Receipt> getRecievableList() {

        ArrayList<Receipt> list = new ArrayList<>();

        try {

            for (Receipt rec : receipts) {

                if (rec.getReceivedAmount() != 0.0)
                    list.add(rec);
            }

        } catch (NullPointerException e) {
            Log.d(TAG, "getRecievableList Exception  " + e.getMessage());

        }
        return list;
    }

    @Override
    public int getItemCount() {

        return (null != receipts ? receipts.size() : 0);
    }


    class RvReceiptHolder extends RecyclerView.ViewHolder {
        TextView tvSlNo, tvInvoiceNo, tvTotalAmount, tvBalance, tvReceivable;

        RvReceiptHolder(View itemView) {
            super(itemView);

            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_slNo);
            tvInvoiceNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_invoiceNo);
            tvTotalAmount = (TextView) itemView.findViewById(R.id.textView_item_receipt_totalAmount);
            tvBalance = (TextView) itemView.findViewById(R.id.textView_item_receipt_balanceAmount);
            tvReceivable = (TextView) itemView.findViewById(R.id.textView_item_receipt_receivableAmount);


        }

    }


}
