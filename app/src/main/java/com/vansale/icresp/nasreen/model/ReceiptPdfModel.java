package com.vansale.icresp.nasreen.model;

import java.io.Serializable;
import java.util.List;

public class ReceiptPdfModel implements Serializable {


    private int lineNumber;

    private List<Receipt> receiptList;

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public List<Receipt> getReceiptList() {
        return receiptList;
    }

    public void setReceiptList(List<Receipt> receiptList) {
        this.receiptList = receiptList;
    }
}
