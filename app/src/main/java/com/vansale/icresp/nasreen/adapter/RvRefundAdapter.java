package com.vansale.icresp.nasreen.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.Refund;

import java.sql.Ref;
import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;

/**
 * Created by mentor on 27/10/17.
 */

public class RvRefundAdapter extends RecyclerView.Adapter<RvRefundAdapter.RvRefundHolder> {

    private Context context;
    private String TAG = "RvRefundAdapter";

    private ArrayList<Refund> refunds;
    private OnNotifyListener listener;

    public RvRefundAdapter(ArrayList<Refund> refunds) {
        this.refunds = refunds;

        Log.e("refund adapter", ""+refunds.size());

    }

    @Override
    public RvRefundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receipt, parent, false);

        this.listener = (OnNotifyListener) (parent.getContext());
        return new RvRefundHolder(view);
    }

    @Override
    public void onBindViewHolder(final RvRefundHolder holder, @SuppressLint("RecyclerView") final int position) {

        final Refund r = refunds.get(position);

        try {
            int s = position + 1;

            holder.tvSlNo.setText(String.valueOf(s));
            holder.tvTotalAmount.setText(getAmount(r.getTotalRefund()));
            holder.tvInvoiceNo.setText(String.valueOf(r.getSaleRefundId()));

            double displayBalance = r.getTotalBalance() - r.getReceivedAmount();

            holder.tvBalance.setText(getAmount(displayBalance));
            holder.tvReceivable.setText(getAmount(r.getReceivedAmount()));

            if (r.getReceivedAmount() == 0.0f)
                holder.tvReceivable.setText("");

        } catch (NullPointerException e) {

            Log.d(TAG, "onBindViewHolder  Exception  " + e.getMessage());
        }
    }

    public void updateItem(final Refund item, final int pos) {

        this.refunds.set(pos, item);
        notifyItemChanged(pos);
    }

    private void delete(int position) { //removes the row

        refunds.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, refunds.size());
    }

    private void addItem(Refund item) {
        refunds.add(item);
        notifyItemInserted(refunds.size() - 1);
        notifyDataSetChanged();
    }

    public double getReceivedTotal() {
        double netTotal = 0.0f;
        try {

            for (Refund refund : refunds) {
                if (refund.getReceivedAmount() != 0) {
                    double d = refund.getReceivedAmount();
                    netTotal += d;
                }
            }

        } catch (NullPointerException e) {
            Log.d(TAG, "getReceivedTotal Exception  " + e.getMessage());

        }
        return netTotal;
    }

    public ArrayList<Refund> getPaymentList() {

        return refunds;
    }

    public ArrayList<Refund> getRecievableList() {

        ArrayList<Refund> list = new ArrayList<>();

        try {

            for (Refund rec : refunds) {

                if (rec.getReceivedAmount() != 0.0)
                    list.add(rec);
            }
        } catch (NullPointerException e) {
            Log.d(TAG, "getRecievableList Exception  " + e.getMessage());

        }
        return list;
    }

    @Override
    public int getItemCount() {

        return (null != refunds ? refunds.size() : 0);
    }

    class RvRefundHolder extends RecyclerView.ViewHolder {
        TextView tvSlNo, tvInvoiceNo, tvTotalAmount, tvBalance, tvReceivable;

        RvRefundHolder(View itemView) {
            super(itemView);

            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_slNo);
            tvInvoiceNo = (TextView) itemView.findViewById(R.id.textView_item_receipt_invoiceNo);
            tvTotalAmount = (TextView) itemView.findViewById(R.id.textView_item_receipt_totalAmount);
            tvBalance = (TextView) itemView.findViewById(R.id.textView_item_receipt_balanceAmount);
            tvReceivable = (TextView) itemView.findViewById(R.id.textView_item_receipt_receivableAmount);

        }
    }
}
