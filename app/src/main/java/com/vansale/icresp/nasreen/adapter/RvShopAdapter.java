package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.listener.ShopClickListener;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionValue;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


public class RvShopAdapter extends RecyclerView.Adapter<RvShopAdapter.ViewHolder> implements Filterable {

    private final ArrayList<Shop> shops;

    String TAG = "RvShopAdapter";
    private ArrayList<Shop> filteredList;
    private SessionValue sessionValue;

    ShopClickListener listener;

    private Context context;

    public RvShopAdapter(ArrayList<Shop> shops) {
        this.shops = shops;
        this.filteredList = shops;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        this.sessionValue = new SessionValue(context);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_item, parent, false);

        listener = (ShopClickListener) parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Shop shop = filteredList.get(position);
        holder.tvShopName.setText(shop.getShopName());
        holder.tvAddress.setText(shop.getShopAddress());
        holder.tvCode.setText(shop.getShopCode());


        holder.tvPhone.setText(getAmount(shop.getDebit() - shop.getCredit()));


        holder.viewIndicate.setBackgroundColor(shop.isVisit() ? context.getResources().getColor(R.color.colorGreen) : context.getResources().getColor(R.color.colorRed));

        holder.tvShopName.setTextColor(shop.isVisit() ? context.getResources().getColor(R.color.colorGreen) : context.getResources().getColor(R.color.colorRed));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (sessionValue.isGroupRegister()) {

                    if (listener != null)
                        listener.onShopClick(shop);

                } else
                    Toast.makeText(context, "Please Register", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != filteredList ? filteredList.size() : 0);

    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    filteredList = shops;
                } else {

                    ArrayList<Shop> list = new ArrayList<>();

                    for (Shop s : shops) {

                        if (s.getShopName().toLowerCase().contains(charString) || s.getShopCode().toLowerCase().contains(charString)) {

                            list.add(s);
                        }
                    }

                    filteredList = list;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Shop>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvShopName, tvAddress, tvCode, tvPhone;
        public View viewIndicate;

        public ViewHolder(View itemView) {
            super(itemView);
            tvShopName = (TextView) itemView.findViewById(R.id.textView_shop_name);
            tvAddress = (TextView) itemView.findViewById(R.id.textView_shop_address);
            tvCode = (TextView) itemView.findViewById(R.id.textView_shop_code);
            tvPhone = (TextView) itemView.findViewById(R.id.textView_shop_phone);
            viewIndicate = (View) itemView.findViewById(R.id.view_shop_item_status_indicate);

        }


    }
}
