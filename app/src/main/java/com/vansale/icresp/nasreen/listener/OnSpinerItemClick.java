package com.vansale.icresp.nasreen.listener;

/**
 * Created by mentor on 22/6/17.
 */

public interface OnSpinerItemClick {
    void onClick(Object item, int position);
}
