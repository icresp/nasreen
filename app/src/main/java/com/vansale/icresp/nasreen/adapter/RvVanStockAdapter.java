package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.model.Product;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.Generic.getAmount;


/**
 * Created by mentor on 26/10/17.
 */

public class RvVanStockAdapter extends RecyclerView.Adapter<RvVanStockAdapter.RvVanStockHolder> {

    private Context context;
    private ArrayList<Product> products;

    public RvVanStockAdapter(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public RvVanStockHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_van_stock, parent, false);
        return new RvVanStockHolder(view);
    }

    @Override
    public void onBindViewHolder(RvVanStockHolder holder, int position) {

        Product p = products.get(position);

        int s = position + 1;
        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvProductType.setText(p.getProductType());
        holder.tvProductCode.setText(p.getProductCode());
        holder.tvProductName.setText(p.getProductName());
        holder.tvRetailPrice.setText(String.valueOf(getAmount(p.getRetailPrice()) + " " + context.getString(R.string.currency)));

        int bo=0,bal=0;
        if (p.getPiecepercart()>1&& p.getPiecepercart()<=p.getStockQuantity()) {
            bo = p.getStockQuantity() / p.getPiecepercart();
        }

        bal = p.getStockQuantity() -(bo*p.getPiecepercart());

     //   holder.tvQuantity.setText(String.valueOf(p.getStockQuantity()));
        holder.tvQuantity.setText(String.valueOf((bo+"/"+bal)));

    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    class RvVanStockHolder extends RecyclerView.ViewHolder {

        TextView tvSlNo, tvProductType, tvProductName, tvRetailPrice, tvQuantity, tvProductCode;

        private RvVanStockHolder(View itemView) {
            super(itemView);
            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_vanStock_slNo);
            tvProductType = (TextView) itemView.findViewById(R.id.textView_item_vanStock_productType);
            tvProductCode = (TextView) itemView.findViewById(R.id.textView_item_vanStock_productCode);
            tvProductName = (TextView) itemView.findViewById(R.id.textView_item_vanStock_productName);
            tvRetailPrice = (TextView) itemView.findViewById(R.id.textView_item_vanStock_RetailPrice);
            tvQuantity = (TextView) itemView.findViewById(R.id.textView_item_vanStock_totalQty);


        }
    }
}
