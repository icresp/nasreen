package com.vansale.icresp.nasreen.activity.printpreview;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.languages.ArabicLigaturizer;
import com.itextpdf.text.pdf.languages.LanguageProcessor;
import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvPreviewReceiptAdapter;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.ReceiptPdfModel;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.vansale.icresp.nasreen.config.ConfigKey.WRITE_REQUEST_CODE;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.INVOICE_LIST_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.OPENING_BALANCE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PAID_AMOUNT_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.RECEIPT_NO_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.DigitToWordConverter.convertNumberToEnglishWords;
import static com.vansale.icresp.nasreen.config.Generic.dateFormat;
import static com.vansale.icresp.nasreen.config.Generic.dateToFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.Generic.getMaximumChar;
import static com.vansale.icresp.nasreen.config.Generic.stringToDate;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_1;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_1_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_2;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_2_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_CR;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_EMAIL;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_MOBILE;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_NAME;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_NAME_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_VAT;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_ID;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_MOBILE;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_NAME;

public class ReceiptPreviewActivity extends AppCompatActivity implements View.OnClickListener {

    private static String FILE_PATH = Environment.getExternalStorageDirectory() + "/icresp_receipt.pdf";


    String TAG = "ReceiptPreviewActivity";
    int callingActivity = 0;
    String strExecutive = " ", strReceiptDetails = "", strReceiptNo = "", strPaymentType = "", compName = "", companyAddress = "", companyCr = "", companyVat = "", execId = "", execMob = "";
    String strShopName = "", strShopNameArabic = "", strCustomerMob = "", strCustomerVat = "", strSalePo = "", strCustomerNo = "", strShopAddress = "", strDate = "";


    private final int MAX_LINE = 6;
    ViewGroup viewReceipt, viewOpeningBalance;
    private Button btnPrint, btnHome;
    private TextView tvOpeningBalance, tvBalancePaid, tvTitle, tvReceiptNo, tvDate, tvShopDetails, tvTotal;
    private Shop SELECTED_SHOPE = null;
    private OpeningBalanceTransaction OPENING_BALANCE = null;
    private ArrayList<Receipt> RECEIPTS = new ArrayList<>();
    private double paid_amount = 0;


    private SessionValue sessionValue;


    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_preview);
        btnPrint = findViewById(R.id.button_receipt_preview_print);
        btnHome = findViewById(R.id.button_receipt_preview_home);

        tvTitle = findViewById(R.id.textView_receipt_preview_title);

        tvShopDetails = findViewById(R.id.textView_receipt_preview_shopDetails);
        tvReceiptNo = findViewById(R.id.textView_receipt_preview_receiptNo);
        tvDate = findViewById(R.id.textView_receipt_preview_date);
        tvTotal = findViewById(R.id.textView_preview_total);
        viewReceipt = findViewById(R.id.view_invoice_receipt);
        viewOpeningBalance = findViewById(R.id.view_opening_balance);
        recyclerView = findViewById(R.id.recyclerView_receipt_preview);

        tvOpeningBalance = findViewById(R.id.textView_balance_opening);
        tvBalancePaid = findViewById(R.id.textView_balance_paid);
        sessionValue = new SessionValue(getApplicationContext());


        strExecutive = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_NAME);
        compName = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME);  //name get from session
        companyAddress = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1);  //address get from session
        companyCr = sessionValue.getCompanyDetails().get(PREF_COMPANY_CR);
        companyVat = sessionValue.getCompanyDetails().get(PREF_COMPANY_VAT);
        execId = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_ID);  //id get from session
        execMob = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_MOBILE);  //mob get from session


/******/

        try {

            SELECTED_SHOPE = (Shop) getIntent().getSerializableExtra(SHOP_VALUE_KEY);


            strShopName = SELECTED_SHOPE.getShopName();
            strShopAddress = SELECTED_SHOPE.getShopAddress();

            if (SELECTED_SHOPE.getShopArabicName() != null && !TextUtils.isEmpty(SELECTED_SHOPE.getShopArabicName()))
                strShopNameArabic = SELECTED_SHOPE.getShopArabicName();

            if (SELECTED_SHOPE.getVatNumber() != null && !TextUtils.isEmpty(SELECTED_SHOPE.getVatNumber()))
                strCustomerVat = SELECTED_SHOPE.getVatNumber();

            if (SELECTED_SHOPE.getShopCode() != null && !TextUtils.isEmpty(SELECTED_SHOPE.getShopCode()))
                strCustomerNo = SELECTED_SHOPE.getShopCode();

            if (SELECTED_SHOPE.getShopMobile() != null && !TextUtils.isEmpty(SELECTED_SHOPE.getShopMobile()))
                strCustomerMob = SELECTED_SHOPE.getShopMobile();


            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, 0);
            switch (callingActivity) {

                case ActivityConstants.ACTIVITY_OPENING_BALANCE_RECEIPT:


                    viewOpeningBalance.setVisibility(View.VISIBLE);

                    tvTitle.setText("RECEIPT VOUCHER");


                    OPENING_BALANCE = (OpeningBalanceTransaction) getIntent().getSerializableExtra(OPENING_BALANCE_KEY);

                    tvOpeningBalance.setText(String.valueOf("Opening Balance\n" + getAmount(OPENING_BALANCE.getBalanceAmount()) + " " + getString(R.string.currency)));
                    tvBalancePaid.setText(String.valueOf("Paid\n" + getAmount(OPENING_BALANCE.getReceivedAmount()) + " " + getString(R.string.currency)));
                    paid_amount = OPENING_BALANCE.getReceivedAmount();

                    strReceiptNo = OPENING_BALANCE.getReceiptNo();

                    strReceiptDetails = OPENING_BALANCE.getTransactionType() + "\n" + OPENING_BALANCE.getBankName() + "\n" + OPENING_BALANCE.getChequeNumber();
                    strPaymentType = OPENING_BALANCE.getTransactionType();

                    final Date date = stringToDate(OPENING_BALANCE.getLogDate());
//                strDate = getPrintDate(date);
                    strDate = dateToFormat(date);


                    break;
                case ActivityConstants.ACTIVITY_INVOICE_RECEIPT:


                    viewReceipt.setVisibility(View.VISIBLE);
                    viewOpeningBalance.setVisibility(View.GONE);
                    tvTitle.setText("RECEIPT VOUCHER");

                    paid_amount = getIntent().getDoubleExtra(PAID_AMOUNT_VALUE_KEY, 0);


                    strReceiptNo = getIntent().getStringExtra(RECEIPT_NO_KEY);
                    RECEIPTS.clear();
                    ArrayList<Receipt> li = (ArrayList<Receipt>) getIntent().getSerializableExtra(INVOICE_LIST_KEY);

                    if (li.size() > 0) {
                        Receipt r = li.get(0);
                        strReceiptDetails = r.getTransactionType() + "\n" + r.getBankName() + "\n" + r.getChequeNumber();

                        strPaymentType = r.getTransactionType();
                    }
                    RECEIPTS.addAll(li);


                    final Date dt = stringToDate(li.get(0).getLogDate());

                    strDate = dateToFormat(dt);


                    setRecyclerView();


                    break;
                default:


            }
        } catch (NullPointerException e) {
            printLog(TAG, "exception   " + e.getMessage());
        }
        tvDate.setText(strDate);
        if (!TextUtils.isEmpty(strReceiptNo)) {
            tvDate.setText(String.valueOf(strDate + "\n" + strReceiptDetails));
        }


        tvShopDetails.setText(String.valueOf(strShopName + "\n" + strShopAddress));
        tvReceiptNo.setText("Receipt No : " + strReceiptNo);

        btnPrint.setOnClickListener(this);
        btnHome.setOnClickListener(this);

    }


    private void setRecyclerView() {

        if (RECEIPTS.isEmpty()) {
            showToast("Some wrong");
            return;
        }


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());

        recyclerView.setAdapter(new RvPreviewReceiptAdapter(RECEIPTS));


        String total = String.valueOf("TOTAL     : " + getAmount(paid_amount) + " " + getString(R.string.currency));

        tvTotal.setText(total);


    }

    /**
     * *************PRINTER
     ********************/


    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button_receipt_preview_home:

                onBackPressed();

                break;
            case R.id.button_receipt_preview_print:

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_OPENING_BALANCE_RECEIPT:

                        if (isStoragePermissionGranted())
                            printOpeningBalance();
//                        showToast("Can't print file");

                        break;
                    case ActivityConstants.ACTIVITY_INVOICE_RECEIPT:

                        if (isStoragePermissionGranted())
                            printReceipt(getPdfModels(RECEIPTS));

//                        showToast("Can't print file");

                        break;
                    default:


                }


                break;

        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void printOpeningBalance() {

        if (OPENING_BALANCE == null)
            showToast("wrong Opening data");

        File myFile = null;

        try {

            String compName = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME);  //name get from session
            String compNameArab = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME_ARAB);  //name get from session
            String address1Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1);  //address get from session
            String address1ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1_ARAB);  //address get from session
            String address2Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2);  //address get from session
            String address2ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2_ARAB);  //address get from session
            String compemailStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_EMAIL);  //address get from session
            String mobileStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_MOBILE);  //address get from session

            String compRegisterStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_CR);
            String companyVatStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_VAT);

            String execName = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_NAME);  //name get from session
            assert execName != null;
            execName=execName.toUpperCase();

            String execId = "Code     : " + sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_ID);  //id get from session
            String execMob = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_MOBILE);  //mob get from session


            // Create New Blank Document
            Document document = new Document(PageSize.A4);


            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE_PATH));
            myFile = new File(FILE_PATH);


            document.open();


            BaseFont bf = BaseFont.createFont("/assets/dejavu_sans_condensed.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);


            Font font20 = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);

            Font font18 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

            Font font14 = new Font(Font.FontFamily.TIMES_ROMAN, 14);
            Font font16Ul_Bold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.UNDERLINE | Font.BOLD);


            Font font10Bold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
            Font font10 = new Font(Font.FontFamily.TIMES_ROMAN, 10);

            Font font6 = new Font(Font.FontFamily.TIMES_ROMAN, 6);
            Font font8 = new Font(Font.FontFamily.TIMES_ROMAN, 8);
            Font font8Bold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);

            Font font12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC);


            LanguageProcessor arabicPro = new ArabicLigaturizer();
            Font fontArb8 = new Font(bf, 8);
            Font fontArb8Bold = new Font(bf, 8, Font.BOLD);
            Font fontArb10 = new Font(bf, 10);
            Font fontArb14 = new Font(bf, 14);


            Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font8Bold);
            compMobileTag.setAlignment(Element.ALIGN_LEFT);

            Paragraph compFaxTag = new Paragraph("FAX: 02 4457022", font8Bold);
            compFaxTag.setAlignment(Element.ALIGN_LEFT);

            Paragraph compAddress1Tag = new Paragraph(address1Str, font8Bold);
            compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

            Paragraph compAddress2Tag = new Paragraph(address2Str, font8Bold);
            compAddress2Tag.setAlignment(Element.ALIGN_LEFT);


            Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font8Bold);
            compVatTag.setAlignment(Element.ALIGN_RIGHT);


            PdfPCell cell;  //default cell

            //space cell
            PdfPCell cellSpace = new PdfPCell();
            cellSpace.setPadding(10);
            cellSpace.setBorder(PdfPCell.NO_BORDER);
            cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


            //Create the table which will be 2 Columns wide and make it 100% of the page
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100.0f);
//            table.setSpacingBefore(10);
            table.setWidths(new int[]{1, 1, 1});


            PdfPCell cellLogo = new PdfPCell();
            cellLogo.setBorder(PdfPCell.NO_BORDER);
            cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
            cellLogo.setPaddingTop(5);
            cellLogo.setPaddingBottom(5);
            cellLogo.setPaddingRight(5);
            cellLogo.setPaddingLeft(7);

            try {

                Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.nasreen_pdf_logo);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image img = Image.getInstance(stream.toByteArray());

                cellLogo.addElement(img);


            } catch (IOException e) {

                e.printStackTrace();
            }
            table.addCell(cellSpace);
            table.addCell(cellLogo);
            table.addCell(cellSpace);

            document.add(table);

            //Create the table which will be 2 Columns wide and make it 100% of the page
            table = new PdfPTable(2);
            table.setWidthPercentage(100.0f);
            table.setWidths(new int[]{2, 1});

            ////


            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setPadding(5);


            cell.addElement(compMobileTag);
            cell.addElement(compFaxTag);
            cell.addElement(compAddress1Tag);
            cell.addElement(compAddress2Tag);

            table.addCell(cell);


//top right
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setPadding(5);


            Paragraph paragraph = new Paragraph("Receipt Voucher", font16Ul_Bold);
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            paragraph.setSpacingAfter(2);

            cell.addElement(paragraph);
            cell.addElement(compVatTag);

            table.addCell(cell);


//Add the PdfPTable to the table
            document.add(table);


            //Create the table which will be 3 Columns wide and make it 100% of the page
            table = new PdfPTable(2);
            table.setWidthPercentage(100.0f);
            table.setWidths(new int[]{4, 2});


            /////////////////*****  customer data ******/////
//            customer details
            cell = new PdfPCell();
            cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
            cell.setVerticalAlignment(Element.ALIGN_BASELINE);
            cell.setPadding(5);


//            customer name
            String nam = strShopName;
            nam = getMaximumChar(nam, 60);
            paragraph = new Paragraph("Customer : " + nam, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);


            //            vat number
            String justifiedVatNumber = strCustomerVat;
            if (justifiedVatNumber.length() < 44)
                justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
            else
                justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

            paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);

//tel
            String justifiedMobNumber = strCustomerMob;
            if (justifiedMobNumber.length() < 44)
                justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
            else
                justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

            paragraph = new Paragraph("Tel : " + justifiedMobNumber, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);


            //            customer Address
            String justifiedCustomerAddress = strShopAddress;
            if (justifiedCustomerAddress.length() < 44)
                justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
            else
                justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
            paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


            //             executive name and mobile
            String justifiedExecutiveMobile = " - "+execMob;

            if (justifiedExecutiveMobile.length() > 20)
                justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

            String justifiedExecutiveName = execName;
            if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());


            paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);

            table.addCell(cell);

            /////////////////*****  receipt data ******/////

            cell = new PdfPCell();
            cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
            cell.setVerticalAlignment(Element.ALIGN_BASELINE);
            cell.setPadding(5);


            //            receipt number
            String justifiedBill = strReceiptNo;
            if (justifiedBill.length() < 28)
                justifiedBill = String.format("%-40s", justifiedBill);
            else
                justifiedBill = getMaximumChar(justifiedBill, 28);

            paragraph = new Paragraph("Receipt No : " + justifiedBill, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);


            //            po number
            String justifiedPo = strSalePo;
            if (justifiedPo.length() < 28)
                justifiedPo = String.format("%-40s", justifiedPo);
            else
                justifiedPo = getMaximumChar(justifiedPo, 28);

            paragraph = new Paragraph("PO No : " + justifiedPo, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


            //           bill date
            String justifiedDate = String.format("%-40s", strDate);
            paragraph = new Paragraph("Inv . Date : " + justifiedDate, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);

//                pay method
            paragraph = new Paragraph("Pay.Method : " + strPaymentType, font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);

            //                currency
            paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);
            table.addCell(cell);
            document.add(table);


            //Create the table which will be 1 Columns wide and make it 100% of the page
            table = new PdfPTable(1);
            table.setWidthPercentage(100.0f);
            table.setWidths(new int[]{1});
            table.setSpacingBefore(5);

            //            total
            cell = new PdfPCell(new Phrase("Total Amount : " + paid_amount, font8Bold));
            cell.setBorder(PdfPCell.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_BASELINE);
            cell.setPadding(5);

            table.addCell(cell);


            document.add(table);


            //Create the table which will be 4 Columns wide and make it 100% of the page
            table = new PdfPTable(4);
            table.setWidths(new int[]{1, 1, 1, 1});
            table.setWidthPercentage(100);
            table.setSpacingBefore(5);

//opening balance label
            cell = new PdfPCell();
            cell.setPaddingBottom(7);
            paragraph = new Paragraph("Opening Balance", font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);
            table.addCell(cell);

//paid amount label
            cell = new PdfPCell();
            cell.setPaddingBottom(7);
            paragraph = new Paragraph("Paid Amount", font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);
            table.addCell(cell);


//Discount label
            cell = new PdfPCell();
            cell.setPaddingBottom(7);
            paragraph = new Paragraph("Discount", font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);
            table.addCell(cell);

//Balance label
            cell = new PdfPCell();
            cell.setPaddingBottom(7);
            paragraph = new Paragraph("Balance", font8Bold);
            paragraph.setAlignment(Element.ALIGN_LEFT);
            cell.addElement(paragraph);
            table.addCell(cell);

            String strOpening_Bal = " ", strInv_paid = " ", strInv_discount = "  ", strInv_balance = " ";

            strOpening_Bal = getAmount(OPENING_BALANCE.getReceivedAmount() + OPENING_BALANCE.getBalanceAmount());

            strInv_paid = getAmount(OPENING_BALANCE.getReceivedAmount());
            strInv_discount = getAmount(0);
            strInv_balance = getAmount(OPENING_BALANCE.getBalanceAmount());


//                   opening balance
            cell = new PdfPCell(new Phrase(strOpening_Bal, font8));
            cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setFixedHeight(5f);
            cell.setPaddingLeft(10);
            cell.setPaddingRight(6);
            table.addCell(cell);


//paid amount
            cell = new PdfPCell(new Phrase(strInv_paid, font8));
            cell.setBorder(Rectangle.RIGHT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(10);
            cell.setPaddingRight(6);
            table.addCell(cell);

            //discount
            cell = new PdfPCell(new Phrase(strInv_discount, font8));
            cell.setBorder(Rectangle.RIGHT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(10);
            cell.setPaddingRight(6);
            table.addCell(cell);


            //balance
            cell = new PdfPCell(new Phrase(strInv_balance, font8));
            cell.setBorder(Rectangle.RIGHT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(10);
            cell.setPaddingRight(6);
            table.addCell(cell);

            document.add(table);


            ///////////////*****************//////////////////


            //Create the table which will be 1 Columns wide and make it 100% of the page
            table = new PdfPTable(1);

            table.setWidthPercentage(100.0f);
            table.setWidths(new int[]{1});

//
            String val_in_english = convertNumberToEnglishWords(String.valueOf(paid_amount));
            cell = new PdfPCell(new Phrase("Amount In Words : " + val_in_english , font8Bold));
            cell.setBorder(PdfPCell.BOX);
            cell.setPaddingTop(5);
            cell.setPaddingBottom(10);
            table.addCell(cell);

            document.add(table);


            //Create the table which will be 2 Columns wide and make it 100% of the page
            table = new PdfPTable(2);
            table.setWidthPercentage(100.0f);
            table.setWidths(new int[]{2, 1});
            table.setSpacingBefore(5);

            //            Receiver Sign
            cell = new PdfPCell(new Phrase("Receiver’s Sign : ", font8Bold));
            cell.setBorder(PdfPCell.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_BASELINE);
            cell.setPadding(5);

            table.addCell(cell);

            //            sales executive
            cell = new PdfPCell(new Phrase("Sales Executive : ", font8Bold));
            cell.setBorder(PdfPCell.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_BASELINE);
            cell.setPadding(5);

            table.addCell(cell);
            document.add(table);


            document.close();
//Print PDF File
            printPDF(myFile);

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
            Log.d(TAG, "exception  " + e.getMessage());
            Toast.makeText(this, "Error, unable to write to file\n" + e.getMessage(), Toast.LENGTH_SHORT).show();

        }


    }



    /****
     * PDF Print
     * */
    private List<ReceiptPdfModel> getPdfModels(ArrayList<Receipt> list) {


        final List<ReceiptPdfModel> models = new ArrayList<>();


        try {

            List<Receipt> sublist;


            if (list.size() <= MAX_LINE) {

                ReceiptPdfModel pdfModel = new ReceiptPdfModel();
                pdfModel.setReceiptList(list);
                models.add(pdfModel);

            } else {

                int count = list.size() / MAX_LINE;

                int SUBLIST_START_SIZE = 0, SUBLIST_END_SIZE = MAX_LINE;

                for (int i = 0; i < count; i++) {

                    if (list.size() >= SUBLIST_END_SIZE) {
                        sublist = list.subList(SUBLIST_START_SIZE, SUBLIST_END_SIZE);
                        ReceiptPdfModel pdfModel = new ReceiptPdfModel();
                        pdfModel.setReceiptList(sublist);
                        models.add(pdfModel);

                    }

                    if (SUBLIST_END_SIZE < list.size()) {
//                        return models;
                        SUBLIST_START_SIZE = SUBLIST_START_SIZE + MAX_LINE;
                        SUBLIST_END_SIZE = SUBLIST_END_SIZE + MAX_LINE;

                    }

                }


                if (list.size() < SUBLIST_END_SIZE) {
                    sublist = list.subList(SUBLIST_START_SIZE, list.size());
                    ReceiptPdfModel pdfModel = new ReceiptPdfModel();
                    pdfModel.setReceiptList(sublist);
                    models.add(pdfModel);
                }

            }

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return models;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void printReceipt(List<ReceiptPdfModel> pdfList) {

        File myFile = null;

        PdfWriter writer;
        try {

            String compName = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME);  //name get from session
            String compNameArab = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME_ARAB);  //name get from session
            String address1Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1);  //address get from session
            String address1ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1_ARAB);  //address get from session
            String address2Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2);  //address get from session
            String address2ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2_ARAB);  //address get from session
            String compemailStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_EMAIL);  //address get from session
            String mobileStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_MOBILE);  //address get from session

            String compRegisterStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_CR);
            String companyVatStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_VAT);

            String execName = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_NAME);  //name get from session
            assert execName != null;
            execName=execName.toUpperCase();

            String execId = "Code     : " + sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_ID);  //id get from session
            String execMob = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_MOBILE);  //mob get from session


            // Create New Blank Document
            Document document = new Document(PageSize.A4);

//            Document document = new Document();

            writer = PdfWriter.getInstance(document, new FileOutputStream(FILE_PATH));

            myFile = new File(FILE_PATH);


            document.open();

/*

            PdfContentByte canvas = writer.getDirectContent();
//            Rectangle rect = new Rectangle(36, 36, 559, 806);
//            Rectangle rect = new Rectangle(30, 430, 565, 806);
            Rectangle rect = new Rectangle(30, 500, 565, 806);
            rect.setBorder(Rectangle.BOX);
            rect.setBorderWidth(.7f);
            canvas.rectangle(rect);
*/

            ////
//            BaseFont bf = BaseFont.createFont("/assets/tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont bf = BaseFont.createFont("/assets/dejavu_sans_condensed.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);


            Font font20 = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);

            Font font18 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

            Font font14 = new Font(Font.FontFamily.TIMES_ROMAN, 14);
            Font font16Ul_Bold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.UNDERLINE | Font.BOLD);


            Font font10Bold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
            Font font10 = new Font(Font.FontFamily.TIMES_ROMAN, 10);

            Font font6 = new Font(Font.FontFamily.TIMES_ROMAN, 6);
            Font font8 = new Font(Font.FontFamily.TIMES_ROMAN, 8);
            Font font8Bold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);

            Font font12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC);


            LanguageProcessor arabicPro = new ArabicLigaturizer();
            Font fontArb8 = new Font(bf, 8);
            Font fontArb8Bold = new Font(bf, 8, Font.BOLD);
            Font fontArb10 = new Font(bf, 10);
            Font fontArb14 = new Font(bf, 14);


            for (int i = 0; i < pdfList.size(); i++) {


                ReceiptPdfModel pdfData = pdfList.get(i);

                List<Receipt> receipts = pdfData.getReceiptList();


                Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font8Bold);
                compMobileTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compMailTag = new Paragraph("Email:"+compemailStr, font8Bold);
                compMailTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress1Tag = new Paragraph(address1Str, font8Bold);
                compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress2Tag = new Paragraph(address2Str, font8Bold);
                compAddress2Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compWebTag = new Paragraph("www.nasreenoil.com", font8Bold);
                compWebTag.setAlignment(Element.ALIGN_LEFT);


                Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font8Bold);
                compVatTag.setAlignment(Element.ALIGN_RIGHT);


                PdfPCell cell;  //default cell

                //space cell
                PdfPCell cellSpace = new PdfPCell();
                cellSpace.setPadding(10);
                cellSpace.setBorder(PdfPCell.NO_BORDER);
                cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


                //Create the table which will be 2 Columns wide and make it 100% of the page
                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(100.0f);
//            table.setSpacingBefore(10);
                table.setWidths(new int[]{1, 1, 1});


                PdfPCell cellLogo = new PdfPCell();
                cellLogo.setBorder(PdfPCell.NO_BORDER);
                cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cellLogo.setPaddingTop(5);
                cellLogo.setPaddingBottom(5);
                cellLogo.setPaddingRight(5);
                cellLogo.setPaddingLeft(7);

                try {

                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.nasreen_pdf_logo);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image img = Image.getInstance(stream.toByteArray());

                    cellLogo.addElement(img);


                } catch (IOException e) {

                    e.printStackTrace();
                }
                table.addCell(cellSpace);
                table.addCell(cellLogo);
                table.addCell(cellSpace);

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});

                ////


                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                cell.addElement(compMobileTag);
                cell.addElement(compMailTag);
                cell.addElement(compAddress1Tag);
                cell.addElement(compAddress2Tag);
                cell.addElement(compWebTag);

                table.addCell(cell);


//top right
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                Paragraph paragraph = new Paragraph("Receipt Voucher", font16Ul_Bold);
                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setSpacingAfter(2);

                cell.addElement(paragraph);
                cell.addElement(compVatTag);


                table.addCell(cell);


//Add the PdfPTable to the table
                document.add(table);


                //Create the table which will be 3 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{4, 2});


                /////////////////*****  customer data ******/////
//            customer details
                cell = new PdfPCell();
                cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


//            customer name
                String nam = strShopName;
                nam = getMaximumChar(nam, 60);
                paragraph = new Paragraph("Customer : " + nam, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer No label
//                paragraph = new Paragraph("Customer No ", font8);
//                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);

                //            vat number
                String justifiedVatNumber = strCustomerVat;
                if (justifiedVatNumber.length() < 44)
                    justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
                else
                    justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

                paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


//tel
                String justifiedMobNumber = strCustomerMob;
                if (justifiedMobNumber.length() < 44)
                    justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
                else
                    justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

                paragraph = new Paragraph("Tel : " + justifiedMobNumber, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer Address
                String justifiedCustomerAddress = strShopAddress;
                if (justifiedCustomerAddress.length() < 44)
                    justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
                else
                    justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
                paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);



                //             executive name and mobile
                String justifiedExecutiveMobile = " - "+execMob;

                if (justifiedExecutiveMobile.length() > 20)
                    justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

                String justifiedExecutiveName = execName;
                if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                    justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());

                paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                table.addCell(cell);

                /////////////////*****  invoice data ******/////

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


                //            receipt number
                String justifiedBill = strReceiptNo;
                if (justifiedBill.length() < 28)
                    justifiedBill = String.format("%-40s", justifiedBill);
                else
                    justifiedBill = getMaximumChar(justifiedBill, 28);

                paragraph = new Paragraph("Receipt No : " + justifiedBill, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            po number
                String justifiedPo = strSalePo;
                if (justifiedPo.length() < 28)
                    justifiedPo = String.format("%-40s", justifiedPo);
                else
                    justifiedPo = getMaximumChar(justifiedPo, 28);

                paragraph = new Paragraph("PO No : " + justifiedPo, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


                //           bill date
                String justifiedDate = String.format("%-40s", strDate);
                paragraph = new Paragraph("Inv . Date : " + justifiedDate, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

//                pay method
                paragraph = new Paragraph("Pay.Method : " + strPaymentType, font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                //                currency
                paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                table.addCell(cell);
                document.add(table);


                //Create the table which will be 1 Columns wide and make it 100% of the page
                table = new PdfPTable(1);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{1});
                table.setSpacingBefore(5);

                //            total
                cell = new PdfPCell(new Phrase("Total Amount : " + paid_amount, font8Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);


                document.add(table);


                //Create the table which will be 4 Columns wide and make it 100% of the page
                table = new PdfPTable(4);
                table.setWidths(new int[]{1, 1, 1, 1});
                table.setWidthPercentage(100);
                table.setSpacingBefore(5);

//invoice number label
                cell = new PdfPCell();
                cell.setPaddingBottom(7);
                paragraph = new Paragraph("Invoice No", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                table.addCell(cell);

//paid amount label
                cell = new PdfPCell();
                cell.setPaddingBottom(7);
                paragraph = new Paragraph("Paid Amount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                table.addCell(cell);


//Discount label
                cell = new PdfPCell();
                cell.setPaddingBottom(7);
                paragraph = new Paragraph("Discount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                table.addCell(cell);


//Balance label
                cell = new PdfPCell();
                cell.setPaddingBottom(7);
                paragraph = new Paragraph("Balance", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                table.addCell(cell);


                for (int j = 0; j < MAX_LINE; j++) {

                    String strSl_No = " ", strInv_no = " ", strInv_paid = " ", strInv_discount = "  ", strInv_balance = " ";

                    if (receipts.size() > j) {
                        Receipt receipt = receipts.get(j);

                        int slNo = i * MAX_LINE + j + 1;
                        strSl_No = String.valueOf(slNo);
                        strInv_no = receipt.getInvoiceNo();
                        strInv_paid = getAmount(receipt.getReceivedAmount());
                        strInv_discount = getAmount(0);
                        strInv_balance = getAmount(receipt.getBalanceAmount());


                    }


                    if (strInv_no.length() > 40)
                        strInv_no = getMaximumChar(strInv_no, 40);


                    strSl_No = String.format("%-3s", strSl_No);
                    strInv_no = String.format("%-5s", strInv_no);

                    strInv_paid = String.format("%-5s", strInv_paid);
                    strInv_discount = String.format("%-5s", strInv_discount);
                    strInv_balance = String.format("%-5s", strInv_balance);


//sl number
                    cell = new PdfPCell(new Phrase(strSl_No, font8));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setFixedHeight(3f);
//                    table.addCell(cell);


//                    invoice no
                    cell = new PdfPCell(new Phrase(strInv_no, font8));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setFixedHeight(5f);
                    cell.setPaddingLeft(10);
                    cell.setPaddingRight(6);
                    table.addCell(cell);


//paid amount
                    cell = new PdfPCell(new Phrase(strInv_paid, font8));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setPaddingLeft(10);
                    cell.setPaddingRight(6);
                    table.addCell(cell);

                    //discount
                    cell = new PdfPCell(new Phrase(strInv_discount, font8));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setPaddingLeft(10);
                    cell.setPaddingRight(6);
                    table.addCell(cell);


                    //balance
                    cell = new PdfPCell(new Phrase(strInv_balance, font8));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setPaddingLeft(10);
                    cell.setPaddingRight(6);
                    table.addCell(cell);


                }

                document.add(table);


                ///////////////*****************//////////////////


                //Create the table which will be 1 Columns wide and make it 100% of the page
                table = new PdfPTable(1);

                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{1});

//
                String val_in_english = convertNumberToEnglishWords(String.valueOf(paid_amount));
                cell = new PdfPCell(new Phrase("Amount In Words : " + val_in_english , font8Bold));
                cell.setBorder(PdfPCell.BOX);
                cell.setPaddingTop(5);
                cell.setPaddingBottom(10);
                table.addCell(cell);

                document.add(table);


                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});
                table.setSpacingBefore(5);

                //            Receiver Sign
                cell = new PdfPCell(new Phrase("Receiver’s Sign : ", font8Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

                //            sales executive
                cell = new PdfPCell(new Phrase("Sales Executive : ", font8Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);
                document.add(table);


            }


            document.close();
//Print PDF File
            printPDF(myFile);

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
            Log.d(TAG, "exception  " + e.getMessage());
            Toast.makeText(this, "Error, unable to write to file\n" + e.getMessage(), Toast.LENGTH_SHORT).show();

        }


    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void printPDF(final File file) {


        PrintDocumentAdapter pda = new PrintDocumentAdapter() {

            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
                InputStream input = null;
                FileOutputStream output = null;

                try {

                    input = new FileInputStream(file);

                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (Exception e) {
                    //Catch exception
                } finally {
                    try {
                        assert input != null;
                        input.close();
                        assert output != null;
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback callback, Bundle extras) {

                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }


                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }
        };


        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

        printManager.print(file.getAbsolutePath(), pda, null);


        /*

        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        String printName = getDocName();
        printManager.print(printName, pda, null);

*/
    }


    // Method for creating a pdf file from text, saving it then opening it for display
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void createandDisplayPdf(String text) {

        Document doc = new Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Dir";

            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "newFile.pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            Paragraph p1 = new Paragraph(text);


            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);


            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(paraFont);

            //add paragraph to document
            doc.add(p1);

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } finally {
            doc.close();
        }

//    viewPdf("newFile.pdf", "Dir");

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Dir/newFile.pdf");
        printPDF(pdfFile);
    }


    // Method for opening a pdf file
    private void viewPdf(String file, String directory) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        Uri path = Uri.fromFile(pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(ReceiptPreviewActivity.this, "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }


    /****/

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == WRITE_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
            printReceipt(getPdfModels(RECEIPTS));

        }


    }


    private static String getTodayDate() {

        return dateFormat.format(Calendar.getInstance().getTime());
    }

}
