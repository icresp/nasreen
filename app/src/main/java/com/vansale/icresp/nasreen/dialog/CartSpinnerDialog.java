package com.vansale.icresp.nasreen.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.CartCodeAdapter;
import com.vansale.icresp.nasreen.listener.OnSpinerItemClick;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.textwatcher.TextValidator;

import java.util.ArrayList;


/**
 * Created by mentor on 22/6/17.
 */

public class CartSpinnerDialog {
    ArrayList<CartItem> items = null;
    Activity context;
    String dTitle;
    OnSpinerItemClick onSpinerItemClick;
    AlertDialog alertDialog;
    int pos;
    int style;

    String TAG = "CartSpinnerDialog";

    public CartSpinnerDialog(Activity activity, ArrayList<CartItem> items, String dialogTitle, int style) {
        this.items = items;
        this.context = activity;
        this.dTitle = dialogTitle;
        this.style = style;
    }

    public void bindOnSpinerListener(OnSpinerItemClick onSpinerItemClick1) {
        this.onSpinerItemClick = onSpinerItemClick1;
    }

    public void showSpinerDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        View v = context.getLayoutInflater().inflate(R.layout.dialog_layout, null);
        TextView rippleViewClose = (TextView) v.findViewById(R.id.close);
        TextView title = (TextView) v.findViewById(R.id.spinerTitle);
        title.setText(dTitle);
        final ListView listView = (ListView) v.findViewById(R.id.list);
        final EditText searchBox = (EditText) v.findViewById(R.id.searchBox);


        final ArrayAdapter<CartItem> adapter = new ArrayAdapter<CartItem>(context, R.layout.items_view, items);
        listView.setAdapter(adapter);
        adb.setView(v);
        alertDialog = adb.create();
        alertDialog.getWindow().getAttributes().windowAnimations = style;//R.style.DialogAnimations_SmileWindow;
        alertDialog.setCancelable(false);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view.findViewById(R.id.text1);
                for (int j = 0; j < items.size(); j++) {

                    if (t.getText().toString().equalsIgnoreCase(items.get(j).toString())) {
                        pos = j;
                        onSpinerItemClick.onClick(items.get(pos), pos);
                        alertDialog.dismiss();
                    }
                }
//                onSpinerItemClick.onClick(items.get(pos),pos);
//                alertDialog.dismiss();
            }
        });

        searchBox.addTextChangedListener(new TextValidator(searchBox) {


            @Override
            public void validate(TextView textView, String text) {
                adapter.getFilter().filter(searchBox.getText().toString());
            }
        });

        rippleViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void showCodeSpinerDialog() {


        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        View v = context.getLayoutInflater().inflate(R.layout.dialog_layout, null);
        TextView rippleViewClose = (TextView) v.findViewById(R.id.close);
        TextView title = (TextView) v.findViewById(R.id.spinerTitle);
        title.setText("Product Code");
        final ListView listView = (ListView) v.findViewById(R.id.list);
        final EditText searchBox = (EditText) v.findViewById(R.id.searchBox);


//android.R.layout.simple_dropdown_item_1line
        final CartCodeAdapter productCodeAdapter = new CartCodeAdapter(context, R.layout.items_view, items);
        listView.setAdapter(productCodeAdapter);
        adb.setView(v);
        alertDialog = adb.create();
        alertDialog.getWindow().getAttributes().windowAnimations = style;//R.style.DialogAnimations_SmileWindow;
        alertDialog.setCancelable(false);

        productCodeAdapter.notifyDataSetChanged();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                TextView t = (TextView) view.findViewById(R.id.text1);
                for (int j = 0; j < items.size(); j++) {
                    if (t.getText().toString().equalsIgnoreCase(items.get(j).getProductCode())) {
                        pos = j;
                        onSpinerItemClick.onClick(items.get(pos), pos);
                        alertDialog.dismiss();

                    }
                }
//                onSpinerItemClick.onClick(items.get(pos),pos);
//                alertDialog.dismiss();

            }
        });


        searchBox.setVisibility(View.GONE);
        searchBox.addTextChangedListener(new TextValidator(searchBox) {


            @Override
            public void validate(TextView textView, String text) {
//                productCodeAdapter.getFilter().filter(searchBox.getText().toString());

            }

        });


        rippleViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();


    }


}
