package com.vansale.icresp.nasreen.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mentor on 23/10/17.
 */

public class Shop implements Serializable {

    private int localId;
    private int shopId;

    private String shopArabicName;
    private String vatNumber;

    private String shopCode;
    private String shopName;
    private String shopMail;
    private String shopMobile;
    private String shopAddress;
    private String routeCode;
    private String typeId;
    private boolean isVisit;
    private double credit;
    private double debit;
    private double openingBalance;
    private double creditLimit;
    private float productDiscount;
    private float collectionDiscount;

    private ArrayList<Invoice> invoices;
    private ArrayList<CustomerProduct> products;


    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }


    public String getShopArabicName() {
        return shopArabicName;
    }

    public void setShopArabicName(String shopArabicName) {
        this.shopArabicName = shopArabicName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }


    public String getShopMail() {
        return shopMail;
    }

    public void setShopMail(String shopMail) {
        this.shopMail = shopMail;
    }

    public String getShopMobile() {
        return shopMobile;
    }

    public void setShopMobile(String shopMobile) {
        this.shopMobile = shopMobile;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }


    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public ArrayList<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(ArrayList<Invoice> invoices) {
        this.invoices = invoices;
    }

    public ArrayList<CustomerProduct> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<CustomerProduct> products) {
        this.products = products;
    }

    public boolean isVisit() {
        return isVisit;
    }

    public void setVisit(boolean visit) {
        isVisit = visit;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public float getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(float productDiscount) {
        this.productDiscount = productDiscount;
    }

    public float getCollectionDiscount() {
        return collectionDiscount;
    }

    public void setCollectionDiscount(float collectionDiscount) {
        this.collectionDiscount = collectionDiscount;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return getShopName();
    }
}
