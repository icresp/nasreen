package com.vansale.icresp.nasreen.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.vansale.icresp.nasreen.model.Banks;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.CartItemCode;
import com.vansale.icresp.nasreen.model.CustomerProduct;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.NoSale;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Product;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.model.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.vansale.icresp.nasreen.config.AmountCalculator.getNetValueFromDiscountedAmount;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getSalePrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getTaxPrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getWithoutTaxPrice;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_ANY_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_CUSTOMER_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_NO_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_QUOTATION_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RECEIPT_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RETURN_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.Generic.dbDateFormat;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;


/**
 * Created by mentor on 24/11/17.
 */

public class MyDatabase extends SQLiteOpenHelper {


    private static final String TAG = "MyDatabase";


    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "nasreen_db";
    //tbl_customer table name
    private static final String TABLE_CUSTOMER = "tbl_customer";
    private static final String TABLE_CUSTOMER_PRODUCTS = "tbl_customer_products";
    private static final String TABLE_CUSTOMER_VISIT = "tbl_customer_visit";
    private static final String TABLE_RECEIPT = "tbl_receipts";
    private static final String TABLE_RECEIPT_TRANSACTION = "tbl_receipt_transactions";
    private static final String TABLE_OPENING_BALANCE_TRANSACTION = "tbl_opening_transactions";

    private static final String TABLE_STOCK = "tbl_stock";
    private static final String TABLE_SALE_CUSTOMER = "tbl_sales_customer";
    private static final String TABLE_SALE_PRODUCTS = "tbl_sales_products";

    private static final String TABLE_QUOTATION_CUSTOMER = "tbl_quotation_customer";
    private static final String TABLE_QUOTATION_PRODUCTS = "tbl_quotation_products";

    private static final String TABLE_BANKS = "tbl_banks";

    /**
     * columns ids
     */

    private static final String WHERE_CLAUSE = " =?";
//     pk

    private static final String COL_ID = "id_int";
    private static final String COL_FK_CUSTOMER_ID = "fk_customer_int";
    private static final String COL_FK_SALE_ID = "fk_sale_id_int";
    private static final String COL_FK_INVOICE_COL_ID = "fk_invoice_no_txt";
    private static final String COL_CREATED_AT = "created_at_date";


    //    customer visit status table
    private static final String COL_VISIT_SALE = "sale_status_int";
    private static final String COL_VISIT_QUOTATION = "quotation_status_int";
    private static final String COL_VISIT_RETURN = "return_status_int";
    private static final String COL_VISIT_RECEIPT = "receipt_status_int";
    private static final String COL_VISIT_NO_SALE = "nosale_status_int";
    private static final String COL_VISIT_NO_SALE_REASON = "reason_status_txt";

    //    Product table
    private static final String COL_PRODUCT_ID = "product_id_int";
    private static final String COL_PRODUCT_NAME = "product_name_txt";
    private static final String COL_PRODUCT_ARABIC_NAME = "product_arab_name_txt";
    private static final String COL_PRODUCT_CODE = "product_code_txt";
    private static final String COL_ARTICLE_NUMBER = "product_article_no";
    private static final String COL_PRODUCT_BRAND = "product_brand_txt";
    private static final String COL_PRODUCT_TYPE = "product_type_txt";
    private static final String COL_PRODUCT_MRP = "product_mrp_real";
    private static final String COL_PRODUCT_WHOLESALE = "product_wholesale_real";
    private static final String COL_PRODUCT_COST = "product_cost_real";
    private static final String COL_PRODUCT_TAX = "product_tax_real";
    private static final String COL_PRODUCT_UNIT = "product_unit";

    private static final String COL_PRODUCT_PEACE_PER_CART = "product_cart_quantity_int";
    private static final String COL_PRODUCT_QUANTITY = "product_quantity_int";

    //       Customer  columns
    private static final String COL_CUSTOMER_ID = "customer_id_int";
    private static final String COL_CUSTOMER_NAME = "customer_name_txt";
    private static final String COL_CUSTOMER_NAME_ARABIC = "customer_name_arab_txt";
    private static final String COL_CUSTOMER_CODE = "customer_code_txt";
    private static final String COL_CUSTOMER_VAT = "customer_vat_txt";
    private static final String COL_CUSTOMER_MAIL = "customer_mail_txt";
    private static final String COL_CUSTOMER_MOBILE = "customer_mobile_txt";
    private static final String COL_CUSTOMER_ADDRESS = "customer_address_txt";
    private static final String COL_CUSTOMER_CREDIT = "customer_credit_real";
    private static final String COL_CUSTOMER_DEBIT = "customer_debit_real";
    private static final String COL_CUSTOMER_PRODUCT_DISC_PERC = "customer_product_disc_perc_real";
    private static final String COL_CUSTOMER_COLLECTION_DISC_PERC = "customer_collection_disc_perc_real";
    private static final String COL_CUSTOMER_CREDITLIMIT = "customer_creditlimit_real";
    private static final String COL_ROUTE_CODE = "customer_route_code_txt";
    private static final String COL_CUSTOMER_OPENING_BALANCE = "opening_balance_real";
    private static final String COL_CUSTOMER_VISIT_STATUS = "customer_close_status_int";
    private static final String COL_CUSTOMER_REGISTERED_STATUS = "customer_register_status_int";
    private static final String COL_CUSTOMER_TYPE = "customer_type";

    private static final String COL_CUSTOMER_COST = "customer_price_real";


    //       Receipt  columns

    private static final String COL_INVOICE_ID = "invoice_id_txt";
    private static final String COL_INVOICE_NO = "invoice_no_txt";
    private static final String COL_RECEIPT_BALANCE = "receipt_balance_real";
    private static final String COL_RECEIPT_TOTAL = "receipt_total_real";
    //  Receipt  transaction  columns
    private static final String COL_RECEIPT_NO = "receipt_no_txt";
    private static final String COL_RECEIVABLE_AMOUNT = "receipt_receivable_real";

    private static final String COL_TRANSACTION_TYPE = "trans_type_txt";
    private static final String COL_CHEQUE_ISSUE_DATE = "trans_cheque_issue_txt";
    private static final String COL_CHEQUE_CLEAR_DATE = "trans_cheque_clear_txt";
    private static final String COL_CHEQUE_NUMBER = "cheque_number_txt";
    private static final String COL_BANK_ID = "bank_id";
    private static final String COL_BANK_NAME = "bank_name_txt";
    private static final String COL_TRANSACTION_REMARK = "remark_txt";
    private static final String COL_BALANCE_AMOUNT = "trans_balance_real";


    //    TABLE_SALE_CUSTOMER  columns

    private static final String COL_INVOICE_CODE = "invoice_no_txt";
    private static final String COL_SALE_TYPE = "sale_type_txt";
    private static final String COL_SALE_TOTAL = "sale_total_real";
    private static final String COL_SALE_PAID = "sale_paid_real";
    private static final String COL_SALE_PO = "sale_po_txt";
    private static final String COL_SALE_DISCOUNT = "sale_discount_real";
    private static final String COL_SALE_COLLECTION_DISCOUNT = "sale_collection_discount_real";
    private static final String COL_PRODUCT_DISC_PERC = "product_disc_perce_real";
    private static final String COL_SALE_LATITUDE = "sale_latitude";
    private static final String COL_SALE_LONGITUDE = "sale_longitude";


    //    TABLE_SALE_PRODUCTS  columns
    private static final String COL_PRODUCT_PRICE = "prod_price_real";
    private static final String COL_SALE_PRODUCT_QUANTITY = "sale_piece_quantity_int";


    private static final String COL_SALE_PRODUCT_ORDER_TYPE = "sale_order_type_txt";
    private static final String COL_SALE_ORDER_TYPE_QUANTITY = "order_type_quantity_int";


    private Context context;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private static String getDateTime() {
        Date date = new Date();
        return dbDateFormat.format(date);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_STOCK = "CREATE TABLE IF NOT EXISTS " + TABLE_STOCK + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_PRODUCT_ID + " INTEGER NOT NULL UNIQUE ," + COL_PRODUCT_NAME + " TEXT NOT NULL ," + COL_PRODUCT_ARABIC_NAME + " TEXT,"+ COL_ARTICLE_NUMBER + " TEXT ," + COL_PRODUCT_CODE + " TEXT NOT NULL ," + COL_PRODUCT_BRAND + " TEXT NOT NULL ," + COL_PRODUCT_TYPE + " TEXT NOT NULL ," + COL_PRODUCT_MRP + " REAL NOT NULL ," + COL_PRODUCT_WHOLESALE + " REAL NOT NULL ," + COL_PRODUCT_COST + " REAL NOT NULL ," + COL_PRODUCT_TAX + " REAL NOT NULL ," + COL_PRODUCT_PEACE_PER_CART + " INTEGER NOT NULL," + COL_PRODUCT_QUANTITY + " INTEGER NOT NULL )";

        String CREATE_CUSTOMER = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_CUSTOMER_ID + " INTEGER NOT NULL UNIQUE ," + COL_CUSTOMER_NAME + " TEXT NOT NULL ," + COL_CUSTOMER_TYPE + " TEXT ," + COL_CUSTOMER_NAME_ARABIC + " TEXT DEFAULT ''," + COL_CUSTOMER_CODE + " TEXT NOT NULL ," + COL_CUSTOMER_VAT + " TEXT NOT NULL ," + COL_CUSTOMER_MAIL + " TEXT NOT NULL ," + COL_CUSTOMER_MOBILE + " TEXT NOT NULL ," + COL_CUSTOMER_ADDRESS + " TEXT NOT NULL ," + COL_ROUTE_CODE + " TEXT NOT NULL ," + COL_CUSTOMER_CREDIT + " REAL NOT NULL ," + COL_CUSTOMER_DEBIT + " REAL NOT NULL DEFAULT 0 ," + COL_CUSTOMER_CREDITLIMIT + " REAL NOT NULL DEFAULT 0 ," + COL_CUSTOMER_OPENING_BALANCE + " REAL NOT NULL DEFAULT 0 ," + COL_CUSTOMER_PRODUCT_DISC_PERC + " REAL NOT NULL DEFAULT 0 ,"+ COL_CUSTOMER_COLLECTION_DISC_PERC + " REAL NOT NULL DEFAULT 0 ," + COL_CUSTOMER_VISIT_STATUS + " flag INTEGER DEFAULT 0, " + COL_CUSTOMER_REGISTERED_STATUS + " flag INTEGER DEFAULT 0);";

        String CREATE_CUSTOMER_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_PRODUCTS + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_PRODUCT_ID + " INTEGER NOT NULL ," + COL_CUSTOMER_COST + " REAL NOT NULL DEFAULT 0 ," + COL_FK_CUSTOMER_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_CUSTOMER_ID + ") REFERENCES " + TABLE_CUSTOMER + " (" + COL_CUSTOMER_ID + ") ON DELETE CASCADE);";


        String CREATE_CUSTOMER_STATUS = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_VISIT + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_VISIT_SALE + " flag INTEGER DEFAULT 0 ," + COL_VISIT_QUOTATION + " flag INTEGER DEFAULT 0 ," + COL_VISIT_RETURN + " flag INTEGER DEFAULT 0 ," + COL_VISIT_RECEIPT + " flag INTEGER DEFAULT 0 ," + COL_VISIT_NO_SALE + " flag INTEGER DEFAULT 0 ," + COL_VISIT_NO_SALE_REASON + " TEXT NOT NULL ," + COL_SALE_LATITUDE + " TEXT ," + COL_SALE_LONGITUDE + " TEXT ," + COL_FK_CUSTOMER_ID + " INTEGER NOT NULL UNIQUE, FOREIGN KEY (" + COL_FK_CUSTOMER_ID + ") REFERENCES " + TABLE_CUSTOMER + " (" + COL_CUSTOMER_ID + ") ON DELETE CASCADE);";

        String CREATE_RECEIPT = "CREATE TABLE IF NOT EXISTS " + TABLE_RECEIPT + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_INVOICE_ID + " TEXT NOT NULL UNIQUE ," + COL_INVOICE_NO + " TEXT NOT NULL UNIQUE ," + COL_RECEIPT_BALANCE + " REAL NOT NULL ," + COL_RECEIPT_TOTAL + " REAL NOT NULL," + COL_CREATED_AT + "  DATETIME DEFAULT CURRENT_TIMESTAMP," + COL_FK_CUSTOMER_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_CUSTOMER_ID + ") REFERENCES " + TABLE_CUSTOMER + " (" + COL_CUSTOMER_ID + ") ON DELETE CASCADE);";


        String CREATE_TRANSACTIONS = "CREATE TABLE IF NOT EXISTS " + TABLE_RECEIPT_TRANSACTION + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_RECEIPT_NO + " TEXT NOT NULL," + COL_RECEIVABLE_AMOUNT + " REAL NOT NULL ," + COL_INVOICE_NO + " TEXT NOT NULL ," + COL_RECEIPT_BALANCE + " REAL NOT NULL ," + COL_TRANSACTION_TYPE + " TEXT NOT NULL ," + COL_CHEQUE_NUMBER + " TEXT DEFAULT ''," + COL_BANK_NAME + " TEXT DEFAULT ''," + COL_TRANSACTION_REMARK + " TEXT DEFAULT ''," + COL_CHEQUE_ISSUE_DATE + " TEXT DEFAULT ''," + COL_CHEQUE_CLEAR_DATE + " TEXT DEFAULT ''," + COL_CREATED_AT + "  DATETIME DEFAULT CURRENT_TIMESTAMP," + COL_FK_INVOICE_COL_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_INVOICE_COL_ID + ") REFERENCES " + TABLE_RECEIPT + " (" + COL_ID + ") ON DELETE CASCADE);";

        String CREATE_OPENING_BALANCE_TRANSACTIONS = "CREATE TABLE IF NOT EXISTS " + TABLE_OPENING_BALANCE_TRANSACTION + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_RECEIPT_NO + " TEXT NOT NULL UNIQUE ," + COL_BALANCE_AMOUNT + " REAL NOT NULL ," + COL_RECEIVABLE_AMOUNT + " REAL NOT NULL ," + COL_TRANSACTION_TYPE + " TEXT NOT NULL ," + COL_CHEQUE_NUMBER + " TEXT DEFAULT ''," + COL_BANK_NAME + " TEXT DEFAULT ''," + COL_TRANSACTION_REMARK + " TEXT DEFAULT ''," + COL_CHEQUE_ISSUE_DATE + " TEXT DEFAULT ''," + COL_CHEQUE_CLEAR_DATE + " TEXT DEFAULT ''," + COL_CREATED_AT + "  DATETIME DEFAULT CURRENT_TIMESTAMP," + COL_FK_CUSTOMER_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_CUSTOMER_ID + ") REFERENCES " + TABLE_CUSTOMER + " (" + COL_CUSTOMER_ID + ") ON DELETE CASCADE);";


        String CREATE_SALE_CUSTOMER = "CREATE TABLE IF NOT EXISTS " + TABLE_SALE_CUSTOMER + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_CUSTOMER_ID + " INTEGER NOT NULL ," + COL_SALE_TYPE + " TEXT NOT NULL ," + COL_SALE_TOTAL + " REAL NOT NULL ,"+COL_SALE_COLLECTION_DISCOUNT + " REAL NOT NULL DEFAULT 0," + COL_SALE_PAID + " REAL NOT NULL," + COL_SALE_DISCOUNT + " REAL NOT NULL DEFAULT 0 ," + COL_INVOICE_CODE + " TEXT NOT NULL UNIQUE ," + COL_SALE_PO + " TEXT DEFAULT ''," + COL_CREATED_AT + "  DATETIME DEFAULT CURRENT_TIMESTAMP  ," + COL_SALE_LATITUDE + " TEXT," + COL_SALE_LONGITUDE + " TEXT);";
        String CREATE_SALE_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_SALE_PRODUCTS + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_PRODUCT_ID + " INTEGER NOT NULL ," + COL_PRODUCT_NAME + " TEXT NOT NULL ," + COL_ARTICLE_NUMBER + " TEXT ," + COL_PRODUCT_UNIT + " TEXT ," + COL_PRODUCT_ARABIC_NAME + " TEXT DEFAULT ''," + COL_PRODUCT_CODE + " TEXT NOT NULL ," + COL_PRODUCT_PRICE + " REAL NOT NULL ," + COL_SALE_PRODUCT_QUANTITY + " INTEGER NOT NULL ," + COL_PRODUCT_TAX + " REAL NOT NULL DEFAULT 0,"+  COL_PRODUCT_DISC_PERC + " REAL NOT NULL DEFAULT 0," + COL_SALE_PRODUCT_ORDER_TYPE + " TEXT NOT NULL," + COL_SALE_ORDER_TYPE_QUANTITY + " INTEGER NOT NULL," + COL_PRODUCT_PEACE_PER_CART + " INTEGER NOT NULL," + COL_SALE_TOTAL + " REAL NOT NULL," + COL_FK_SALE_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_SALE_ID + ") REFERENCES " + TABLE_SALE_CUSTOMER + " (" + COL_ID + ") ON DELETE CASCADE);";

        String CREATE_QUOTATION_CUSTOMER = "CREATE TABLE IF NOT EXISTS " + TABLE_QUOTATION_CUSTOMER + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_CUSTOMER_ID + " INTEGER NOT NULL ," + COL_SALE_TYPE + " TEXT NOT NULL ," + COL_SALE_TOTAL + " REAL NOT NULL," + COL_CREATED_AT + "  DATETIME DEFAULT CURRENT_TIMESTAMP)";
        String CREATE_QUOTATION_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_QUOTATION_PRODUCTS + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_PRODUCT_ID + " INTEGER NOT NULL ," + COL_PRODUCT_PRICE + " REAL NOT NULL ," + COL_SALE_PRODUCT_QUANTITY + " INTEGER NOT NULL," + COL_PRODUCT_TAX + " REAL NOT NULL DEFAULT 0,"+  COL_PRODUCT_DISC_PERC + " REAL NOT NULL DEFAULT 0," + COL_SALE_TOTAL + " REAL NOT NULL," + COL_FK_SALE_ID + " INTEGER NOT NULL, FOREIGN KEY (" + COL_FK_SALE_ID + ") REFERENCES " + TABLE_QUOTATION_CUSTOMER + " (" + COL_ID + ") ON DELETE CASCADE);";

        String CREATE_BANK_DETAILS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_BANKS + "(" + COL_BANK_ID +" INTEGER NOT NULL ,"+ COL_BANK_NAME + " TEXT NOT NULL )";

        db.execSQL(CREATE_STOCK);

        db.execSQL(CREATE_CUSTOMER);
        db.execSQL(CREATE_CUSTOMER_PRODUCTS);
        db.execSQL(CREATE_CUSTOMER_STATUS);
        db.execSQL(CREATE_RECEIPT);
        db.execSQL(CREATE_TRANSACTIONS);
        db.execSQL(CREATE_OPENING_BALANCE_TRANSACTIONS);

        db.execSQL(CREATE_SALE_CUSTOMER);
        db.execSQL(CREATE_SALE_PRODUCTS);
        db.execSQL(CREATE_BANK_DETAILS_TABLE);
        db.execSQL(CREATE_QUOTATION_CUSTOMER);
        db.execSQL(CREATE_QUOTATION_PRODUCTS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCK);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_VISIT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPT);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPT_TRANSACTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPENING_BALANCE_TRANSACTION);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALE_CUSTOMER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALE_PRODUCTS);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTATION_CUSTOMER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTATION_PRODUCTS);
        onCreate(db);



       /* if (oldVersion < 2) {
            db.execSQL("ALTER TABLE  " + TABLE_SALE_CUSTOMER + " ADD COLUMN " + COL_SALE_COLLECTION_DISCOUNT + " REAL NOT NULL DEFAULT 0");
        }
*/

    /*
        // You will not need to modify this unless you need to do some android specific things.
        // When upgrading the database, all you need to do is add a file to the assets folder and name it:
        // from_1_to_2.sql with the version that you are upgrading to as the last version.

        try {
            for (int i = oldVersion; i < newVersion; ++i) {

            }
        } catch (Exception exception) {
            Log.e(TAG, "Exception running upgrade script:", exception);
        }
*/
//Added new column to customer table
/*
        if (oldVersion < 2) {

            String CREATE_CUSTOMER_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_PRODUCTS + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_PRODUCT_ID + " INTEGER NOT NULL ," + COL_CUSTOMER_COST + " REAL NOT NULL DEFAULT 0 ," + COL_FK_CUSTOMER_ID + " INTEGER NOT NULL UNIQUE, FOREIGN KEY (" + COL_FK_CUSTOMER_ID + ") REFERENCES " + TABLE_CUSTOMER + " (" + COL_CUSTOMER_ID + ") ON DELETE CASCADE);";

            db.execSQL(CREATE_CUSTOMER_PRODUCTS);
            db.execSQL("ALTER TABLE  " + TABLE_SALE_CUSTOMER + " ADD COLUMN " + COL_SALE_DISCOUNT + " REAL NOT NULL DEFAULT 0");
            db.execSQL("ALTER TABLE  " + TABLE_SALE_CUSTOMER + " ADD COLUMN " + COL_SALE_PO + " TEXT DEFAULT ''");


        } else {

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCK);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_PRODUCTS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_VISIT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPT);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPT_TRANSACTION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPENING_BALANCE_TRANSACTION);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALE_CUSTOMER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALE_PRODUCTS);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTATION_CUSTOMER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTATION_PRODUCTS);
            onCreate(db);
        }
*/

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        final String FK_ON = "=ON;";
        try {

            if (!db.isReadOnly()) {
                // Enable foreign key constraints

                db.execSQL("PRAGMA foreign_keys" + FK_ON);


            }
        } catch (SQLiteException e) {

            e.getMessage();

        }
    }

    /**
     * stock functions
     */
    //    insert stock
    public boolean insertStock(Product product) {

        boolean isExist = isExistProduct(product.getProductId());  // check stock in table

        if (isExist)
            return false;


        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_PRODUCT_ID, product.getProductId());
            values.put(COL_PRODUCT_NAME, product.getProductName());
            values.put(COL_PRODUCT_ARABIC_NAME, product.getArabicName());
            values.put(COL_ARTICLE_NUMBER, product.getArticleCode());
            values.put(COL_PRODUCT_CODE, product.getProductCode());
            values.put(COL_PRODUCT_BRAND, product.getBrandName());
            values.put(COL_PRODUCT_TYPE, product.getProductType());
            values.put(COL_PRODUCT_MRP, product.getRetailPrice());
            values.put(COL_PRODUCT_WHOLESALE, product.getWholeSalePrice());
            values.put(COL_PRODUCT_COST, product.getCost());
            values.put(COL_PRODUCT_TAX, product.getTax());
            values.put(COL_PRODUCT_PEACE_PER_CART, product.getPiecepercart());
            values.put(COL_PRODUCT_QUANTITY, product.getStockQuantity());

            // Inserting Row
            long l = db.insert(TABLE_STOCK, null, values);

            db.close(); // Closing database connection

            return l != -1;

        } catch (SQLiteException e) {

            Log.v(TAG, "insertStock  Exception  " + e.getMessage());
            return false;
        }
    }

    //    update updateStock
    public boolean updateStock(CartItem cartItem, int type) {

        boolean isExist = isExistProduct(cartItem.getProductId());  // check stock in table

        if (!isExist) {
            cartItem.setStockQuantity(cartItem.getReturnQuantity());
            insertStock(cartItem);
            return false;

        }

        Product p = getStockProduct(cartItem.getProductId());

        if (p == null)
            return false;

        int qty = p.getStockQuantity();

        switch (type) {
            case REQ_SALE_TYPE:
                qty = p.getStockQuantity() - cartItem.getPieceQuantity();
                break;
            case REQ_RETURN_TYPE:
                qty = p.getStockQuantity() + cartItem.getReturnQuantity();
                break;
        }

        Log.v(TAG, "updateStock  getStockQuantity  " + p.getStockQuantity() + "\t" + cartItem.getStockQuantity() + "\t" + cartItem.getReturnQuantity());

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_PRODUCT_QUANTITY, qty);


            int result = db.update(TABLE_STOCK, values, COL_PRODUCT_ID + " =?", new String[]{String.valueOf(cartItem.getProductId())});

            db.close();

            return result != -1;
        } catch (SQLiteException e) {
            Log.v(TAG, "updateStock  Exception  " + e.getMessage());
            return false;
        }

    }

    //    get getAllStock list
    public ArrayList<CartItem> getAllStock() {

        final ArrayList<CartItem> cartItems = new ArrayList<>();
        try {
            String sql = "SELECT * FROM " + TABLE_STOCK;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    CartItem c = new CartItem();
                    c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    c.setProductName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME)));
                    c.setArabicName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ARABIC_NAME)));
                    c.setProductCode(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CODE)));
                    c.setBrandName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND)));

                    c.setArticleCode(cursor.getString(cursor.getColumnIndex(COL_ARTICLE_NUMBER)));

                    c.setProductType(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TYPE)));
                    c.setRetailPrice(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                    c.setWholeSalePrice(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_WHOLESALE)));
                    c.setCost(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_COST)));
                    c.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));

                    c.setPiecepercart(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_PEACE_PER_CART)));

                    c.setStockQuantity(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY)));

                    cartItems.add(c);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();

        } catch (SQLiteException e) {

            Log.v(TAG, "getAllStock  Exception  " + e.getMessage());
        }

        return cartItems;
    }

    //    get getAllStock list
    public ArrayList<CartItemCode> getAllStockCode() {

        final ArrayList<CartItemCode> cartItems = new ArrayList<>();
        try {
            String sql = "SELECT * FROM " + TABLE_STOCK;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    CartItemCode c = new CartItemCode();
                    c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    c.setProductName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME)));
                    c.setArabicName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ARABIC_NAME)));
                    c.setProductCode(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CODE)));
                    c.setBrandName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND)));
                    c.setProductType(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TYPE)));
                    c.setRetailPrice(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                    c.setWholeSalePrice(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_WHOLESALE)));
                    c.setCost(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_COST)));
                    c.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));
                    c.setPiecepercart(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_PEACE_PER_CART)));
                    c.setStockQuantity(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY)));

                    cartItems.add(c);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();

        } catch (SQLiteException e) {

            Log.v(TAG, "getAllStock  Exception  " + e.getMessage());
        }

        return cartItems;
    }




    //    get getProductDetails corresponding id
    public CartItem getProductDetails(int productId) {


        CartItem c = new CartItem();
        try {
//            String sql = "SELECT * FROM " + TABLE_STOCK;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_STOCK + " WHERE " + COL_PRODUCT_ID + WHERE_CLAUSE, new String[]{String.valueOf(productId)});


            if (cursor != null) {
                cursor.moveToFirst();

                c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                c.setProductName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME)));
                c.setArabicName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ARABIC_NAME)));
                c.setProductCode(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CODE)));
                c.setBrandName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND)));

                c.setProductType(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TYPE)));
                c.setRetailPrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                c.setWholeSalePrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_WHOLESALE)));
                c.setCost(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_COST)));
                c.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));

                c.setPiecepercart(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_PEACE_PER_CART)));

                c.setStockQuantity(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY)));


                cursor.close();
                db.close();
            }


        } catch (SQLiteException e) {

            Log.v(TAG, "getProductDetails  Exception  " + e.getMessage());
        }

        return c;
    }

    //    get products corresponding id
    private Product getStockProduct(int id) {

        Product p = new Product();
        try {
            String sql = "SELECT * FROM " + TABLE_STOCK + " WHERE " + COL_PRODUCT_ID + " = '" + id + "'";


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


//            Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(customerId)});


            if (cursor.moveToFirst())
                do {

                    p.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    p.setProductName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME)));
                    p.setArabicName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ARABIC_NAME)));
                    p.setProductCode(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CODE)));
                    p.setBrandName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND)));
                    p.setProductType(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TYPE)));

                    p.setRetailPrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_MRP)));

                    p.setWholeSalePrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_WHOLESALE)));
                    p.setCost(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_COST)));
                    p.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));
                    p.setPiecepercart(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_PEACE_PER_CART)));

                    p.setStockQuantity(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY)));


                    cursor.close();
                    db.close();
                    return p;

                } while (cursor.moveToNext());


        } catch (SQLiteException e) {
            Log.v(TAG, "getStockProduct  Exception  " + e.getMessage());

        }

        return p;
    }

    //   Get amount TABLE_STOCK
    public float getStockAmount() {


        float stockAmount = 0;


        try {
            String sql = "SELECT " + COL_PRODUCT_QUANTITY + "," + COL_PRODUCT_MRP + " FROM " + TABLE_STOCK;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    int qty = cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY));
                    float f = cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_MRP));
                    stockAmount += qty * f;


                } while (cursor.moveToNext());

            cursor.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getStockAmount  Exception  " + e.getMessage());

        }

        Log.v(TAG, "getStockAmount  stockAmount  " + stockAmount);
        return stockAmount;
    }

    //   Get stock Quantity TABLE_STOCK
    public long getStockQuantity() {


        long stockQuantity = 0;


        try {
            String sql = "SELECT " + COL_PRODUCT_QUANTITY + " FROM " + TABLE_STOCK;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    int qty = cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_QUANTITY));
                    stockQuantity += qty;

                } while (cursor.moveToNext());

            cursor.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getStockQuantity  Exception  " + e.getMessage());

        }


        return stockQuantity;
    }


//      Count Row From to SQLite  "TABLE_STOCK" Table

    //      check isavailable stock
//     Count Row From to SQLite  "TABLE_STOCK" Table
    public boolean isExistProducts() {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_STOCK, new String[]{});

            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            e.fillInStackTrace();
        }

        return count != 0;


    }

    private boolean isExistProduct(int productId) {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_STOCK + " WHERE " + COL_PRODUCT_ID + WHERE_CLAUSE, new String[]{String.valueOf(productId)});

//            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_CUSTOMER , new String[]{});

            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            e.fillInStackTrace();
        }

        return count != 0;


    }

    /**
     * customer functions
     */

    //    insert Registered customers
    public boolean insertRegisteredCustomer(Shop shop) {

        boolean isExist = isExistCustomer(shop.getShopId());  // check customer in table

        if (isExist)
            return false;

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_ID, shop.getShopId());
            values.put(COL_CUSTOMER_NAME, shop.getShopName());
            values.put(COL_CUSTOMER_NAME_ARABIC, shop.getShopArabicName());
            values.put(COL_CUSTOMER_VAT, shop.getVatNumber());
            values.put(COL_CUSTOMER_CODE, shop.getShopCode());
            values.put(COL_CUSTOMER_TYPE, shop.getTypeId());
            values.put(COL_CUSTOMER_MAIL, shop.getShopMail());
            values.put(COL_CUSTOMER_MOBILE, shop.getShopMobile());
            values.put(COL_CUSTOMER_ADDRESS, shop.getShopAddress());
            values.put(COL_CUSTOMER_CREDITLIMIT, shop.getCreditLimit());
            values.put(COL_CUSTOMER_CREDIT, shop.getCredit());
            values.put(COL_CUSTOMER_DEBIT, shop.getDebit());
            values.put(COL_ROUTE_CODE, shop.getRouteCode());
            values.put(COL_CUSTOMER_OPENING_BALANCE, shop.getOpeningBalance());
            values.put(COL_CUSTOMER_PRODUCT_DISC_PERC, shop.getProductDiscount());
            values.put(COL_CUSTOMER_COLLECTION_DISC_PERC, shop.getCollectionDiscount());
            values.put(COL_CUSTOMER_VISIT_STATUS, false);
            values.put(COL_CUSTOMER_REGISTERED_STATUS, true);

            // Inserting Row
            long l = db.insert(TABLE_CUSTOMER, null, values);

            db.close(); // Closing database connection


            if (l == -1) {
                return false;
            } else {

                insertVisitStatus(shop.getShopId());
                insertInvoices(shop.getShopId(), shop.getInvoices());
                insertCustomerProducts(shop.getProducts());

                return true;

            }

        } catch (SQLiteException e) {

            Log.v(TAG, "insertCustomer  Exception  " + e.getMessage());
            return false;
        }
    }

    //    insert Un Registered customers
    public boolean insertUnRegisteredCustomer(Shop shop) {

        boolean isExist = isExistCustomer(shop.getShopId());  // check customer in table

        if (isExist)
            return false;

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_ID, shop.getShopId());
            values.put(COL_CUSTOMER_NAME, shop.getShopName());
            values.put(COL_CUSTOMER_NAME_ARABIC, shop.getShopArabicName());
            values.put(COL_CUSTOMER_VAT, shop.getVatNumber());
            values.put(COL_CUSTOMER_CODE, shop.getShopCode());
            values.put(COL_CUSTOMER_TYPE, shop.getTypeId());
            values.put(COL_CUSTOMER_MAIL, shop.getShopMail());
            values.put(COL_CUSTOMER_MOBILE, shop.getShopMobile());
            values.put(COL_CUSTOMER_ADDRESS, shop.getShopAddress());
            values.put(COL_CUSTOMER_CREDITLIMIT, shop.getCreditLimit());
            values.put(COL_CUSTOMER_CREDIT, shop.getCredit());
            values.put(COL_CUSTOMER_DEBIT, shop.getDebit());
            values.put(COL_ROUTE_CODE, shop.getRouteCode());
            values.put(COL_CUSTOMER_OPENING_BALANCE, shop.getOpeningBalance());
            values.put(COL_CUSTOMER_PRODUCT_DISC_PERC, shop.getProductDiscount());
            values.put(COL_CUSTOMER_COLLECTION_DISC_PERC, shop.getCollectionDiscount());

            values.put(COL_CUSTOMER_VISIT_STATUS, true);
            values.put(COL_CUSTOMER_REGISTERED_STATUS, false);

            // Inserting Row
            long l = db.insert(TABLE_CUSTOMER, null, values);

            db.close(); // Closing database connection


            if (l == -1) {
                return false;
            } else {

                insertVisitStatus(shop.getShopId());
                insertInvoices(shop.getShopId(), shop.getInvoices());
                insertCustomerProducts(shop.getProducts());

                return true;

            }

        } catch (SQLiteException e) {

            printLog(TAG, "insertCustomer  Exception  " + e.getMessage());
            return false;
        }
    }

    //    Get un registered Customer list TABLE_CUSTOMER
    public ArrayList<Shop> getUnRegisteredCustomers() {

        ArrayList<Shop> list = new ArrayList<>();
        try {

            String sql = "SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_REGISTERED_STATUS + " = '" + "0" + "'";


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {
                    Shop s = new Shop();


                    s.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    s.setShopId(cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_ID)));
                    s.setShopName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME)));
                    s.setShopArabicName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME_ARABIC)));

                    s.setVatNumber(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_VAT)));

                    s.setShopCode(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_CODE)));
                    s.setTypeId(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_TYPE)));

                    s.setShopMail(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MAIL)));

                    s.setShopMobile(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MOBILE)));

                    s.setShopAddress(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_ADDRESS)));

                    s.setCredit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_CREDIT)));
                    s.setCreditLimit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_CREDITLIMIT)));
                    s.setDebit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_DEBIT)));
                    s.setRouteCode(cursor.getString(cursor.getColumnIndex(COL_ROUTE_CODE)));

                    s.setOpeningBalance(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_OPENING_BALANCE)));
                    s.setProductDiscount(cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_PRODUCT_DISC_PERC)));
                    s.setCollectionDiscount(cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_COLLECTION_DISC_PERC)));

                    Boolean flag = (cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_VISIT_STATUS)) == 1);
                    s.setVisit(flag);

                    list.add(s);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {
            Log.v(TAG, "getAllCustomers  Exception  " + e.getMessage());

        }

        return list;
    }

    //    Get registered Customer list TABLE_CUSTOMER
    public ArrayList<Shop> getRegisteredCustomers() {

        ArrayList<Shop> list = new ArrayList<>();
        try {

            String sql = "SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_REGISTERED_STATUS + " = '" + "1" + "'";


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {
                    Shop s = new Shop();


                    s.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    s.setShopId(cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_ID)));
                    s.setShopName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME)));
                    s.setShopArabicName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME_ARABIC)));

                    s.setVatNumber(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_VAT)));

                    s.setShopCode(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_CODE)));
                    s.setTypeId(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_TYPE)));

                    s.setShopMail(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MAIL)));

                    s.setShopMobile(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MOBILE)));

                    s.setShopAddress(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_ADDRESS)));

                    s.setCreditLimit(cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_CREDITLIMIT)));
                    s.setCredit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_CREDIT)));
                    s.setCreditLimit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_CREDITLIMIT)));
                    s.setDebit(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_DEBIT)));
                    s.setRouteCode(cursor.getString(cursor.getColumnIndex(COL_ROUTE_CODE)));

                    s.setOpeningBalance(cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_OPENING_BALANCE)));
                    s.setProductDiscount(cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_PRODUCT_DISC_PERC)));
                    s.setCollectionDiscount(cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_COLLECTION_DISC_PERC)));

                    Boolean flag = (cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_VISIT_STATUS)) == 1);
                    s.setVisit(flag);

                    list.add(s);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {
            Log.v(TAG, "getAllCustomers  Exception  " + e.getMessage());

        }

        return list;
    }

    //      check isavailable contact
//      Count Row From to SQLite  "TABLE_CUSTOMER" Table
    private boolean isExistCustomer(int custId) {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + WHERE_CLAUSE, new String[]{String.valueOf(custId)});

//            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_CUSTOMER , new String[]{});

            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            e.fillInStackTrace();
        }

        return count != 0;


    }

    /**
     * customer product
     */

    /*Insert customer product*/
    private void insertCustomerProducts(ArrayList<CustomerProduct> list) {


        if (list == null || list.isEmpty())
            return;

        long l = 0;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            for (CustomerProduct cProduct : list) {

                ContentValues values = new ContentValues();
                values.put(COL_PRODUCT_ID, cProduct.getProductId());
                values.put(COL_CUSTOMER_COST, cProduct.getPrice());
                values.put(COL_FK_CUSTOMER_ID, cProduct.getCustomerId());

                l = db.insert(TABLE_CUSTOMER_PRODUCTS, null, values);

            }
            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            printLog(TAG, "insertCustomerProducts  Exception  " + e.getMessage());
        }

    }


    //   Get customer product balance from customer product table
    public double getCustomerProductPrice(int customerId, int productId) {

        double price = 0;

        try {
//            String sql = "SELECT " + COL_CUSTOMER_COST + " FROM " + TABLE_CUSTOMER_PRODUCTS + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";
            String sql = "SELECT " + COL_CUSTOMER_COST + " FROM " + TABLE_CUSTOMER_PRODUCTS + " WHERE " + COL_FK_CUSTOMER_ID + WHERE_CLAUSE + " AND " + COL_PRODUCT_ID + WHERE_CLAUSE;

            SQLiteDatabase db = this.getReadableDatabase();
//            Cursor cursor = db.rawQuery(sql, null);
            Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(customerId), String.valueOf(productId)});

            if (cursor != null) {
                cursor.moveToFirst();
                price = cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_COST));
                cursor.close();

            }
        } catch (SQLiteException | CursorIndexOutOfBoundsException e) {
            Log.v(TAG, "getCustomerProductPrice  Exception  " + e.getMessage());
        }

        return price;
    }


    public boolean isExistCustomerProduct(int customerId, int productId) {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_CUSTOMER_PRODUCTS + " WHERE " + COL_PRODUCT_ID + WHERE_CLAUSE + " AND " + COL_FK_CUSTOMER_ID + WHERE_CLAUSE, new String[]{String.valueOf(productId), String.valueOf(customerId)});

            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            printLog(TAG, "isExistCustomerProduct Exception  " + e.getMessage());
        }

        printLog(TAG, "isExistCustomerProduct count  " + count);
        return count != 0;


    }


    /**
     * credit and debit
     */


    //    update credit balance
    public boolean updateCreditBal(int customerId, double credit) {

        boolean isExist = isExistCustomer(customerId);  // check customer in table


        if (!isExist)
            return false;

        double existCredit = getCreditBalance(customerId);

        double receive = existCredit + credit;

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_CREDIT, receive);
            int result = db.update(TABLE_CUSTOMER, values, COL_CUSTOMER_ID + " =?", new String[]{String.valueOf(customerId)});

            db.close();
            return result != -1;
        } catch (SQLiteException e) {
            printLog(TAG, "updateCreditBal  Exception  " + e.getMessage());
            return false;
        }

    }

    //    update debit balance
    public boolean updateDebitBal(int customerId, double debit) {


        boolean isExist = isExistCustomer(customerId);  // check customer in table

        if (!isExist)
            return false;

        double existDebit = getDebitBalance(customerId);

        double total = existDebit + debit;

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_DEBIT, total);
            int result = db.update(TABLE_CUSTOMER, values, COL_CUSTOMER_ID + " =?", new String[]{String.valueOf(customerId)});

            db.close();
            return result != -1;
        } catch (SQLiteException e) {
            printLog(TAG, "updateDebitBal  Exception  " + e.getMessage());
            return false;
        }


    }

    //   Get credit balance from customer table
    public double getCreditBalance(int customerId) {

        float credit = 0.0f;


        try {
            String sql = "SELECT " + COL_CUSTOMER_CREDIT + " FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor != null) {
                cursor.moveToFirst();
                credit = cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_CREDIT));

                cursor.close();

            }
        } catch (SQLiteException e) {

            Log.v(TAG, "getCreditBalance  Exception  " + e.getMessage());

        }

        return credit;
    }

    //   Get debit balance from customer table
    public double getDebitBalance(int customerId) {

        double debit = 0;


        try {
            String sql = "SELECT " + COL_CUSTOMER_DEBIT + " FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor != null) {
                cursor.moveToFirst();
                debit = cursor.getFloat(cursor.getColumnIndex(COL_CUSTOMER_DEBIT));

                cursor.close();

            }
        } catch (SQLiteException e) {

            printLog(TAG, "getDebitBalance  Exception  " + e.getMessage());

        }

        return debit;
    }

    /**
     * Opening balance functions
     */


    public boolean insertOpeningTransactionLog(OpeningBalanceTransaction ob) {

        long l = -1;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_RECEIPT_NO, ob.getReceiptNo());
            values.put(COL_BALANCE_AMOUNT, ob.getBalanceAmount());
            values.put(COL_RECEIVABLE_AMOUNT, ob.getReceivedAmount());
            values.put(COL_TRANSACTION_TYPE, ob.getTransactionType());
            values.put(COL_CHEQUE_NUMBER, ob.getChequeNumber());
            values.put(COL_BANK_NAME, ob.getBankName());
            values.put(COL_TRANSACTION_REMARK, ob.getRemark());
            values.put(COL_CHEQUE_ISSUE_DATE, ob.getIssueDate());
            values.put(COL_CHEQUE_CLEAR_DATE, ob.getClearDate());
            values.put(COL_CREATED_AT, TextUtils.isEmpty(ob.getLogDate()) ? getDateTime() : ob.getLogDate());

//            values.put(COL_CREATED_AT, getDateTime());
            values.put(COL_FK_CUSTOMER_ID, ob.getCustomerId());

            l = db.insert(TABLE_OPENING_BALANCE_TRANSACTION, null, values);


            db.close(); // Closing database connection

        } catch (SQLiteException e) {
            Log.d(TAG, "insertOpeningTransactionLog  Exception  " + e.getMessage());
        }

        return l != -1;


    }

    //    update opening balance for customer table
    public boolean updateOpeningBal(int customerId, double newOpeningBalance) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_OPENING_BALANCE, newOpeningBalance);
            int result = db.update(TABLE_CUSTOMER, values, COL_CUSTOMER_ID + " =?", new String[]{String.valueOf(customerId)});
            db.close();


            return result != -1;
        } catch (SQLiteException e) {
            printLog(TAG, "updateOpeningBal  Exception  " + e.getMessage());
            return false;
        }

    }

    //   Get all transaction list from o/s balance table
    public ArrayList<OpeningBalanceTransaction> getOpeningBalancesTransactions() {

        ArrayList<OpeningBalanceTransaction> list = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_OPENING_BALANCE_TRANSACTION;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    OpeningBalanceTransaction obt = new OpeningBalanceTransaction();

                    obt.setTransactionId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    obt.setReceiptNo(cursor.getString(cursor.getColumnIndex(COL_RECEIPT_NO)));
                    obt.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_BALANCE_AMOUNT)));
                    obt.setReceivedAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT)));
                    obt.setTransactionType(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_TYPE)));
                    obt.setChequeNumber(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_NUMBER)));
                    obt.setBankName(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    obt.setRemark(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_REMARK)));
                    obt.setIssueDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_ISSUE_DATE)));
                    obt.setClearDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_CLEAR_DATE)));
                    obt.setLogDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));
                    obt.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_FK_CUSTOMER_ID)));
                    list.add(obt);

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            printLog(TAG, "getOpeningBalancesTransactions  Exception  " + e.getMessage());

        }


        return list;
    }

    //   Get customer transaction list from o/s balance table
    public ArrayList<OpeningBalanceTransaction> getCustomerOpeningBalancesTransactions(int customerId) {

        ArrayList<OpeningBalanceTransaction> list = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_OPENING_BALANCE_TRANSACTION + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    OpeningBalanceTransaction obt = new OpeningBalanceTransaction();

                    obt.setTransactionId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    obt.setReceiptNo(cursor.getString(cursor.getColumnIndex(COL_RECEIPT_NO)));
                    obt.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_BALANCE_AMOUNT)));
                    obt.setReceivedAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT)));
                    obt.setTransactionType(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_TYPE)));
                    obt.setChequeNumber(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_NUMBER)));
                    obt.setBankName(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    obt.setRemark(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_REMARK)));
                    obt.setIssueDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_ISSUE_DATE)));
                    obt.setClearDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_CLEAR_DATE)));
                    obt.setLogDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));
                    obt.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_FK_CUSTOMER_ID)));
                    list.add(obt);

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            printLog(TAG, "getOpeningBalancesTransactions  Exception  " + e.getMessage());

        }


        return list;
    }

    //   Get opening balance from customer table
    public double getCustomerOpeningBalance(int customerId) {


        double openingBalance = 0;
        try {
            String sql = "SELECT " + COL_CUSTOMER_OPENING_BALANCE + " FROM " + TABLE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                openingBalance = cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_OPENING_BALANCE));
                cursor.close();

            }

        } catch (SQLiteException e) {
            printLog(TAG, "getCustomerOpeningBalance  Exception  " + e.getMessage());

        }

        return openingBalance;
    }

    //   Get all received opening balance from o/s balance table
    public double getTotalReceivableOpeningBalance() {

        double receivable = 0;
        try {
            String sql = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_OPENING_BALANCE_TRANSACTION;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {
                    double d = cursor.getDouble(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                    receivable += d;
                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            printLog(TAG, "getReceivableOpeningBalance  Exception  " + e.getMessage());

        }

        return receivable;
    }


    //   Get customer received opening balance from o/s balance table
    public double getCustomerTotalReceivableOpeningBalance(int customerId) {

        double receivable = 0;
        try {
            String sql = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_OPENING_BALANCE_TRANSACTION + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + String.valueOf(customerId) + "'";


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {
                    double d = cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                    receivable += d;
                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getCustomerTotalReceivableOpeningBalance  Exception  " + e.getMessage());

        }

        return receivable;
    }

    /**
     * Invoice functions
     */



    /*Insert invoices*/
    private void insertInvoices(int id, ArrayList<Invoice> list) {


        if (list == null || list.isEmpty())
            return;

        long l = 0;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            for (Invoice invoice : list) {

                ContentValues values = new ContentValues();
                values.put(COL_INVOICE_ID, invoice.getInvoiceId());
                values.put(COL_INVOICE_NO, invoice.getInvoiceNo());
                values.put(COL_RECEIPT_BALANCE, invoice.getBalanceAmount());
                values.put(COL_RECEIPT_TOTAL, invoice.getTotalAmount());
                values.put(COL_CREATED_AT, getDateTime());
                values.put(COL_FK_CUSTOMER_ID, id);

                l = db.insert(TABLE_RECEIPT, null, values);

//                printLog(TAG, "insertInvoices  status  " + l);
            }
            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            Log.d(TAG, "insertInvoices  Exception  " + e.getMessage());
        }

    }

    /*Insert receipt transaction*/

    /*Insert invoices*/
    public boolean insertInvoice(int customerId, Invoice invoice) {

        long l = -1;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_INVOICE_ID, invoice.getInvoiceId());
            values.put(COL_INVOICE_NO, invoice.getInvoiceNo());
            values.put(COL_RECEIPT_BALANCE, invoice.getBalanceAmount());
            values.put(COL_RECEIPT_TOTAL, invoice.getTotalAmount());
            values.put(COL_CREATED_AT, getDateTime());
            values.put(COL_FK_CUSTOMER_ID, customerId);

            l = db.insert(TABLE_RECEIPT, null, values);


            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            Log.d(TAG, "insertInvoice  Exception  " + e.getMessage());
        }

        return l != -1;
    }

    public boolean insertReceipt(int customerId, Receipt receipt) {

        long l = -1;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_INVOICE_ID, receipt.getInvoiceId());
            values.put(COL_INVOICE_NO, receipt.getInvoiceNo());
            values.put(COL_RECEIPT_BALANCE, receipt.getBalanceAmount());
            values.put(COL_RECEIPT_TOTAL, receipt.getTotalAmount());
            values.put(COL_CREATED_AT, getDateTime());
            values.put(COL_FK_CUSTOMER_ID, customerId);

            l = db.insert(TABLE_RECEIPT, null, values);

            Log.d(TAG, "insertTransaction  status  " + l);
            db.close(); // Closing database connection

            receipt.setLocalId(Integer.parseInt(String.valueOf(l)));
            return l != -1 && insertReceiptTransactionLog(receipt);


        } catch (SQLiteException e) {
            Log.d(TAG, "insertTransaction  Exception  " + e.getMessage());
        }

        return l != -1;

    }

    public boolean insertReceiptTransactionLog(Receipt receipt) {

        long l = -1;

        boolean isExist = isExistInvoice(receipt.getInvoiceId());
        if (!isExist || receipt.getReceivedAmount() == 0)
            return false;


        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_RECEIPT_NO, receipt.getReceiptNo());
            values.put(COL_RECEIVABLE_AMOUNT, receipt.getReceivedAmount());
            values.put(COL_CREATED_AT, getDateTime());
            values.put(COL_INVOICE_NO, receipt.getInvoiceNo());
            values.put(COL_RECEIPT_BALANCE, receipt.getBalanceAmount());

            values.put(COL_TRANSACTION_TYPE, receipt.getTransactionType());
            values.put(COL_CHEQUE_NUMBER, receipt.getChequeNumber());
            values.put(COL_BANK_NAME, receipt.getBankName());
            values.put(COL_TRANSACTION_REMARK, receipt.getRemark());
            values.put(COL_CHEQUE_ISSUE_DATE, receipt.getIssueDate());
            values.put(COL_CHEQUE_CLEAR_DATE, receipt.getClearDate());

            values.put(COL_FK_INVOICE_COL_ID, receipt.getLocalId());


            l = db.insert(TABLE_RECEIPT_TRANSACTION, null, values);


            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            printLog(TAG, "insertTransactionLog  Exception  " + e.getMessage());
        }

        return l != -1;


    }

    //   Get Invoice list customer wise TABLE_RECEIPT
    public ArrayList<Invoice> getCustomerWiseInvoices(int customerId) {

        ArrayList<Invoice> list = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_RECEIPT + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    Invoice inv = new Invoice();

                    inv.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    inv.setInvoiceId(cursor.getString(cursor.getColumnIndex(COL_INVOICE_ID)));

                    inv.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));

                    inv.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));
                    inv.setTotalAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_TOTAL)));
                    inv.setTransactions(getTransactions(inv));


                    list.add(inv);

                } while (cursor.moveToNext());

            cursor.close();
//            db.close();

        } catch (SQLiteException e) {

            Log.d(TAG, "getCustomerWiseInvoices  Exception  " + e.getMessage());

        }

        return list;
    }

    //   Get Invoice id wise TABLE_RECEIPT
    private Invoice getIdWiseInvoice(String invoiceId) {

        Invoice inv = new Invoice();


        try {
            String sql = "SELECT * FROM " + TABLE_RECEIPT + " WHERE " + COL_INVOICE_ID + " = '" + invoiceId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor != null) {
                cursor.moveToFirst();

                inv.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                inv.setInvoiceId(cursor.getString(cursor.getColumnIndex(COL_INVOICE_ID)));

                inv.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));

                inv.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));
                inv.setTotalAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_TOTAL)));
                inv.setTransactions(getTransactions(inv));


                cursor.close();

            }
            db.close();
        } catch (SQLiteException e) {

            Log.d(TAG, "getCustomerWiseInvoices  Exception  " + e.getMessage());

        }

        return inv;
    }

    //   Get Invoice list customer wise TABLE_RECEIPT
    public ArrayList<Invoice> getInvoices() {

        ArrayList<Invoice> list = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_RECEIPT;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {
                    Invoice inv = new Invoice();

                    inv.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    inv.setInvoiceId(cursor.getString(cursor.getColumnIndex(COL_INVOICE_ID)));

                    inv.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));
                    inv.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));
                    inv.setTotalAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_TOTAL)));

                    inv.setTransactions(getTransactions(inv));


                    list.add(inv);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getInvoices  Exception  " + e.getMessage());

        }

        return list;
    }

    public boolean isExistInvoice(String invoiceId) {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_RECEIPT + " WHERE " + COL_INVOICE_ID + WHERE_CLAUSE, new String[]{String.valueOf(invoiceId)});


            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            e.fillInStackTrace();
        }

        return count != 0;


    }

    public double getCustomerWiseReceiptBalance(String customerId) {


        double balanceAmount = 0;


        try {
            String sql = "SELECT " + COL_RECEIPT_BALANCE + "," + COL_ID + " FROM " + TABLE_RECEIPT + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    int id = cursor.getInt(cursor.getColumnIndex(COL_ID));
                    double d = cursor.getDouble(cursor.getColumnIndex(COL_RECEIPT_BALANCE));
                    balanceAmount += d;

                    String query = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + id + "'";


                    Cursor queryCursor = db.rawQuery(query, null);

                    if (queryCursor.moveToFirst())
                        do {

                            double recAmount = queryCursor.getDouble(queryCursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                            balanceAmount -= recAmount; //minus


                        } while (queryCursor.moveToNext());

                    queryCursor.close();

                    /****/


                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getCustomerWiseBalance  Exception  " + e.getMessage());

        }

        return balanceAmount;
    }


    public double getCustomerWiseReceiptCollectionAmount(int customerId) {


        double receivable = 0;


        try {
            String sql = "SELECT " + COL_ID + " FROM " + TABLE_RECEIPT + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    int id = cursor.getInt(cursor.getColumnIndex(COL_ID));

                    String query = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + id + "'";
                    Cursor queryCursor = db.rawQuery(query, null);

                    if (queryCursor.moveToFirst())
                        do {

                            double recAmount = queryCursor.getDouble(queryCursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                            receivable += recAmount;

                        } while (queryCursor.moveToNext());

                    queryCursor.close();


                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            printLog(TAG, "getCustomerWiseBalance  Exception  " + e.getMessage());

        }

        return receivable;
    }

    //   Get invoice wisecollections amount from TABLE_RECEIPT_TRANSACTION
    public float getInvoiceCollectionAmount(String invoiceColId) {


        float receivedAmount = 0;


        try {
            String sql = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + invoiceColId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    float d = cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                    receivedAmount += d;

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getCollectionAmount  Exception  " + e.getMessage());

        }

        return receivedAmount;


    }

    //   Get collections amount from TABLE_RECEIPT_TRANSACTION
    public double getCollectionAmount() {


        double receivedAmount = 0;


        try {
            String sql = "SELECT " + COL_RECEIVABLE_AMOUNT + " FROM " + TABLE_RECEIPT_TRANSACTION;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {


                    double d = cursor.getDouble(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT));
                    receivedAmount += d;

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            printLog(TAG, "getCollectionAmount  Exception  " + e.getMessage());

        }

        return receivedAmount;
    }

    //    get products list corresponding customer
    private ArrayList<Transaction> getTransactions(Invoice invoice) {

        final ArrayList<Transaction> transactions = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + invoice.getLocalId() + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {

                    Transaction t = new Transaction();
                    t.setReceiptNo(cursor.getString(cursor.getColumnIndex(COL_RECEIPT_NO)));
                    t.setReceivedAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT)));


//                        t.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));
//                        t.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));

                    t.setLogDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));
                    t.setTransactionId(cursor.getInt(cursor.getColumnIndex(COL_ID)));


                    t.setTransactionType(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_TYPE)));
                    t.setChequeNumber(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_NUMBER)));
                    t.setBankName(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    t.setRemark(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_REMARK)));
                    t.setIssueDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_ISSUE_DATE)));
                    t.setClearDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_CLEAR_DATE)));


                    transactions.add(t);

                } while (cursor.moveToNext());

            invoice.setTransactions(transactions);

            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getTransactions  Exception  " + e.getMessage());

        }

        return transactions;
    }

    //   Get id customer wise  TABLE_RECEIPT
    public ArrayList<String> getCustomerWiseReceiptIds(int customerId) {

        Set<String> ids = new LinkedHashSet<>();


        try {
            String sql_receipt = "SELECT " + COL_ID + " FROM " + TABLE_RECEIPT + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor_receipt = db.rawQuery(sql_receipt, null);

            if (cursor_receipt.moveToFirst())
                do {

                    int receiptLocId = cursor_receipt.getInt(cursor_receipt.getColumnIndex(COL_ID));


                    String sql_transaction = "SELECT " + COL_RECEIPT_NO + " FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + receiptLocId + "'";

                    Cursor cursor_transaction = db.rawQuery(sql_transaction, null);


                    if (cursor_transaction.moveToFirst())
                        do {

                            String receiptNo = (cursor_transaction.getString(cursor_transaction.getColumnIndex(COL_RECEIPT_NO)));

                            ids.add(String.valueOf(receiptNo));

                            Log.d(TAG, "getCustomerWiseReceiptIds  receiptNo  " + receiptNo);

                        } while (cursor_transaction.moveToNext());


                    cursor_transaction.close();


                    ////////////


                } while (cursor_receipt.moveToNext());

            cursor_receipt.close();

            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getCustomerWiseReceiptIds  Exception  " + e.getMessage());

        }

        return new ArrayList<String>(ids);

    }

    //    get Transaction list ReceiptNumberWise
    public ArrayList<Receipt> getTransactionsReceiptNumberWise(String receiptNo) {

        final ArrayList<Receipt> receipts = new ArrayList<>();

        try {
            String sql = "SELECT * FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_RECEIPT_NO + " = '" + receiptNo + "'";

//

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {

                    Receipt r = new Receipt();
                    r.setReceiptNo(cursor.getString(cursor.getColumnIndex(COL_RECEIPT_NO)));
                    r.setReceivedAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT)));

                    r.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));
                    r.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));

                    r.setLogDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));
                    r.setTransactionId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    r.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_FK_INVOICE_COL_ID)));

                    r.setTransactionType(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_TYPE)));
                    r.setChequeNumber(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_NUMBER)));
                    r.setBankName(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    r.setRemark(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_REMARK)));
                    r.setIssueDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_ISSUE_DATE)));
                    r.setClearDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_CLEAR_DATE)));

                    receipts.add(r);

                } while (cursor.moveToNext());


            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getTransactions  Exception  " + e.getMessage());

        }

        return receipts;
    }

    public ArrayList<Receipt> getAllTransactions() {

        final ArrayList<Receipt> receipts = new ArrayList<>();

        try {
//            String sql = "SELECT * FROM " + TABLE_RECEIPT_TRANSACTION + " WHERE " + COL_FK_INVOICE_COL_ID + " = '" + receiptLocId + "'";
            String sql = "SELECT * FROM " + TABLE_RECEIPT_TRANSACTION;
//

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {

                    Receipt r = new Receipt();


                    r.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    r.setReceiptNo(cursor.getString(cursor.getColumnIndex(COL_RECEIPT_NO)));
                    r.setReceivedAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIVABLE_AMOUNT)));

                    r.setInvoiceNo(cursor.getString(cursor.getColumnIndex(COL_INVOICE_NO)));
                    r.setBalanceAmount(cursor.getFloat(cursor.getColumnIndex(COL_RECEIPT_BALANCE)));

                    r.setLogDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));
                    r.setTransactionId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    r.setLocalId(cursor.getInt(cursor.getColumnIndex(COL_FK_INVOICE_COL_ID)));


                    r.setTransactionType(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_TYPE)));
                    r.setChequeNumber(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_NUMBER)));
                    r.setBankName(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    r.setRemark(cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_REMARK)));
                    r.setIssueDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_ISSUE_DATE)));
                    r.setClearDate(cursor.getString(cursor.getColumnIndex(COL_CHEQUE_CLEAR_DATE)));

                    receipts.add(r);

                } while (cursor.moveToNext());


            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Log.d(TAG, "getAllTransactions  Exception  " + e.getMessage());

        }

        return receipts;
    }

    /**
     * Sale functions
     */

    //    insert sales
    public long insertSale(Sales sales) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_ID, sales.getCustomerId());
            values.put(COL_SALE_TYPE, sales.getSaleType());
            values.put(COL_SALE_TOTAL, sales.getTotal());
            values.put(COL_SALE_COLLECTION_DISCOUNT, sales.getCollectionDiscount());
            values.put(COL_SALE_PAID, sales.getPaid());
            values.put(COL_INVOICE_CODE, sales.getInvoiceCode());
            values.put(COL_SALE_PO, sales.getSalePo());
            values.put(COL_SALE_DISCOUNT, sales.getDiscountTotal());
            values.put(COL_CREATED_AT, TextUtils.isEmpty(sales.getDate()) ? getDateTime() : sales.getDate());
            values.put(COL_SALE_LATITUDE, sales.getSaleLatitude());
            values.put(COL_SALE_LONGITUDE, sales.getSaleLongitude());

            // Inserting Row
            long l = db.insert(TABLE_SALE_CUSTOMER, null, values);

            db.close(); // Closing database connection

            if (l == -1) {
                return l;
            } else {

                updateVisitStatus(sales.getCustomerId(), REQ_SALE_TYPE, "", "", "");
                insertSaleProducts(Integer.valueOf(String.valueOf(l)), sales.getCartItems());
                return l;

            }


        } catch (SQLiteException e) {

            Log.v(TAG, "insertSale  Exception  " + e.getMessage());
            return -1;
        }
    }

    /*insert sale items*/
    private void insertSaleProducts(int saleId, ArrayList<CartItem> list) {

        long l = 0;
        try {

            SQLiteDatabase db = this.getWritableDatabase();

            for (CartItem cartItem : list) {

                ContentValues values = new ContentValues();
                values.put(COL_PRODUCT_ID, cartItem.getProductId());
                values.put(COL_PRODUCT_PRICE, cartItem.getProductPrice());

                values.put(COL_PRODUCT_CODE, cartItem.getProductCode());
                values.put(COL_PRODUCT_NAME, cartItem.getProductName());
                values.put(COL_PRODUCT_ARABIC_NAME, cartItem.getArabicName());
                values.put(COL_ARTICLE_NUMBER, cartItem.getArticleCode());
                values.put(COL_PRODUCT_UNIT, cartItem.getProductUnit());

                values.put(COL_SALE_PRODUCT_QUANTITY, cartItem.getPieceQuantity());
                values.put(COL_SALE_TOTAL, cartItem.getTotalPrice());
                values.put(COL_PRODUCT_TAX, cartItem.getTax());
                values.put(COL_PRODUCT_DISC_PERC, cartItem.getDiscountPerce());
                values.put(COL_SALE_PRODUCT_ORDER_TYPE, cartItem.getOrderType());
                values.put(COL_SALE_ORDER_TYPE_QUANTITY, cartItem.getTypeQuantity());
                values.put(COL_PRODUCT_PEACE_PER_CART, cartItem.getPiecepercart());

                values.put(COL_FK_SALE_ID, saleId);

                l = db.insert(TABLE_SALE_PRODUCTS, null, values);


            }


            if (db != null && db.isOpen()) {
                db.close(); // Closing database connection
            }

        } catch (SQLiteException | IllegalStateException e) {
            Log.v(TAG, "insertSaleProducts  Exception  " + e.getMessage());
        }


    }

    //    update sales
    public boolean updateSaleAmount(long salesId, float paidAmount) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_SALE_PAID, paidAmount);


            int result = db.update(TABLE_SALE_CUSTOMER, values, COL_ID + " =?", new String[]{String.valueOf(salesId)});


            db.close(); // Closing database connection

            if (result == -1) {
                return false;
            } else {
                return true;

            }

        } catch (SQLiteException e) {

            Log.v(TAG, "insertSale  Exception  " + e.getMessage());
            return false;
        }
    }

    //    Get All Sale list TABLE_SALE_CUSTOMER
    public ArrayList<Sales> getAllSales() {

        ArrayList<Sales> list = new ArrayList<>();
        try {

            String sql = "SELECT * FROM " + TABLE_SALE_CUSTOMER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {
                    Sales s = new Sales();

                    s.setLocId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    s.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_ID)));
                    s.setSaleType(cursor.getString(cursor.getColumnIndex(COL_SALE_TYPE)));

                    s.setDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));

                    s.setTotal(cursor.getDouble(cursor.getColumnIndex(COL_SALE_TOTAL)));
                    s.setCollectionDiscount(cursor.getDouble(cursor.getColumnIndex(COL_SALE_COLLECTION_DISCOUNT)));
                    s.setInvoiceCode(cursor.getString(cursor.getColumnIndex(COL_INVOICE_CODE)));
                    s.setPaid(cursor.getDouble(cursor.getColumnIndex(COL_SALE_PAID)));

                    s.setSalePo(cursor.getString(cursor.getColumnIndex(COL_SALE_PO)));
                    s.setDiscountTotal(cursor.getDouble(cursor.getColumnIndex(COL_SALE_DISCOUNT)));
                    s.setSaleLatitude(cursor.getString(cursor.getColumnIndex(COL_SALE_LATITUDE)));
                    s.setSaleLongitude(cursor.getString(cursor.getColumnIndex(COL_SALE_LONGITUDE)));

                    getSaleProduct(s, cursor.getInt(cursor.getColumnIndex(COL_ID)));

                    list.add(s);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {
            Log.w(TAG, "getAllSales  Exception " + e.getMessage());
        }

        return list;
    }

    //    Get customer wise Sale list TABLE_SALE_CUSTOMER
    public ArrayList<Sales> getCustomerSales(String customerId) {

        ArrayList<Sales> list = new ArrayList<>();
        try {

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_SALE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + WHERE_CLAUSE, new String[]{customerId});


            if (cursor.moveToFirst())
                do {
                    Sales s = new Sales();


                    s.setLocId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    s.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_ID)));
                    s.setSaleType(cursor.getString(cursor.getColumnIndex(COL_SALE_TYPE)));

                    s.setDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));

                    s.setTotal(cursor.getFloat(cursor.getColumnIndex(COL_SALE_TOTAL)));
                    s.setCollectionDiscount(cursor.getDouble(cursor.getColumnIndex(COL_SALE_COLLECTION_DISCOUNT)));


                    s.setInvoiceCode(cursor.getString(cursor.getColumnIndex(COL_INVOICE_CODE)));
                    s.setPaid(cursor.getFloat(cursor.getColumnIndex(COL_SALE_PAID)));


                    String createdDate = cursor.getString(cursor.getColumnIndex(COL_CREATED_AT));
                    s.setSalePo(cursor.getString(cursor.getColumnIndex(COL_SALE_PO)));
                    s.setDiscountTotal(cursor.getDouble(cursor.getColumnIndex(COL_SALE_DISCOUNT)));


                    getSaleProduct(s, cursor.getInt(cursor.getColumnIndex(COL_ID)));

                    list.add(s);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {

          printLog(TAG, "getCustomerSales  Exception  " + e.getMessage());
        }

        return list;
    }

    //    get products list corresponding customer
    private Sales getSaleProduct(Sales sales, int saleId) {

        try {
            String sql = "SELECT * FROM " + TABLE_SALE_PRODUCTS + " WHERE " + COL_FK_SALE_ID + " = '" + saleId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            final ArrayList<CartItem> carts = new ArrayList<>();
            if (cursor.moveToFirst())
                do {

                    CartItem c = new CartItem();


                    c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    c.setCartId(cursor.getInt(cursor.getColumnIndex(COL_ID)));

                    c.setProductCode(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CODE)));
                    c.setProductName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME)));
                    c.setArabicName(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ARABIC_NAME)));
                    c.setArticleCode(cursor.getString(cursor.getColumnIndex(COL_ARTICLE_NUMBER)));
                    c.setProductPrice(cursor.getDouble(cursor.getColumnIndex(COL_PRODUCT_PRICE)));
                    c.setTotalPrice(cursor.getDouble(cursor.getColumnIndex(COL_SALE_TOTAL)));
                    c.setPieceQuantity(cursor.getInt(cursor.getColumnIndex(COL_SALE_PRODUCT_QUANTITY)));
                    c.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));
                    c.setDiscountPerce(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_DISC_PERC)));
                    c.setTypeQuantity(cursor.getInt(cursor.getColumnIndex(COL_SALE_ORDER_TYPE_QUANTITY)));
                    c.setPiecepercart(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_PEACE_PER_CART)));
                    c.setOrderType(cursor.getString(cursor.getColumnIndex(COL_SALE_PRODUCT_ORDER_TYPE)));
                    c.setProductUnit(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_UNIT)));

                    c.setSalePrice(getSalePrice(c.getProductPrice(), c.getTax()));
                    c.setNetPrice(getNetValueFromDiscountedAmount(c.getProductPrice(), c.getDiscountPerce()));
                    c.setTaxValue(getTaxPrice(c.getProductPrice(), c.getTax()));


                    carts.add(c);

                } while (cursor.moveToNext());

            sales.setCartItems(carts);

            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getSaleProduct  Exception  " + e.getMessage());

        }

        return sales;
    }

    //   Get collections wise TABLE_SALE_CUSTOMER
    public double getSalePaidAmount() {


        float paidAmount = 0;

        try {
            String sql = "SELECT " + COL_SALE_PAID + " FROM " + TABLE_SALE_CUSTOMER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    float d = cursor.getFloat(cursor.getColumnIndex(COL_SALE_PAID));
                    paidAmount += d;

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getSalePaidAmount  Exception  " + e.getMessage());

        }

        return paidAmount;
    }

    //   Get credit amount   TABLE_SALE_CUSTOMER customer wise in sales
    public double getCustomerSaleCreditedAmount(int customerId) {


        double paidAmount = 0, saleAmount = 0;

        try {


            String sql = "SELECT " + COL_SALE_PAID + "," + COL_SALE_TOTAL + " FROM " + TABLE_SALE_CUSTOMER + " WHERE " + COL_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    double p = cursor.getDouble(cursor.getColumnIndex(COL_SALE_PAID));
                    paidAmount += p;
                    double s = cursor.getDouble(cursor.getColumnIndex(COL_SALE_TOTAL));
                    saleAmount += s;

                } while (cursor.moveToNext());

            cursor.close();

            db.close();

        } catch (SQLiteException e) {

            printLog(TAG, "getCustomerSaleCreditedAmount  Exception  " + e.getMessage());
        }


        return saleAmount - paidAmount;
    }

    /**
     * Count Row From to SQLite  "TABLE_SALE_CUSTOMER" Table
     */

    private boolean isExistSale() {

        int count = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
//            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_SALE_CUSTOMER + " WHERE " + COL_CONTACT_ID + WHERE_CLAUSE, new String[]{contactId});

            Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_SALE_CUSTOMER, new String[]{});

            if (null != cursor)
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            assert cursor != null;
            cursor.close();

            db.close();
        } catch (SQLiteException | IllegalArgumentException e) {
            e.fillInStackTrace();
        }

        return count != 0;


    }

    /**
     * Quotation functions
     */
    //    insert Quotation
    public boolean insertQuotation(Sales sales) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_ID, sales.getCustomerId());
            values.put(COL_SALE_TYPE, sales.getSaleType());
            values.put(COL_CREATED_AT, TextUtils.isEmpty(sales.getDate()) ? getDateTime() : sales.getDate());
            values.put(COL_SALE_TOTAL, sales.getTotal());

            // Inserting Row
            long l = db.insert(TABLE_QUOTATION_CUSTOMER, null, values);

            db.close(); // Closing database connection

            if (l == -1) {
                return false;
            } else {

                updateVisitStatus(sales.getCustomerId(), REQ_QUOTATION_TYPE, "", "", "");
                insertQuotationProducts(Integer.valueOf(String.valueOf(l)), sales.getCartItems());
                return true;

            }

        } catch (SQLiteException e) {

            Log.v(TAG, "insertQuotation  Exception  " + e.getMessage());
            return false;
        }
    }

    /*insert Quotation items*/
    private void insertQuotationProducts(int saleId, ArrayList<CartItem> list) {

        long l = 0;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            for (CartItem cartItem : list) {

                ContentValues values = new ContentValues();
                values.put(COL_PRODUCT_ID, cartItem.getProductId());
                values.put(COL_PRODUCT_PRICE, cartItem.getProductPrice());
                values.put(COL_SALE_PRODUCT_QUANTITY, cartItem.getPieceQuantity());
                values.put(COL_SALE_TOTAL, cartItem.getTotalPrice());
                values.put(COL_PRODUCT_TAX, cartItem.getTax());
                values.put(COL_PRODUCT_DISC_PERC, cartItem.getDiscountPerce());
                values.put(COL_FK_SALE_ID, saleId);

                l = db.insert(TABLE_QUOTATION_PRODUCTS, null, values);

            }
            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            Log.v(TAG, "insertQuotationProducts  Exception  " + e.getMessage());
        }

    }

    //    Get All Sale list TABLE_QUOTATION_CUSTOMER
    public ArrayList<Sales> getAllQuotations() {

        ArrayList<Sales> list = new ArrayList<>();
        try {

            String sql = "SELECT * FROM " + TABLE_QUOTATION_CUSTOMER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {
                    Sales s = new Sales();

                    s.setLocId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    s.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_ID)));
                    s.setSaleType(cursor.getString(cursor.getColumnIndex(COL_SALE_TYPE)));

                    s.setDate(cursor.getString(cursor.getColumnIndex(COL_CREATED_AT)));

                    s.setTotal(cursor.getFloat(cursor.getColumnIndex(COL_SALE_TOTAL)));

                    getQuotationsProduct(s, cursor.getInt(cursor.getColumnIndex(COL_ID)));


                    s.setSalePo(" ");
                    s.setInvoiceCode("");
                    s.setPaid(0);
                    s.setDiscountTotal(0);


                    list.add(s);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {

            Log.v(TAG, "getAllQuotations  " + e.getMessage());
        }

        return list;
    }

    //    get Quotations products list corresponding customer
    private Sales getQuotationsProduct(Sales sales, int quotation) {

        try {
            String sql = "SELECT * FROM " + TABLE_QUOTATION_PRODUCTS + " WHERE " + COL_FK_SALE_ID + " = '" + quotation + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            final ArrayList<CartItem> carts = new ArrayList<>();
            if (cursor.moveToFirst())
                do {

                    CartItem c = new CartItem();
                    c.setCartId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                    c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    c.setProductPrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_PRICE)));
                    c.setTotalPrice(cursor.getFloat(cursor.getColumnIndex(COL_SALE_TOTAL)));
                    c.setPieceQuantity(cursor.getInt(cursor.getColumnIndex(COL_SALE_PRODUCT_QUANTITY)));

                    c.setTax(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_TAX)));
                    c.setDiscountPerce(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_DISC_PERC)));

                    c.setSalePrice(getSalePrice(c.getProductPrice(), c.getTax()));
//                    c.setNetPrice(getWithoutTaxPrice(c.getProductPrice(), c.getTax()));
                    c.setNetPrice(getNetValueFromDiscountedAmount(c.getProductPrice(), c.getDiscountPerce()));
                    c.setTaxValue(getTaxPrice(c.getProductPrice(), c.getTax()));

                    carts.add(c);

                } while (cursor.moveToNext());

            sales.setCartItems(carts);

            cursor.close();

            db.close();
        } catch (SQLiteException e) {
            Log.v(TAG, "getQuotationsProduct  Exception  " + e.getMessage());

        }

        return sales;
    }

    //   Get today sale wise TABLE_SALE_CUSTOMER
    public double getTodaySaleAmount() {


        double saleAmount = 0;


        try {
            String sql = "SELECT " + COL_SALE_TOTAL + " FROM " + TABLE_SALE_CUSTOMER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    double f = cursor.getDouble(cursor.getColumnIndex(COL_SALE_TOTAL));
                    saleAmount += f;
                    Log.v(TAG, "getTodaySaleAmount  saleAmount  " + f + " total" + saleAmount);


                } while (cursor.moveToNext());

            cursor.close();

        } catch (SQLiteException e) {
            Log.v(TAG, "getTodaySaleAmount  Exception  " + e.getMessage());

        }

        return saleAmount;
    }

    /**
     * visiting status functions
     */


    /*Add customer visit status*/
    private void insertVisitStatus(int customerId) {

        long l = 0;

        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_VISIT_SALE, false);
            values.put(COL_VISIT_QUOTATION, false);
            values.put(COL_VISIT_RETURN, false);
            values.put(COL_VISIT_RECEIPT, false);
            values.put(COL_VISIT_NO_SALE, false);
            values.put(COL_VISIT_NO_SALE_REASON, "");

            values.put(COL_FK_CUSTOMER_ID, customerId);

            l = db.insert(TABLE_CUSTOMER_VISIT, null, values);

            Log.v(TAG, "insertStatus  status  " + l);

            db.close(); // Closing database connection


        } catch (SQLiteException e) {
            Log.v(TAG, "insertStatus  Exception  " + e.getMessage());
        }

    }

    //    update Visit Status
    public boolean updateVisitStatus(int id, int type, String reason, String latitude, String longitude) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            switch (type) {
                case REQ_SALE_TYPE:
                    values.put(COL_VISIT_SALE, true);
                    values.put(COL_VISIT_NO_SALE, false);
                    values.put(COL_VISIT_NO_SALE_REASON, "");

                    break;
                case REQ_QUOTATION_TYPE:

                    values.put(COL_VISIT_QUOTATION, true);
                    values.put(COL_VISIT_NO_SALE, false);
                    values.put(COL_VISIT_NO_SALE_REASON, "");

                    break;
                case REQ_RETURN_TYPE:
                    values.put(COL_VISIT_RETURN, true);
                    values.put(COL_VISIT_NO_SALE, false);
                    values.put(COL_VISIT_NO_SALE_REASON, "");


                    break;
                case REQ_RECEIPT_TYPE:

                    values.put(COL_VISIT_RECEIPT, true);

                    values.put(COL_VISIT_NO_SALE, false);
                    values.put(COL_VISIT_NO_SALE_REASON, "");

                    break;
                case REQ_NO_SALE_TYPE:

                    values.put(COL_VISIT_NO_SALE, true);
                    values.put(COL_VISIT_NO_SALE_REASON, reason);
                    values.put(COL_SALE_LATITUDE, latitude);
                    values.put(COL_SALE_LONGITUDE, longitude);

                    break;
            }


            int result = db.update(TABLE_CUSTOMER_VISIT, values, COL_FK_CUSTOMER_ID + " =?", new String[]{String.valueOf(id)});

            printLog(TAG, "updateVisitStatus  result  " + result);


            return result != -1;
        } catch (SQLiteException e) {
            printLog(TAG, "updateVisitStatus  Exception  " + e.getMessage());
            return false;
        }

    }

    //   Get Visiting status TABLE_CUSTOMER_VISIT
    public boolean getVisitingStatus(int visitingType, int customerId) {


        boolean isExist = isExistCustomer(customerId);  // check customer in table

        if (!isExist)   //if  not exit customer return false
            return false;

        String col_name = "*";

        switch (visitingType) {
            case REQ_SALE_TYPE:

                col_name = COL_VISIT_SALE;
                break;

            case REQ_QUOTATION_TYPE:

                col_name = COL_VISIT_QUOTATION;

                break;

            case REQ_RETURN_TYPE:
                col_name = COL_VISIT_RETURN;
                break;

            case REQ_RECEIPT_TYPE:

                col_name = COL_VISIT_RECEIPT;
                break;
            case REQ_NO_SALE_TYPE:
                col_name = COL_VISIT_NO_SALE;
                break;

            default:   //REQ_ANY_TYPE
                col_name = "*";
                break;
        }


        try {

            String selectQuery = "SELECT " + col_name + " FROM " + TABLE_CUSTOMER_VISIT + " WHERE " + COL_FK_CUSTOMER_ID + "=?";


//            String sql = "SELECT " + col_name + " FROM " + TABLE_CUSTOMER_VISIT + " WHERE " + COL_FK_CUSTOMER_ID + " = '" + customerId + "'";

            SQLiteDatabase db = this.getReadableDatabase();


            Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(customerId)});
//            Cursor cursor = db.rawQuery(sql,null);


            cursor.moveToFirst();


            if (visitingType == REQ_ANY_TYPE) {


                Boolean sal = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_SALE)) == 1);

                Boolean quo = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_QUOTATION)) == 1);
                Boolean ret = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_RETURN)) == 1);
                Boolean rec = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_RECEIPT)) == 1);
                Boolean n_sal = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_NO_SALE)) == 1);


                cursor.close();


                return sal || quo || ret || rec || n_sal;


            } else {

                Boolean flag = (cursor.getInt(cursor.getColumnIndex(col_name)) == 1);


                cursor.close();


                return flag;

            }

        } catch (SQLiteException e) {

            Log.v(TAG, "getVisitingStatus  Exception  " + e.getMessage());

        }

        return false;
    }

    //    update closeVisit
    public boolean updateVisitFinish(int customerId) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_VISIT_STATUS, true);
            int result = db.update(TABLE_CUSTOMER, values, COL_CUSTOMER_ID + " =?", new String[]{String.valueOf(customerId)});

            return result != -1;
        } catch (SQLiteException e) {
            Log.v(TAG, "updateVisitFinish  Exception  " + e.getMessage());
            return false;
        }

    }

    //    Get All Sale list TABLE_CUSTOMER_VISIT
    public ArrayList<NoSale> getAllNoSaleReasons() {

        ArrayList<NoSale> list = new ArrayList<>();
        try {

            String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_VISIT + " WHERE " + COL_VISIT_NO_SALE + "=?";


            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(1)});


            if (cursor.moveToFirst())
                do {
                    NoSale n = new NoSale();

                    n.setCustomerId(cursor.getInt(cursor.getColumnIndex(COL_FK_CUSTOMER_ID)));
                    n.setReason(cursor.getString(cursor.getColumnIndex(COL_VISIT_NO_SALE_REASON)));
                    n.setLatitude(cursor.getString(cursor.getColumnIndex(COL_SALE_LATITUDE)));
                    n.setLongitude(cursor.getString(cursor.getColumnIndex(COL_SALE_LONGITUDE)));

                    list.add(n);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {

            Log.v(TAG, "getAllQuotations  " + e.getMessage());
        }

        return list;
    }

    //test
    public void getVisitingStatus() {


        try {

            String sql = "SELECT * FROM " + TABLE_CUSTOMER_VISIT;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {

                    Boolean a = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_SALE)) == 1);

                    Boolean b = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_QUOTATION)) == 1);
                    Boolean c = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_RETURN)) == 1);
                    Boolean d = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_RECEIPT)) == 1);
                    Boolean e = (cursor.getInt(cursor.getColumnIndex(COL_VISIT_NO_SALE)) == 1);


                    Log.v(TAG, "getVisitingStatus   a " + a + "\nb " + b + "\nc " + c + "\nd " + d + "\ne " + e);


                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {
            Log.v(TAG, "getVisitingStatus  Exception  " + e.getMessage());

        }


    }

    //    customer visit status Customer list TABLE_CUSTOMER
    public boolean isDayclosePrmission() {

        boolean status = true;
        try {


            String sql = "SELECT " + COL_CUSTOMER_VISIT_STATUS + " FROM " + TABLE_CUSTOMER;

//            String sql = "SELECT * FROM " + TABLE_CUSTOMER;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);


            if (cursor.moveToFirst())
                do {

                    Boolean isVisit = (cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_VISIT_STATUS)) == 1);

                    if (!isVisit)
                        return false;

                } while (cursor.moveToNext());

            cursor.close();
            db.close();


        } catch (SQLiteException e) {
            Log.v(TAG, "getAllCustomers  Exception  " + e.getMessage());

        }

        return status;
    }

    //    get students list corresponding time table
    public ArrayList<CartItem> getAllSaleProducts() {


        final ArrayList<CartItem> carts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM " + TABLE_SALE_PRODUCTS;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    CartItem c = new CartItem();
                    c.setProductId(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    c.setSalePrice(cursor.getFloat(cursor.getColumnIndex(COL_PRODUCT_PRICE)));
                    c.setTotalPrice(cursor.getFloat(cursor.getColumnIndex(COL_SALE_TOTAL)));
                    c.setPieceQuantity(cursor.getInt(cursor.getColumnIndex(COL_SALE_PRODUCT_QUANTITY)));


                    carts.add(c);

                } while (cursor.moveToNext());


            cursor.close();

        } catch (SQLiteException e) {

        }

        return carts;
    }

    //    delete TABLE
    public boolean deleteTableRequest(int deleteType) {

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            int deleteValue = -1;
            switch (deleteType) {
                case REQ_SALE_TYPE:
                    deleteValue = db.delete(TABLE_SALE_CUSTOMER, null, null);
                    break;
                case REQ_QUOTATION_TYPE:
                    deleteValue = db.delete(TABLE_QUOTATION_CUSTOMER, null, null);
                    break;

                case REQ_RECEIPT_TYPE:
                    deleteValue = db.delete(TABLE_RECEIPT, null, null);
                    deleteValue = db.delete(TABLE_RECEIPT_TRANSACTION, null, null);
                    deleteValue = db.delete(TABLE_OPENING_BALANCE_TRANSACTION, null, null);
                    break;
                case REQ_CUSTOMER_TYPE:
                    deleteValue = db.delete(TABLE_CUSTOMER, null, null);
                    deleteValue = db.delete(TABLE_CUSTOMER_VISIT, null, null);
                    break;
                case REQ_ANY_TYPE:

                    deleteValue = db.delete(TABLE_RECEIPT, null, null);
                    deleteValue = db.delete(TABLE_RECEIPT_TRANSACTION, null, null);
                    deleteValue = db.delete(TABLE_OPENING_BALANCE_TRANSACTION, null, null);

                    deleteValue = db.delete(TABLE_SALE_PRODUCTS, null, null);
                    deleteValue = db.delete(TABLE_SALE_CUSTOMER, null, null);

                    deleteValue = db.delete(TABLE_QUOTATION_PRODUCTS, null, null);
                    deleteValue = db.delete(TABLE_QUOTATION_CUSTOMER, null, null);

                    deleteValue = db.delete(TABLE_STOCK, null, null);

                    deleteValue = db.delete(TABLE_CUSTOMER_VISIT, null, null);
                    deleteValue = db.delete(TABLE_CUSTOMER, null, null);

                    break;

            }


            db.close();

            return deleteValue != -1;

        } catch (SQLiteException e) {
            e.getStackTrace();
            return false;
        }

    }



    //    insert banks
    public boolean insertBanksName(Banks bank) {

        boolean isExist = isExistProduct(bank.getBank_id()); // check stock in table

        if (isExist)
            return false;
        try {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_BANK_ID, bank.getBank_id());
            values.put(COL_BANK_NAME, bank.getBank_name());

            long l = db.insert(TABLE_BANKS, null, values);
            db.close(); // Closing database connection
            return l != -1;

        } catch (SQLiteException e) {
            Log.v(TAG, "insertStock  Exception  " + e.getMessage());
            return false;
        }
    }


    // get all banks
    public ArrayList<Banks> getAllBanks() {

        final ArrayList<Banks> banksarray = new ArrayList<>();
        try {
            String sql = "SELECT * FROM " + TABLE_BANKS;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
                do {

                    Banks b = new Banks();

                    b.setBank_name(cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    b.setBank_id(cursor.getInt(cursor.getColumnIndex(COL_BANK_ID)));

                    //   Log.e("getBanks  Exceptio" , ""+cursor.getString(cursor.getColumnIndex(COL_BANK_NAME)));
                    banksarray.add(b);

                } while (cursor.moveToNext());

            cursor.close();
            db.close();

        } catch (SQLiteException e)
        {
            Log.v(TAG, "getBanks  Exception  " + e.getMessage());
        }

        return banksarray;
    }


    //    delete TABLE
    public boolean deleteAllTable() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            int stockDlt = db.delete(TABLE_STOCK, null, null);
            int saleCustomertDlt = db.delete(TABLE_SALE_CUSTOMER, null, null);
            int customerstDlt = db.delete(TABLE_CUSTOMER, null, null);


            db.close();

            return saleCustomertDlt != -1;

        } catch (SQLiteException e) {
            e.getStackTrace();
            return false;
        }
    }

}
