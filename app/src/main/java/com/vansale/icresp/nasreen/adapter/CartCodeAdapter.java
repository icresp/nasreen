package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.vansale.icresp.nasreen.model.CartItem;

import java.util.ArrayList;

/**
 * Created by mentor on 25/5/17.
 */

public class CartCodeAdapter extends ArrayAdapter<CartItem> {
    private final String TAG = "CartCodeAdapter";
    private ArrayList<CartItem> items;
    private ArrayList<CartItem> itemsAll;
    private ArrayList<CartItem> suggestions;
    private int viewResourceId;


    public CartCodeAdapter(Context context, int viewResourceId, ArrayList<CartItem> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<CartItem>) items.clone();
        this.suggestions = new ArrayList<CartItem>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater.from(parent.getContext()));


//            v = inflater.inflate(R.layout.item_textview, null);

//            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(viewResourceId, null);
        }
        CartItem cartItem = items.get(position);
        if (cartItem != null) {
            TextView textView = (TextView) v;
            textView.setPadding(10, 10, 10, 10);


//            TextView customerNameLabel = (TextView) v.findViewById(R.id.textView_item_textView);
            if (textView != null) {

                textView.setText(cartItem.getProductCode());
            }
        }
        return v;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    private Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((CartItem) (resultValue)).getProductCode();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            if (constraint != null) {
                suggestions.clear();
                for (CartItem cartItem : itemsAll) {
                    if (cartItem.getProductCode().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(cartItem);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }


        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {


            ArrayList<CartItem> filteredList = (ArrayList<CartItem>) results.values;
            if (results.count > 0) {
                clear();
                for (CartItem c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };


}
