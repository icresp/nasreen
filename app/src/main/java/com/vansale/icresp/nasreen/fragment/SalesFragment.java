package com.vansale.icresp.nasreen.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.SalePreviewActivity;
import com.vansale.icresp.nasreen.adapter.RvCartAdapter;
import com.vansale.icresp.nasreen.dialog.CartSpinnerDialogNew;
import com.vansale.icresp.nasreen.dialog.PaymentTypeDialog;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.listener.OnSpinerItemClick;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.CartItemCode;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.textwatcher.TextValidator;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentageValue;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getSalePrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getTaxPrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getWithoutTaxPrice;
import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_SALE_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_CASE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALES_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALE_RETAIL;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALE_WHOLESALE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dbDateFormat;
import static com.vansale.icresp.nasreen.config.Generic.generateNewNumber;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;


public class SalesFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_SHOP = "customer_arg";

    String TAG = "SalesFragment", _poNo = "";
    int callingActivity = 0;
    private Shop SELECTED_SHOP = null;
    private String EXECUTIVE_ID = "";
    private ViewGroup lytCart;
    private Button btnAddToCart, btnMakePayment, btnfinish;
    private TextView tvProductSpinner, tvCodeSpinner, tvQtyTotal, tvNetTotal, tvNetPrice;
    private EditText etQuantity;
    private RecyclerView recyclerView;
    private RvCartAdapter adapter;
    private ErrorView errorView;
    private ProgressBar progressBar;
    private AppCompatSpinner spinnerCartUnit;

    private ArrayList<CartItem> productList = new ArrayList<>();
    private ArrayList<CartItemCode> productCodeList = new ArrayList<>();

    private ArrayList<CartItem> cartItems = new ArrayList<>();
    private CartItem select_Cart = null;
    private double paid_amount = 0;
    private String SALE_TYPE = SALE_RETAIL;
    private MyDatabase myDatabase;
    private SessionValue sessionValue;

    private String PAYMENT_TYPE = "", str_Latitude = "0", str_Longitude = "0";

    private String provider;
    LocationManager locationManager;

    public static SalesFragment newInstance(Shop shop) {
        SalesFragment fragment = new SalesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOP, shop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SELECTED_SHOP = (Shop) getArguments().getSerializable(ARG_SHOP);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sales, container, false);


        lytCart = (ViewGroup) view.findViewById(R.id.layout_sales_cart);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar_sales_products);

        errorView = (ErrorView) view.findViewById(R.id.errorView_sales_products);

        btnAddToCart = (Button) view.findViewById(R.id.button_sales_addCart);

        etQuantity = (EditText) view.findViewById(R.id.EditText_sales_Qty);

        tvProductSpinner = (TextView) view.findViewById(R.id.textView_sales_item);

        tvQtyTotal = (TextView) view.findViewById(R.id.textView_sales_qtyTotal);

        tvNetTotal = (TextView) view.findViewById(R.id.textView_sales_netTotal);
        tvNetPrice = (TextView) view.findViewById(R.id.textView_sales_netPrice);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_sales);

        btnMakePayment = (Button) view.findViewById(R.id.button_sales_makePayment);
        btnfinish = (Button) view.findViewById(R.id.button_sales_finish);


        tvCodeSpinner = (TextView) view.findViewById(R.id.textView_sales_code);
        spinnerCartUnit = (AppCompatSpinner) view.findViewById(R.id.spinner_sales_orderUnit);

        myDatabase = new MyDatabase(getContext());
        sessionValue = new SessionValue(getContext());


        adapter = new RvCartAdapter(cartItems, new OnNotifyListener() {
            @Override
            public void onNotified() {

                setTotalPriceView();

                if (adapter.getItemCount() == 0)
                    btnMakePayment.setEnabled(false);
                else
                    btnMakePayment.setEnabled(true);

            }
        });


        EXECUTIVE_ID = new SessionAuth(getActivity()).getExecutiveId();

        callingActivity = getActivity().getIntent().getIntExtra(CALLING_ACTIVITY_KEY, 0);
        switch (callingActivity) {
            case ActivityConstants.ACTIVITY_SALES:
                btnMakePayment.setVisibility(View.VISIBLE);
                break;
            case ActivityConstants.ACTIVITY_QUOTATION:

                btnMakePayment.setVisibility(View.GONE);
                break;
            default:
                btnMakePayment.setVisibility(View.VISIBLE);

        }

        getVanStockList();

        setRecyclerView();

        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Requires Permission", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                LocationListener mlocListener = new MyLocationListener();
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                provider = locationManager.getBestProvider(criteria, true);
                locationManager.requestLocationUpdates(provider, 61000, 250,
                        mlocListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Error with location manager", Toast.LENGTH_SHORT).show();
        }


        //Add to textWatcher

        etQuantity.addTextChangedListener(new TextValidator(etQuantity) {
            @Override
            public void validate(TextView textView, String qtyString) {


                try {


                    if (!TextUtils.isEmpty(qtyString) && select_Cart != null) {

                        int quantity = TextUtils.isEmpty(qtyString) ? 0 : Integer.valueOf(qtyString);

                        if (SALE_TYPE.equals(SALE_WHOLESALE))
                            tvNetPrice.setText(getAmount(getWithoutTaxPrice(select_Cart.getWholeSalePrice(), select_Cart.getTax())));
                        else
                            tvNetPrice.setText(getAmount(getWithoutTaxPrice(select_Cart.getRetailPrice(), select_Cart.getTax())));


                        if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
                            quantity = quantity * select_Cart.getPiecepercart();


                        tvQtyTotal.setText(getAmount(select_Cart.getNetPrice() * quantity));


                    } else {
                        tvQtyTotal.setText(getAmount(0.0f));
                    }

                } catch (Exception ignored) {

                }


            }
        });


        setShopTypeSpinner();
        btnAddToCart.setOnClickListener(this);
        btnMakePayment.setOnClickListener(this);
        btnfinish.setOnClickListener(this);


        return view;

    }


    private void setShopTypeSpinner() {


//order type spinner
        ArrayAdapter<CharSequence> orderTypeAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.cart_type, R.layout.spinner_background_dark);

        orderTypeAdapter.setDropDownViewResource(R.layout.spinner_list);
        spinnerCartUnit.setAdapter(orderTypeAdapter);

        spinnerCartUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setCartProduct();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    //    Load Stock All List from Server
    private void getVanStockList() {

        setCartProgressBar(true);

        productList.clear();
        productCodeList.clear();

        final JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        productList.addAll(myDatabase.getAllStock());

        productCodeList.addAll(myDatabase.getAllStockCode());


        if (productList.isEmpty())
            setCartErrorView("No Stock");
        else
            setSearchableProductList(productList, productCodeList);

    }


    private void setRecyclerView() {

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(itemAnimator);

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext())
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        setTotalPriceView();


    }

    private void setTotalPriceView() {

        String net = String.valueOf("TOTAL : " + getAmount(adapter.getNetTotal()) + " " + getString(R.string.currency));
        String vat = "VAT  : 00 " + getString(R.string.currency);
        if (adapter.getTaxTotal() != 0)
            vat = "VAT  : " + getAmount(adapter.getTaxTotal()) + " " + getString(R.string.currency);


        String grandTotal = String.valueOf("GRAND TOTAL : " + getAmount(adapter.getGrandTotal()) + " " + getString(R.string.currency));
        tvNetTotal.setText(String.valueOf(net + ",\t\t" + vat + ",\t\t" + grandTotal));

        tvNetTotal.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvNetTotal.setSelected(true);
    }

    //  Working

   /* private void setSearchableProductList(final ArrayList<CartItem> list) {

        setCartProgressBar(false);
        errorView.setVisibility(View.GONE);

        final CartSpinnerDialog spinnerCart = new CartSpinnerDialog(getActivity(), list, "Select Product", R.style.DialogAnimations_SmileWindow);// With 	Animation

        tvProductSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showSpinerDialog();

            }
        });

        tvCodeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showCodeSpinerDialog();
            }
        });

        spinnerCart.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(Object item, int position) {

                select_Cart = (CartItem) item;//flagNoExtractUi


                if (myDatabase.isExistCustomerProduct(SELECTED_SHOP.getShopId(), select_Cart.getProductId())) {
                    double p = myDatabase.getCustomerProductPrice(SELECTED_SHOP.getShopId(), select_Cart.getProductId());
                    select_Cart.setRetailPrice(p);
                    select_Cart.setWholeSalePrice(p);
                }

                select_Cart.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
                setCartProduct();
            }

        });


        *//*code Adapter*//*


        setCartProgressBar(false);
        lytCart.setVisibility(View.VISIBLE);

    }*/

    // New Code with product code search

    private void setSearchableProductList(final ArrayList<CartItem> list, final ArrayList<CartItemCode> listCode) {

        setCartProgressBar(false);
        errorView.setVisibility(View.GONE);

        final CartSpinnerDialogNew spinnerCart = new CartSpinnerDialogNew(getActivity(), list, listCode, "Select Product", R.style.DialogAnimations_SmileWindow);// With 	Animation

        tvProductSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showSpinerDialog();

            }
        });

        tvCodeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showCodeSpinerDialog();
            }
        });

        spinnerCart.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(Object item, int position) {

                select_Cart = (CartItem) item;//flagNoExtractUi


                if (myDatabase.isExistCustomerProduct(SELECTED_SHOP.getShopId(), select_Cart.getProductId())) {

                    double p = myDatabase.getCustomerProductPrice(SELECTED_SHOP.getShopId(), select_Cart.getProductId());
                    select_Cart.setRetailPrice(p);
                    select_Cart.setWholeSalePrice(p);

                    Log.e("Selling price exist", "" + p);

                }

                Log.e("Selected Article", "" + select_Cart.getArticleCode());

                select_Cart.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
                setCartProduct();
            }

        });


        /*code Adapter*/

        setCartProgressBar(false);
        lytCart.setVisibility(View.VISIBLE);

    }


    private void setCartProduct() {

        if (select_Cart == null)
            return;

//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(etQuantity, InputMethodManager.SHOW_IMPLICIT);
//
        etQuantity.setText("");

      /*  etQuantity.requestFocus();
          etQuantity.setFocusableInTouchMode(true);
          etQuantity.setFocusable(true);*/

        /*
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//      imm.showSoftInput(etQuantity, InputMethodManager.SHOW_FORCED);

        */

        showSoftKeyboard(etQuantity);

        select_Cart.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
        double normalPrice = select_Cart.getRetailPrice();

        if (SALE_TYPE.equals(SALE_WHOLESALE))
            normalPrice = select_Cart.getWholeSalePrice();


        double netPrice = getWithoutTaxPrice(normalPrice, select_Cart.getTax());


        double discountedPrice = getPercentageValue(netPrice, select_Cart.getDiscountPerce());
        discountedPrice = netPrice - discountedPrice;


        int quantity = TextUtils.isEmpty(etQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etQuantity.getText().toString().trim());


        if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
            quantity = quantity * select_Cart.getPiecepercart();

        double salePrice = getSalePrice(discountedPrice, select_Cart.getTax());


        select_Cart.setTaxValue(getTaxPrice(discountedPrice, select_Cart.getTax()));

        select_Cart.setNetPrice(netPrice);
        select_Cart.setSalePrice(salePrice);
        select_Cart.setProductPrice(discountedPrice);


        tvQtyTotal.setText(getAmount(select_Cart.getNetPrice() * quantity));
        tvNetPrice.setText(getAmount(select_Cart.getNetPrice()));


        tvProductSpinner.setTag(select_Cart);
        tvCodeSpinner.setTag(select_Cart);

        tvProductSpinner.setText(select_Cart.getProductName());
        tvCodeSpinner.setText(select_Cart.getProductCode());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.button_sales_addCart:

                addToCart();


               /* if (ContextCompat.checkSelfPermission( getActivity(),android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
                {

                    Toast.makeText(getActivity(), "Requires Permission", Toast.LENGTH_SHORT).show();

                  *//*  ActivityCompat.requestPermissions(
                            this,
                            new String [] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                            LocationService.MY_PERMISSION_ACCESS_COURSE_LOCATION
                    );*//*
                }else {

                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    LocationListener mlocListener = new MyLocationListener();
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    provider = locationManager.getBestProvider(criteria, true);
                locationManager.requestLocationUpdates(provider, 61000, 250,
                        mlocListener);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
                }*/

                break;
            case R.id.button_sales_makePayment:

                double saleCredit = myDatabase.getCustomerSaleCreditedAmount(SELECTED_SHOP.getShopId());
                double openingCollection = myDatabase.getCustomerTotalReceivableOpeningBalance(SELECTED_SHOP.getShopId());
                double receiptCollection = myDatabase.getCustomerWiseReceiptCollectionAmount(SELECTED_SHOP.getShopId());

                double maxCredit = (SELECTED_SHOP.getCreditLimit() - saleCredit) + openingCollection + receiptCollection;

                if (SELECTED_SHOP.getCreditLimit() < maxCredit)
                    maxCredit = SELECTED_SHOP.getCreditLimit();

                final PaymentTypeDialog paymentDialog = new PaymentTypeDialog(getActivity(), maxCredit, adapter.getNetTotal(), PAYMENT_TYPE, new PaymentTypeDialog.paymentTypeClickListener() {
                    @Override
                    public void onPaymentTypeClick(String type) {

                        PAYMENT_TYPE = type;

                        if (type.equals("Cash"))
                            paid_amount = adapter.getNetTotal();
                        else
                            paid_amount = 0;
                    }

                });

                paymentDialog.show();

                break;
            case R.id.button_sales_finish:

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_SALES:

                        if (validatePlaceOrder()) {

                            purchaseDialog();
                        }
                        break;
                    case ActivityConstants.ACTIVITY_QUOTATION:

                        if (validateQuotation())
                            saveToLocalQuotation();

                        break;
                }
                break;
        }
    }


    private void addToCart() {

        if (addCartValidate() && select_Cart != null) {

            CartItem cart = select_Cart;

            int quantity = TextUtils.isEmpty(etQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etQuantity.getText().toString().trim());

            cart.setTypeQuantity(quantity);

            Log.e("Article Code sel", "" + cart.getArticleCode());

            if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
                quantity = quantity * cart.getPiecepercart();

            cart.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
            double normalPrice = cart.getRetailPrice();

            if (SALE_TYPE.equals(SALE_WHOLESALE))
                normalPrice = cart.getWholeSalePrice();

            double netPrice = getWithoutTaxPrice(normalPrice, select_Cart.getTax());

            cart.setNetPrice(netPrice);

            cart.setProductUnit("" + spinnerCartUnit.getSelectedItem().toString());

            double discountedPrice = getPercentageValue(netPrice, cart.getDiscountPerce());
            discountedPrice = netPrice - discountedPrice;

            cart.setProductPrice(discountedPrice);

            double salePrice = getSalePrice(discountedPrice, select_Cart.getTax());

            cart.setTaxValue(getTaxPrice(discountedPrice, select_Cart.getTax()));

//            printLog(TAG,"addToCart productDisc%  "+SELECTED_SHOP.getProductDiscount()+
//                    "  normalPrice : "+normalPrice+"   netPrice  : "+netPrice+"  discountedPrice : "+discountedPrice+"  salePrice : "+salePrice);

            cart.setSalePrice(salePrice);

            cart.setPieceQuantity(quantity);
            cart.setTotalPrice(salePrice * quantity);

            cart.setOrderType(spinnerCartUnit.getSelectedItem().toString());


            // Binds all strings into an array
            adapter.changeItem(cart);

            refreshProductType();
        }
    }

    private boolean validatePlaceOrder() {

        double saleCredit = myDatabase.getCustomerSaleCreditedAmount(SELECTED_SHOP.getShopId());
        double openingCollection = myDatabase.getCustomerTotalReceivableOpeningBalance(SELECTED_SHOP.getShopId());
        double receiptCollection = myDatabase.getCustomerWiseReceiptCollectionAmount(SELECTED_SHOP.getShopId());


        double maxCredit = (SELECTED_SHOP.getCreditLimit() - saleCredit) + openingCollection + receiptCollection;


        if (SELECTED_SHOP.getCreditLimit() < maxCredit)
            maxCredit = SELECTED_SHOP.getCreditLimit();

        boolean status = false;

        if (SELECTED_SHOP == null) {
            Toast.makeText(getContext(), "Please Select Customer", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (adapter.getCartItems().isEmpty()) {
            Toast.makeText(getContext(), SELECTED_SHOP.getShopName() + "'s Cart is Empty", Toast.LENGTH_SHORT).show();
            status = false;

        } else if (adapter.getNetTotal() == 0.0) {
            Toast.makeText(getContext(), "Total Amount is 0.0", Toast.LENGTH_SHORT).show();
            status = false;

        } else if (adapter.getNetTotal() < paid_amount) {
            Toast.makeText(getContext(), "Paid Amount is Over", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (TextUtils.isEmpty(PAYMENT_TYPE)) {
            Toast.makeText(getContext(), "Please Select payment type", Toast.LENGTH_SHORT).show();
            status = false;


        } else if (PAYMENT_TYPE.equals("Credit") && adapter.getNetTotal() > maxCredit) {
            Toast.makeText(getContext(), "Insufficient Credit Limit", Toast.LENGTH_SHORT).show();
            status = false;

        } else
            status = true;

        return status;
    }


    // add LPO purchase dialog
    private void purchaseDialog() {

        final AlertDialog createDialog;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("LPO Number");

        /** Create TextView and EditText */
        final TextView tvPoNumber = new TextView(getContext());
        final EditText etPoNumber = new EditText(getContext());

        /** set Configure TextView and EditText */
        etPoNumber.setTransformationMethod(null);
        etPoNumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        etPoNumber.setBackgroundResource(R.color.colorTransparent);

        tvPoNumber.setText("Enter the LPO Number");

        /** Set Positive and Negative Buttons to Dialog */
        alertDialog.setPositiveButton("OK", null);
        alertDialog.setNegativeButton("Cancel", null);

        /** Create LinearLayout */
        LinearLayout ll = new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setPadding(20, 20, 20, 20);


        /** Add  TextView and EditText to LinearLayout*/
        ll.addView(tvPoNumber);
        ll.addView(etPoNumber);
        /** Set LinearLayout to Dialog*/

        alertDialog.setView(ll);

        createDialog = alertDialog.create();

        //        show input keyboard
        etPoNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    createDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });

        createDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {
                /** Initial Buttons */
                android.widget.Button positive = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                android.widget.Button negative = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE);
                positive.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        _poNo = etPoNumber.getText().toString().trim();


                        saveToLocalSales(_poNo);

                        /*
                        if (!TextUtils.isEmpty(_poNo)) {
                            createDialog.dismiss();
                            if (validatePlaceOrder())
                                saveToLocalSales(_poNo);
                        }else {
                            Toast.makeText(getContext(),"Invalid Po Number",Toast.LENGTH_SHORT).show();
                        }
*/

                    }
                });

                negative.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        createDialog.show();
    }

    //        method can pass sale data to local db class
    private void saveToLocalSales(String pOrderNo) {
        /*** NOTE:
         * show only total amount in print and collection discount applicable in all customer accounts (receipt)
         * */

        double collectionDiscount = 0;
        if (PAYMENT_TYPE.equals("Cash"))
            paid_amount = adapter.getGrandTotal();
        else {
            paid_amount = 0;
            collectionDiscount = getPercentageValue(adapter.getGrandTotal(), SELECTED_SHOP.getCollectionDiscount());
        }
        final String strDate = getDateTime();

        Sales sales = new Sales();

        sales.setCustomerId(SELECTED_SHOP.getShopId());
        sales.setDate(strDate);
        sales.setTotal(adapter.getGrandTotal());
        sales.setSaleType(SALE_TYPE);
        sales.setPaid(paid_amount);
        sales.setSalePo(pOrderNo);
        sales.setCartItems(adapter.getCartItems());
        sales.setDiscountTotal(adapter.getDiscountTotal());
        sales.setCollectionDiscount(collectionDiscount);


        sales.setSaleLatitude("" + str_Latitude);
        sales.setSaleLongitude("" + str_Longitude);

        String invoiceNumber = generateNewNumber(sessionValue.getInvoiceCode(SELECTED_SHOP.getRouteCode()));

        sales.setInvoiceCode(SELECTED_SHOP.getRouteCode() + invoiceNumber);

        long insertStatus = myDatabase.insertSale(sales);

        if (insertStatus != -1) {

            sessionValue.storeInvoiceCode(SELECTED_SHOP.getRouteCode(), invoiceNumber);

            for (CartItem c : sales.getCartItems()) {
                myDatabase.updateStock(c, REQ_SALE_TYPE);
            }

            if (PAYMENT_TYPE.equals("Cash")) {

                myDatabase.updateCreditBal(sales.getCustomerId(), sales.getPaid());//update credit for customer table
                myDatabase.updateDebitBal(sales.getCustomerId(), sales.getTotal() - sales.getCollectionDiscount());//update debit for customer table

            } else {

                double discountedTotal = sales.getTotal() - sales.getCollectionDiscount();

                Invoice inv = new Invoice();
                inv.setInvoiceId(sales.getInvoiceCode());
                inv.setInvoiceNo(sales.getInvoiceCode());

                inv.setTotalAmount(discountedTotal);

                inv.setBalanceAmount(discountedTotal - sales.getPaid());


                boolean invStatus = myDatabase.insertInvoice(sales.getCustomerId(), inv);

                if (invStatus) {
                    myDatabase.updateDebitBal(sales.getCustomerId(), discountedTotal);//update debit for customer table

                }
            }
            Toast.makeText(getActivity(), "Successfully", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getActivity(), SalePreviewActivity.class);
            intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_SALES);


            intent.putExtra(SALES_VALUE_KEY, sales);
            intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);

            startActivity(intent);
            getActivity().finish();


        } else
            Toast.makeText(getActivity(), "Insertion Failed", Toast.LENGTH_SHORT).show();


    }


    private boolean validateQuotation() {

        boolean status = false;

        if (SELECTED_SHOP == null) {
            Toast.makeText(getContext(), "Please Select Customer", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (adapter.getCartItems().isEmpty()) {
            Toast.makeText(getContext(), SELECTED_SHOP.getShopName() + "'s Cart is Empty", Toast.LENGTH_SHORT).show();
            status = false;


        } else
            status = true;

        return status;
    }

    //        method can pass quotation data to local db class
    private void saveToLocalQuotation() {

        final String strDate = getDateTime();

        Sales sales = new Sales();

        sales.setCustomerId(SELECTED_SHOP.getShopId());
        sales.setDate(strDate);
        sales.setTotal(adapter.getGrandTotal());
        sales.setSaleType(SALE_TYPE);
        sales.setPaid(0);
        sales.setCartItems(adapter.getCartItems());
        sales.setDiscountTotal(0);

        boolean insertStatus = myDatabase.insertQuotation(sales);


        if (insertStatus) {
            Toast.makeText(getActivity(), "Successfully", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getActivity(), SalePreviewActivity.class);
            intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_QUOTATION);

            sales.setInvoiceCode("");

            intent.putExtra(SALES_VALUE_KEY, sales);
            intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);
            startActivity(intent);
            getActivity().finish();

        } else
            Toast.makeText(getActivity(), "Insertion Failed", Toast.LENGTH_SHORT).show();

        printLog(TAG, "saveToLocalQuotation status  " + insertStatus);

    }


    private void setCartProgressBar(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            lytCart.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
        } else
            progressBar.setVisibility(View.GONE);

    }


    //set ErrorView
    private void setCartErrorView(final String title) {

        lytCart.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);

        setCartProgressBar(false);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
//                .subtitle(subTitle)
                .retryVisible(false)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getVanStockList();

            }
        });
    }


    //    sales type selection time changing all related values`
    public void initSelectedCustomer(Shop shop) {
        this.SELECTED_SHOP = shop;
    }

    //    sales type selection time changing all related values`
    public void changeSaleType(String type) {


        this.SALE_TYPE = type;
        ArrayList<CartItem> list = adapter.getCartItems();

        if (!list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {

                CartItem c = list.get(i);

                c.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
                double productPrice = c.getRetailPrice();


                if (SALE_TYPE.equals(SALE_WHOLESALE))
                    productPrice = c.getWholeSalePrice();

                c.setProductPrice(productPrice);

                double netPrice = getWithoutTaxPrice(productPrice, c.getTax());
                c.setNetPrice(netPrice);

                double discountedPrice = getPercentageValue(netPrice, c.getDiscountPerce());
                discountedPrice = netPrice - discountedPrice;

                double salePrice = getSalePrice(discountedPrice, c.getTax());

                c.setTaxValue(getTaxPrice(discountedPrice, c.getTax()));

                c.setSalePrice(salePrice);

                c.setTotalPrice(salePrice * c.getPieceQuantity());


                adapter.updateItem(c, i);
            }

        }

    }


//    change sale type retail or wholesale

    //    validation add to cart
    private boolean addCartValidate() {


        boolean status = false;


        tvProductSpinner.setError(null);
        tvCodeSpinner.setError(null);
        etQuantity.setError(null);


        if (select_Cart == null) {
            tvProductSpinner.setError("Select Product");
            return false;
        }


        int typeQuantity = TextUtils.isEmpty(etQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etQuantity.getText().toString().trim());

        int productQuantity = typeQuantity;


        if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
            productQuantity = typeQuantity * select_Cart.getPiecepercart();


        CartItem c1 = (CartItem) tvProductSpinner.getTag();
        CartItem c2 = (CartItem) tvCodeSpinner.getTag();
        if (c1 == null) {
            status = false;
            tvProductSpinner.setError("Select Product");
        } else if (c2 == null) {
            tvCodeSpinner.setError("Select Code");
            status = false;
        } else if (typeQuantity == 0) {
            etQuantity.setError("Invalid Quantity");
            status = false;

        } else if (callingActivity == ActivityConstants.ACTIVITY_SALES && productQuantity > select_Cart.getStockQuantity()) {
            etQuantity.setError("Maximum Stock is " + select_Cart.getStockQuantity());
            status = false;

        } else if (TextUtils.isEmpty(tvQtyTotal.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Total Amount is Empty...!", Toast.LENGTH_SHORT).show();
            status = false;

        } else
            status = true;

        return status;

    }

    private void refreshProductType() {

        tvProductSpinner.setText("");
        tvCodeSpinner.setText("");
        etQuantity.setText("");
        tvNetPrice.setText("");
        tvQtyTotal.setText("");
        select_Cart = null;

//      etQuantity.setFocusable(false);
        tvNetTotal.setFocusable(true);

        hideSoftKeyboard();

    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
//            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
        }
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//            inputMethodManager.showSoftInput(view, 0);
        }
    }

    private static String getDateTime() {

        Date date = new Date();
        return dbDateFormat.format(date);
    }

    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();
            String Text = "My current location is: " + "Latitude = "
                    + loc.getLatitude() + "\nLongitude = " + loc.getLongitude();

            str_Latitude = "" + loc.getLatitude();
            str_Longitude = "" + loc.getLongitude();

            //  tvNetTotal.setText("Lat : "+str_Latitude+" / Long : "+str_Longitude);
            /*Toast.makeText(getActivity(), Text, Toast.LENGTH_SHORT)
                    .show();*/
            Log.d("TAG", "Starting..");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }


}
