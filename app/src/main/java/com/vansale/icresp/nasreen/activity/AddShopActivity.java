package com.vansale.icresp.nasreen.activity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.Division;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.model.Type;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.webservice.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.webservice.WebService.webAddCustomer;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetCustomerDivisionanType;

public class AddShopActivity extends AppCompatActivity {


    protected Button addShop;
    protected TextInputEditText etShopName, etArabicName, etVat, etContactPerson, etPhone, etMail, etAddress;
    protected TextInputLayout tilShopName, tilVat, tilContactPerson, tilPhone, tilMail, tilAddress;

    protected TextView tvRoute, tvGroup;
    protected AppCompatSpinner spinnerDivision, spinnerType;


    private SessionAuth sessionAuth;
    private SessionValue sessionValue;


    private String TAG = "AddShopActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);

        spinnerDivision = (AppCompatSpinner) findViewById(R.id.spinner_addNewShop_division);
        spinnerType = (AppCompatSpinner) findViewById(R.id.spinner_addNewShop_type);

        tvRoute = (TextView) findViewById(R.id.textView_addNewShop_route);
        tvGroup = (TextView) findViewById(R.id.textView_addNewShop_group);
        etShopName = (TextInputEditText) findViewById(R.id.editText_addNewShop_name);
        etArabicName = findViewById(R.id.editText_addNewShop_arabic_name);

        etContactPerson = (TextInputEditText) findViewById(R.id.editText_addNewShop_contact_person);
        etPhone = (TextInputEditText) findViewById(R.id.editText_addNewShop_phone);
        etMail = (TextInputEditText) findViewById(R.id.editText_addNewShop_mail);
        etAddress = (TextInputEditText) findViewById(R.id.editText_addNewShop_address);
        etVat = findViewById(R.id.editText_addNewShop_vat);

        tilShopName = (TextInputLayout) findViewById(R.id.til_addNewShop_name);
        tilContactPerson = (TextInputLayout) findViewById(R.id.til_addNewShop_contact_person);
        tilVat = findViewById(R.id.til_addNewShop_vat);
        tilPhone = (TextInputLayout) findViewById(R.id.til_addNewShop_phone);
        tilMail = (TextInputLayout) findViewById(R.id.til_addNewShop_mail);
        tilAddress = (TextInputLayout) findViewById(R.id.til_addNewShop_address);


        addShop = (Button) findViewById(R.id.button_addNewShop_add);


        sessionAuth = new SessionAuth(AddShopActivity.this);

        this.sessionValue = new SessionValue(AddShopActivity.this);


        tvGroup.setText(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_GROUP));

        tvRoute.setText(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_ROUTE));


        addShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCustomer();


            }
        });


        loadCustomerType();

    }


    private void loadCustomerType() {


        if (!checkConnection()) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }


        final ArrayList<Division> divisions = new ArrayList<>();
        final ArrayList<Type> types = new ArrayList<>();
        Division d0 = new Division();
        d0.setDivisionName("CUSTOMER DIVISION ");
        d0.setDivisionId(-1);
        divisions.add(0, d0);


        Type t0 = new Type();
        t0.setTypeName("CUSTOMER TYPE ");
        t0.setTypeId(-1);
        types.add(0, t0);


        final ProgressDialog pd = ProgressDialog.show(AddShopActivity.this, null, "Please wait...", false, false);


        webGetCustomerDivisionanType(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                printLog(TAG, "loadCustomerType   " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        if (!response.isNull("Divisions")) {

                            JSONArray divisionArray = response.getJSONArray("Divisions");
                            for (int i = 0; i < divisionArray.length(); i++) {
                                JSONObject divObj = divisionArray.getJSONObject(i);

                                Division d = new Division();

                                d.setDivisionId(divObj.getInt("id"));
                                d.setDivisionName(divObj.getString("name"));

                                divisions.add(d);

                            }

                        }


                        if (!response.isNull("Types")) {

                            JSONArray typeArray = response.getJSONArray("Types");
                            for (int i = 0; i < typeArray.length(); i++) {

                                JSONObject typeObj = typeArray.getJSONObject(i);

                                Type t = new Type();

                                t.setTypeId(typeObj.getInt("id"));
                                t.setTypeName(typeObj.getString("name"));

                                types.add(t);


                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setTypeSpinner(divisions, types);


                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                setTypeSpinner(divisions, types);

                pd.dismiss();
                Toast.makeText(AddShopActivity.this, error, Toast.LENGTH_SHORT).show();

            }
        });


    }


    private void setTypeSpinner(ArrayList<Division> divisions, ArrayList<Type> types) {

        if (divisions.size() == 1 && types.size() == 1) {
            Toast.makeText(AddShopActivity.this, "No Divisions and Types", Toast.LENGTH_SHORT).show();
        } else if (divisions.size() == 1) {

            Toast.makeText(AddShopActivity.this, "No Divisions", Toast.LENGTH_SHORT).show();
        } else if (types.size() == 1) {

            Toast.makeText(AddShopActivity.this, "No Types", Toast.LENGTH_SHORT).show();
        }


        // Initializing an ArrayAdapter
        final ArrayAdapter<Division> divisionAdapter = new ArrayAdapter<Division>(this, R.layout.spinner_background_dark, divisions) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(getResources().getColor(R.color.colorGrayLight));
                }

                return view;
            }
        };


//
        divisionAdapter.setDropDownViewResource(R.layout.spinner_list);


        spinnerDivision.setAdapter(divisionAdapter);


        // Initializing an ArrayAdapter
        final ArrayAdapter<Type> typeAdapter = new ArrayAdapter<Type>(this, R.layout.spinner_background_dark, types) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(getResources().getColor(R.color.colorGrayLight));
                }

                return view;
            }
        };


//order type spinner
//        ArrayAdapter<CharSequence> orderTypeAdapter = ArrayAdapter.createFromResource(this, R.array.cart_type, R.layout.spinner_background_dark);

        typeAdapter.setDropDownViewResource(R.layout.spinner_list);


        spinnerType.setAdapter(typeAdapter);


    }


    private boolean isValidate() {
        boolean status = false;


        tilShopName.setErrorEnabled(false);
        tilContactPerson.setErrorEnabled(false);
        tilPhone.setErrorEnabled(false);
        tilMail.setErrorEnabled(false);
        tilAddress.setErrorEnabled(false);


        try {


            Division d = (Division) spinnerDivision.getSelectedItem();
            Type t = (Type) spinnerType.getSelectedItem();

            if (d == null) {
                Toast.makeText(this, "Division not Loaded", Toast.LENGTH_SHORT).show();
                status = false;
            } else if (t == null) {
                Toast.makeText(this, "Type not Loaded", Toast.LENGTH_SHORT).show();
                status = false;

            } else if (d.getDivisionId() == -1) {
                Toast.makeText(this, "Select Division", Toast.LENGTH_SHORT).show();
                status = false;

            } else if (t.getTypeId() == -1) {
                Toast.makeText(this, "Select Type", Toast.LENGTH_SHORT).show();
                status = false;

            } else if (TextUtils.isEmpty(etShopName.getText().toString().trim())) {
                tilShopName.setError("Shop Name Required..!");
                status = false;
            } else if (TextUtils.isEmpty(etContactPerson.getText().toString().trim())) {
                tilContactPerson.setError("Contact Person Required..!");
                status = false;
            } else if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
                tilPhone.setError("Phone Number Required..!");
                status = false;
            } else if (!TextUtils.isEmpty(etMail.getText().toString().trim()) && !isValidEmail(etMail.getText().toString().trim())) {
                tilMail.setError("Invalid Mail ..!");
                status = false;
            } else if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
                tilAddress.setError("Address  Required..!");
                status = false;
            } else
                status = true;


        } catch (NullPointerException e) {
            e.getMessage();
        }
        return status;
    }


    // validating email
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void addCustomer() {

        if (!isValidate())
            return;

        else if (!checkConnection()) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog pd = ProgressDialog.show(AddShopActivity.this, null, "Please wait...", false, false);


        Division d = (Division) spinnerDivision.getSelectedItem();
        Type t = (Type) spinnerType.getSelectedItem();

        JSONObject object = new JSONObject();
        JSONObject customerObj = new JSONObject();
        try {

            customerObj.put("route_id", sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_ROUTE_ID));
            customerObj.put("customer_group_id", sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_GROUP_ID));
            customerObj.put("name", etShopName.getText().toString().trim());
            customerObj.put("arabic_name", etArabicName.getText().toString().trim());
            customerObj.put("vat_number", etVat.getText().toString().trim());
            customerObj.put("contact_person", etContactPerson.getText().toString().trim());
            customerObj.put("mobile", etPhone.getText().toString().trim());
            customerObj.put("email", etMail.getText().toString().trim());
            customerObj.put("place", etAddress.getText().toString().trim());

            customerObj.put("customer_type_id", t.getTypeId());
            customerObj.put("division_id", d.getDivisionId());

            customerObj.put("credit_limit", 0);
            customerObj.put("opening_balance", 0);
            customerObj.put("description", "demo");
            object.put("Customer", customerObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        webAddCustomer(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                printLog(TAG, "Add customer response " + response);

                try {
                    if (response.getString("status").equals("success")) {


/********************
 *
 */
                        JSONObject shopObj = response.getJSONObject("customer");


                        Shop shop = new Shop();


                        int custId = shopObj.getInt("id");

                        shop.setShopId(custId);
                        shop.setShopCode(shopObj.getString("shope_code"));
                        shop.setShopName(shopObj.getString("name"));
                        shop.setVatNumber(etVat.getText().toString().trim());

                        shop.setShopArabicName(etArabicName.getText().toString().trim());
                        shop.setShopMail(shopObj.getString("email"));
                        shop.setShopMobile(shopObj.getString("mobile"));
                        shop.setShopAddress(shopObj.getString("address"));
                        shop.setCredit((float) shopObj.getDouble("credit"));
                        shop.setDebit((float) shopObj.getDouble("debit"));
                        shop.setRouteCode(shopObj.getString("route_code"));


                        shop.setVisit(false);
                        boolean b = new MyDatabase(AddShopActivity.this).insertRegisteredCustomer(shop);


/**
 *
 */


                        Toast.makeText(AddShopActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                        finish();
                    } else
                        Toast.makeText(AddShopActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(AddShopActivity.this, error, Toast.LENGTH_SHORT).show();

            }
        }, object);


    }


    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

}
