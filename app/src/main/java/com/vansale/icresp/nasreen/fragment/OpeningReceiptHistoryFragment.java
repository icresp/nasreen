package com.vansale.icresp.nasreen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvOpeningReceiptHistoryAdapter;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigKey.SHOP_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.VIEW_ERRORVIEW;
import static com.vansale.icresp.nasreen.config.ConfigKey.VIEW_RECYCLERVIEW;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;

public class OpeningReceiptHistoryFragment extends Fragment {

    private String TAG = "OpeningReceiptHistoryFragment";
    private RecyclerView recyclerView;
    private ErrorView errorView;
    private ViewGroup viewSalesReport;
    private MyDatabase myDatabase;
    private RvOpeningReceiptHistoryAdapter adapter;

    private ArrayList<OpeningBalanceTransaction> receiptHistoryList;

    private static final String ARG_SHOP = "key_shop";


    private Shop SELECTED_SHOP;


    public OpeningReceiptHistoryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static OpeningReceiptHistoryFragment newInstance(Shop shop) {
        OpeningReceiptHistoryFragment fragment = new OpeningReceiptHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOP, shop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SELECTED_SHOP = (Shop) getArguments().getSerializable(ARG_SHOP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice_receipt_history, container, false);
//        View view= inflater.inflate(R.layout.fragment_opening_receipt_history, container, false);
        recyclerView = view.findViewById(R.id.recyclerView_receipt_history);

        errorView = view.findViewById(R.id.errorView_receipt_history);

        viewSalesReport = view.findViewById(R.id.view_receipt_history);


        myDatabase = new MyDatabase(getActivity());
        receiptHistoryList = new ArrayList<>();


        try {
            SELECTED_SHOP = (Shop) getActivity().getIntent().getSerializableExtra(SHOP_KEY);
        } catch (Exception e) {
            e.getStackTrace();
        }

        if (SELECTED_SHOP == null) {
            getActivity().finish();

        }

        adapter = new RvOpeningReceiptHistoryAdapter(receiptHistoryList, SELECTED_SHOP);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());
        recyclerView.setAdapter(adapter);

        setRecyclerView();

        return view;
    }


    private void setRecyclerView() {

        ArrayList<OpeningBalanceTransaction> li = myDatabase.getCustomerOpeningBalancesTransactions(SELECTED_SHOP.getShopId());

        receiptHistoryList.addAll(li);

        printLog(TAG, "setRecyclerView   li   " + li.size());


        if (receiptHistoryList.isEmpty()) {
            setErrorView();
            return;
        }


        adapter.notifyDataSetChanged();
        updateViews(VIEW_RECYCLERVIEW);

    }


    //set ErrorView
    private void setErrorView() {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title("No Receipts")
//                .subtitle(subTitle)
                .retryVisible(false)
                .build());

    }


    public void updateViews(int viewCode) {


        switch (viewCode) {

            case VIEW_RECYCLERVIEW:

                viewSalesReport.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:

                viewSalesReport.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);

                break;


        }


    }
}
