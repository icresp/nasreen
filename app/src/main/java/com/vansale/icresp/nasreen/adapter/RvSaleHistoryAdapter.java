package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.SalePreviewActivity;
import com.vansale.icresp.nasreen.holder.RvHistoryHolder;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;

import java.util.ArrayList;
import java.util.Date;

import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALES_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dateToFormat;
import static com.vansale.icresp.nasreen.config.Generic.stringToDate;
import static com.vansale.icresp.nasreen.config.Generic.timeToFormat;


/**
 * Created by mentor on 15/1/18.
 */

public class RvSaleHistoryAdapter extends RecyclerView.Adapter<RvHistoryHolder> {

    private ArrayList<Sales> salesList;
    private Context context;

    private Shop SELECTED_SHOP;

    public RvSaleHistoryAdapter(ArrayList<Sales> salesList, Shop shop) {
        this.salesList = salesList;
        this.SELECTED_SHOP = shop;
    }

    @Override
    public RvHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_report, parent, false);

        this.context = parent.getContext();
        return new RvHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(RvHistoryHolder holder, int position) {
        final Sales sale = salesList.get(position);

        int s = position + 1;

        final Date date = stringToDate(sale.getDate());


        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvInvoiceNo.setText(sale.getInvoiceCode());
        holder.tvDate.setText(dateToFormat(date));
        holder.tvTime.setText(timeToFormat(date));
        holder.tvQty.setText(String.valueOf(sale.getCartItems().size()));
        holder.ibView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SalePreviewActivity.class);
                intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_SALE_REPORT);

                intent.putExtra(SALES_VALUE_KEY, sale);
                intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);

                context.startActivity(intent);
//                getActivity().finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != salesList ? salesList.size() : 0);
    }


}
