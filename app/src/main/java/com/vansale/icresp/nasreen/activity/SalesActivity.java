package com.vansale.icresp.nasreen.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.fragment.SalesFragment;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;

import static com.vansale.icresp.nasreen.config.ConfigKey.SHOP_KEY;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;

public class SalesActivity extends AppCompatActivity implements View.OnClickListener {


    protected DrawerLayout drawer;
    protected RelativeLayout relativeDrawer;
    String TAG = "SalesActivity";

    String EXECUTIVE_ID = "";
    private ViewGroup viewSaleType;
    private AppCompatSpinner spinnerSaleType;
    private Button btnSaleList;
    private TextView tvToolBarShopName, tvShopName, tvShopType, tvShopAddress, tvShopEmail, tvShopPhone, tvBalance, tvCredit, tvDrawerTitle;

    private ImageButton ibDrawer, ibBack;

    private Shop SELECTED_SHOP = null;

    private SalesFragment salesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_sales);
        relativeDrawer = (RelativeLayout) findViewById(R.id.relativeLayout_drawer);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ibDrawer = (ImageButton) findViewById(R.id.imageButton_drawer);
        ibBack = (ImageButton) findViewById(R.id.imageButton_toolbar_back);

        viewSaleType = (ViewGroup) findViewById(R.id.viewGroup_saleType);

        spinnerSaleType = (AppCompatSpinner) findViewById(R.id.spinner_toolbar_ShopType);

        btnSaleList = (Button) findViewById(R.id.button_sales_view);

        tvDrawerTitle = (TextView) findViewById(R.id.textView_drawer_title);

        tvToolBarShopName = (TextView) findViewById(R.id.textView_toolbar_shopNameAndCode);

        tvShopName = (TextView) findViewById(R.id.textView_sales_ShopName);
        tvShopType = (TextView) findViewById(R.id.textView_sales_ShopType);

        tvShopAddress = (TextView) findViewById(R.id.textView_sales_address);
        tvShopEmail = (TextView) findViewById(R.id.textView_sales_email);
        tvShopPhone = (TextView) findViewById(R.id.textView_sales_phone);

        tvCredit = (TextView) findViewById(R.id.textView_percentageBar_credit);
        tvBalance = (TextView) findViewById(R.id.textView_percentageBar_balance);

        viewSaleType.setVisibility(View.VISIBLE);
        FragmentManager fm = getSupportFragmentManager();

//if you added fragment via layout xml
        salesFragment = (SalesFragment) fm.findFragmentById(R.id.fragment_sales);

        try {
            SELECTED_SHOP = (Shop) getIntent().getSerializableExtra(SHOP_KEY);
            EXECUTIVE_ID = new SessionAuth(SalesActivity.this).getExecutiveId();
        } catch (Exception e) {
            e.getStackTrace();
        }

        printLog(TAG, "Shop  Type " + SELECTED_SHOP.getTypeId());

        if (SELECTED_SHOP == null) {
            finish();
            return;

        }

        setDrawerLayout();

        seShopView(SELECTED_SHOP);

        setShopTypeSpinner();

        ibDrawer.setOnClickListener(this);

        btnSaleList.setOnClickListener(this);

        tvDrawerTitle.setOnClickListener(this);
        ibBack.setOnClickListener(this);

    }

    //nav Drawer layout
    private void setDrawerLayout() {

        int width = getResources().getDisplayMetrics().widthPixels / 3;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) relativeDrawer.getLayoutParams();
        params.width = width;
        relativeDrawer.setLayoutParams(params);

    }

    private void setShopTypeSpinner() {

        //spinner_background
        ArrayAdapter<CharSequence> shopTypeAdapter = ArrayAdapter.createFromResource(this, R.array.shop_type, R.layout.spinner_background);

        shopTypeAdapter.setDropDownViewResource(R.layout.spinner_list);

        tvShopType.setText(shopTypeAdapter.getItem(0));

        spinnerSaleType.setAdapter(shopTypeAdapter);

        spinnerSaleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                final String item = parent.getSelectedItem().toString();
                tvShopType.setText(item);

                changeSaleType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    //    delivered report graph (custom view)
    public void setBalanceGraph(double total, double balanced) {

        float percentage = 0.0f;

//        get Percentage  payment in TotalAmount
        percentage = (float) ((balanced * 100) / total);

        /**
         Dividing  percentage value in 100
         * this value use view(Custom Graph Layout) width
         */
        percentage = percentage / 100;


        View view = findViewById(R.id.view_child_balance);

        PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams) view.getLayoutParams();
// This will currently return null, if it was not constructed from XML.
        PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
        info.widthPercent = percentage;
        view.requestLayout();

        tvBalance.setText(String.valueOf(balanced));

    }

    //    delivered report graph (custom view)
    public void setCreditGraph(double total, double credit) {

        float percentage = 0.0f;

//        get Percentage  payment in TotalAmount
        percentage = (float) ((credit * 100) / total);

        /**
         Dividing  percentage value in 100
         * this value use view(Custom Graph Layout) width
         */
        percentage = percentage / 100;

        View view = findViewById(R.id.view_child_credit);
        PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams) view.getLayoutParams();
     // This will currently return null, if it was not constructed from XML.
        PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
        info.widthPercent = percentage;
        view.requestLayout();

        tvCredit.setText(String.valueOf(credit));

       }

       //    shops
    private void seShopView(Shop shop) {

        tvToolBarShopName.setText(String.valueOf(SELECTED_SHOP.getShopName() + "\t" + SELECTED_SHOP.getShopCode()));
        tvToolBarShopName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvToolBarShopName.setSelected(true);

        salesFragment.initSelectedCustomer(shop);

        tvShopName.setText(shop.getShopName());

        tvShopEmail.setText(shop.getShopMail());
        tvShopAddress.setText(shop.getShopAddress());
        tvShopPhone.setText(shop.getShopMobile());

        setCreditGraph(shop.getCredit() + shop.getDebit(), shop.getCredit());
        setBalanceGraph(shop.getCredit() + shop.getDebit(), shop.getDebit());

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageButton_drawer:
                if (drawer.isDrawerOpen(GravityCompat.START))
                    drawer.closeDrawer(GravityCompat.START);
                else
                    drawer.openDrawer(GravityCompat.START);

                break;
            case R.id.textView_drawer_title:

                if (drawer.isDrawerOpen(Gravity.START))
                    drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.imageButton_toolbar_back:
                onBackPressed();
                break;

            case R.id.button_sales_view:

                Intent intent = new Intent(getApplicationContext(), SaleHistoryActivity.class);
                intent.putExtra(SHOP_KEY, SELECTED_SHOP);
                startActivity(intent);

                break;
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void changeSaleType() {

        if (salesFragment != null)
            salesFragment.changeSaleType(spinnerSaleType.getSelectedItem().toString());

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {  /*Closes the Appropriate Drawer*/
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

/*
    public interface ActivityConstants {
        public static final int ACTIVITY_SALES = 1001; //call Sales Activity
        public static final int ACTIVITY_QUOTATION = 1002; //call quotation Activity
    }*/
}
