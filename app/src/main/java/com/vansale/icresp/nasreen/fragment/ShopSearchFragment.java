package com.vansale.icresp.nasreen.fragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvShopAdapter;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CustomerProduct;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.VIEW_ERRORVIEW;
import static com.vansale.icresp.nasreen.config.ConfigKey.VIEW_PROGRESSBAR;
import static com.vansale.icresp.nasreen.config.ConfigKey.VIEW_RECYCLERVIEW;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.webservice.WebService.webAllRouteShop;


public class ShopSearchFragment extends Fragment implements View.OnClickListener {


    String TAG = "ShopSearchFragment";
    private ArrayList<Shop> shops = new ArrayList<>();


    private SessionValue sessionValue;
    private SessionAuth sessionAuth;
    private RvShopAdapter shopAdapter;
    private ImageButton ibRefresh, ibBack;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ErrorView errorView;
    private SearchView searchView;
    private MyDatabase myDatabase;
    private String EXECUTIVE_ID = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shop_search, container, false);


        searchView = (SearchView) view.findViewById(R.id.searchView_shopList);
        ibBack = (ImageButton) view.findViewById(R.id.imageButton_shop_search_back);
        ibRefresh = (ImageButton) view.findViewById(R.id.imageButton_shop_search_refresh);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        errorView = (ErrorView) view.findViewById(R.id.errorView);

        sessionValue = new SessionValue(getActivity());
        sessionAuth = new SessionAuth(getActivity());


        myDatabase = new MyDatabase(getActivity());

        shopAdapter = new RvShopAdapter(shops);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(shopAdapter);


        try {
            EXECUTIVE_ID = sessionAuth.getExecutiveId();
        } catch (Exception e) {
            e.getMessage();
        }


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                shopAdapter.getFilter().filter(newText);
                return true;
            }
        });

        ibBack.setOnClickListener(this);
        ibRefresh.setOnClickListener(this);

        setRecyclerView();


        return view;
    }

    //get Shops
    private void getAllRouteCustomer() {


        if (!checkConnection()) {
            setErrorView(getActivity().getString(R.string.no_internet), "", false);
            return;
        }

        updateViews(VIEW_PROGRESSBAR);

        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayList<Shop> list = new ArrayList<>();
        list.clear();
        webAllRouteShop(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                Log.v(TAG, "getAllRouteCustomer  response " + response);

                try {

                    if (response.getString("status").equals("success") && !response.isNull("Customers")) {


                        JSONArray array = response.getJSONArray("Customers");
                        new AsyncTaskLoadData().execute(array);

                    }


                } catch (JSONException e) {


                    e.getMessage();
                }


            }

            @Override
            public void onErrorResponse(String error) {


                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
            }
        }, object);


    }


    //    customer list with invoices to sqllite db
    private void storeCustomersToOffline(ArrayList<Shop> list) {

        for (Shop s : list) {

            boolean b = myDatabase.insertUnRegisteredCustomer(s);
            Log.v(TAG, "insertion   " + b);

        }

        setRecyclerView();

    }


    private void setRecyclerView() {

        ArrayList<Shop> list = myDatabase.getUnRegisteredCustomers();
        if (list.isEmpty()) {
            setErrorView("No Shops", "", false);
            return;
        }


        shops.clear();
        shops.addAll(list);
        shopAdapter.notifyDataSetChanged();
        updateViews(VIEW_RECYCLERVIEW);

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                getAllRouteCustomer();

            }
        });
    }


    public void updateViews(int viewCode) {

        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                progressBar.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.GONE);
                break;
            case VIEW_RECYCLERVIEW:
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);

                break;


        }


    }


    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskLoadData extends AsyncTask<JSONArray, String, String> {
        private final static String TAG = "AsyncTaskLoadImage";


        @Override
        protected String doInBackground(JSONArray... jsonArrays) {


            JSONArray array = jsonArrays[0];
            try {

                for (int i = 0; i < array.length(); i++) {

                    JSONObject shopObj = array.getJSONObject(i);


                    printLog(TAG, "getAllRouteCustomer  response " + shopObj);

                    Shop shop = new Shop();

                    int custId = shopObj.getInt("id");

                    shop.setShopId(custId);
                    shop.setShopCode(shopObj.getString("shope_code"));
                    shop.setShopName(shopObj.getString("name"));
                    shop.setShopArabicName(shopObj.getString("arabic_name"));
                    shop.setVatNumber(shopObj.getString("vat_no"));
                    shop.setCreditLimit(shopObj.getDouble("credit_limit"));
                    shop.setShopMail(shopObj.getString("email"));
                    shop.setShopMobile(shopObj.getString("mobile"));
                    shop.setShopAddress(shopObj.getString("address"));
                    shop.setCredit(shopObj.getDouble("credit"));
                    shop.setDebit(shopObj.getDouble("debit"));

                    shop.setRouteCode(shopObj.getString("route_code"));
                    shop.setOpeningBalance(shopObj.getDouble("opening_balance"));
                    shop.setProductDiscount((float) shopObj.getDouble("product_discount"));
                    shop.setCollectionDiscount((float) shopObj.getDouble("collection_discount"));


                    ArrayList<Invoice> invoices = new ArrayList<>();

                    if (!shopObj.isNull("receipt")) {


                        JSONArray invoiceArray = shopObj.getJSONArray("receipt");


                        for (int j = 0; j < invoiceArray.length(); j++) {
                            JSONObject invObj = invoiceArray.getJSONObject(j);

                            Invoice inv = new Invoice();
                            inv.setInvoiceId(invObj.getString("sale_id"));
                            inv.setInvoiceNo(invObj.getString("invoice_no"));
                            inv.setTotalAmount(invObj.getDouble("total"));
                            inv.setBalanceAmount(invObj.getDouble("balance"));
                            invoices.add(inv);

                        }


                    }


                    ArrayList<CustomerProduct> customerProducts = new ArrayList<>();

                    if (!shopObj.isNull("Customer_Products")) {

                        JSONArray productArray = shopObj.getJSONArray("Customer_Products");

                        for (int j = 0; j < productArray.length(); j++) {
                            JSONObject invObj = productArray.getJSONObject(j);
                            CustomerProduct p = new CustomerProduct();
                            p.setProductId(invObj.getInt("product_id"));
                            p.setCustomerId(custId);
                            p.setPrice(invObj.getDouble("selling_rate"));

                            customerProducts.add(p);

                        }
                    }


                    shop.setVisit(true);
                    shop.setInvoices(invoices);
                    shop.setProducts(customerProducts);
                    boolean b = myDatabase.insertUnRegisteredCustomer(shop);

                }

            } catch (JSONException e) {

                Log.v(TAG, "getAllRouteCustomer  exception " + e.getMessage());


            }

            return "";
        }

        @Override
        protected void onPreExecute() {
            updateViews(VIEW_PROGRESSBAR);
            /*
            //show progress dialog while image is loading
            progress = new ProgressDialog(getContext());
            progress.setMessage("Please Wait....");
            progress.setCancelable(false);
            progress.show();
            */
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRecyclerView();

                }
            }, 500);

        }
    }

    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageButton_shop_search_refresh:

                getAllRouteCustomer();

                break;
            case R.id.imageButton_shop_search_back:
                getActivity().onBackPressed();

                break;

        }

    }


}
