package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.ReceiptPreviewActivity;
import com.vansale.icresp.nasreen.holder.RvHistoryHolder;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Shop;

import java.util.ArrayList;
import java.util.Date;

import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.OPENING_BALANCE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dateToFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.Generic.stringToDate;
import static com.vansale.icresp.nasreen.config.Generic.timeToFormat;

public class RvOpeningReceiptHistoryAdapter extends RecyclerView.Adapter<RvHistoryHolder> {


    private ArrayList<OpeningBalanceTransaction> historyList;

    private Context context;

    private Shop SELECTED_SHOP;


    public RvOpeningReceiptHistoryAdapter(ArrayList<OpeningBalanceTransaction> list, Shop SELECTED_SHOP) {
        this.historyList = list;
        this.SELECTED_SHOP = SELECTED_SHOP;
    }

    @NonNull
    @Override
    public RvHistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_report, parent, false);
        this.context = parent.getContext();
        return new RvHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvHistoryHolder holder, int position) {


        final OpeningBalanceTransaction item = historyList.get(position);


        int s = position + 1;


        final Date date = stringToDate(item.getLogDate());

        holder.tvDate.setText(dateToFormat(date));
        holder.tvTime.setText(timeToFormat(date));
        holder.tvQty.setText(getAmount(item.getReceivedAmount()) + context.getString(R.string.currency));

        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvInvoiceNo.setText(item.getReceiptNo() + " (" + item.getTransactionType() + ")");


        holder.ibView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReceiptPreviewActivity.class);


                intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_OPENING_BALANCE_RECEIPT);
                intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);
                intent.putExtra(OPENING_BALANCE_KEY, item);

                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != historyList ? historyList.size() : 0);
    }
}
