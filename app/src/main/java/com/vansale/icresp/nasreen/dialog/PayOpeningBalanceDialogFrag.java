package com.vansale.icresp.nasreen.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RECEIPT_TYPE;
import static com.vansale.icresp.nasreen.config.Generic.dateFormat;
import static com.vansale.icresp.nasreen.config.Generic.generateNewNumber;

@SuppressLint("ValidFragment")
public class PayOpeningBalanceDialogFrag extends DialogFragment implements View.OnClickListener {


    String TAG = "PayOpeningBalanceDialogFrag";
    private OpeningBalanceClickListener listener;
    private Button btnOk, btnCancel;
    private TextView tvName, tvDate, tvBalancelAmount, tvIssueDate, tvClearDate;
    private EditText etPaid;

    private Shop SELECTED_SHOP;
    private MyDatabase myDatabase;

    private double openingBalance;

    private SessionValue sessionValue;


    private EditText etCheque, etBankName, etRemarks;

    private ViewGroup chequeView;

    private RadioGroup rgType;

    private Calendar issueDateCalendar;
    private Calendar clearingDateCalendar;

    private DatePickerDialog issueDatePicker;
    private DatePickerDialog clearingDatePicker;

    @SuppressLint("ValidFragment")
    public PayOpeningBalanceDialogFrag(Shop customer, OpeningBalanceClickListener listener) {

        this.listener = listener;

        this.SELECTED_SHOP = customer;


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_make_pay_opening_balance, container, false);

    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        // Do all the stuff to initialize your custom view


        btnOk = (Button) v.findViewById(R.id.button_opening_Dialog_ok);
        btnCancel = (Button) v.findViewById(R.id.button_opening_Dialog_cancel);
        tvDate = (TextView) v.findViewById(R.id.textView_opening_Dialog_date);
        tvName = (TextView) v.findViewById(R.id.textView_opening_Dialog_name);

        tvIssueDate = (TextView) v.findViewById(R.id.textView_opening_Dialog_issue_date);
        tvClearDate = (TextView) v.findViewById(R.id.textView_opening_Dialog_clear_date);

        tvBalancelAmount = (TextView) v.findViewById(R.id.textView_opening_Dialog_balance);
        etPaid = (EditText) v.findViewById(R.id.editText_opening_Dialog_paidAmount);


        rgType = (RadioGroup) v.findViewById(R.id.radioGroup_opening_Dialog);

        etCheque = (EditText) v.findViewById(R.id.editText_opening_Dialog_chequeNumber);
        etBankName = (EditText) v.findViewById(R.id.editText_opening_Dialog_bankName);
        etRemarks = (EditText) v.findViewById(R.id.editText_opening_Dialog_remarks);
        chequeView = (ViewGroup) v.findViewById(R.id.view_opening_Dialog_chequeView);


        this.sessionValue = new SessionValue(getContext());

        myDatabase = new MyDatabase(getContext());

        issueDateCalendar = Calendar.getInstance();
        clearingDateCalendar = Calendar.getInstance();

        openingBalance = myDatabase.getCustomerOpeningBalance(SELECTED_SHOP.getShopId());


        tvBalancelAmount.setText(String.valueOf("Balance : " + openingBalance + getContext().getString(R.string.currency)));


        tvName.setText(SELECTED_SHOP.getShopName());


        final String strDate = getTodayDate();
        tvDate.setText(strDate);


        //        show input keyboard
        etPaid.requestFocus();
        etPaid.setFocusableInTouchMode(true);
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());


                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    // Changes the textview's text to "Checked: example radiobutton text"

                    int rbTypeId = checkedRadioButton.getId();
                    viewEnable(rbTypeId == R.id.radioButton_opening_Dialog_cheque);
                }
            }
        });
        initialise();
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        tvIssueDate.setOnClickListener(this);
        tvClearDate.setOnClickListener(this);

    }

    private void viewEnable(boolean isEnable) {
        etRemarks.setEnabled(isEnable);
        etBankName.setEnabled(isEnable);
        etCheque.setEnabled(isEnable);

        chequeView.setVisibility(isEnable ? View.VISIBLE : View.GONE);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }


    private void initialise() {


        issueDatePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        issueDateCalendar.set(year, monthOfYear, dayOfMonth);
                        tvIssueDate.setText(dateFormat.format(issueDateCalendar.getTime()));


                        if (issueDateCalendar.getTime().after(clearingDateCalendar.getTime())) {
                            clearingDateCalendar.set(year, monthOfYear, dayOfMonth);
                            tvClearDate.setText(dateFormat.format(clearingDateCalendar.getTime()));
                        }
                    }

                },
                issueDateCalendar.get(Calendar.YEAR), // Initial year selection
                issueDateCalendar.get(Calendar.MONTH), // Initial month selection
                issueDateCalendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
//////

        clearingDatePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        clearingDateCalendar.set(year, monthOfYear, dayOfMonth);

                        tvClearDate.setText(dateFormat.format(clearingDateCalendar.getTime()));
                    }

                },
                clearingDateCalendar.get(Calendar.YEAR), // Initial year selection
                clearingDateCalendar.get(Calendar.MONTH), // Initial month selection
                clearingDateCalendar.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );


        /****/


        issueDatePicker.setMaxDate(Calendar.getInstance());

    }


    @Override
    public void onClick(View v) {

        hideSoftKeyboard();
        switch (v.getId()) {


            case R.id.textView_opening_Dialog_issue_date:
                issueDatePicker.show(getActivity().getFragmentManager(), "issueDatePickerDialog");
                break;
            case R.id.textView_opening_Dialog_clear_date:
                clearingDatePicker.setMinDate(Calendar.getInstance());
//                clearingDatePicker.setMinDate(issueDateCalendar);
                clearingDatePicker.show(getActivity().getFragmentManager(), "clearingDatePickerDatePickerDialog");

                break;
            case R.id.button_opening_Dialog_ok:
                if (isValidate()) {

                    try {

                        double paid = TextUtils.isEmpty(etPaid.getText().toString().trim()) ? 0 : Double.valueOf(etPaid.getText().toString().trim());

                        RadioButton rbType = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());

                        String cheque = "", bank = "", remarks = "", issueDate = "", clearDate = "";
                        if (rbType.getText().toString().trim().equals(getActivity().getString(R.string.cheque))) {
                            cheque = TextUtils.isEmpty(etCheque.getText().toString().trim()) ? "" : etCheque.getText().toString().trim();
                            bank = TextUtils.isEmpty(etBankName.getText().toString().trim()) ? "" : etBankName.getText().toString().trim();
                            remarks = TextUtils.isEmpty(etRemarks.getText().toString().trim()) ? "" : etRemarks.getText().toString().trim();
                            issueDate = TextUtils.isEmpty(tvIssueDate.getText().toString().trim()) ? "" : tvIssueDate.getText().toString().trim();
                            clearDate = TextUtils.isEmpty(tvClearDate.getText().toString().trim()) ? "" : tvClearDate.getText().toString().trim();
                        }

                        final String receiptNo = generateNewNumber(sessionValue.getReceiptCode(SELECTED_SHOP.getRouteCode()));

                        OpeningBalanceTransaction tra = new OpeningBalanceTransaction();
                        tra.setCustomerId(SELECTED_SHOP.getShopId());
                        tra.setReceivedAmount(paid);
                        tra.setBalanceAmount(openingBalance - paid);
                        tra.setTransactionType(rbType.getText().toString().trim());
                        tra.setChequeNumber(cheque);
                        tra.setBankName(bank);
                        tra.setRemark(remarks);
                        tra.setIssueDate(issueDate);
                        tra.setClearDate(clearDate);
                        tra.setReceiptNo(SELECTED_SHOP.getRouteCode() + receiptNo);


                        if (myDatabase.insertOpeningTransactionLog(tra)) {
                            sessionValue.storeReceiptCode(SELECTED_SHOP.getRouteCode(), receiptNo);
                            myDatabase.updateVisitStatus(SELECTED_SHOP.getShopId(), REQ_RECEIPT_TYPE, "", "", "");
                            if (!myDatabase.updateOpeningBal(SELECTED_SHOP.getShopId(), openingBalance - paid) || !myDatabase.updateCreditBal(SELECTED_SHOP.getShopId(), paid))//update opening balance for customer table
                                Toast.makeText(getContext(), "status update failed", Toast.LENGTH_SHORT).show();

                            if (listener != null)
                                listener.onPaymentClick(tra);
                            dismiss();

                        } else {
                            Toast.makeText(getContext(), "Payment Failed", Toast.LENGTH_SHORT).show();
                        }

                    } catch (NumberFormatException e) {
                        e.getMessage();
                    }
                }

                break;
            case R.id.button_opening_Dialog_cancel:

                dismiss();
                break;

        }

    }

    boolean isValidate() {


        boolean status = false;

        etCheque.setError(null);
        etBankName.setError(null);

        etPaid.setError(null);


        float paid = 0.0f;
        try {

            paid = TextUtils.isEmpty(etPaid.getText().toString().trim()) ? 0 : Float.valueOf(etPaid.getText().toString().trim());
        } catch (NumberFormatException e) {
            e.getMessage();
        }

        RadioButton checkedRadioButton = (RadioButton) rgType.findViewById(rgType.getCheckedRadioButtonId());


        if (TextUtils.isEmpty(etPaid.getText().toString().trim())) {
            status = false;
            etPaid.setError("Paid Balance is empty");


        } else if (paid > openingBalance) {

            status = true;

            // If opening balance 0, cant enter amount

            /*status = false;
            etPaid.setError("Amount should be less than due Total Amount..!");*/
        } else if (checkedRadioButton == null) {
            Toast.makeText(getContext(), "Please select type", Toast.LENGTH_SHORT).show();
        } else {


            int rbTypeId = checkedRadioButton.getId();

            if (rbTypeId == R.id.radioButton_opening_Dialog_cash)
                status = true;

            else if (TextUtils.isEmpty(tvIssueDate.getText().toString().trim()))
                Toast.makeText(getContext(), "Please select Issue Date", Toast.LENGTH_SHORT).show();

            else if (TextUtils.isEmpty(tvClearDate.getText().toString().trim()))
                Toast.makeText(getContext(), "Please select ClearingDate", Toast.LENGTH_SHORT).show();

            else if (TextUtils.isEmpty(etCheque.getText().toString().trim())) {
                etCheque.setError("Cheque is empty");
            } else if (TextUtils.isEmpty(etBankName.getText().toString().trim())) {
                etBankName.setError("Bank name is empty");
            } else
                status = true;

        }
        return status;


    }

    @Override
    public void onStop() {
        super.onStop();

        hideSoftKeyboard();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public interface OpeningBalanceClickListener {

        void onPaymentClick(OpeningBalanceTransaction balance);
    }


    private static String getTodayDate() {

        return dateFormat.format(Calendar.getInstance().getTime());
    }

}
