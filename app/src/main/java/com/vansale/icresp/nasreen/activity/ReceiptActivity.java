package com.vansale.icresp.nasreen.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.ReceiptPreviewActivity;
import com.vansale.icresp.nasreen.adapter.RvReceiptAdapter;
import com.vansale.icresp.nasreen.adapter.RvRefundAdapter;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.dialog.PayOpeningBalanceDialogFrag;
import com.vansale.icresp.nasreen.dialog.ReceiptTypeDialogFrag;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.listener.ReceiptTypeClickListener;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.OpeningBalanceTransaction;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.Refund;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.textwatcher.TextValidator;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import static com.vansale.icresp.nasreen.config.ConfigKey.CUSTOMER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.DAY_REGISTER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RECEIPT_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigKey.SHOP_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.INVOICE_LIST_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.OPENING_BALANCE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PAID_AMOUNT_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.RECEIPT_NO_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.convertTotalTransactionsAmount;
import static com.vansale.icresp.nasreen.config.Generic.dbDateFormat;
import static com.vansale.icresp.nasreen.config.Generic.generateNewNumber;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetAllRefund;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetReceipt;
import static com.vansale.icresp.nasreen.webservice.WebService.webPaidRefunds;

public class ReceiptActivity extends AppCompatActivity implements View.OnClickListener, OnNotifyListener {

    private String EXECUTIVE_ID = "", strSwitchStatus = "",    dayRegId="";

    private TextView tvToolBarShopName, tvTotalAmount, tv_RefundTotal;
    private EditText etPaidAmount, et_RefundPaidAmount;
    double bal_total=0, openingBalance=0;
    String outstanding_bal="", str_invoice_selected="";

    ArrayList<String> invoice_array = new ArrayList<>();

    private ProgressBar progressBar;
    private ErrorView errorView;
    private ImageButton ibBack;
    private Button btnFinish, btnRefundFinish;
    private RecyclerView recyclerView, recycler_refund;

    private FloatingActionButton fabOpeningBalance;
    private AppCompatSpinner spinner_paymentmode, spinner_receipt_invoice;

    private ArrayList<Receipt> receipts = new ArrayList<Receipt>();
    private ArrayList<Refund> refunds = new ArrayList<Refund>();

    private RvReceiptAdapter adapter;
    private RvRefundAdapter refundAdapter;

    private String TAG = "ReceiptActivity", str_paymentmode="";

    private LinearLayout listLayout, refundLayout;

    private Shop SELECTED_SHOP = null;

    private MyDatabase myDatabase;
    private SessionValue sessionValue;

    private SwitchCompat switchtype;
    double invoice_bal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        ibBack = (ImageButton) findViewById(R.id.imageButton_toolbar_back);
        switchtype = (SwitchCompat)findViewById(R.id.switch_type);
        tvToolBarShopName = (TextView) findViewById(R.id.textView_toolbar_shopNameAndCode);
        btnFinish = (Button) findViewById(R.id.button_receipt_finish);
        tvTotalAmount = (TextView) findViewById(R.id.textView_receipt_totalAmount);
        etPaidAmount = (EditText) findViewById(R.id.EditText_receipt_paid);
        errorView = (ErrorView) findViewById(R.id.errorView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_receipt);
        listLayout = (LinearLayout) findViewById(R.id.layout_receipt_lists);
        refundLayout = (LinearLayout)findViewById(R.id.layout_refund_lists);
        fabOpeningBalance = (FloatingActionButton) findViewById(R.id.fab_receipt_openingBalance);
        spinner_paymentmode = (AppCompatSpinner)findViewById(R.id.spinner_receipt_payment_mode);
        spinner_receipt_invoice = (AppCompatSpinner)findViewById(R.id.spinner_receipt_invoice);

        // Refund fields
        recycler_refund = (RecyclerView)findViewById(R.id.recyclerView_refund);
        tv_RefundTotal = (TextView) findViewById(R.id.textView_refund_totalAmount);
        et_RefundPaidAmount = (EditText)findViewById(R.id.EditText_refund_paid);
        btnRefundFinish = (Button)findViewById(R.id.button_refund_finish);

        this.sessionValue = new SessionValue(this);
        myDatabase = new MyDatabase(ReceiptActivity.this);
        adapter = new RvReceiptAdapter(receipts);
        refundAdapter = new RvRefundAdapter(refunds);

        switchtype.setChecked(true);
        switchtype.setText("Receipt");
        listLayout.setVisibility(View.VISIBLE);
        refundLayout.setVisibility(View.INVISIBLE);
        switchtype.setTextColor(getResources().getColor(R.color.text_green));


        switchtype.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    switchtype.setText("Receipt");
                    strSwitchStatus = "Receipt";
                    switchtype.setTextColor(getResources().getColor(R.color.text_green));
                    listLayout.setVisibility(View.VISIBLE);
                    refundLayout.setVisibility(View.INVISIBLE);
                    //  getInvoiceList();

                    etPaidAmount.setText("");

                   // adapter.notifyDataSetChanged();

                }else
                    {

                    et_RefundPaidAmount.setText("");
                    switchtype.setText("Refund");
                    strSwitchStatus = "Refund";
                    switchtype.setTextColor(getResources().getColor(R.color.colorRed));
                    listLayout.setVisibility(View.INVISIBLE);
                    refundLayout.setVisibility(View.VISIBLE);
                      //  Log.e("Refund", "calling");
                    loadOnlineRefunds();

                    //  setErrorView("No Refund for Offline", "", true);
                   }
                 }
             });

        try {

            SELECTED_SHOP = (Shop) getIntent().getSerializableExtra(SHOP_KEY);
            EXECUTIVE_ID = new SessionAuth(ReceiptActivity.this).getExecutiveId();
            this.dayRegId = sessionValue.getDayRegisterId();

            openingBalance = myDatabase.getCustomerOpeningBalance(SELECTED_SHOP.getShopId());
            outstanding_bal = ""+getAmount(SELECTED_SHOP.getDebit()-SELECTED_SHOP.getCredit());

        } catch (Exception e) {
            e.getStackTrace();
        }

        if (SELECTED_SHOP == null) {
            finish();
            return;
        }

     //  Receipt Recycler

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(itemAnimator);

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //  Refund Recycler

        RecyclerView.ItemAnimator itemAnimator1 = new DefaultItemAnimator();
        itemAnimator1.setAddDuration(1000);
        itemAnimator1.setRemoveDuration(1000);
        recycler_refund.setHasFixedSize(true);
        recycler_refund.setItemAnimator(itemAnimator1);

        //        Item Divider in recyclerView
        recycler_refund.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());
        recycler_refund.setLayoutManager(new LinearLayoutManager(this));
        recycler_refund.setAdapter(refundAdapter);


        tvToolBarShopName.setText(String.valueOf(SELECTED_SHOP.getShopName() + "\t" + SELECTED_SHOP.getShopCode()));
        tvToolBarShopName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvToolBarShopName.setSelected(true);

        getInvoiceList();

        ibBack.setOnClickListener(this);
        btnFinish.setOnClickListener(this);
        btnRefundFinish.setOnClickListener(this);
        fabOpeningBalance.setOnClickListener(this);

        //Add to textWatcher

        etPaidAmount.addTextChangedListener(new TextValidator(etPaidAmount) {
            @Override
            public void validate(TextView textView, String text) {

               /* float paid = TextUtils.isEmpty(text) ? 0 : Float.valueOf(text);
                paidNotify(paid);*/

                if (!str_invoice_selected.equalsIgnoreCase("Select Invoice")){

                    try {
                        double paid = Double.parseDouble(getAmount(TextUtils.isEmpty(text) ? 0 : Float.valueOf(text)));

                        Log.e("paid : "+paid, "inv bal : "+invoice_bal);

                        if (paid>invoice_bal){
                            Toast.makeText(getApplicationContext(), "Paid amount exceeds balance amount !", Toast.LENGTH_SHORT).show();
                            btnFinish.setEnabled(false);

                        }else {
                            btnFinish.setEnabled(true);
                            paidInvoiceNotify(paid);
                        }


                    } catch (NumberFormatException e) {
                        e.getMessage();
                    }
            }else {
                    Toast.makeText(getApplicationContext(), "Select Invoice", Toast.LENGTH_SHORT).show();
                }
            }
        });

        et_RefundPaidAmount.addTextChangedListener(new TextValidator(et_RefundPaidAmount) {
            @Override
            public void validate(TextView textView, String text) {
                try {

                float refund = TextUtils.isEmpty(text)? 0 : Float.valueOf(text);
                PaidRefurndNotify(refund);

                }catch (NumberFormatException e){
                    e.getMessage();
                }
            }
        });

        spinner_paymentmode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                str_paymentmode = spinner_paymentmode.getSelectedItem().toString();

                if (str_paymentmode.equals("Cash")){

                    btnFinish.setEnabled(true);

                }else {

                    btnFinish.setEnabled(false);

                   // Show_Dialog_cheque();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_receipt_invoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                etPaidAmount.setText("");
                invoice_bal = 0;

                str_invoice_selected = spinner_receipt_invoice.getSelectedItem().toString();
                invoice_bal = getBalance(str_invoice_selected);

                Log.e("sel Balance", ""+invoice_bal);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private double getBalance(String inv_no){

        double balance = 0;

        Log.e("Inv No ", ""+inv_no);
        try {
            ArrayList<Receipt> list = adapter.getPaymentList();

            for (int i = 0; i <= list.size(); i++) {

                Log.e("Array Invoice"+list.size(), ""+list.get(i).getInvoiceNo());
                if (inv_no.equals(list.get(i).getInvoiceNo())){

                    balance = Double.parseDouble(getAmount(list.get(i).getBalanceAmount()-list.get(i).getReceivedAmount()));

                }
            }

        }catch (Exception e){

        }

        return balance;
    }


    private void paidInvoiceNotify(double amount){

        try {
            ArrayList<Receipt> list = adapter.getPaymentList();

            for (int i = 0; i <= list.size(); i++) {

                Receipt rec = list.get(i);

                Log.e("Array Invoice"+list.size(), ""+rec.getInvoiceNo());
                if (str_invoice_selected.equals(rec.getInvoiceNo())){

                   rec.setReceivedAmount(amount);

                   adapter.updateItem(rec, i);

                }
            }

        }catch (Exception e){

        }


    }

    private void paidNotify(float amounts) {

        try {

            ArrayList<Receipt> list = adapter.getPaymentList();

            double balance = amounts;

            for (int i = list.size() - 1; i >= 0; i--) {

                Receipt tempP = list.get(i);

                Receipt rec = new Receipt();
                rec.setLocalId(tempP.getLocalId());
                rec.setInvoiceNo(tempP.getInvoiceNo());
                rec.setInvoiceId(tempP.getInvoiceId());
                rec.setBalanceAmount(tempP.getBalanceAmount());
                rec.setTotalAmount(tempP.getTotalAmount());

                if (tempP.getBalanceAmount() >= balance) {
                    rec.setReceivedAmount(balance);

                    balance = 0;

                } else if (tempP.getBalanceAmount() < balance && tempP.getBalanceAmount() > 0) {

                    double d = balance - tempP.getBalanceAmount();
                    rec.setReceivedAmount(tempP.getBalanceAmount());

                    balance = d;

                } else {
                    rec.setReceivedAmount(0.0f);
                }

                adapter.updateItem(rec, i);
            }

        } catch (NullPointerException | NumberFormatException e) {
            e.getMessage();
        }
    }

    private void PaidRefurndNotify(float amounts) {

        try {

            ArrayList<Refund> list_refund = refundAdapter.getPaymentList();

            double balance = Double.parseDouble(getAmount(amounts));

            for (int i = list_refund.size() - 1; i >= 0; i--) {

                Refund tempP = list_refund.get(i);

                Refund rec = new Refund();
                rec.setSaleRefundId(tempP.getSaleRefundId());
                rec.setInvoiceId(tempP.getInvoiceId());
                rec.setInvoiceId(tempP.getInvoiceId());
                rec.setTotalBalance(tempP.getTotalBalance());
                rec.setTotalRefund(tempP.getTotalRefund());

                if (tempP.getTotalBalance() >= balance) {
                    rec.setReceivedAmount(balance);

                    balance = 0;

                } else if (tempP.getTotalBalance() < balance && tempP.getTotalBalance() > 0) {

                    double d = balance - tempP.getTotalBalance();
                    rec.setReceivedAmount(tempP.getTotalBalance());

                    balance = d;

                } else {
                    rec.setReceivedAmount(0.0f);
                }
                refundAdapter.updateItem(rec, i);
            }

        } catch (NullPointerException | NumberFormatException e) {
            e.getMessage();
        }
    }

    @SuppressLint("RestrictedApi")
    private void getInvoiceList() {

        receipts.clear();
        invoice_array.clear();
        invoice_array.add("Select Invoice");

        if (SELECTED_SHOP.getShopId() != 0) {

            ArrayList<Invoice> list = myDatabase.getCustomerWiseInvoices(SELECTED_SHOP.getShopId());

//          this method using to set 0 for receivableAmount because adapter view  edittext can empty

            for (int i = list.size() - 1; i >= 0; i--) {
                Invoice inv = list.get(i);
                Receipt rec = new Receipt();

                double balanceAmt = inv.getBalanceAmount() - convertTotalTransactionsAmount(inv.getTransactions());

                rec.setLocalId(inv.getLocalId());
                rec.setInvoiceId(inv.getInvoiceId());

                rec.setInvoiceNo(inv.getInvoiceNo());
                rec.setBalanceAmount(balanceAmt);
                rec.setTotalAmount(inv.getTotalAmount());

                double tot_amount = inv.getTotalAmount();

                rec.setReceivedAmount(0);

                if (balanceAmt != 0.)
                    invoice_array.add(""+rec.getInvoiceNo());
                    receipts.add(rec);

                bal_total = bal_total+tot_amount;
                Log.e("Balance Amnt", ""+tot_amount);
            }

            tvTotalAmount.setText("Opening Balance : "+openingBalance+" "+getString(R.string.currency)+"\n"+String.valueOf("Total Balance : " + getAmount(myDatabase.getCustomerWiseReceiptBalance
                    (String.valueOf(SELECTED_SHOP.getShopId()))) + " " + getString(R.string.currency))+"\nOutstanding : "+outstanding_bal+" "+getString(R.string.currency));

//           button is GONE opening balance is 0
         //   fabOpeningBalance.setVisibility(myDatabase.getCustomerOpeningBalance(SELECTED_SHOP.getShopId()) != 0.0f ? View.ISIBLE : View.GONE);

        }
        if (receipts.isEmpty())
            setErrorView("No Invoices for Offline", "", true);
        else {
            setRecyclerView();
            setInvoiceSpinner();
        }
    }

    private void loadOnlineReceipt() {

        if (SELECTED_SHOP == null) {

            finish();
            return;
        }

        if (!checkConnection()) {
            setErrorView(this.getString(R.string.no_internet), "", false);
            return;
        }

        setProgressBar(true);

        final JSONObject object = new JSONObject();
        try {
            object.put(CUSTOMER_KEY, SELECTED_SHOP.getShopId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        receipts.clear();

        webGetReceipt(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    if (response.getString("status").equals("Success")) {
                        JSONObject receiptObj = response.getJSONObject("receipt");
                        JSONArray array = receiptObj.getJSONArray("Sale");

                        float toatalSale = receiptObj.getLong("total_sale");
                        float totalBalance = receiptObj.getLong("total_balance");

                        tvTotalAmount.setText("Total Amount : " + getAmount(totalBalance) + " " + getString(R.string.currency));

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject obj = array.getJSONObject(i);

                            Receipt receipt = new Receipt();

                            receipt.setInvoiceId(obj.getString("invoice_no")); //sale_id
                            receipt.setInvoiceNo(obj.getString("invoice_no")); //both are same
                            receipt.setBalanceAmount(obj.getDouble("balance"));
                            receipt.setTotalAmount(obj.getDouble("total"));
                            receipt.setReceivedAmount(0);

                            receipts.add(receipt);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (receipts.isEmpty())
                    setErrorView("No Invoices", "", false);
                else {
                    setRecyclerView();

                }
            }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
            }
        }, object);
    }

    private void setInvoiceSpinner(){

        ArrayAdapter<String> adapterinv = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,invoice_array);
        spinner_receipt_invoice.setAdapter(adapterinv);


    }

    private void setRecyclerView() {

        Log.e("set recycler", "entered");

        setProgressBar(false);
        errorView.setVisibility(View.GONE);
        listLayout.setVisibility(View.VISIBLE);

        adapter.notifyDataSetChanged();

    }

    private void loadOnlineRefunds() {

        if (SELECTED_SHOP == null) {
            finish();
            return;
        }

        if (!checkConnection()) {
            setErrorView(this.getString(R.string.no_internet), "", false);
            return;
        }

        setProgressBar(true);

        final JSONObject object = new JSONObject();
        try {
            object.put(CUSTOMER_KEY, SELECTED_SHOP.getShopId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        refunds.clear();

        Log.e("Refund", "Object: "+object.toString());

        webGetAllRefund(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    if (response.getString("status").equals("Success")) {

                        Log.e("Refund", "calling inside : "+response.toString());

                        JSONObject receiptObj = response.getJSONObject("refund");
                        JSONArray array = receiptObj.getJSONArray("SalesReturn");

                        float toatalSalereturn = receiptObj.getLong("total_sale_return");
                        double totalBalance = receiptObj.getDouble("total_balance");

                        tv_RefundTotal.setText("Total Refund : " + totalBalance + " " + getString(R.string.currency));  //getAmount(totalBalance)
                        Log.e("totalBalance", " : "+totalBalance);

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject obj = array.getJSONObject(i);

                            Refund refund = new Refund();

                            refund.setInvoiceId(obj.getString("salesreturn_id")); //sale_id
                            refund.setSaleRefundId(obj.getString("invoice_no")); //both are same
                            refund.setTotalRefund(obj.getDouble("total"));
                            refund.setTotalBalance(obj.getDouble("balance"));
                            refund.setReceivedAmount(0);

                            refunds.add(refund);

                          //  Log.e("array pos", " : "+i);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (refunds.isEmpty()) {

                  //  Log.e("refund is", "is empty");
                    setErrorView("No Refunds", "", false);

                    /*setProgressBar(false);
                    errorView.setVisibility(View.GONE);
                    listLayout.setVisibility(View.GONE);
                    refundLayout.setVisibility(View.VISIBLE);
                    refundAdapter.notifyDataSetChanged();
                    */
                }
                else {

                    setProgressBar(false);
                    errorView.setVisibility(View.GONE);
                    listLayout.setVisibility(View.GONE);
                    refundLayout.setVisibility(View.VISIBLE);

                  //Log.e("refund else", "not empty");
                    refundAdapter.notifyDataSetChanged();
                }
             }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
            }
        }, object);
    }

    // set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        String str_msg = "";
        if (strSwitchStatus.equals("Receipt")){
            listLayout.setVisibility(View.GONE);
            str_msg = "Loan online Receipts";
        }else {
            refundLayout.setVisibility(View.GONE);
            str_msg = "Loan online Refunds";
        }

        errorView.setVisibility(View.VISIBLE);

        setProgressBar(false);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryText(str_msg)
                .retryVisible(isRetry)
                .build());

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                if (strSwitchStatus.equals("Receipt")) {
                    loadOnlineReceipt();
                }else {
                    Toast.makeText(ReceiptActivity.this, "No Refunds", Toast.LENGTH_SHORT).show();
                }
             }
        });
    }

    //  ProgressBar
    private void setProgressBar(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            listLayout.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private boolean isValidate() {
        boolean status = false;

        etPaidAmount.setError(null);
        float paid = TextUtils.isEmpty(etPaidAmount.getText().toString().trim()) ? 0 : Float.valueOf(etPaidAmount.getText().toString().trim());

        if (adapter.getPaymentList().isEmpty()) {
            Toast.makeText(this, "No Payment List", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (TextUtils.isEmpty(etPaidAmount.getText().toString().trim())) {
            etPaidAmount.setError("Invalid Amount");
            status = false;

        } else if (adapter.getReceivedTotal() == 0) {
            Toast.makeText(this, "Enter Paid Amount for Invoices", Toast.LENGTH_SHORT).show();
            status = false;

        } /*else if (adapter.getReceivedTotal() != paid) {
//            Toast.makeText(this, "Mismatch Amounts", Toast.LENGTH_SHORT).show();

            double bal = paid - adapter.getReceivedTotal();
            Toast.makeText(this, "About " + bal + " Excess you paid", Toast.LENGTH_SHORT).show();
            status = false;

        }*/ else
            status = true;

        return status;
    }

    private boolean isrefundValidate() {

        boolean status = false;

        et_RefundPaidAmount.setError(null);
        float refundpaid = TextUtils.isEmpty(et_RefundPaidAmount.getText().toString().trim()) ? 0 : Float.valueOf(et_RefundPaidAmount.getText().toString().trim());
        float rec_amnt = (float) refundAdapter.getReceivedTotal();


        if (refundAdapter.getPaymentList().isEmpty()) {
            Toast.makeText(this, "No Payment List", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (TextUtils.isEmpty(et_RefundPaidAmount.getText().toString().trim())) {
            et_RefundPaidAmount.setError("Invalid Amount");
            status = false;

        } else if (refundAdapter.getReceivedTotal() == 0) {
            Toast.makeText(this, "Enter Paid Amount for Invoices", Toast.LENGTH_SHORT).show();
            status = false;

        } else if (rec_amnt != refundpaid) {   // refundAdapter.getReceivedTotal() != refundpaid
//            Toast.makeText(this, "Mismatch Amounts", Toast.LENGTH_SHORT).show();

            Log.e("received total", ":"+refundAdapter.getReceivedTotal());
            Log.e("received paid", ":"+refundpaid);

            double bal = refundpaid - refundAdapter.getReceivedTotal();
            String bala = getAmount(refundpaid - refundAdapter.getReceivedTotal());

            Toast.makeText(this, "About " + bala + " Excess you paid", Toast.LENGTH_SHORT).show();
            status = false;

         } else
            status = true;

        return status;
    }


    //    updateReceipt
    private void storePaidReceipt(String type, String cheque, String bankName, String remark, String issueDate, String clearDate) {

        if (!isValidate())
            return;

        final String receiptNo = generateNewNumber(sessionValue.getReceiptCode(SELECTED_SHOP.getRouteCode()));

        ArrayList<Receipt> list = new ArrayList<>();

        for (Receipt rec : adapter.getRecievableList()) {

            rec.setTransactionType(type);
            rec.setChequeNumber(cheque);
            rec.setBankName(bankName);
            rec.setRemark(remark);
            rec.setLogDate(getDateTime());
            rec.setIssueDate(issueDate);
            rec.setClearDate(clearDate);

            rec.setReceiptNo(SELECTED_SHOP.getRouteCode() + receiptNo);

            boolean isExist = myDatabase.isExistInvoice(rec.getInvoiceId());

            if (isExist) {

                if (myDatabase.insertReceiptTransactionLog(rec)) {
                    list.add(rec);
                    myDatabase.updateCreditBal(SELECTED_SHOP.getShopId(), rec.getReceivedAmount()); //update credit for customer table

                } else {
                    Toast.makeText(getApplicationContext(), "Insertion Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                if (myDatabase.insertReceipt(SELECTED_SHOP.getShopId(), rec)) {
                    list.add(rec);
                    myDatabase.updateCreditBal(SELECTED_SHOP.getShopId(), rec.getReceivedAmount()); //update credit for customer table

                } else {
                    Toast.makeText(getApplicationContext(), "Insertion Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }

        sessionValue.storeReceiptCode(SELECTED_SHOP.getRouteCode(), receiptNo);
        boolean status = myDatabase.updateVisitStatus(SELECTED_SHOP.getShopId(), REQ_RECEIPT_TYPE, "", "", "");

        if (status) {
            Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(ReceiptActivity.this, ReceiptPreviewActivity.class);

            intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_INVOICE_RECEIPT);
            intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);
            intent.putExtra(PAID_AMOUNT_VALUE_KEY, adapter.getReceivedTotal());

            intent.putExtra(RECEIPT_NO_KEY, SELECTED_SHOP.getRouteCode() + receiptNo);

            intent.putExtra(INVOICE_LIST_KEY, list);

            startActivity(intent);
            finish();

        } else
            Toast.makeText(this, "insertion failed", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        switch (v.getId()) {

            case R.id.imageButton_toolbar_back:

                onBackPressed();

                break;

            case R.id.button_receipt_finish:

                double paid = TextUtils.isEmpty(etPaidAmount.getText().toString().trim()) ? 0 : Double.valueOf(etPaidAmount.getText().toString().trim());

                if (paid>invoice_bal){

                    Toast.makeText(getApplicationContext(), "Paid amount exceeds balance amount !", Toast.LENGTH_SHORT).show();

                }else {

                    ReceiptTypeDialogFrag dialogFragment = new ReceiptTypeDialogFrag(paid, new ReceiptTypeClickListener() {
                        @Override
                        public void onReceiptClick(String type, String cheque, String bankName, String remark, String issueDate, String clearDate) {
                            storePaidReceipt(type, cheque, bankName, remark, issueDate, clearDate);
                        }


                    });
                    if (isValidate())
                        dialogFragment.show(getSupportFragmentManager(), "dialog");

                }
                break;

            case R.id.button_refund_finish:

              //  double refundpaid = TextUtils.isEmpty(et_RefundPaidAmount.getText().toString().trim()) ? 0 : Double.valueOf(et_RefundPaidAmount.getText().toString().trim());
                if (isrefundValidate()){

                    /*for (Refund ref : refundAdapter.getRecievableList()) {

                        String invoiceNo = ref.getInvoiceId();
                        String paidref = ""+ref.getReceivedAmount();

                        Log.e("inv:"+invoiceNo, "paid:"+paidref);
                    }*/

                        SavePaidRefunds();
                }

                break;

            case R.id.fab_receipt_openingBalance:



                PayOpeningBalanceDialogFrag openingFragment = new PayOpeningBalanceDialogFrag(SELECTED_SHOP, new PayOpeningBalanceDialogFrag.OpeningBalanceClickListener() {

                    @Override
                    public void onPaymentClick(OpeningBalanceTransaction balance) {

                        Toast.makeText(ReceiptActivity.this, "Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ReceiptActivity.this, ReceiptPreviewActivity.class);

                        intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_OPENING_BALANCE_RECEIPT);

                        intent.putExtra(OPENING_BALANCE_KEY, balance);
                        intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);

                        startActivity(intent);


                    }
                });
                openingFragment.show(getSupportFragmentManager(), "dialog");

                break;

        }
    }

    //    paid refunds
    private void SavePaidRefunds() {

        final ProgressDialog pd = ProgressDialog.show(ReceiptActivity.this, null, "Please wait...", false, false);

        JSONObject object = new JSONObject();
        JSONArray refundArray = new JSONArray();

        try {

            for (Refund ref : refundAdapter.getRecievableList()) {

                String invoiceNo = ref.getInvoiceId();
                String paid = ""+ref.getReceivedAmount();

                JSONObject obj = new JSONObject();
                obj.put("invoice_no", invoiceNo);
                obj.put("received_amount", paid);

                refundArray.put(obj);

            }

            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("RefundList", refundArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "paidRefund  object  " + object);

        webPaidRefunds(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "paidRefund  response  " + response);

                try {
                    if (response.getString("status").equals("success")) {

                        loadOnlineRefunds();
                        Toast.makeText(getApplicationContext(), "Refund success", Toast.LENGTH_SHORT).show();
                        et_RefundPaidAmount.setText("");

                    } else
                        Toast.makeText(getApplicationContext(), "Receipt " + response.getString("status"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();

            }
        }, object);
    }


    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    @Override
    public void onNotified() {


//        tvPaidAmount.setText(getAmount(adapter. getNetTotal()));


    }


    private static String getDateTime() {
        Date date = new Date();
        return dbDateFormat.format(date);
    }

    /**
     * Hides the soft keyboard
     */
    private void hideKeyboard() {
        //Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
        }


    }


    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }
}
