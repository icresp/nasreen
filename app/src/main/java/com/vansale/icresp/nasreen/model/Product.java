package com.vansale.icresp.nasreen.model;

import java.io.Serializable;


/**
 * Created by mentor on 6/5/17.
 */

public class Product implements Serializable {


    private int productId;
    private String productName;
    private String arabicName;
    private double retailPrice;
    private double wholeSalePrice;

    private double cost;
    private float tax;
    private String brandName;
    private String productType;
    private String productCode;
    private int stockQuantity;
    private int piecepercart;

    public String getArticleCode() {
        return ArticleCode;
    }

    public void setArticleCode(String articleCode) {
        ArticleCode = articleCode;
    }

    private String ArticleCode;


    public Product() {
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public double getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(double wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }


    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }


    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }


    public int getPiecepercart() {
        return piecepercart;
    }

    public void setPiecepercart(int piecepercart) {
        this.piecepercart = piecepercart;
    }

    @Override
    public String toString() {
//        return super.toString();

        return productName;
    }


}
