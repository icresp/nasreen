package com.vansale.icresp.nasreen.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;


public class RvHistoryHolder extends RecyclerView.ViewHolder {

    public TextView tvSlNo, tvInvoiceNo, tvDate, tvTime, tvQty;
    public ImageButton ibView;


    public RvHistoryHolder(View itemView) {
        super(itemView);
        tvSlNo = itemView.findViewById(R.id.textView_item_sale_report_no);
        tvInvoiceNo = itemView.findViewById(R.id.textView_item_sale_report_invoice);
        tvQty = itemView.findViewById(R.id.textView_item_sale_report_quantity);
        tvDate = itemView.findViewById(R.id.textView_item_sale_report_date);
        tvTime = itemView.findViewById(R.id.textView_item_sale_report_time);
        ibView = itemView.findViewById(R.id.imageButton_item_sale_report_view);


    }
}
