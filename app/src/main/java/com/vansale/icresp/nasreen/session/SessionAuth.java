package com.vansale.icresp.nasreen.session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.vansale.icresp.nasreen.activity.LoginActivity;
import com.vansale.icresp.nasreen.activity.MainActivity;
import com.vansale.icresp.nasreen.localdb.MyDatabase;

import java.util.HashMap;

import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_IS_LOGIN;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_KEY_ID;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_KEY_NAME;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_KEY_PASSWORD;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_NAME;
import static com.vansale.icresp.nasreen.config.ConfigKey.PRIVATE_MODE;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_ANY_TYPE;

/**
 * Created by mentor on 26/10/17.
 */

public class SessionAuth {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;


    // Constructor
    public SessionAuth(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();


    }

    /**
     * Create login session
     */


    public void createLoginSession(String name, String password, String regId) {

        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Clearing all data from Shared Preferences
        editor.clear();

        // Storing login value as TRUE
        editor.putBoolean(PREF_IS_LOGIN, true);

        // Storing name in pref
        editor.putString(PREF_KEY_NAME, name);

        // Storing email in pref
        editor.putString(PREF_KEY_PASSWORD, password);

        editor.putString(PREF_KEY_ID, regId);

        // commit changes
        editor.commit();


        context.startActivity(new Intent(context, MainActivity.class));
        ((Activity) context).finish();
    }


    /**
     * update Password session
     */


    public void updatePassword(String password) {

        // Storing email in pref
        editor.putString(PREF_KEY_PASSWORD, password);
        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
//     not using this App
    public void checkLogin() {

        // Check login status
        if (this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, MainActivity.class);


            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            context.startActivity(i);
            ((Activity) context).finish();

        }

    }

    public void checkLogin_() {

        Intent i;
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            i = new Intent(context, LoginActivity.class);
        } else {
            i = new Intent(context, MainActivity.class);

        }
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        context.startActivity(i);
        ((Activity) context).finish();

    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(PREF_KEY_NAME, preferences.getString(PREF_KEY_NAME, null));

        // user email id
        user.put(PREF_KEY_PASSWORD, preferences.getString(PREF_KEY_PASSWORD, null));

        // user email id
        user.put(PREF_KEY_ID, preferences.getString(PREF_KEY_ID, null));

        // return user
        return user;
    }


    /**
     * Get stored session data
     */
    public String getExecutiveId() {

        if (isLoggedIn())
            return preferences.getString(PREF_KEY_ID, null);// return user id
        else
            logoutUser();


        return "";

    }


    /**
     * Clear session details
     */
    public void logoutUser() {

        new MyDatabase(context).deleteTableRequest(REQ_ANY_TYPE);

        new SessionValue(context).clearCode();
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();


        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Staring Login Activity
        context.startActivity(i);
        ((Activity) context).finish();
    }


    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return preferences.getBoolean(PREF_IS_LOGIN, false);
    }

}
