package com.vansale.icresp.nasreen.activity.printpreview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.languages.ArabicLigaturizer;
import com.itextpdf.text.pdf.languages.LanguageProcessor;
import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvPreviewCartAdapter;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.PdfModel;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentageValue;
import static com.vansale.icresp.nasreen.config.ConfigKey.WRITE_REQUEST_CODE;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_CASE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALES_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALE_ID_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.DigitToWordConverter.convertNumberToArabicWords;
import static com.vansale.icresp.nasreen.config.DigitToWordConverter.convertNumberToEnglishWords;
import static com.vansale.icresp.nasreen.config.Generic.dateToFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.Generic.getMaximumChar;
import static com.vansale.icresp.nasreen.config.Generic.stringToDate;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_1;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_1_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_2;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_ADDRESS_2_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_CR;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_EMAIL;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_MOBILE;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_NAME;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_NAME_ARAB;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_COMPANY_VAT;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_ID;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_MOBILE;
import static com.vansale.icresp.nasreen.session.SessionValue.PREF_EXECUTIVE_NAME;

public class  SalePreviewActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "PreviewActivity";

    String strShopName = "", strShopNameArabic = "", strCustomerMob = "", strCustomerVat = "", strSalePo = "", strCustomerNo = "",
            strBillNumber = "", strShopAddress = "", strDate = "";


    int callingActivity = 0;
    private Shop SELECTED_SHOP = null;
    private Sales SELECTED_SALES = null;
    private RecyclerView recyclerView;
    private Button btnPrint, btnHome;
    private TextView tvTitle, tvNetTotal, tvInvoiceNo, tvDate, tvShopDetails;

    private double paid_amount = 0;
    private long saleId = -1;

    private SessionValue sessionValue;

    private ArrayList<CartItem> cartItems = null;

    private static String FILE_PATH = Environment.getExternalStorageDirectory() + "/icresp_invoice.pdf";

    private final int MAX_LINE = 25;  //36 // 26

    private String print_flag = "0";

  //  File myFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_preview);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_preview);
        btnPrint = (Button) findViewById(R.id.button_preview_print);
        btnHome = (Button) findViewById(R.id.button_preview_home);

        tvTitle = (TextView) findViewById(R.id.textView_preview_title);
        tvNetTotal = (TextView) findViewById(R.id.textView_preview_netTotal);
        tvShopDetails = (TextView) findViewById(R.id.textView_preview_shopDetails);
        tvInvoiceNo = (TextView) findViewById(R.id.textView_preview_invoiceNo);
        tvDate = (TextView) findViewById(R.id.textView_preview_date);

        sessionValue = new SessionValue(getApplicationContext());

        btnPrint.setEnabled(true);
        btnPrint.setText("Print PDF");


        /***  ***/


        try {
            SELECTED_SHOP = (Shop) getIntent().getSerializableExtra(SHOP_VALUE_KEY);

            SELECTED_SALES = (Sales) (getIntent().getSerializableExtra(SALES_VALUE_KEY));

            strShopName = SELECTED_SHOP.getShopName();
            strShopAddress = SELECTED_SHOP.getShopAddress();

            if (SELECTED_SHOP.getShopArabicName() != null && !TextUtils.isEmpty(SELECTED_SHOP.getShopArabicName()))
                strShopNameArabic = SELECTED_SHOP.getShopArabicName();

            if (SELECTED_SHOP.getVatNumber() != null && !TextUtils.isEmpty(SELECTED_SHOP.getVatNumber()))
                strCustomerVat = SELECTED_SHOP.getVatNumber();

            if (SELECTED_SHOP.getShopCode() != null && !TextUtils.isEmpty(SELECTED_SHOP.getShopCode()))
                strCustomerNo = SELECTED_SHOP.getShopCode();

            if (SELECTED_SHOP.getShopMobile() != null && !TextUtils.isEmpty(SELECTED_SHOP.getShopMobile()))
                strCustomerMob = SELECTED_SHOP.getShopMobile();

            if (SELECTED_SALES == null)
                finish();

            final Date date = stringToDate(SELECTED_SALES.getDate());
//            strDate = getPrintDate(date);
            strDate = dateToFormat(date);


            Log.e("Shop Type", ""+SELECTED_SHOP.getTypeId());

            cartItems = SELECTED_SALES.getCartItems();
            strBillNumber = SELECTED_SALES.getInvoiceCode();
            strSalePo = SELECTED_SALES.getSalePo();

            paid_amount = SELECTED_SALES.getPaid();


            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY,
                    0);
            switch (callingActivity) {
                case ActivityConstants.ACTIVITY_SALES:
                    tvTitle.setText(getString(R.string.invoice));
                    saleId = getIntent().getLongExtra(SALE_ID_KEY, -1);
                    if (paid_amount == SELECTED_SALES.getTotal())
                        tvTitle.setText("CASH INVOICE");
                    else
                        tvTitle.setText("CREDIT INVOICE");


                    break;
                case ActivityConstants.ACTIVITY_QUOTATION:

                    tvTitle.setText(getString(R.string.quotation));

                    break;
                case ActivityConstants.ACTIVITY_SALE_REPORT:
                    tvTitle.setText(getString(R.string.invoice));


                    saleId = getIntent().getLongExtra(SALE_ID_KEY, -1);


                    if (paid_amount == SELECTED_SALES.getTotal())
                        tvTitle.setText("CASH INVOICE");
                    else
                        tvTitle.setText("CREDIT INVOICE");

                    break;
                default:

            }
        } catch (NullPointerException e) {
            printLog(TAG, "NullPointerException exception   " + e.getMessage());
        }

        tvDate.setText(strDate);

        tvShopDetails.setText(strShopName + "\n" + strShopAddress);
        tvInvoiceNo.setText(strBillNumber);

        if (cartItems != null)
            setRecyclerView();


        btnPrint.setOnClickListener(this);
        btnHome.setOnClickListener(this);

    }


    /****/


    @SuppressLint("SetTextI18n")
    private void setRecyclerView() {


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());

        recyclerView.setAdapter(new RvPreviewCartAdapter(cartItems));


        String net = String.valueOf("TOTAL     : " + getAmount(getNetTotal()) + " " + getString(R.string.currency));
        String vat = "VAT    : " + getAmount(getTaxTotal()) + " " + getString(R.string.currency);

        String grandTotal = String.valueOf("GRAND TOTAL : " + getAmount(getGrandTotal()) + " " + getString(R.string.currency));
        tvNetTotal.setText(String.valueOf(net + ",\t\t" + vat + ",\t\t" + grandTotal));

    }


    public double getNetTotal() {

        double netTotal = 0;

        for (CartItem cartItem : cartItems) {
            if (cartItem.getNetPrice() != 0.0) {
                double d = cartItem.getNetPrice() * cartItem.getPieceQuantity();
                netTotal += d;
            }
        }
        return netTotal;
    }


    public double getGrandTotal() {

        double grandTotal = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem cartItem : cartItems) {
                if (cartItem.getTotalPrice() != 0.0) {
                    double f = cartItem.getTotalPrice();
                    grandTotal += f;
                }
            }
        }
        return grandTotal;
    }


    public double getTaxTotal() {
        double totalTax = 0.0;

        if (!cartItems.isEmpty()) {
            for (CartItem c : cartItems) {
                if (c.getTaxValue() != 0.0) {
                    double f = c.getTaxValue() * c.getPieceQuantity();
                    totalTax += f;
                }
            }
        }
        return totalTax;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_preview_print:

                if (isStoragePermissionGranted())
                    try {
                        String shoptypeid = ""+SELECTED_SHOP.getTypeId();

                        if (!shoptypeid.equals("")) {

                            if (SELECTED_SHOP.getTypeId().equals("3")) {
                                Toast.makeText(getApplicationContext(), "Printing.. Please wait...", Toast.LENGTH_SHORT).show();
                                printInvoiceLulu(getPdfModels(cartItems));
                            } else {

                                Toast.makeText(getApplicationContext(), "Printing.. Please wait...", Toast.LENGTH_SHORT).show();
                                printInvoice(getPdfModels(cartItems));
                            }
                        }else {
                            printInvoice(getPdfModels(cartItems));
                        }

                    }catch (Exception e){
                   Toast.makeText(getApplicationContext(), "Error : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                break;
            case R.id.button_preview_home:

                onBackPressed();

                break;
        }
    }


    /**
     *
     * ********   PDF Print  ********
     *
     **/

    private List<PdfModel> getPdfModels(ArrayList<CartItem> list) {

        final List<PdfModel> models = new ArrayList<>();

        try {

            List<CartItem> sublist;

            if (list.size() <= MAX_LINE) {

                PdfModel pdfModel = new PdfModel();
                pdfModel.setCartItems(list);
                models.add(pdfModel);

            } else {

                int count = list.size() / MAX_LINE;

                int SUBLIST_START_SIZE = 0, SUBLIST_END_SIZE = MAX_LINE;

                for (int i = 0; i < count; i++) {

                    if (list.size() >= SUBLIST_END_SIZE) {
                        sublist = list.subList(SUBLIST_START_SIZE, SUBLIST_END_SIZE);
                        PdfModel pdfModel = new PdfModel();
                        pdfModel.setCartItems(sublist);
                        models.add(pdfModel);

                    }

                    if (SUBLIST_END_SIZE < list.size()) {
//                        return models;
                        SUBLIST_START_SIZE = SUBLIST_START_SIZE + MAX_LINE;
                        SUBLIST_END_SIZE = SUBLIST_END_SIZE + MAX_LINE;

                    }

                }

                if (list.size() < SUBLIST_END_SIZE) {
                    sublist = list.subList(SUBLIST_START_SIZE, list.size());
                    PdfModel pdfModel = new PdfModel();
                    pdfModel.setCartItems(sublist);
                    models.add(pdfModel);
                }

            }

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return models;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void printInvoiceLulu(List<PdfModel> pdfList) {

        File myFile = null;

        PdfWriter writer;

        try {

            String compName = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME);  //name get from session
            String compNameArab = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME_ARAB);  //name get from session
            String address1Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1);  //address get from session
            String address1ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1_ARAB);  //address get from session
            String address2Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2);  //address get from session
            String address2ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2_ARAB);  //address get from session
            String compemailStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_EMAIL);  //mail get from session
            String mobileStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_MOBILE);  //mobile get from session

            String compRegisterStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_CR);
            String companyVatStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_VAT);

            String execName = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_NAME);  //name get from session
            assert execName != null;
            execName=execName.toUpperCase();
            String execId = "Code     : " + sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_ID);  //id get from session
            String execMob = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_MOBILE);  //mob get from session


          //  page size custom
          /*  Rectangle pagesize = new Rectangle(521, 757);
              Document document = new Document(pagesize);*/

          // Create New Blank Document
           Document document = new Document(PageSize.A4);

          // document.setMargins(40, 0,0,0);

            writer = PdfWriter.getInstance(document, new FileOutputStream(FILE_PATH));

            myFile = new File(FILE_PATH);

            document.open();

//          BaseFont bf = BaseFont.createFont("/assets/tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont bf = BaseFont.createFont("/assets/dejavu_sans_condensed.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            Font font20 = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);

            Font font18 = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);

            Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
            Font font16Ul_Bold = new Font(Font.FontFamily.HELVETICA, 23, Font.UNDERLINE | Font.BOLD);


            Font font10Bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
            Font font9 = new Font(Font.FontFamily.HELVETICA, 9);

            Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
            Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
            Font font8Bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);

            Font font12 = new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC);

            LanguageProcessor arabicPro = new ArabicLigaturizer();
            Font fontArb8 = new Font(bf, 8);
            Font fontArb8Bold = new Font(bf, 8, Font.BOLD);
            Font fontArb10 = new Font(bf, 10);
            Font fontArb14 = new Font(bf, 14);

            for (int i = 0; i < pdfList.size(); i++) {


                PdfModel pdfData = pdfList.get(i);

                List<CartItem> cartList = pdfData.getCartItems();

                String netTotal = "", discount = "", totalWithoutDiscount = "", roundOff = "";

                String grandTotal = "";
                String paid = "";
                String balance = "";

                String val_in_english = "";
                String val_in_Arabic = "";


                String totalVat = "";

                if (pdfList.size() == i + 1) {

                    totalWithoutDiscount = getAmount(SELECTED_SALES.getTotal() + SELECTED_SALES.getDiscountTotal());
                    netTotal = getAmount(getNetTotal());

                    grandTotal = getAmount(SELECTED_SALES.getTotal());
                    paid = getAmount(paid_amount);
                    discount = getAmount(SELECTED_SALES.getDiscountTotal());
                    balance = " " + getString(R.string.currency);
                    totalVat = getAmount(getTaxTotal());


                    val_in_english = convertNumberToEnglishWords(String.valueOf(SELECTED_SALES.getTotal()));
                    val_in_Arabic = convertNumberToArabicWords(String.valueOf(SELECTED_SALES.getTotal()));

                }


                Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font10Bold);
                compMobileTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compMailTag = new Paragraph("Email:"+compemailStr, font10Bold);
                compMailTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress1Tag = new Paragraph(address1Str, font10Bold);
                compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress2Tag = new Paragraph(address2Str, font10Bold);
                compAddress2Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compWebTag = new Paragraph("www.nasreenoil.com", font10Bold);
                compWebTag.setAlignment(Element.ALIGN_LEFT);


                Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font10Bold);
                compVatTag.setAlignment(Element.ALIGN_RIGHT);

                Paragraph compPoTag = new Paragraph("LPO No : " + strSalePo, font10Bold);
                compPoTag.setAlignment(Element.ALIGN_RIGHT);

                PdfPCell cell;  //default cell

                //space cell
                PdfPCell cellSpace = new PdfPCell();
                cellSpace.setPadding(10);
                cellSpace.setBorder(PdfPCell.NO_BORDER);
                cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


                //Create the table which will be 2 Columns wide and make it 100% of the page

                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(130.0f);
//              table.setSpacingBefore(10);
                table.setWidths(new int[]{1, 1, 1});


                PdfPCell cellLogo = new PdfPCell();
                cellLogo.setBorder(PdfPCell.NO_BORDER);
                cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cellLogo.setPaddingTop(5);
                cellLogo.setPaddingBottom(5);
                cellLogo.setPaddingRight(5);
                cellLogo.setPaddingLeft(7);

                try
                {

                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logonasreennew);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image img = Image.getInstance(stream.toByteArray());

                    cellLogo.addElement(img);

                } catch (IOException e) {

                    e.printStackTrace();
                }
                table.addCell(cellSpace);
                table.addCell(cellLogo);
                table.addCell(cellSpace);

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});

                ////

                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                cell.addElement(compMobileTag);
                cell.addElement(compMailTag);
                cell.addElement(compAddress1Tag);
                cell.addElement(compAddress2Tag);
                cell.addElement(compWebTag);

                table.addCell(cell);


//top right
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                Paragraph paragraph = new Paragraph("TAX INVOICE", font16Ul_Bold);


                switch (callingActivity) {

                    case ActivityConstants.ACTIVITY_QUOTATION:


                        paragraph = new Paragraph(" QUOTATION", font16Ul_Bold);

                        break;
                }


                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setSpacingAfter(2);

                cell.addElement(paragraph);
                cell.addElement(compVatTag);
                cell.addElement(compPoTag);


                table.addCell(cell);


               //Add the PdfPTable to the table
                document.add(table);


                //Create the table which will be 3 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{4, 2});


                /////////////////*****  customer data ******/////
                //            customer details

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


//            customer name
                String nam = strShopName;
                nam = getMaximumChar(nam, 110);
                paragraph = new Paragraph("Customer : " + nam, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer No label
//                paragraph = new Paragraph("Customer No ", font8);
//                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);

                //            vat number
                String justifiedVatNumber = strCustomerVat;
                if (justifiedVatNumber.length() < 44)
                    justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
                else
                    justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

                paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


//tel
                String justifiedMobNumber = strCustomerMob;
                if (justifiedMobNumber.length() < 44)
                    justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
                else
                    justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

                paragraph = new Paragraph("Tel : " + justifiedMobNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer Address
                String justifiedCustomerAddress = strShopAddress;
                if (justifiedCustomerAddress.length() < 44)
                    justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
                else
                    justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
                paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


                //             executive name and mobile

                String justifiedExecutiveMobile = " - "+execMob;

                if (justifiedExecutiveMobile.length() > 20)
                    justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

                String justifiedExecutiveName = execName;
                if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                    justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());

                paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                table.addCell(cell);

                /////////////////*****  invoice data ******/////

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


                //            invoice number
                String justifiedBill = strBillNumber;
                if (justifiedBill.length() < 28)
                    justifiedBill = String.format("%-40s", justifiedBill);
                else
                    justifiedBill = getMaximumChar(justifiedBill, 28);

                paragraph = new Paragraph("Invoice No : " + justifiedBill, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            sale order number label

                String justifiedPo = strSalePo;
                if (justifiedPo.length() < 28)
                    justifiedPo = String.format("%-40s", justifiedPo);
                else
                    justifiedPo = getMaximumChar(justifiedPo, 28);

                paragraph = new Paragraph("PO No : " + justifiedPo, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
             //   cell.addElement(paragraph);

                //           bill date
                String justifiedDate = String.format("%-40s", strDate);
                paragraph = new Paragraph("Inv . Date : " + justifiedDate, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                String payType = "";

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_SALES:


                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }


                        break;
                    case ActivityConstants.ACTIVITY_SALE_REPORT:

                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }

                        break;
                }



//                pay method
                paragraph = new Paragraph("Pay.Method : "+payType, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                //                currency
                paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                table.addCell(cell);

/*
                cell = new PdfPCell();
                cell.setBorder(PdfPCell.BOX);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);

                table.addCell(cell);*/

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(10);
                                        //3
                table.setWidths(new int[]{2, 4, 14, 3, 3, 4, 3, 3, 4, 4});  // {2, 15, 3, 4, 4, 4, 3, 4, 5}
                table.setWidthPercentage(100);
                table.setSpacingBefore(5);


                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("SN", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


// item code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Article No", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//description
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Product", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//quantity
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Qty", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);

                table.addCell(cell);

//  unit
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);

            //  unit price
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit Price", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             discount percentage
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                // item code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             tax %
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT(5%)", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//              product total
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount+Vat", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                for (int j = 0; j < MAX_LINE; j++) {

                    String strSl_No = " ", strP_Code = " ", strP_Name = " ", strP_Arabic = "  ", strP_Unit = " ", strQty = " ",
                            strUnitPrice = " ", strDisc = " ", strAmount = " ", strTotalPrice = " ", str_vatPercentage = " ",
                            str_vatAmount = " ", str_netAmount = "", strArticlecode="";

                    if (cartList.size() > j) {
                        CartItem cartItem = cartList.get(j);

                        int slNo = i * MAX_LINE + j + 1;
                        strSl_No = String.valueOf(slNo);
                        strP_Name = cartItem.getProductName();
                        strP_Arabic = cartItem.getArabicName();
                        strP_Code = cartItem.getProductCode();
                        strArticlecode = cartItem.getArticleCode();

                      /*  strArticlecode = cartItem.getArticleCode();
                        Log.e("Article Code", ""+strArticlecode);*/

                        if (strP_Arabic == null || TextUtils.isEmpty(strP_Arabic.trim()) || strP_Arabic.equals("null"))
                            strP_Arabic = "  ";

                        strQty = "0/" + String.valueOf(cartItem.getTypeQuantity());
                        double netPrice = cartItem.getNetPrice();
                        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE)) {
                            netPrice = netPrice * cartItem.getPiecepercart();
                            strQty = cartItem.getTypeQuantity() + "/0";
                        }

                        strQty = String.valueOf(cartItem.getTypeQuantity());

                        double discountedPrice=  getPercentageValue(netPrice,cartItem.getDiscountPerce());

                        strDisc =  getAmount(discountedPrice);

                        str_vatPercentage = String.valueOf(cartItem.getTax() + " %");

                        str_vatAmount = getAmount(cartItem.getTaxValue() * cartItem.getPieceQuantity());

                        double vatamnt = Double.parseDouble(str_vatAmount);

                        strTotalPrice = getAmount((cartItem.getNetPrice() * cartItem.getPieceQuantity())+vatamnt);
                        str_netAmount = getAmount(cartItem.getNetPrice() * cartItem.getPieceQuantity());


                        strP_Unit = cartItem.getOrderType();

                        strUnitPrice = getAmount(netPrice);


                    }


                    if (strP_Name.length() > 40)
                        strP_Name = getMaximumChar(strP_Name, 40);


                    if (strP_Arabic.length() > 42)
                        strP_Arabic = getMaximumChar(strP_Arabic, 42);


                    String justifiedSlNo = String.format("%-3s", strSl_No);
                    String justifiedCode = String.format("%-5s", strP_Code);
                    String justifiedArticleCode = String.format("%-5s", strArticlecode);

                    String justifiedQuantity = String.format("%-5s", strQty);
                    String justifiedUnitPrice = String.format("%-5s", strUnitPrice);
                    String justifiedDisc = String.format("%-5s", strDisc);
                    String justifiedVatPercentage = String.format("%-5s", str_vatPercentage);
                    String justifiedVatAmount = String.format("%-5s", str_vatAmount);
                    String justifiedTotal = String.format("%-5s", strTotalPrice);
                    String justifiedNetTotal = String.format("%-5s", str_netAmount);



//sl number
                    cell = new PdfPCell(new Phrase(justifiedSlNo, font9));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setFixedHeight(3f);
                    table.addCell(cell);


//                    Article code
                    cell = new PdfPCell(new Phrase(""+justifiedArticleCode, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


//                    item desc
                    /*
                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                        cell = new PdfPCell(new Phrase(strP_Name + "\n" + arabicPro.process(strP_Arabic), fontArb8));
                    else
                        cell = new PdfPCell(new Phrase(strP_Name +"\n"+ Chunk.NEWLINE, fontArb8));
*/

                    cell = new PdfPCell(new Phrase(strP_Name, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

/*
//                  item desc
                    cell = new PdfPCell();
                    paragraph = new Paragraph(strP_Name, font8);
                    cell.addElement(paragraph);
                    paragraph = new Paragraph(arabicPro.process(strP_Arabic), fontArb8);
                    paragraph.setAlignment(Element.ALIGN_RIGHT);

                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                    cell.addElement(paragraph);
                    else
                    cell.addElement( Chunk.NEWLINE);
                    cell.setBorder(Rectangle.RIGHT);
                    table.addCell(cell);
*/


            //  quantity
                    cell = new PdfPCell(new Phrase(justifiedQuantity, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //  unit
                    cell = new PdfPCell(new Phrase(strP_Unit, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

            //  unit price
                    cell = new PdfPCell(new Phrase(justifiedUnitPrice, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

            //Net Amount
                    cell = new PdfPCell(new Phrase(justifiedNetTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

            //  Vat Percentage
                    cell = new PdfPCell(new Phrase(justifiedVatPercentage, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


            //vat amount
                    cell = new PdfPCell(new Phrase(justifiedVatAmount, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


            // total
                    cell = new PdfPCell(new Phrase(justifiedTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);


                }

                document.add(table);

                //////////////////////******   total details ****//////////////////////

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{6, 2});

                /* Taxable Value label */
                cell = new PdfPCell(new Phrase("Total Amount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.TOP);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            Taxable amount
                cell = new PdfPCell(new Phrase(netTotal, font10Bold));
                cell.setBorder(Rectangle.BOX);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* discount Value label */
                cell = new PdfPCell(new Phrase("Discount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            discount amount
                cell = new PdfPCell(new Phrase(discount, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* vat Value label */
                cell = new PdfPCell(new Phrase("Total Vat 5%", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            vat amount
                cell = new PdfPCell(new Phrase(totalVat, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                /*amount in words  */
                cell = new PdfPCell(new Phrase("Net : " + val_in_english , font10Bold)); //Total amount inclusive of Tax
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            grand total  amount
                cell = new PdfPCell(new Phrase(grandTotal, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                document.add(table);

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});
                table.setSpacingBefore(40);

                //            Receiver Sign
                cell = new PdfPCell(new Phrase("Receiver’s Sign ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

                //            sales executive
                cell = new PdfPCell(new Phrase("Sales Executive ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

           /*

                String justifiedExecutiveWords = String.format("%-40s", execName);

                paragraph = new Paragraph(justifiedExecutiveWords, font8);
                paragraph.setPaddingTop(20);
                cell.addElement(paragraph);
*/
                document.add(table);

            }

            // ################# *********************  #####################

            for (int i = 0; i < pdfList.size(); i++) {


                PdfModel pdfData = pdfList.get(i);

                List<CartItem> cartList = pdfData.getCartItems();

                String netTotal = "", discount = "", totalWithoutDiscount = "", roundOff = "";

                String grandTotal = "";
                String paid = "";
                String balance = "";

                String val_in_english = "";
                String val_in_Arabic = "";


                String totalVat = "";

                if (pdfList.size() == i + 1) {

                    totalWithoutDiscount = getAmount(SELECTED_SALES.getTotal() + SELECTED_SALES.getDiscountTotal());
                    netTotal = getAmount(getNetTotal());

                    grandTotal = getAmount(SELECTED_SALES.getTotal());
                    paid = getAmount(paid_amount);
                    discount = getAmount(SELECTED_SALES.getDiscountTotal());
                    balance = " " + getString(R.string.currency);
                    totalVat = getAmount(getTaxTotal());


                    val_in_english = convertNumberToEnglishWords(String.valueOf(SELECTED_SALES.getTotal()));
                    val_in_Arabic = convertNumberToArabicWords(String.valueOf(SELECTED_SALES.getTotal()));

                }


                Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font10Bold);
                compMobileTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compMailTag = new Paragraph("Email:"+compemailStr, font10Bold);
                compMailTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress1Tag = new Paragraph(address1Str, font10Bold);
                compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress2Tag = new Paragraph(address2Str, font10Bold);
                compAddress2Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compWebTag = new Paragraph("www.nasreenoil.com", font10Bold);
                compWebTag.setAlignment(Element.ALIGN_LEFT);


                Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font10Bold);
                compVatTag.setAlignment(Element.ALIGN_RIGHT);

                Paragraph compPoTag = new Paragraph("LPO No : " + strSalePo, font10Bold);
                compPoTag.setAlignment(Element.ALIGN_RIGHT);

                PdfPCell cell;  //default cell

                //space cell
                PdfPCell cellSpace = new PdfPCell();
                cellSpace.setPadding(10);
                cellSpace.setBorder(PdfPCell.NO_BORDER);
                cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


                //Create the table which will be 2 Columns wide and make it 100% of the page

                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(130.0f);
//              table.setSpacingBefore(10);
                table.setWidths(new int[]{1, 1, 1});


                PdfPCell cellLogo = new PdfPCell();
                cellLogo.setBorder(PdfPCell.NO_BORDER);
                cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cellLogo.setPaddingTop(5);
                cellLogo.setPaddingBottom(5);
                cellLogo.setPaddingRight(5);
                cellLogo.setPaddingLeft(7);

                try
                {

                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logonasreennew);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image img = Image.getInstance(stream.toByteArray());

                    cellLogo.addElement(img);

                } catch (IOException e) {

                    e.printStackTrace();
                }
                table.addCell(cellSpace);
                table.addCell(cellLogo);
                table.addCell(cellSpace);

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});

                ////

                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                cell.addElement(compMobileTag);
                cell.addElement(compMailTag);
                cell.addElement(compAddress1Tag);
                cell.addElement(compAddress2Tag);
                cell.addElement(compWebTag);

                table.addCell(cell);


//top right
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                Paragraph paragraph = new Paragraph("TAX INVOICE", font16Ul_Bold);


                switch (callingActivity) {

                    case ActivityConstants.ACTIVITY_QUOTATION:


                        paragraph = new Paragraph(" QUOTATION", font16Ul_Bold);

                        break;
                }


                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setSpacingAfter(2);

                cell.addElement(paragraph);
                cell.addElement(compVatTag);
                cell.addElement(compPoTag);


                table.addCell(cell);


                //Add the PdfPTable to the table
                document.add(table);


                //Create the table which will be 3 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{4, 2});


                /////////////////*****  customer data ******/////
                //            customer details

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


//            customer name
                String nam = strShopName;
                nam = getMaximumChar(nam, 110);
                paragraph = new Paragraph("Customer : " + nam, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer No label
//                paragraph = new Paragraph("Customer No ", font8);
//                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);

                //            vat number
                String justifiedVatNumber = strCustomerVat;
                if (justifiedVatNumber.length() < 44)
                    justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
                else
                    justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

                paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


//tel
                String justifiedMobNumber = strCustomerMob;
                if (justifiedMobNumber.length() < 44)
                    justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
                else
                    justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

                paragraph = new Paragraph("Tel : " + justifiedMobNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer Address
                String justifiedCustomerAddress = strShopAddress;
                if (justifiedCustomerAddress.length() < 44)
                    justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
                else
                    justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
                paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


                //             executive name and mobile

                String justifiedExecutiveMobile = " - "+execMob;

                if (justifiedExecutiveMobile.length() > 20)
                    justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

                String justifiedExecutiveName = execName;
                if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                    justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());

                paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                table.addCell(cell);

                /////////////////*****  invoice data ******/////

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


                //            invoice number
                String justifiedBill = strBillNumber;
                if (justifiedBill.length() < 28)
                    justifiedBill = String.format("%-40s", justifiedBill);
                else
                    justifiedBill = getMaximumChar(justifiedBill, 28);

                paragraph = new Paragraph("Invoice No : " + justifiedBill, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            sale order number label

                String justifiedPo = strSalePo;
                if (justifiedPo.length() < 28)
                    justifiedPo = String.format("%-40s", justifiedPo);
                else
                    justifiedPo = getMaximumChar(justifiedPo, 28);

                paragraph = new Paragraph("PO No : " + justifiedPo, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                //   cell.addElement(paragraph);

                //           bill date
                String justifiedDate = String.format("%-40s", strDate);
                paragraph = new Paragraph("Inv . Date : " + justifiedDate, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                String payType = "";

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_SALES:


                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }


                        break;
                    case ActivityConstants.ACTIVITY_SALE_REPORT:

                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }

                        break;
                }



//                pay method
                paragraph = new Paragraph("Pay.Method : "+payType, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                //                currency
                paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                table.addCell(cell);

/*
                cell = new PdfPCell();
                cell.setBorder(PdfPCell.BOX);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);

                table.addCell(cell);*/

                document.add(table);


                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(10);

                table.setWidths(new int[]{2, 4, 14, 3, 3, 4, 3, 3, 4, 4});  // {3, 5, 15, 3, 4, 4, 3, 4, 5}
                table.setWidthPercentage(100);
                table.setSpacingBefore(5);


                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("SN", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


   // Article code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Article No", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


     //description
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Product", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//quantity
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Qty", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);

                table.addCell(cell);

//  unit
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);

                //  unit price
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit Price", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             discount percentage
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                // item code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             tax %
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT(5%)", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//              product total
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount+Vat", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                for (int j = 0; j < MAX_LINE; j++) {

                    String strSl_No = " ", strP_Code = " ", strP_Name = " ", strP_Arabic = "  ", strP_Unit = " ", strQty = " ",
                            strUnitPrice = " ", strDisc = " ", strAmount = " ", strTotalPrice = " ", str_vatPercentage = " ",
                            str_vatAmount = " ", str_netAmount = "", strArticlecode="";

                    if (cartList.size() > j) {
                        CartItem cartItem = cartList.get(j);

                        int slNo = i * MAX_LINE + j + 1;
                        strSl_No = String.valueOf(slNo);
                        strP_Name = cartItem.getProductName();
                        strP_Arabic = cartItem.getArabicName();
                        strP_Code = cartItem.getProductCode();
                        strArticlecode = cartItem.getArticleCode();


                        if (strP_Arabic == null || TextUtils.isEmpty(strP_Arabic.trim()) || strP_Arabic.equals("null"))
                            strP_Arabic = "  ";

                        strQty = "0/" + String.valueOf(cartItem.getTypeQuantity());
                        double netPrice = cartItem.getNetPrice();
                        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE)) {
                            netPrice = netPrice * cartItem.getPiecepercart();
                            strQty = cartItem.getTypeQuantity() + "/0";
                        }

                        strQty = String.valueOf(cartItem.getTypeQuantity());

                        double discountedPrice=  getPercentageValue(netPrice,cartItem.getDiscountPerce());


                        strDisc =  getAmount(discountedPrice);



                        str_vatPercentage = String.valueOf(cartItem.getTax() + " %");

                        str_vatAmount = getAmount(cartItem.getTaxValue() * cartItem.getPieceQuantity());

                        double vatamnt = Double.parseDouble(str_vatAmount);

                        strTotalPrice = getAmount((cartItem.getNetPrice() * cartItem.getPieceQuantity())+vatamnt);
                        str_netAmount = getAmount(cartItem.getNetPrice() * cartItem.getPieceQuantity());


                        strP_Unit = cartItem.getOrderType();

                        strUnitPrice = getAmount(netPrice);


                    }


                    if (strP_Name.length() > 40)
                        strP_Name = getMaximumChar(strP_Name, 40);


                    if (strP_Arabic.length() > 42)
                        strP_Arabic = getMaximumChar(strP_Arabic, 42);


                    String justifiedSlNo = String.format("%-3s", strSl_No);
                    String justifiedCode = String.format("%-5s", strP_Code);
                    String justifiedArticleCode = String.format("%-5s", strArticlecode);
                    String justifiedQuantity = String.format("%-5s", strQty);
                    String justifiedUnitPrice = String.format("%-5s", strUnitPrice);
                    String justifiedDisc = String.format("%-5s", strDisc);
                    String justifiedVatPercentage = String.format("%-5s", str_vatPercentage);
                    String justifiedVatAmount = String.format("%-5s", str_vatAmount);
                    String justifiedTotal = String.format("%-5s", strTotalPrice);
                    String justifiedNetTotal = String.format("%-5s", str_netAmount);



//sl number
                    cell = new PdfPCell(new Phrase(justifiedSlNo, font9));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setFixedHeight(3f);
                    table.addCell(cell);


//                    Article code
                    cell = new PdfPCell(new Phrase(""+strArticlecode, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


//                    item desc
                    /*
                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                        cell = new PdfPCell(new Phrase(strP_Name + "\n" + arabicPro.process(strP_Arabic), fontArb8));
                    else
                        cell = new PdfPCell(new Phrase(strP_Name +"\n"+ Chunk.NEWLINE, fontArb8));
*/

                    cell = new PdfPCell(new Phrase(strP_Name, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

/*
//                    item desc
                    cell = new PdfPCell();
                    paragraph = new Paragraph(strP_Name, font8);
                    cell.addElement(paragraph);

                    paragraph = new Paragraph(arabicPro.process(strP_Arabic), fontArb8);
                    paragraph.setAlignment(Element.ALIGN_RIGHT);

                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                    cell.addElement(paragraph);
                    else
                    cell.addElement( Chunk.NEWLINE);

                    cell.setBorder(Rectangle.RIGHT);
                    table.addCell(cell);
*/


                    //  quantity
                    cell = new PdfPCell(new Phrase(justifiedQuantity, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //  unit
                    cell = new PdfPCell(new Phrase(strP_Unit, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    //  unit price
                    cell = new PdfPCell(new Phrase(justifiedUnitPrice, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //Net Amount
                    cell = new PdfPCell(new Phrase(justifiedNetTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //  Vat Percentage
                    cell = new PdfPCell(new Phrase(justifiedVatPercentage, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //vat amount
                    cell = new PdfPCell(new Phrase(justifiedVatAmount, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    // total
                    cell = new PdfPCell(new Phrase(justifiedTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);


                }

                document.add(table);

                //////////////////////******   total details ****//////////////////////

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{6, 2});

                /* Taxable Value label */
                cell = new PdfPCell(new Phrase("Total Amount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.TOP);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            Taxable amount
                cell = new PdfPCell(new Phrase(netTotal, font10Bold));
                cell.setBorder(Rectangle.BOX);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* discount Value label */
                cell = new PdfPCell(new Phrase("Discount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            discount amount
                cell = new PdfPCell(new Phrase(discount, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* vat Value label */
                cell = new PdfPCell(new Phrase("Total Vat 5%", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            vat amount
                cell = new PdfPCell(new Phrase(totalVat, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                /*amount in words  */
                cell = new PdfPCell(new Phrase("Net : " + val_in_english , font10Bold)); //Total amount inclusive of Tax
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            grand total  amount
                cell = new PdfPCell(new Phrase(grandTotal, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                document.add(table);

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});
                table.setSpacingBefore(40);

                //            Receiver Sign
                cell = new PdfPCell(new Phrase("Receiver’s Sign ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

                //            sales executive
                cell = new PdfPCell(new Phrase("Sales Executive ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

           /*
                String justifiedExecutiveWords = String.format("%-40s", execName);

                paragraph = new Paragraph(justifiedExecutiveWords, font8);
                paragraph.setPaddingTop(20);
                cell.addElement(paragraph);
*/
                document.add(table);

            }


            //  ################ *********************  ####################

            document.close();

            printPDF(myFile);  //Print PDF File

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
            printLog(TAG, "exception  " + e.getMessage());
            Toast.makeText(this, "Error, unable to write to file\n" + e.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }




    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void printInvoice(List<PdfModel> pdfList) {

        File myFile = null;

        PdfWriter writer;

        try {

            String compName = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME);  //name get from session
            String compNameArab = sessionValue.getCompanyDetails().get(PREF_COMPANY_NAME_ARAB);  //name get from session
            String address1Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1);  //address get from session
            String address1ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_1_ARAB);  //address get from session
            String address2Str = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2);  //address get from session
            String address2ArabStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_ADDRESS_2_ARAB);  //address get from session
            String compemailStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_EMAIL);  //mail get from session
            String mobileStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_MOBILE);  //mobile get from session

            String compRegisterStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_CR);
            String companyVatStr = sessionValue.getCompanyDetails().get(PREF_COMPANY_VAT);

            String execName = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_NAME);  //name get from session
            assert execName != null;
            execName=execName.toUpperCase();
            String execId = "Code     : " + sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_ID);  //id get from session
            String execMob = sessionValue.getExecutiveDetails().get(PREF_EXECUTIVE_MOBILE);  //mob get from session


            //  page size custom
          /*  Rectangle pagesize = new Rectangle(521, 757);
              Document document = new Document(pagesize);*/

            // Create New Blank Document
            Document document = new Document(PageSize.A4);

            // document.setMargins(40, 0,0,0);

            writer = PdfWriter.getInstance(document, new FileOutputStream(FILE_PATH));

            myFile = new File(FILE_PATH);

            document.open();

//          BaseFont bf = BaseFont.createFont("/assets/tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont bf = BaseFont.createFont("/assets/dejavu_sans_condensed.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            Font font20 = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);

            Font font18 = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);

            Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
            Font font16Ul_Bold = new Font(Font.FontFamily.HELVETICA, 23, Font.UNDERLINE | Font.BOLD);


            Font font10Bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
            Font font9 = new Font(Font.FontFamily.HELVETICA, 9);

            Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
            Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
            Font font8Bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);

            Font font12 = new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC);

            LanguageProcessor arabicPro = new ArabicLigaturizer();
            Font fontArb8 = new Font(bf, 8);
            Font fontArb8Bold = new Font(bf, 8, Font.BOLD);
            Font fontArb10 = new Font(bf, 10);
            Font fontArb14 = new Font(bf, 14);

            for (int i = 0; i < pdfList.size(); i++) {


                PdfModel pdfData = pdfList.get(i);

                List<CartItem> cartList = pdfData.getCartItems();

                String netTotal = "", discount = "", totalWithoutDiscount = "", roundOff = "";

                String grandTotal = "";
                String paid = "";
                String balance = "";

                String val_in_english = "";
                String val_in_Arabic = "";


                String totalVat = "";

                if (pdfList.size() == i + 1) {

                    totalWithoutDiscount = getAmount(SELECTED_SALES.getTotal() + SELECTED_SALES.getDiscountTotal());
                    netTotal = getAmount(getNetTotal());

                    grandTotal = getAmount(SELECTED_SALES.getTotal());
                    paid = getAmount(paid_amount);
                    discount = getAmount(SELECTED_SALES.getDiscountTotal());
                    balance = " " + getString(R.string.currency);
                    totalVat = getAmount(getTaxTotal());


                    val_in_english = convertNumberToEnglishWords(String.valueOf(SELECTED_SALES.getTotal()));
                    val_in_Arabic = convertNumberToArabicWords(String.valueOf(SELECTED_SALES.getTotal()));

                }


                Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font10Bold);
                compMobileTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compMailTag = new Paragraph("Email:"+compemailStr, font10Bold);
                compMailTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress1Tag = new Paragraph(address1Str, font10Bold);
                compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress2Tag = new Paragraph(address2Str, font10Bold);
                compAddress2Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compWebTag = new Paragraph("www.nasreenoil.com", font10Bold);
                compWebTag.setAlignment(Element.ALIGN_LEFT);


                Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font10Bold);
                compVatTag.setAlignment(Element.ALIGN_RIGHT);

                Paragraph compPoTag = new Paragraph("LPO No : " + strSalePo, font10Bold);
                compPoTag.setAlignment(Element.ALIGN_RIGHT);

                PdfPCell cell;  //default cell

                //space cell
                PdfPCell cellSpace = new PdfPCell();
                cellSpace.setPadding(10);
                cellSpace.setBorder(PdfPCell.NO_BORDER);
                cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


                //Create the table which will be 2 Columns wide and make it 100% of the page

                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(130.0f);
//              table.setSpacingBefore(10);
                table.setWidths(new int[]{1, 1, 1});


                PdfPCell cellLogo = new PdfPCell();
                cellLogo.setBorder(PdfPCell.NO_BORDER);
                cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cellLogo.setPaddingTop(5);
                cellLogo.setPaddingBottom(5);
                cellLogo.setPaddingRight(5);
                cellLogo.setPaddingLeft(7);

                try
                {

                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logonasreennew);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image img = Image.getInstance(stream.toByteArray());

                    cellLogo.addElement(img);

                } catch (IOException e) {

                    e.printStackTrace();
                }
                table.addCell(cellSpace);
                table.addCell(cellLogo);
                table.addCell(cellSpace);

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});

                ////

                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                cell.addElement(compMobileTag);
                cell.addElement(compMailTag);
                cell.addElement(compAddress1Tag);
                cell.addElement(compAddress2Tag);
                cell.addElement(compWebTag);

                table.addCell(cell);


//top right
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                Paragraph paragraph = new Paragraph("TAX INVOICE", font16Ul_Bold);


                switch (callingActivity) {

                    case ActivityConstants.ACTIVITY_QUOTATION:


                        paragraph = new Paragraph(" QUOTATION", font16Ul_Bold);

                        break;
                }


                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setSpacingAfter(2);

                cell.addElement(paragraph);
                cell.addElement(compVatTag);
                cell.addElement(compPoTag);


                table.addCell(cell);


                //Add the PdfPTable to the table
                document.add(table);


                //Create the table which will be 3 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{4, 2});


                /////////////////*****  customer data ******/////
                //            customer details

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


//            customer name
                String nam = strShopName;
                nam = getMaximumChar(nam, 60);
                paragraph = new Paragraph("Customer : " + nam, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer No label
//                paragraph = new Paragraph("Customer No ", font8);
//                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);

                //            vat number
                String justifiedVatNumber = strCustomerVat;
                if (justifiedVatNumber.length() < 44)
                    justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
                else
                    justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

                paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


//tel
                String justifiedMobNumber = strCustomerMob;
                if (justifiedMobNumber.length() < 44)
                    justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
                else
                    justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

                paragraph = new Paragraph("Tel : " + justifiedMobNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer Address
                String justifiedCustomerAddress = strShopAddress;
                if (justifiedCustomerAddress.length() < 44)
                    justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
                else
                    justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
                paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


                //             executive name and mobile

                String justifiedExecutiveMobile = " - "+execMob;

                if (justifiedExecutiveMobile.length() > 20)
                    justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

                String justifiedExecutiveName = execName;
                if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                    justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());

                paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                table.addCell(cell);

                /////////////////*****  invoice data ******/////

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


                //            invoice number
                String justifiedBill = strBillNumber;
                if (justifiedBill.length() < 28)
                    justifiedBill = String.format("%-40s", justifiedBill);
                else
                    justifiedBill = getMaximumChar(justifiedBill, 28);

                paragraph = new Paragraph("Invoice No : " + justifiedBill, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            sale order number label

                String justifiedPo = strSalePo;
                if (justifiedPo.length() < 28)
                    justifiedPo = String.format("%-40s", justifiedPo);
                else
                    justifiedPo = getMaximumChar(justifiedPo, 28);

                paragraph = new Paragraph("PO No : " + justifiedPo, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                //   cell.addElement(paragraph);

                //           bill date
                String justifiedDate = String.format("%-40s", strDate);
                paragraph = new Paragraph("Inv . Date : " + justifiedDate, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                String payType = "";

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_SALES:


                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }


                        break;
                    case ActivityConstants.ACTIVITY_SALE_REPORT:

                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }

                        break;
                }



//                pay method
                paragraph = new Paragraph("Pay.Method : "+payType, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                //                currency
                paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                table.addCell(cell);

/*
                cell = new PdfPCell();
                cell.setBorder(PdfPCell.BOX);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);

                table.addCell(cell);*/

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(9);
                //3
                table.setWidths(new int[]{2, 15, 3, 4, 4, 4, 3, 4, 5});  // {2, 15, 3, 4, 4, 4, 3, 4, 5}
                table.setWidthPercentage(100);
                table.setSpacingBefore(5);


                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("SN", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


/*// Article code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Article No", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);*/


//description
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Product", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//quantity
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Qty", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);

                table.addCell(cell);

//  unit
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);

                //  unit price
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit Price", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             discount percentage
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                // item code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             tax %
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT(5%)", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//              product total
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount+Vat", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                for (int j = 0; j < MAX_LINE; j++) {

                    String strSl_No = " ", strP_Code = " ", strP_Name = " ", strP_Arabic = "  ", strP_Unit = " ", strQty = " ",
                            strUnitPrice = " ", strDisc = " ", strAmount = " ", strTotalPrice = " ", str_vatPercentage = " ",
                            str_vatAmount = " ", str_netAmount = "";

                    if (cartList.size() > j) {
                        CartItem cartItem = cartList.get(j);

                        int slNo = i * MAX_LINE + j + 1;
                        strSl_No = String.valueOf(slNo);
                        strP_Name = cartItem.getProductName();
                        strP_Arabic = cartItem.getArabicName();
                        strP_Code = cartItem.getProductCode();


                      /*  strArticlecode = cartItem.getArticleCode();
                        Log.e("Article Code", ""+strArticlecode);*/

                        if (strP_Arabic == null || TextUtils.isEmpty(strP_Arabic.trim()) || strP_Arabic.equals("null"))
                            strP_Arabic = "  ";

                        strQty = "0/" + String.valueOf(cartItem.getTypeQuantity());
                        double netPrice = cartItem.getNetPrice();
                        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE)) {
                            netPrice = netPrice * cartItem.getPiecepercart();
                            strQty = cartItem.getTypeQuantity() + "/0";
                        }

                        strQty = String.valueOf(cartItem.getTypeQuantity());

                        double discountedPrice=  getPercentageValue(netPrice,cartItem.getDiscountPerce());

                        strDisc =  getAmount(discountedPrice);

                        str_vatPercentage = String.valueOf(cartItem.getTax() + " %");

                        str_vatAmount = getAmount(cartItem.getTaxValue() * cartItem.getPieceQuantity());

                        double vatamnt = Double.parseDouble(str_vatAmount);

                        strTotalPrice = getAmount((cartItem.getNetPrice() * cartItem.getPieceQuantity())+vatamnt);
                        str_netAmount = getAmount(cartItem.getNetPrice() * cartItem.getPieceQuantity());


                        strP_Unit = cartItem.getOrderType();

                        strUnitPrice = getAmount(netPrice);


                    }


                    if (strP_Name.length() > 40)
                        strP_Name = getMaximumChar(strP_Name, 40);


                    if (strP_Arabic.length() > 42)
                        strP_Arabic = getMaximumChar(strP_Arabic, 42);


                    String justifiedSlNo = String.format("%-3s", strSl_No);
                    String justifiedCode = String.format("%-5s", strP_Code);
                    String justifiedQuantity = String.format("%-5s", strQty);
                    String justifiedUnitPrice = String.format("%-5s", strUnitPrice);
                    String justifiedDisc = String.format("%-5s", strDisc);
                    String justifiedVatPercentage = String.format("%-5s", str_vatPercentage);
                    String justifiedVatAmount = String.format("%-5s", str_vatAmount);
                    String justifiedTotal = String.format("%-5s", strTotalPrice);
                    String justifiedNetTotal = String.format("%-5s", str_netAmount);



//sl number
                    cell = new PdfPCell(new Phrase(justifiedSlNo, font9));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setFixedHeight(3f);
                    table.addCell(cell);


/*//                    Article code
                    cell = new PdfPCell(new Phrase(""+justifiedArticleCode, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);*/


//                    item desc
                    /*
                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                        cell = new PdfPCell(new Phrase(strP_Name + "\n" + arabicPro.process(strP_Arabic), fontArb8));
                    else
                        cell = new PdfPCell(new Phrase(strP_Name +"\n"+ Chunk.NEWLINE, fontArb8));
*/

                    cell = new PdfPCell(new Phrase(strP_Name, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

/*
//                  item desc
                    cell = new PdfPCell();
                    paragraph = new Paragraph(strP_Name, font8);
                    cell.addElement(paragraph);
                    paragraph = new Paragraph(arabicPro.process(strP_Arabic), fontArb8);
                    paragraph.setAlignment(Element.ALIGN_RIGHT);

                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                    cell.addElement(paragraph);
                    else
                    cell.addElement( Chunk.NEWLINE);
                    cell.setBorder(Rectangle.RIGHT);
                    table.addCell(cell);
*/


                    //  quantity
                    cell = new PdfPCell(new Phrase(justifiedQuantity, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //  unit
                    cell = new PdfPCell(new Phrase(strP_Unit, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    //  unit price
                    cell = new PdfPCell(new Phrase(justifiedUnitPrice, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //Net Amount
                    cell = new PdfPCell(new Phrase(justifiedNetTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //  Vat Percentage
                    cell = new PdfPCell(new Phrase(justifiedVatPercentage, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //vat amount
                    cell = new PdfPCell(new Phrase(justifiedVatAmount, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    // total
                    cell = new PdfPCell(new Phrase(justifiedTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);


                }

                document.add(table);

                //////////////////////******   total details ****//////////////////////

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{6, 2});

                /* Taxable Value label */
                cell = new PdfPCell(new Phrase("Total Amount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.TOP);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            Taxable amount
                cell = new PdfPCell(new Phrase(netTotal, font10Bold));
                cell.setBorder(Rectangle.BOX);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* discount Value label */
                cell = new PdfPCell(new Phrase("Discount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            discount amount
                cell = new PdfPCell(new Phrase(discount, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* vat Value label */
                cell = new PdfPCell(new Phrase("Total Vat 5%", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            vat amount
                cell = new PdfPCell(new Phrase(totalVat, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                /*amount in words  */
                cell = new PdfPCell(new Phrase("Net : " + val_in_english , font10Bold)); //Total amount inclusive of Tax
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            grand total  amount
                cell = new PdfPCell(new Phrase(grandTotal, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                document.add(table);

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});
                table.setSpacingBefore(40);

                //            Receiver Sign
                cell = new PdfPCell(new Phrase("Receiver’s Sign ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

                //            sales executive
                cell = new PdfPCell(new Phrase("Sales Executive ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

           /*

                String justifiedExecutiveWords = String.format("%-40s", execName);

                paragraph = new Paragraph(justifiedExecutiveWords, font8);
                paragraph.setPaddingTop(20);
                cell.addElement(paragraph);
*/
                document.add(table);

            }

            // ################# *********************  #####################

            for (int i = 0; i < pdfList.size(); i++) {


                PdfModel pdfData = pdfList.get(i);

                List<CartItem> cartList = pdfData.getCartItems();

                String netTotal = "", discount = "", totalWithoutDiscount = "", roundOff = "";

                String grandTotal = "";
                String paid = "";
                String balance = "";

                String val_in_english = "";
                String val_in_Arabic = "";


                String totalVat = "";

                if (pdfList.size() == i + 1) {

                    totalWithoutDiscount = getAmount(SELECTED_SALES.getTotal() + SELECTED_SALES.getDiscountTotal());
                    netTotal = getAmount(getNetTotal());

                    grandTotal = getAmount(SELECTED_SALES.getTotal());
                    paid = getAmount(paid_amount);
                    discount = getAmount(SELECTED_SALES.getDiscountTotal());
                    balance = " " + getString(R.string.currency);
                    totalVat = getAmount(getTaxTotal());


                    val_in_english = convertNumberToEnglishWords(String.valueOf(SELECTED_SALES.getTotal()));
                    val_in_Arabic = convertNumberToArabicWords(String.valueOf(SELECTED_SALES.getTotal()));

                }


                Paragraph compMobileTag = new Paragraph("Tel: " + mobileStr, font10Bold);
                compMobileTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compMailTag = new Paragraph("Email:"+compemailStr, font10Bold);
                compMailTag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress1Tag = new Paragraph(address1Str, font10Bold);
                compAddress1Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compAddress2Tag = new Paragraph(address2Str, font10Bold);
                compAddress2Tag.setAlignment(Element.ALIGN_LEFT);

                Paragraph compWebTag = new Paragraph("www.nasreenoil.com", font10Bold);
                compWebTag.setAlignment(Element.ALIGN_LEFT);


                Paragraph compVatTag = new Paragraph("Tax.Reg.No : " + companyVatStr, font10Bold);
                compVatTag.setAlignment(Element.ALIGN_RIGHT);

                Paragraph compPoTag = new Paragraph("LPO No : " + strSalePo, font10Bold);
                compPoTag.setAlignment(Element.ALIGN_RIGHT);

                PdfPCell cell;  //default cell

                //space cell
                PdfPCell cellSpace = new PdfPCell();
                cellSpace.setPadding(10);
                cellSpace.setBorder(PdfPCell.NO_BORDER);
                cellSpace.setHorizontalAlignment(Element.ALIGN_CENTER);


                //Create the table which will be 2 Columns wide and make it 100% of the page

                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(130.0f);
//              table.setSpacingBefore(10);
                table.setWidths(new int[]{1, 1, 1});


                PdfPCell cellLogo = new PdfPCell();
                cellLogo.setBorder(PdfPCell.NO_BORDER);
                cellLogo.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cellLogo.setPaddingTop(5);
                cellLogo.setPaddingBottom(5);
                cellLogo.setPaddingRight(5);
                cellLogo.setPaddingLeft(7);

                try
                {

                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logonasreennew);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image img = Image.getInstance(stream.toByteArray());

                    cellLogo.addElement(img);

                } catch (IOException e) {

                    e.printStackTrace();
                }
                table.addCell(cellSpace);
                table.addCell(cellLogo);
                table.addCell(cellSpace);

                document.add(table);

                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});

                ////

                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                cell.addElement(compMobileTag);
                cell.addElement(compMailTag);
                cell.addElement(compAddress1Tag);
                cell.addElement(compAddress2Tag);
                cell.addElement(compWebTag);

                table.addCell(cell);


//top right
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setPadding(5);


                Paragraph paragraph = new Paragraph("TAX INVOICE", font16Ul_Bold);


                switch (callingActivity) {

                    case ActivityConstants.ACTIVITY_QUOTATION:


                        paragraph = new Paragraph(" QUOTATION", font16Ul_Bold);

                        break;
                }


                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setSpacingAfter(2);

                cell.addElement(paragraph);
                cell.addElement(compVatTag);
                cell.addElement(compPoTag);


                table.addCell(cell);


                //Add the PdfPTable to the table
                document.add(table);


                //Create the table which will be 3 Columns wide and make it 100% of the page
                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{4, 2});


                /////////////////*****  customer data ******/////
                //            customer details

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.LEFT | PdfPCell.TOP | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


//            customer name
                String nam = strShopName;
                nam = getMaximumChar(nam, 60);
                paragraph = new Paragraph("Customer : " + nam, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer No label
//                paragraph = new Paragraph("Customer No ", font8);
//                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);

                //            vat number
                String justifiedVatNumber = strCustomerVat;
                if (justifiedVatNumber.length() < 44)
                    justifiedVatNumber = String.format("%-63s", justifiedVatNumber);
                else
                    justifiedVatNumber = getMaximumChar(justifiedVatNumber, 44);

                paragraph = new Paragraph("TRN No : " + justifiedVatNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


//tel
                String justifiedMobNumber = strCustomerMob;
                if (justifiedMobNumber.length() < 44)
                    justifiedMobNumber = String.format("%-63s", justifiedMobNumber);
                else
                    justifiedMobNumber = getMaximumChar(justifiedMobNumber, 44);

                paragraph = new Paragraph("Tel : " + justifiedMobNumber, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            customer Address
                String justifiedCustomerAddress = strShopAddress;
                if (justifiedCustomerAddress.length() < 44)
                    justifiedCustomerAddress = String.format("%-63s", justifiedCustomerAddress);
                else
                    justifiedCustomerAddress = getMaximumChar(justifiedCustomerAddress, 60);
                paragraph = new Paragraph("Address : " + justifiedCustomerAddress, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
//                cell.addElement(paragraph);


                //             executive name and mobile

                String justifiedExecutiveMobile = " - "+execMob;

                if (justifiedExecutiveMobile.length() > 20)
                    justifiedExecutiveMobile = getMaximumChar(justifiedExecutiveMobile, 20);

                String justifiedExecutiveName = execName;
                if (justifiedExecutiveName.length() > 74 - justifiedExecutiveMobile.length())
                    justifiedExecutiveName = getMaximumChar(justifiedExecutiveName, 74 - justifiedExecutiveMobile.length());

                paragraph = new Paragraph("Sales Executive : " + justifiedExecutiveName+justifiedExecutiveMobile, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                table.addCell(cell);

                /////////////////*****  invoice data ******/////

                cell = new PdfPCell();
                cell.setBorder(PdfPCell.TOP | PdfPCell.RIGHT | PdfPCell.BOTTOM);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);


                //            invoice number
                String justifiedBill = strBillNumber;
                if (justifiedBill.length() < 28)
                    justifiedBill = String.format("%-40s", justifiedBill);
                else
                    justifiedBill = getMaximumChar(justifiedBill, 28);

                paragraph = new Paragraph("Invoice No : " + justifiedBill, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                //            sale order number label

                String justifiedPo = strSalePo;
                if (justifiedPo.length() < 28)
                    justifiedPo = String.format("%-40s", justifiedPo);
                else
                    justifiedPo = getMaximumChar(justifiedPo, 28);

                paragraph = new Paragraph("PO No : " + justifiedPo, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                //   cell.addElement(paragraph);

                //           bill date
                String justifiedDate = String.format("%-40s", strDate);
                paragraph = new Paragraph("Inv . Date : " + justifiedDate, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                String payType = "";

                switch (callingActivity) {
                    case ActivityConstants.ACTIVITY_SALES:


                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }


                        break;
                    case ActivityConstants.ACTIVITY_SALE_REPORT:

                        if (paid_amount == SELECTED_SALES.getTotal()) {
                            payType = "CASH";
                        } else {
                            payType = "CREDIT";
                        }

                        break;
                }



//                pay method
                paragraph = new Paragraph("Pay.Method : "+payType, font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);

                //                currency
                paragraph = new Paragraph("Currency :  " + getString(R.string.currency), font10Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);


                table.addCell(cell);

/*
                cell = new PdfPCell();
                cell.setBorder(PdfPCell.BOX);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);

                table.addCell(cell);*/

                document.add(table);


                //Create the table which will be 2 Columns wide and make it 100% of the page
                table = new PdfPTable(9);

                table.setWidths(new int[]{2, 15, 3, 4, 4, 4, 3, 4, 5});  // {2, 15, 3, 4, 4, 4, 3, 4, 5}
                table.setWidthPercentage(100);
                table.setSpacingBefore(5);

                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("SN", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);

                // Article code
  /*              cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Article No", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);*/


                //description
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Product", font8Bold);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//quantity
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Qty", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);

                table.addCell(cell);

//  unit
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);

                //  unit price
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Unit Price", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             discount percentage
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                // item code
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//             tax %
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("VAT(5%)", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


//              product total
                cell = new PdfPCell();
                cell.setPaddingBottom(5);
                paragraph = new Paragraph("Amount+Vat", font8Bold);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(paragraph);
                cell.setRowspan(2);
                table.addCell(cell);


                for (int j = 0; j < MAX_LINE; j++) {

                    String strSl_No = " ", strP_Code = " ", strP_Name = " ", strP_Arabic = "  ", strP_Unit = " ", strQty = " ",
                            strUnitPrice = " ", strDisc = " ", strAmount = " ", strTotalPrice = " ", str_vatPercentage = " ",
                            str_vatAmount = " ", str_netAmount = "" ;

                    if (cartList.size() > j) {
                        CartItem cartItem = cartList.get(j);

                        int slNo = i * MAX_LINE + j + 1;
                        strSl_No = String.valueOf(slNo);
                        strP_Name = cartItem.getProductName();
                        strP_Arabic = cartItem.getArabicName();
                        strP_Code = cartItem.getProductCode();

                        if (strP_Arabic == null || TextUtils.isEmpty(strP_Arabic.trim()) || strP_Arabic.equals("null"))
                            strP_Arabic = "  ";

                        strQty = "0/" + String.valueOf(cartItem.getTypeQuantity());
                        double netPrice = cartItem.getNetPrice();
                        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE)) {
                            netPrice = netPrice * cartItem.getPiecepercart();
                            strQty = cartItem.getTypeQuantity() + "/0";
                        }

                        strQty = String.valueOf(cartItem.getTypeQuantity());

                        double discountedPrice=  getPercentageValue(netPrice,cartItem.getDiscountPerce());

                        strDisc =  getAmount(discountedPrice);


                        str_vatPercentage = String.valueOf(cartItem.getTax() + " %");

                        str_vatAmount = getAmount(cartItem.getTaxValue() * cartItem.getPieceQuantity());

                        double vatamnt = Double.parseDouble(str_vatAmount);

                        strTotalPrice = getAmount((cartItem.getNetPrice() * cartItem.getPieceQuantity())+vatamnt);
                        str_netAmount = getAmount(cartItem.getNetPrice() * cartItem.getPieceQuantity());


                        strP_Unit = cartItem.getOrderType();

                        strUnitPrice = getAmount(netPrice);


                    }


                    if (strP_Name.length() > 40)
                        strP_Name = getMaximumChar(strP_Name, 40);


                    if (strP_Arabic.length() > 42)
                        strP_Arabic = getMaximumChar(strP_Arabic, 42);


                    String justifiedSlNo = String.format("%-3s", strSl_No);
                    String justifiedCode = String.format("%-5s", strP_Code);

                    String justifiedQuantity = String.format("%-5s", strQty);
                    String justifiedUnitPrice = String.format("%-5s", strUnitPrice);
                    String justifiedDisc = String.format("%-5s", strDisc);
                    String justifiedVatPercentage = String.format("%-5s", str_vatPercentage);
                    String justifiedVatAmount = String.format("%-5s", str_vatAmount);
                    String justifiedTotal = String.format("%-5s", strTotalPrice);
                    String justifiedNetTotal = String.format("%-5s", str_netAmount);



//sl number
                    cell = new PdfPCell(new Phrase(justifiedSlNo, font9));
                    cell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setFixedHeight(3f);
                    table.addCell(cell);


          /*//                    Article code
                    cell = new PdfPCell(new Phrase(""+strArticlecode, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);*/


//                    item desc
                    /*
                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                        cell = new PdfPCell(new Phrase(strP_Name + "\n" + arabicPro.process(strP_Arabic), fontArb8));
                    else
                        cell = new PdfPCell(new Phrase(strP_Name +"\n"+ Chunk.NEWLINE, fontArb8));
*/

                    cell = new PdfPCell(new Phrase(strP_Name, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

/*
//                    item desc
                    cell = new PdfPCell();
                    paragraph = new Paragraph(strP_Name, font8);
                    cell.addElement(paragraph);

                    paragraph = new Paragraph(arabicPro.process(strP_Arabic), fontArb8);
                    paragraph.setAlignment(Element.ALIGN_RIGHT);

                    if (!TextUtils.isEmpty(strP_Arabic.trim()))
                    cell.addElement(paragraph);
                    else
                    cell.addElement( Chunk.NEWLINE);

                    cell.setBorder(Rectangle.RIGHT);
                    table.addCell(cell);
*/


                    //  quantity
                    cell = new PdfPCell(new Phrase(justifiedQuantity, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //  unit
                    cell = new PdfPCell(new Phrase(strP_Unit, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    //  unit price
                    cell = new PdfPCell(new Phrase(justifiedUnitPrice, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //Net Amount
                    cell = new PdfPCell(new Phrase(justifiedNetTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    //  Vat Percentage
                    cell = new PdfPCell(new Phrase(justifiedVatPercentage, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    //vat amount
                    cell = new PdfPCell(new Phrase(justifiedVatAmount, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    // total
                    cell = new PdfPCell(new Phrase(justifiedTotal, font9));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setPadding(2);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);


                }

                document.add(table);

                //////////////////////******   total details ****//////////////////////

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{6, 2});

                /* Taxable Value label */
                cell = new PdfPCell(new Phrase("Total Amount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.TOP);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            Taxable amount
                cell = new PdfPCell(new Phrase(netTotal, font10Bold));
                cell.setBorder(Rectangle.BOX);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* discount Value label */
                cell = new PdfPCell(new Phrase("Discount", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            discount amount
                cell = new PdfPCell(new Phrase(discount, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);


                /* vat Value label */
                cell = new PdfPCell(new Phrase("Total Vat 5%", font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            vat amount
                cell = new PdfPCell(new Phrase(totalVat, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                /*amount in words  */
                cell = new PdfPCell(new Phrase("Net : " + val_in_english , font10Bold)); //Total amount inclusive of Tax
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                //            grand total  amount
                cell = new PdfPCell(new Phrase(grandTotal, font10Bold));
                cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                document.add(table);

                table = new PdfPTable(2);
                table.setWidthPercentage(100.0f);
                table.setWidths(new int[]{2, 1});
                table.setSpacingBefore(40);

                //            Receiver Sign
                cell = new PdfPCell(new Phrase("Receiver’s Sign ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

                //            sales executive
                cell = new PdfPCell(new Phrase("Sales Executive ", font10Bold));
                cell.setBorder(PdfPCell.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_BASELINE);
                cell.setPadding(5);

                table.addCell(cell);

           /*
                String justifiedExecutiveWords = String.format("%-40s", execName);

                paragraph = new Paragraph(justifiedExecutiveWords, font8);
                paragraph.setPaddingTop(20);
                cell.addElement(paragraph);
*/
                document.add(table);

            }


            //  ################ *********************  ####################

            document.close();

            printPDF(myFile);  //Print PDF File

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
            printLog(TAG, "exception  " + e.getMessage());
            Toast.makeText(this, "Error, unable to write to file\n" + e.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void printPDF(final File file) {

        PrintDocumentAdapter pda = new PrintDocumentAdapter() {

            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
                InputStream input = null;
                FileOutputStream output = null;

                try {

//                  File file = new File(FILE_PATH);
                    input = new FileInputStream(file);

                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (Exception e) {
                    //Catch exception
                    printLog(TAG, "Exception  printPDF   2  " + e.getMessage());
                }
                finally {
                    try {

                        assert input != null;
                        input.close();
                        assert output != null;
                        output.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                        printLog(TAG, "Exception  printPDF   1 " + e.getMessage());
                    }
                }
            }

            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback callback, Bundle extras) {


                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }

                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }
        };


        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        String printName = FILE_PATH;
        assert printManager != null;
        printManager.print(printName, pda, null);


    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                printLog(TAG, "Permission is granted");
                return true;
            } else {

                printLog(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            printLog(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == WRITE_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            printLog(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission

            if (SELECTED_SHOP.getTypeId().equals("3")) {
                printInvoiceLulu(getPdfModels(cartItems));
            }else {
                printInvoice(getPdfModels(cartItems));
            }
        }


    }


}
