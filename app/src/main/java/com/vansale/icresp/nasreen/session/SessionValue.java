package com.vansale.icresp.nasreen.session;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.vansale.icresp.nasreen.model.Route;
import com.vansale.icresp.nasreen.model.RouteGroup;

import java.util.HashMap;

import static com.vansale.icresp.nasreen.config.ConfigKey.CODE_PREF_NAME;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_GROUP_IS_REGISTERED;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_NAME;
import static com.vansale.icresp.nasreen.config.ConfigKey.PRIVATE_MODE;


/**
 * Created by mentor on 26/10/17.
 */

public class SessionValue {


    public static final String PREF_SELECTED_GROUP = "group_pref";
    public static final String PREF_SELECTED_GROUP_ID = "group_id_pref";
    public static final String PREF_SELECTED_ROUTE = "route_pref";
    public static final String PREF_SELECTED_ROUTE_ID = "route_pref_id";
    public static final String PREF_DAY_REGISTER_ID = "day_reg_pref_id";

    //    Executive details
    public static final String PREF_EXECUTIVE_NAME = "executive_name_pref";
    public static final String PREF_EXECUTIVE_ID = "executive_id_pref";
    public static final String PREF_EXECUTIVE_MOBILE = "executive_mobile_pref";


    //    Main details
    public static final String PREF_COMPANY_NAME = "main_name_pref";
    public static final String PREF_COMPANY_NAME_ARAB = "main_name_arab_pref";
    public static final String PREF_COMPANY_ADDRESS_1 = "main_address1_pref";
    public static final String PREF_COMPANY_ADDRESS_2 = "main_address2_pref";
    public static final String PREF_COMPANY_ADDRESS_1_ARAB = "main_address1_arab_pref";
    public static final String PREF_COMPANY_ADDRESS_2_ARAB = "main_address2_arab_pref";
    public static final String PREF_COMPANY_EMAIL = "main_email_pref";
    public static final String PREF_COMPANY_MOBILE = "main_mob_pref";
    public static final String PREF_COMPANY_CR = "main_cr_pref";
    public static final String PREF_COMPANY_VAT = "main_vat_pref";


    private static final String IS_POS_ACTIVE = "isPdf_pref_key";


    public static final String PREF_SALE_TARGET = "sale_target_pref";

    private static final String PREF_INVOICE_CODE = "invoice_code";
    private static final String PREF_RECEIPT_CODE = "receipt_code";

    private SharedPreferences preferences, codePref;
    private SharedPreferences.Editor editor, codeEditor;
    private Context context;


    // Constructor
    @SuppressLint("CommitPrefEdits")
    public SessionValue(Context context) {
        this.context = context;

        preferences = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        codePref = this.context.getSharedPreferences(CODE_PREF_NAME, PRIVATE_MODE);


        editor = preferences.edit();
        codeEditor = codePref.edit();

    }

    /**
     * Get stored register status data
     */
    public boolean isGroupRegister() {

        if (new SessionAuth(context).isLoggedIn())
            return preferences.getBoolean(PREF_GROUP_IS_REGISTERED, false);// return register status
        else
            new SessionAuth(context).logoutUser();


        return false;

    }

    /**
     * set stored register status data
     */
    private void setGroupRegister(boolean status) {

        if (new SessionAuth(context).isLoggedIn()) {
            editor.putBoolean(PREF_GROUP_IS_REGISTERED, status);
            // commit changes
            editor.commit();
        } else
            new SessionAuth(context).logoutUser();


    }

    /**
     * set store register  data
     */
    public void storeGroupAndRoute(RouteGroup group, Route route, float target, String dayRegisterId) {

        if (new SessionAuth(context).isLoggedIn()) {

             //Store data
            if (group != null && route != null) {

                editor.putFloat(PREF_SALE_TARGET, target);
                editor.putBoolean(PREF_GROUP_IS_REGISTERED, true);
                editor.putString(PREF_SELECTED_GROUP, group.getGroupName());
                editor.putString(PREF_SELECTED_GROUP_ID, String.valueOf(group.getGroupId()));
                editor.putString(PREF_SELECTED_ROUTE, route.getRoute());
                editor.putString(PREF_SELECTED_ROUTE_ID, String.valueOf(route.getId()));
                editor.putString(PREF_DAY_REGISTER_ID, dayRegisterId);

            } else {

                editor.putBoolean(PREF_GROUP_IS_REGISTERED, false);
                editor.putString(PREF_SELECTED_GROUP, "");
                editor.putString(PREF_SELECTED_GROUP_ID, "");
                editor.putString(PREF_SELECTED_ROUTE, "");
                editor.putString(PREF_SELECTED_ROUTE_ID, "");
                editor.putString(PREF_DAY_REGISTER_ID, "");
                editor.putFloat(PREF_SALE_TARGET, 0);

            }
            // commit changes
            editor.commit();
        } else
            new SessionAuth(context).logoutUser();


    }


    /**
     * set store sale target
     */
    public void storeSaleTarget(float target) {

        if (new SessionAuth(context).isLoggedIn()) {

//Store data

            editor.putFloat(PREF_SALE_TARGET, target);
            // commit changes
            editor.commit();
        } else
            new SessionAuth(context).logoutUser();


    }

    /**
     * Get stored sale target
     */

    public float getSaleTarget() {

        if (new SessionAuth(context).isLoggedIn())
            return preferences.getFloat(PREF_SALE_TARGET, 0.0f);// return stock amount
        else
            new SessionAuth(context).logoutUser();


        return 0.0f;

    }


    /**
     * Get store register  data
     */

    public String getDayRegisterId() {

        // return values
        return preferences.getString(PREF_DAY_REGISTER_ID, "0");
    }

    public HashMap<String, String> getStoredValuesDetails() {
        HashMap<String, String> values = new HashMap<String, String>();
        // group
        values.put(PREF_SELECTED_GROUP, preferences.getString(PREF_SELECTED_GROUP, ""));
        values.put(PREF_SELECTED_GROUP_ID, preferences.getString(PREF_SELECTED_GROUP_ID, "0"));

        // route
        values.put(PREF_SELECTED_ROUTE, preferences.getString(PREF_SELECTED_ROUTE, ""));

        values.put(PREF_SELECTED_ROUTE_ID, preferences.getString(PREF_SELECTED_ROUTE_ID, "0"));

        // return values
        return values;
    }


    /**
     * Create executive details
     */


    public void createExecutiveDetails(String exeName, String exeId, String exeMob) {
        editor.putString(PREF_EXECUTIVE_NAME, exeName);
        editor.putString(PREF_EXECUTIVE_ID, exeId);
        editor.putString(PREF_EXECUTIVE_MOBILE, exeMob);
        // commit changes
        editor.commit();

    }

    /**
     * get executive details
     */

    public HashMap<String, String> getExecutiveDetails() {
        HashMap<String, String> values = new HashMap<String, String>();

        values.put(PREF_EXECUTIVE_NAME, preferences.getString(PREF_EXECUTIVE_NAME, "Executive"));
        values.put(PREF_EXECUTIVE_ID, preferences.getString(PREF_EXECUTIVE_ID, ""));
        values.put(PREF_EXECUTIVE_MOBILE, preferences.getString(PREF_EXECUTIVE_MOBILE, ""));
        // return values
        return values;
    }


    /**
     * Create main details
     */

    public void createCompanyDetails(String companyName, String companyNameArab, String address1, String address1Arab, String address2, String address2Arab, String companyMobile, String companyEmail, String companyCR, String companyVAT) {
        editor.putString(PREF_COMPANY_NAME, companyName);
        editor.putString(PREF_COMPANY_NAME_ARAB, companyNameArab);
        editor.putString(PREF_COMPANY_ADDRESS_1, address1);

        editor.putString(PREF_COMPANY_ADDRESS_1_ARAB, address1Arab);
        editor.putString(PREF_COMPANY_ADDRESS_2, address2);
        editor.putString(PREF_COMPANY_ADDRESS_2_ARAB, address2Arab);
        editor.putString(PREF_COMPANY_MOBILE, companyMobile);
        editor.putString(PREF_COMPANY_EMAIL, companyEmail);
        editor.putString(PREF_COMPANY_CR, companyCR);
        editor.putString(PREF_COMPANY_VAT, companyVAT);
        // commit changes
        editor.commit();

    }



    /**
     * get Main details
     */

    public HashMap<String, String> getCompanyDetails() {
        HashMap<String, String> values = new HashMap<String, String>();

        values.put(PREF_COMPANY_NAME, preferences.getString(PREF_COMPANY_NAME, "Nasreen"));
        values.put(PREF_COMPANY_NAME_ARAB, preferences.getString(PREF_COMPANY_NAME_ARAB, ""));
        values.put(PREF_COMPANY_ADDRESS_1, preferences.getString(PREF_COMPANY_ADDRESS_1, ""));
        values.put(PREF_COMPANY_ADDRESS_1_ARAB, preferences.getString(PREF_COMPANY_ADDRESS_1_ARAB, ""));

        values.put(PREF_COMPANY_ADDRESS_2, preferences.getString(PREF_COMPANY_ADDRESS_2, ""));
        values.put(PREF_COMPANY_ADDRESS_2_ARAB, preferences.getString(PREF_COMPANY_ADDRESS_2_ARAB, ""));
        values.put(PREF_COMPANY_CR, preferences.getString(PREF_COMPANY_CR, ""));
        values.put(PREF_COMPANY_MOBILE, preferences.getString(PREF_COMPANY_MOBILE, ""));
        values.put(PREF_COMPANY_EMAIL, preferences.getString(PREF_COMPANY_EMAIL, ""));
        values.put(PREF_COMPANY_VAT, preferences.getString(PREF_COMPANY_VAT, ""));

        // return values
        return values;
    }


    /****
     * invoice code store and get and clear
     * */

//     store invoice code
    public void storeInvoiceCode(String strKey, String strValue){
        codeEditor.putString(PREF_INVOICE_CODE+strKey, strValue);
        codeEditor.commit();
    }


    public String getInvoiceCode(String strKey){
        return codePref.getString(PREF_INVOICE_CODE+strKey,"110");// return
    }


    /****
     * receipt code store and get and clear
     * */

    public void storeReceiptCode(String strKey, String strValue){
        codeEditor.putString(PREF_RECEIPT_CODE+strKey, strValue);
        codeEditor.commit();
    }


    public String getReceiptCode(String strKey){
        return codePref.getString(PREF_RECEIPT_CODE+strKey,"110");// return
    }




    public void setPOSPrint(boolean isPos) {

        // Storing boolean value in pref
        editor.putBoolean(IS_POS_ACTIVE, isPos);
        // commit changes
        editor.commit();
    }

    // Get Login State
    public boolean isPOSPrint() {
        return preferences.getBoolean(IS_POS_ACTIVE, false);
    }


    public void clearCode() {
        // Clearing all data from Shared Preferences
        codeEditor.clear();
        codeEditor.commit();


    }
}
