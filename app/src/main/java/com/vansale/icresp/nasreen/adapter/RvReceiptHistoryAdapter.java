package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.ReceiptPreviewActivity;
import com.vansale.icresp.nasreen.holder.RvHistoryHolder;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.model.Receipt;
import com.vansale.icresp.nasreen.model.ReceiptHistory;
import com.vansale.icresp.nasreen.model.Shop;

import java.util.ArrayList;
import java.util.Date;

import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.INVOICE_LIST_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PAID_AMOUNT_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.RECEIPT_NO_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dateToFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.Generic.stringToDate;
import static com.vansale.icresp.nasreen.config.Generic.timeToFormat;


public class RvReceiptHistoryAdapter extends RecyclerView.Adapter<RvHistoryHolder> {


    private ArrayList<ReceiptHistory> historyList;

    private Context context;

    private Shop SELECTED_SHOP;


    public RvReceiptHistoryAdapter(ArrayList<ReceiptHistory> list, Shop SELECTED_SHOP) {
        this.historyList = list;
        this.SELECTED_SHOP = SELECTED_SHOP;
    }

    @Override
    public RvHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_report, parent, false);
        this.context = parent.getContext();
        return new RvHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(RvHistoryHolder holder, int position) {


        final ReceiptHistory history = historyList.get(position);
        final ArrayList<Receipt> r = history.getReceipts();

        int s = position + 1;

        if (!r.isEmpty()) {
            final Date date = stringToDate(r.get(0).getLogDate());

            holder.tvDate.setText(dateToFormat(date));
            holder.tvTime.setText(timeToFormat(date));
            holder.tvQty.setText(getAmount(totalReceivedAmount(r)) + context.getString(R.string.currency));
        }
        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvInvoiceNo.setText(history.getReceiptKey() + " (" + r.get(0).getTransactionType() + ")");


        holder.ibView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReceiptPreviewActivity.class);


                intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_INVOICE_RECEIPT);
                intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);
                intent.putExtra(PAID_AMOUNT_VALUE_KEY, totalReceivedAmount(r));
                intent.putExtra(RECEIPT_NO_KEY, history.getReceiptKey());

                intent.putExtra(INVOICE_LIST_KEY, r);

                context.startActivity(intent);
//                getActivity().finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != historyList ? historyList.size() : 0);
    }


    private double totalReceivedAmount(ArrayList<Receipt> receipts) {

        double total = 0;
        for (Receipt r : receipts) {
            double d = r.getReceivedAmount();
            total += d;
        }

        return total;
    }
}
