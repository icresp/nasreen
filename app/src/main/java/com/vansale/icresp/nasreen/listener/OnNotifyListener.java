package com.vansale.icresp.nasreen.listener;

/**
 * Created by mentor on 21/10/17.
 */

public interface OnNotifyListener {
    void onNotified();
}
