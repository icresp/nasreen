package com.vansale.icresp.nasreen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.fragment.HomeDashBoardLeftFragment;
import com.vansale.icresp.nasreen.fragment.ShopDashBoardFragment;
import com.vansale.icresp.nasreen.fragment.ShopFragment;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.listener.ShopClickListener;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionValue;

import static com.vansale.icresp.nasreen.config.ConfigKey.FRAGMENT_DASHBOARD_LEFT;
import static com.vansale.icresp.nasreen.config.ConfigKey.FRAGMENT_SHOPFRAGMENT;
import static com.vansale.icresp.nasreen.config.ConfigKey.FRAGMENT_SHOP_DASHBOARD;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;


public class MainActivity extends AppCompatActivity implements OnNotifyListener, ShopClickListener {

    String TAG = "HomeActivity";

    private SessionValue sessionValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.sessionValue = new SessionValue(MainActivity.this);


        final ShopFragment shopFragment = new ShopFragment();

//        Removing all fragment from fragment Manager
        getSupportFragmentManager().popBackStack();

// Let's first dynamically add a fragment into a frame container
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_home, shopFragment, FRAGMENT_SHOPFRAGMENT)
                .commit();


        onNotified();

    }


    @Override
    public void onBackPressed() {


        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            printLog(TAG, "if : " + getSupportFragmentManager().getBackStackEntryCount());
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {

            printLog(TAG, "else if : " + getSupportFragmentManager().getBackStackEntryCount());
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
        }
    }

    @Override
    public void onNotified() {

        try {

            final HomeDashBoardLeftFragment dashBoardLeftFragment = new HomeDashBoardLeftFragment();

//        Removing all fragment from fragment Manager


// Let's first dynamically add a fragment into a frame container
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragment_dash_board_left, dashBoardLeftFragment, FRAGMENT_DASHBOARD_LEFT)
                    .commit();


        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public void onShopClick(Shop shop) {


        // Let's first dynamically add a fragment into a frame container
        ShopDashBoardFragment shopDashBoardFragment = new ShopDashBoardFragment().newInstance(shop);

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_home, shopDashBoardFragment, FRAGMENT_SHOP_DASHBOARD);
        ft.addToBackStack(null);
        ft.commit();


    }

}
