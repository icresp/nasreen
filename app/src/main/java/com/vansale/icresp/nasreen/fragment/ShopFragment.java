package com.vansale.icresp.nasreen.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.DataServiceOperations;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.adapter.RvShopAdapter;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CustomerProduct;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.Product;
import com.vansale.icresp.nasreen.model.Route;
import com.vansale.icresp.nasreen.model.RouteGroup;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.textwatcher.TextValidator;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.PREF_KEY_NAME;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_ANY_TYPE;

import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.webservice.WebService.webAllRouteShop;
import static com.vansale.icresp.nasreen.webservice.WebService.webChangePassword;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetGroupByRoute;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetVanStock;
import static com.vansale.icresp.nasreen.webservice.WebService.webGroupByShop;
import static com.vansale.icresp.nasreen.webservice.WebService.webGroupList;
import static com.vansale.icresp.nasreen.webservice.WebService.webGroupRegister;
import static com.vansale.icresp.nasreen.webservice.WebService.webRegisteredShop;


public class ShopFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    OnNotifyListener mListener;

    String TAG = "ShopFragment";
    boolean isRegistered = false;
    private ArrayList<Shop> shops = new ArrayList<>();
    private ImageButton ibMore;
    private AppCompatSpinner spinnerRoute, spinnerGroup;

    private Button btnRegister, btnDayClose;
    private SessionValue sessionValue;
    private SessionAuth sessionAuth;
    private RvShopAdapter shopAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ErrorView errorView;
    private MyDatabase myDatabase;
    private String EXECUTIVE_ID = "";
    int route_id=0;

    private android.support.v7.app.AlertDialog createDialog;

    private DataServiceOperations serviceOperations;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);

        spinnerRoute = (AppCompatSpinner) view.findViewById(R.id.spinner_home_route);
        spinnerGroup = (AppCompatSpinner) view.findViewById(R.id.spinner_home_group);

        btnRegister = (Button) view.findViewById(R.id.button_home_register);
        ibMore = (ImageButton) view.findViewById(R.id.imageButton_home_menu);

        btnDayClose = (Button) view.findViewById(R.id.button_day_close);

        sessionValue = new SessionValue(getActivity());
        sessionAuth = new SessionAuth(getActivity());

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        errorView = (ErrorView) view.findViewById(R.id.errorView);
        myDatabase = new MyDatabase(getContext());

        shopAdapter = new RvShopAdapter(shops);
        this.serviceOperations = new DataServiceOperations(getContext(), new OnNotifyListener() {
            @Override
            public void onNotified() {
                enableViews();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(shopAdapter);

        try {
            EXECUTIVE_ID = sessionAuth.getExecutiveId();
        } catch (Exception e) {
            e.getMessage();
        }

        btnRegister.setOnClickListener(this);
        ibMore.setOnClickListener(this);

        btnDayClose.setOnClickListener(this);

        enableViews();
        return view;
    }

    private void getGroupAndRoutList() {

        Log.e("Get Route, Group", " calling1");
        if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }

        setProgressBar(true);

        final ArrayList<RouteGroup> groups = new ArrayList<>();
        groups.clear();

        RouteGroup g = new RouteGroup();
        g.setGroupName("SELECT GROUP ");
        g.setGroupId(-1);
        groups.add(0, g);

        final ArrayList<Route> routs = new ArrayList<>();
        routs.clear();

        Route route = new Route();
        route.setRoute("SELECT ROUTE ");
        route.setId(-1);
        routs.add(0, route);

        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Get Route, Group", " calling2");
        webGroupList(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    Log.e("Get Route, Group", " calling");

                    Log.e("Get Route, Group", "/"+response.toString());

                    if (response.getString("status").equals("success")) {

//                        groups
                      /*  if (!response.isNull("Groups")) {
                            JSONArray array = response.getJSONArray("Groups");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject groupObj = array.getJSONObject(i);
                                RouteGroup g = new RouteGroup();
                                g.setGroupName(groupObj.getString("name"));
                                g.setGroupId(groupObj.getInt("id"));
                                groups.add(g);

                            }
                        }*/
                        //                        groups

                        if (!response.isNull("Routes")) {
                            JSONArray array = response.getJSONArray("Routes");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject groupObj = array.getJSONObject(i);
                                Route r = new Route();
                                r.setRoute(groupObj.getString("name"));
                                r.setId(groupObj.getInt("id"));
                                routs.add(r);

                            }

                            setRouteSpinner(routs);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (groups.isEmpty())
                    setErrorView("No Groups", "", false);
                else{
                    // setRouteSpinner(groups, routs);

                }
            }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
                setRouteSpinner(routs);
            }
        }, object);

        /*spinnerRoute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Route r = (Route) spinnerRoute.getSelectedItem();

                  route_id = r.getId();

                getGroup_ByRoute();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
    }

    private void setLocalGroups() {

        final ArrayList<Route> routs = new ArrayList<>();
        Route route = new Route();
        route.setId(Integer.parseInt(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_ROUTE_ID)));
        route.setRoute(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_ROUTE));
        routs.add(route);

        final ArrayList<RouteGroup> groups = new ArrayList<>();

        RouteGroup g = new RouteGroup();
        g.setGroupName(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_GROUP));
        g.setGroupId(Integer.parseInt(sessionValue.getStoredValuesDetails().get(SessionValue.PREF_SELECTED_GROUP_ID)));

        groups.add(g);

        setRouteSpinner(routs);

        set_groupSpinner(groups);

    }

    private void setRouteSpinner( ArrayList<Route> routs) {


        // Initializing an ArrayAdapter
       /* final ArrayAdapter<RouteGroup> groupAdapter = new ArrayAdapter<RouteGroup>(getActivity(), R.layout.spinner_background, groups) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(getResources().getColor(R.color.colorGrayLight));
                }

                return view;
            }
        };

        groupAdapter.setDropDownViewResource(R.layout.spinner_list);

        spinnerGroup.setAdapter(groupAdapter);
        spinnerGroup.setSelection(0);

        spinnerGroup.setOnItemSelectedListener(this);*/

        // Initializing an ArrayAdapter
        final ArrayAdapter<Route> routeAdapter = new ArrayAdapter<Route>(getActivity(), R.layout.spinner_background, routs) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(getResources().getColor(R.color.colorGrayLight));
                }else {
                    tv.setTextColor(getResources().getColor(R.color.colorBlack));
                }

                return view;
            }
        };

        routeAdapter.setDropDownViewResource(R.layout.spinner_list);
        spinnerRoute.setAdapter(routeAdapter);

        spinnerRoute.setSelection(0);
        spinnerRoute.setOnItemSelectedListener(this);

        setProgressBar(false);

    }


    private void set_groupSpinner(ArrayList<RouteGroup> groups){

        // Initializing an ArrayAdapter
        final ArrayAdapter<RouteGroup> groupAdapter = new ArrayAdapter<RouteGroup>(getActivity(), R.layout.spinner_background, groups) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(getResources().getColor(R.color.colorGrayLight));
                }else {
                    tv.setTextColor(getResources().getColor(R.color.colorBlack));
                }

                return view;
            }
        };

        groupAdapter.setDropDownViewResource(R.layout.spinner_list);

        spinnerGroup.setAdapter(groupAdapter);
        spinnerGroup.setSelection(0);

        spinnerGroup.setOnItemSelectedListener(this);

    }

    //get Shops
    private void getRegisteredShops() {

        Log.e("Get", "Registered Shops");

        final ArrayList<Shop> list = myDatabase.getRegisteredCustomers();

        if (!list.isEmpty()) {

            Log.e("Shops", "Set Recycler");
            setRecyclerView(list);
            return;
        }

        if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }

        setProgressBar(true);

        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        list.clear();

        Log.e("GET Registered Object", "/"+object);

        webRegisteredShop(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                Log.e("GEt Registered shops", "/"+response);
                try {

                    if (response.getString("status").equals("success") && !response.isNull("Customers")) {

                        JSONArray array = response.getJSONArray("Customers");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject shopObj = array.getJSONObject(i);

                         //   printLog(TAG, "getRegisteredShops  shopObj " + shopObj);

                            Shop shop = new Shop();

                            int custId = shopObj.getInt("id");

                            shop.setShopId(custId);
                            shop.setShopCode(shopObj.getString("shope_code"));

                            Log.e("Shop details", "//"+shopObj.getString("shope_code"));

                            shop.setShopName(shopObj.getString("name"));
                            shop.setShopArabicName(shopObj.getString("arabic_name"));
                            shop.setTypeId(shopObj.getString("customer_type_id"));

                          //  shop.setTypeId("3");

                            shop.setVatNumber(shopObj.getString("vat_no"));

                            shop.setShopMail(shopObj.getString("email"));
                            shop.setShopMobile(shopObj.getString("mobile"));
                            shop.setShopAddress(shopObj.getString("address"));

                            shop.setCreditLimit(shopObj.getDouble("credit_limit"));

                            shop.setCredit(shopObj.getDouble("credit"));
                            shop.setDebit(shopObj.getDouble("debit"));

                            shop.setRouteCode(shopObj.getString("route_code"));

                            shop.setOpeningBalance(shopObj.getDouble("opening_balance"));

                            shop.setProductDiscount((float) shopObj.getDouble("product_discount"));
                            shop.setCollectionDiscount((float) shopObj.getDouble("collection_discount"));

                            ArrayList<Invoice> invoices = new ArrayList<>();

                            if (!shopObj.isNull("receipt")) {

                                JSONArray invoiceArray = shopObj.getJSONArray("receipt");

                                for (int j = 0; j < invoiceArray.length(); j++) {
                                    JSONObject invObj = invoiceArray.getJSONObject(j);

                                    Invoice inv = new Invoice();
                                    inv.setInvoiceId(invObj.getString("sale_id"));
                                    inv.setInvoiceNo(invObj.getString("invoice_no"));
                                    inv.setTotalAmount(invObj.getDouble("total"));
                                    inv.setBalanceAmount(invObj.getDouble("balance"));
                                    invoices.add(inv);

                                }
                            }
                            ArrayList<CustomerProduct> customerProducts = new ArrayList<>();

                            if (!shopObj.isNull("Customer_Products")) {

                                JSONArray productArray = shopObj.getJSONArray("Customer_Products");

                                for (int j = 0; j < productArray.length(); j++) {
                                    JSONObject invObj = productArray.getJSONObject(j);
                                    CustomerProduct p = new CustomerProduct();
                                    p.setProductId(invObj.getInt("product_id"));
                                    p.setCustomerId(custId);
                                    p.setPrice(invObj.getDouble("selling_rate"));

                                    customerProducts.add(p);
                                }
                            }

                            shop.setVisit(false);
                            shop.setInvoices(invoices);
                            shop.setProducts(customerProducts);

                            list.add(shop);
                        }
                    }

                } catch (JSONException e) {
                    printLog(TAG, "getRegisteredShops  Exception " + e.getLocalizedMessage());

                    e.getMessage();
                }

                        Log.e("List SIZE", ""+list.size());
                if (list.isEmpty()) {
                    setRecyclerView(list);
                    storeVanStock();// if customers is empty,store van stock
                } else
                    storeCustomersToOffline(list);

            }

            @Override
            public void onErrorResponse(String error) {


                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
            }
        }, object);


    }


    //    customer list with invoices to sqllite db
    private void storeCustomersToOffline(ArrayList<Shop> list) {

        for (Shop s : list) {
            boolean b = myDatabase.insertRegisteredCustomer(s);

        }

        setRecyclerView(myDatabase.getRegisteredCustomers());

        storeVanStock();// store van stock after storing customer

    }



    private void getGroup_ByRoute() {

        Log.e("Get Route by Group", " calling1");
        if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }

        setProgressBar(true);

        final ArrayList<RouteGroup> groups = new ArrayList<>();
        groups.clear();

        RouteGroup g = new RouteGroup();
        g.setGroupName("SELECT GROUP ");
        g.setGroupId(-1);
        groups.add(0, g);


        JSONObject object = new JSONObject();
        try {
            object.put("route_id", route_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("GroupbyRoute Object", "/"+object.toString());
        webGetGroupByRoute(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    Log.e("Get Group By Route", "/"+response.toString());

                   if (response.getString("status").equals("success")) {

//                        groups
                        if (!response.isNull("Groups")) {
                          JSONArray array = response.getJSONArray("Groups");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject groupObj = array.getJSONObject(i);
                                RouteGroup g = new RouteGroup();
                                g.setGroupName(groupObj.getString("name"));
                                g.setGroupId(groupObj.getInt("id"));
                                groups.add(g);

                            }

                        }


                        }
               //     }

                    setErrorView("No Groups", "", false);
                } catch (Exception e) {
                    e.printStackTrace();

                    setErrorView("No Groups", "", false);
                }

                if (groups.isEmpty() && groups.isEmpty())
                    setErrorView("No Groups", "", false);
                else
                    set_groupSpinner(groups);


            }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
              // setRouteSpinner(groups, routs);
            }
        }, object);



      /* spinnerRoute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Route r = (Route) spinnerRoute.getSelectedItem();

                 route_id = r.getId();



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
*/

    }



    //    Load Stock All List from Server
    private void storeVanStock() {

        if (myDatabase.isExistProducts()) {
            return;
        }


        if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }

        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Please wait...", false, false);

        final ArrayList<Product> products = new ArrayList<>();
        final JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        webGetVanStock(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

//                printLog(TAG,"storeVanStock   stockObject   "+response);
                    Log.e(TAG,"storeVanStock   stockObject   "+response);

                try {

                    if (response.getString("status").equals("success") && !response.isNull("Stocks")) {
                        JSONArray stockArray = response.getJSONArray("Stocks");
                        for (int i = 0; i < stockArray.length(); i++) {

                            JSONObject stockObject = stockArray.getJSONObject(i);
                            Product product = new Product();


                            product.setProductId(stockObject.getInt("product_id"));
                            product.setProductCode(stockObject.getString("product_code"));

                            String artCode = ""+stockObject.getString("article_no");

                            if (artCode.equalsIgnoreCase("null")){
                                artCode = " ";
                                product.setArticleCode(artCode);
                            }else {
                                product.setArticleCode(artCode);
                            }

                            product.setProductName(stockObject.getString("product_name"));

                            product.setArabicName(stockObject.getString("arabic_name"));

                            product.setProductType(stockObject.getString("product_type_name"));
                            product.setBrandName(stockObject.getString("brand_name"));

                            double wholeSale = stockObject.getDouble("product_wholesale_price");

                            double retailPrice = stockObject.getDouble("product_mrp");

                            float vat = (float) stockObject.getDouble("product_vat");

                            product.setWholeSalePrice(wholeSale);
                            product.setRetailPrice(retailPrice);
                            product.setTax(vat);

                            product.setCost(stockObject.getDouble("product_cost"));
                            product.setPiecepercart(stockObject.getInt("piece_per_cart"));

                            product.setStockQuantity(stockObject.getInt("stock_quantity"));

                            products.add(product);

                        }

                        float toatalStock = response.getLong("total_stock");
                        int stockQuantity = response.getInt("total_quantity");

                    } else {
                        Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.getMessage();

                    pd.dismiss();
                    printLog(TAG, "storeVanStock   Exception   " + e.getMessage());
                }


                if (!products.isEmpty()) {

                    for (Product p : products) {

                        Log.e("Article Code insert", ""+p.getArticleCode());
                        myDatabase.insertStock(p);   //storeVanStock to local
                    }
                }

                pd.dismiss();
                getAllRouteCustomer();

            }

            @Override
            public void onErrorResponse(String error) {

                Toast.makeText(getContext(), "VanStock  " + error, Toast.LENGTH_SHORT).show();

                pd.dismiss();

            }
        }, object);
    }


    //get group wise Shops
    private void getGroupByShops() {

        RouteGroup g = (RouteGroup) spinnerGroup.getSelectedItem();
        Route r = (Route) spinnerRoute.getSelectedItem();

        if (g.getGroupId() == -1 && r.getId() == -1) {
            Toast.makeText(getActivity(), "Please Select Group and Route", Toast.LENGTH_SHORT).show();
            return;

        } else if (g.getGroupId() == -1 && r.getId() != -1) {
            Toast.makeText(getActivity(), "Please Select Group", Toast.LENGTH_SHORT).show();
            return;
        } else if (g.getGroupId() != -1 && r.getId() == -1) {
            Toast.makeText(getActivity(), "Please Select Route", Toast.LENGTH_SHORT).show();
            return;
        } else if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }


        setProgressBar(true);

        final ArrayList<Shop> list = new ArrayList<>();
        list.clear();


        JSONObject object = new JSONObject();
        try {
            object.put("customer_group_id", g.getGroupId());
            object.put("route_id", r.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        printLog(TAG, "getGroupByShops  object " + object);

        webGroupByShop(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {
//                printLog(TAG, "getGroupByShops  response " + response);

                try {

                    if (response.getString("status").equals("success") && !response.isNull("Customers")) {

                        JSONArray array = response.getJSONArray("Customers");
                        for (int i = 0; i < array.length(); i++) {


                            JSONObject shopObj = array.getJSONObject(i);

                            printLog(TAG, "getGroupByShops  shopObj " + shopObj);

                            Shop shop = new Shop();

                            int custId = shopObj.getInt("id");
                            shop.setShopId(custId);
                            shop.setShopCode(shopObj.getString("shope_code"));
                            shop.setShopName(shopObj.getString("name"));

                            shop.setShopArabicName(shopObj.getString("arabic_name"));
                            shop.setVatNumber(shopObj.getString("vat_no"));
                            shop.setCreditLimit(shopObj.getDouble("credit_limit"));

                          //  shop.setTypeId(shopObj.getString("customer_type_id"));

                            shop.setShopMail(shopObj.getString("email"));
                            shop.setShopMobile(shopObj.getString("mobile"));
                            shop.setShopAddress(shopObj.getString("address"));

                            shop.setCredit(shopObj.getDouble("credit"));
                            shop.setDebit(shopObj.getDouble("debit"));

                            shop.setRouteCode(shopObj.getString("route_code"));

                            shop.setOpeningBalance(shopObj.getDouble("opening_balance"));
                            shop.setProductDiscount((float) shopObj.getDouble("product_discount"));
                            shop.setCollectionDiscount((float) shopObj.getDouble("collection_discount"));

                            ArrayList<Invoice> invoices = new ArrayList<>();

                            if (!shopObj.isNull("receipt")) {


                                JSONArray invoiceArray = shopObj.getJSONArray("receipt");


                                for (int j = 0; j < invoiceArray.length(); j++) {
                                    JSONObject invObj = invoiceArray.getJSONObject(j);
                                    Invoice inv = new Invoice();
                                    inv.setInvoiceId(invObj.getString("sale_id"));
                                    inv.setInvoiceNo(invObj.getString("invoice_no"));
                                    inv.setTotalAmount((float) invObj.getDouble("total"));
                                    inv.setBalanceAmount((float) invObj.getDouble("balance"));
                                    invoices.add(inv);

                                }
                            }

                            //////

                            ArrayList<CustomerProduct> customerProducts = new ArrayList<>();
/*
                            if (!shopObj.isNull("Customer_Products")) {
                                JSONArray productArray = shopObj.getJSONArray("Customer_Products");


                                for (int j = 0; j < productArray.length(); j++) {
                                    JSONObject invObj = productArray.getJSONObject(j);
                                    CustomerProduct p = new CustomerProduct();
                                    p.setProductId(invObj.getInt("product_id"));
                                    p.setCustomerId(custId);
                                    p.setPrice( invObj.getDouble("selling_rate"));

                                    customerProducts.add(p);

                                }
                            }

                            ////////////////
*/
                            shop.setInvoices(invoices);
                            shop.setProducts(customerProducts);

                            list.add(shop);
                        }
                    }

                } catch (JSONException e) {


                    e.getMessage();
                }

                setRecyclerView(list);
            }

            @Override
            public void onErrorResponse(String error) {


                setErrorView(error, getString(R.string.error_subtitle_failed_one_more_time), true);
            }
        }, object);


    }


    private void setRecyclerView(ArrayList<Shop> list) {

        if (list.isEmpty()) {

            Log.e("List", "Shops Empty");

            setErrorView("No Shops", "", false);
            return;
        }

        shops.clear();
        shops.addAll(list);

        shopAdapter.notifyDataSetChanged();
        recyclerView.setVisibility(View.VISIBLE);
        setProgressBar(false);
        errorView.setVisibility(View.GONE);

    }


    private void setProgressBar(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);

        } else
            progressBar.setVisibility(View.GONE);

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        errorView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        setProgressBar(false);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                enableViews();

            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_day_close:

                if (!checkConnection()) {
                    Toast.makeText(getActivity(), getContext().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();

                } else {

                    dayCloseConfirmationDialogue();

                   /* if (myDatabase.isDayclosePrmission())
                        dayCloseConfirmationDialogue();
                    else
                        Toast.makeText(getActivity(), "Please visit finish all shops", Toast.LENGTH_SHORT).show();*/

                }

                break;

            case R.id.button_home_register:

                try {

                    RouteGroup g = (RouteGroup) spinnerGroup.getSelectedItem();
                    Route r = (Route) spinnerRoute.getSelectedItem();

                    if (g.getGroupId() != -1 && r.getId() != -1)
                        registerConfirmationDialogue();
                    else if (g.getGroupId() == -1 && r.getId() != -1)
                        Toast.makeText(getActivity(), "Please Select Group", Toast.LENGTH_SHORT).show();
                    else if (g.getGroupId() != -1 && r.getId() == -1)
                        Toast.makeText(getActivity(), "Please Select Route", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), "Please Select Route and Group", Toast.LENGTH_SHORT).show();

                } catch (NullPointerException e) {
                    e.getMessage();

                }

                break;
            case R.id.imageButton_home_menu:
                showMenu(v);
                break;

        }
    }


    private void showMenu(View view) {


        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_actions, popup.getMenu());


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_change_password:

                        changePasswordDialog();

                        break;
                    case R.id.action_logout:


                        isRegistered = sessionValue.isGroupRegister();

                        if (!isRegistered) {
                            logoutDialogue();
                        } else {

                            Toast.makeText(getActivity(), "Please Day close before logout", Toast.LENGTH_SHORT).show();
                        }

                        break;

                    case R.id.action_exit:
                        getActivity().finish();

                        break;
                }

                return true;
            }
        });
        popup.show(); //showing popup menu
        //closing the setOnClickListener method

        //End PopUp Menu

    }

    //    register confirmation dialog
    private void registerConfirmationDialogue() {

        new AlertDialog.Builder(getActivity())
                .setMessage("Confirm Registration?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        registerGroup();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to register
                    }
                })
                .show();
    }


    private void registerGroup() {
        if (!checkConnection()) {
            setErrorView(getContext().getString(R.string.no_internet), "", false);
            return;
        }

        sessionValue.clearCode();  //clear invoice code

        myDatabase.deleteTableRequest(REQ_ANY_TYPE);

        RouteGroup g = (RouteGroup) spinnerGroup.getSelectedItem();
        Route r = (Route) spinnerRoute.getSelectedItem();

        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put("customer_group_id", g.getGroupId());
            object.put("route_id", r.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Executive reg", ""+object.toString());

        final ProgressDialog pd = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        webGroupRegister(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("Executive reg resp", ""+response.toString());
                try {
                    switch (response.getString("status")) {
                        case "success": {

                            JSONObject executiveObj = response.getJSONObject("ExProfile");

                            String exeName = executiveObj.getString("name");
                            String exeMobile = executiveObj.getString("mobile");
                            String exeId = executiveObj.getString("id");

                            String dayRegId = response.getString("day_register_id");

                            JSONObject companyObj = response.getJSONObject("CompProfile");

                            String compName = companyObj.getString("company_name");
                            String compNameArab = "مؤسسة هدريس جمعة";
//                            String compNameArab = companyObj.getString("company_name_arabic");

                            String compAddress1 = companyObj.getString("address_line_1");

//                            String compAddress1Arabic = companyObj.getString("address_line_1_arabic");
                            String compAddress1Arabic = "مركز القوافل - شارع  الملك فيصل ";

                            String compAddress2 = companyObj.getString("address_line_2");

                            String compAddress2Arabic = " جدة - حى الخمرة  س ت";
//                            String compAddress2Arabic = companyObj.getString("address_line_2_arabic");

                            String compMobile = companyObj.getString("mobile");
                            String compEmail = companyObj.getString("mail_address");
                            String compCr = companyObj.getString("cr_no");
                            String compVat = companyObj.getString("vat_code");


                            if (!response.isNull("RoutesList")) {
                                JSONArray invCodeArray = response.getJSONArray("RoutesList");
                                parceInvoiceCode(invCodeArray);
                            }
                            sessionValue.createExecutiveDetails(exeName, exeId, exeMobile);


                            sessionValue.createCompanyDetails(compName, compNameArab, compAddress1, compAddress1Arabic, compAddress2, compAddress2Arabic, compMobile, compEmail, compCr, compVat);


                            sessionValue.storeGroupAndRoute((RouteGroup) spinnerGroup.getSelectedItem(), (Route) spinnerRoute.getSelectedItem(), (float) response.getDouble("SaleTargets"), dayRegId);

                            Toast.makeText(getActivity(), "Registration Successful", Toast.LENGTH_SHORT).show();

                            enableViews();
                            break;
                        }
                        case "Already Registered": {

                            Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();

//                        rote values
                            Route r = new Route();
                            r.setId(response.getInt("route_id"));
                            r.setRoute(response.getString("route_name"));

                            RouteGroup g = new RouteGroup();
                            g.setGroupId(response.getInt("group_nid"));
                            g.setGroupName(response.getString("group_name"));

                            String dayRegId = response.getString("day_register_id");

                            sessionValue.storeGroupAndRoute(g, r, (float) response.getDouble("SaleTargets"), dayRegId);


                            JSONObject executiveObj = response.getJSONObject("ExProfile");

                            String exeName = executiveObj.getString("name");
                            String exeMobile = executiveObj.getString("mobile");
                            String exeId = executiveObj.getString("id");


                            JSONObject companyObj = response.getJSONObject("CompProfile");


                            String compName = companyObj.getString("company_name");
                            String compNameArab = "مؤسسة هدريس جمعة";
//                            String compNameArab = companyObj.getString("company_name_arabic");

                            String compAddress1 = companyObj.getString("address_line_1");

//                            String compAddress1Arabic = companyObj.getString("address_line_1_arabic");
                            String compAddress1Arabic = "مركز القوافل - شارع  الملك فيصل ";

                            String compAddress2 = companyObj.getString("address_line_2");

                            String compAddress2Arabic = " جدة - حى الخمرة  س ت";
//                            String compAddress2Arabic = companyObj.getString("address_line_2_arabic");

                            String compMobile = companyObj.getString("mobile");
                            String compEmail = companyObj.getString("mail_address");
                            String compCr = companyObj.getString("cr_no");
                            String compVat = companyObj.getString("vat_code");

                            if (!response.isNull("RoutesList")) {
                                JSONArray invCodeArray = response.getJSONArray("RoutesList");
                                parceInvoiceCode(invCodeArray);
                            }

                            sessionValue.createExecutiveDetails(exeName, exeId, exeMobile);

                            sessionValue.createCompanyDetails(compName, compNameArab, compAddress1, compAddress1Arabic, compAddress2, compAddress2Arabic, compMobile, compEmail, compCr, compVat);

                            enableViews();
                            break;
                        }
                        default:
                            Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

            }
        }, object);


    }

    private void parceInvoiceCode(JSONArray array) {

        try {
            for (int i = 0; i < array.length(); i++) {

                JSONObject object = array.getJSONObject(i);

                String code = object.getString("route_code");
                String invoiceNumber = object.getString("invoice_no");
                String receiptNumber = object.getString("receipt_no");

                sessionValue.storeInvoiceCode(code, invoiceNumber);
                sessionValue.storeReceiptCode(code, receiptNumber);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //    logout app
    private void logoutDialogue() {

        new AlertDialog.Builder(getActivity())
                .setMessage("Would you like to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // logout

                        new SessionAuth(getActivity()).logoutUser();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to logout
                    }
                })
                .show();
    }

    //    logout app
    private void dayCloseConfirmationDialogue() {

        new AlertDialog.Builder(getActivity())
                .setMessage("Confirm DayClose?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        serviceOperations.dayClose();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to register
                    }
                })
                .show();
    }

    private void enableViews() {

        isRegistered = sessionValue.isGroupRegister();

        spinnerRoute.setEnabled(!isRegistered);
        spinnerGroup.setEnabled(!isRegistered);
        btnRegister.setEnabled(!isRegistered);

        if (isRegistered) {
            btnRegister.setText("Registered");
            btnDayClose.setVisibility(View.VISIBLE);
            setLocalGroups();
            getRegisteredShops();
        } else
            {

            btnRegister.setText("Register");
            btnDayClose.setVisibility(View.GONE);
            getGroupAndRoutList();

            storeVanStock();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNotifyListener) {
            mListener = (OnNotifyListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_home_group:

                if (!isRegistered)
                    getGroupByShops();

                break;
            case R.id.spinner_home_route:

                if (!isRegistered)
                  //  getGroupByShops();

                    GetGroupCall();

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    @Override
    public void onStart() {
        super.onStart();

        if (shopAdapter != null) {
            final ArrayList<Shop> list = myDatabase.getRegisteredCustomers();
            setRecyclerView(list);

        }
    }

    // add new Customer details dialog
    @SuppressLint("SetTextI18n")
    private void changePasswordDialog() {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Change Password");

        /** Create TextView and EditText */

        final TextView tvExecutiveId = new TextView(getActivity());

        final TextView tvOldPwd = new TextView(getActivity());
        final EditText etOldPwd = new EditText(getActivity());

        final TextView tvNewPass = new TextView(getActivity());
        final EditText etNewPass = new EditText(getActivity());

        final TextView tvConfirmPass = new TextView(getActivity());
        final EditText etConfirmPass = new EditText(getActivity());


        /** set Configure TextView and EditText */

        tvExecutiveId.setTextColor(getActivity().getResources().getColor(R.color.primaryText));
        tvExecutiveId.setPadding(0, 5, 0, 5);


        etOldPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        etOldPwd.setBackgroundResource(R.color.colorTransparent);
        etOldPwd.setLines(1);

        etNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        etNewPass.setBackgroundResource(R.color.colorTransparent);
        etNewPass.setLines(1);


        etConfirmPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        etConfirmPass.setBackgroundResource(R.color.colorTransparent);
        etConfirmPass.setLines(1);


        tvExecutiveId.setText("User : " + sessionAuth.getUserDetails().get(PREF_KEY_NAME));
        tvOldPwd.setText("Old Password");
        tvNewPass.setText("Password");
        tvConfirmPass.setText("Confirm Password");


        /** Set Positive and Negative Buttons to Dialog */
        alertDialog.setPositiveButton("Change Password", null);
        alertDialog.setNegativeButton("Cancel", null);

        /** Create LinearLayout */
        LinearLayout ll = new LinearLayout(getActivity());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setPadding(20, 20, 20, 20);
//        ll.addView(tvExecutiveId);
//        ll.addView(etNewShopName);

        /** Add  TextView and EditText to LinearLayout*/

        ll.addView(tvExecutiveId);


        ll.addView(tvOldPwd);
        ll.addView(etOldPwd);

        ll.addView(tvNewPass);
        ll.addView(etNewPass);

        ll.addView(tvConfirmPass);
        ll.addView(etConfirmPass);
        /** Set LinearLayout to Dialog*/

        alertDialog.setView(ll);

        /** Comparisons PassWord and ConfirmPassWord using  TextValidator Class*/
        etConfirmPass.addTextChangedListener(new TextValidator(etConfirmPass) {
            @Override
            public void validate(TextView textView, String text) {
                /* Validation code here */
                if (!passwordStatus(etNewPass.getText().toString().trim(), text)) {
                    textView.setError("Password is Mismatch");
                }

            }
        });

        createDialog = alertDialog.create();

        //        show input keyboard
        etOldPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    createDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });


        createDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {


                android.widget.Button positive = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                android.widget.Button negative = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE);
                positive.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        String old = etOldPwd.getText().toString().trim();
                        String newPwd = etNewPass.getText().toString().trim();


                        if (validateCredential(etOldPwd, etNewPass, etConfirmPass))
                            changePasswordRequest(old, newPwd);


                    }
                });

                negative.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


            }

        /*    @Override
            public void onShow(final DialogInterface dialog) {
                *//** Initial Buttons *//*
                android.widget.Button positive = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                android.widget.Button negative = createDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE);
                positive.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {



                            if (validateCredential(etOldPwd,etNewPass,etConfirmPass))
                                Toast.makeText(getActivity(), "ok", Toast.LENGTH_SHORT).show();
//                                createUserRequest(object);


                    }
                });

                negative.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }*/
        });

        createDialog.show();

    }

    //  check equal two string
    private boolean passwordStatus(String newPass, String confirmPass) {

        boolean status = false;
        status = newPass.equals(confirmPass);
        return status;
    }


    public void GetGroupCall(){

        Route r = (Route) spinnerRoute.getSelectedItem();

        route_id = r.getId();
        if (route_id != -1) {
            getGroup_ByRoute();
        }

    }

    /**
     * Validation change passwordDialog
     */
    private boolean validateCredential(EditText oldPassword, EditText password, EditText confirmPass) {

        boolean status = false;

        if (TextUtils.isEmpty(oldPassword.getText().toString().trim())) {
            oldPassword.setError("Old Password is Empty");
            status = false;

        } else if (TextUtils.isEmpty(password.getText().toString().trim())) {
            password.setError("Password is Empty");
            status = false;

        } else if (TextUtils.isEmpty(confirmPass.getText().toString().trim())) {
            confirmPass.setError("Confirm Password is Empty");
            status = false;
        } else if (!passwordStatus(password.getText().toString().trim(), confirmPass.getText().toString().trim())) {
            confirmPass.setError("Password is Mismatch");
            status = false;
        } else {
            status = true;
        }


        return status;
    }

    //    change password request
    private void changePasswordRequest(String oldPwd, final String newPwd) {


        final JSONObject object = new JSONObject();
        try {


            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put("old_password", oldPwd);
            object.put("new_password", newPwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ProgressDialog pd = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);


        webChangePassword(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                printLog(TAG, "changePasswordRequest  " + response);

                try {
                    if (response.getString("status").equals("success")) {
                        sessionAuth.updatePassword(newPwd);
                        if (createDialog != null)
                            createDialog.dismiss();

                    } else
                        Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

            }
        }, object);


    }


    /***    customer search
     *
     */

    //get Shops
    private void getAllRouteCustomer() {


        if (!sessionValue.isGroupRegister())
            return;


        if (!checkConnection()) {
            Toast.makeText(getActivity(), getContext().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog pd = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);


        JSONObject object = new JSONObject();
        try {
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        webAllRouteShop(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                try {

                    if (response.getString("status").equals("success") && !response.isNull("Customers")) {

                        JSONArray array = response.getJSONArray("Customers");
                        new AsyncTaskLoadData().execute(array);

                        ////
                    }

                } catch (JSONException e) {


                    pd.dismiss();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

                pd.dismiss();


            }

            @Override
            public void onErrorResponse(String error) {


                pd.dismiss();
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }
        }, object);


    }


    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskLoadData extends AsyncTask<JSONArray, String, String> {
        private final static String TAG = "AsyncTaskLoadImage";
        ProgressDialog progress;


        @Override
        protected String doInBackground(JSONArray... jsonArrays) {
            JSONArray array = jsonArrays[0];
            try {

                for (int i = 0; i < array.length(); i++) {

                    JSONObject shopObj = array.getJSONObject(i);


                    printLog(TAG, "getAllRouteCustomer  response " + shopObj);

                    Shop shop = new Shop();


                    int custId = shopObj.getInt("id");

                    shop.setShopId(custId);
                    shop.setShopCode(shopObj.getString("shope_code"));
                    shop.setShopName(shopObj.getString("name"));

                    shop.setShopArabicName(shopObj.getString("arabic_name"));

                    shop.setTypeId(shopObj.getString("customer_type_id"));

                    shop.setVatNumber(shopObj.getString("vat_no"));

                    shop.setCreditLimit((float) shopObj.getDouble("credit_limit"));


                    shop.setShopMail(shopObj.getString("email"));
                    shop.setShopMobile(shopObj.getString("mobile"));
                    shop.setShopAddress(shopObj.getString("address"));
                    shop.setCredit(shopObj.getDouble("credit"));
                    shop.setDebit(shopObj.getDouble("debit"));

                    shop.setRouteCode(shopObj.getString("route_code"));
                    shop.setProductDiscount((float) shopObj.getDouble("product_discount"));
                    shop.setCollectionDiscount((float) shopObj.getDouble("collection_discount"));

                    shop.setOpeningBalance(shopObj.getDouble("opening_balance"));

                    ArrayList<Invoice> invoices = new ArrayList<>();

                    if (!shopObj.isNull("receipt")) {

                        JSONArray invoiceArray = shopObj.getJSONArray("receipt");

                        for (int j = 0; j < invoiceArray.length(); j++) {
                            JSONObject invObj = invoiceArray.getJSONObject(j);

                            Invoice inv = new Invoice();
                            inv.setInvoiceId(invObj.getString("sale_id"));
                            inv.setInvoiceNo(invObj.getString("invoice_no"));
                            inv.setTotalAmount(invObj.getDouble("total"));
                            inv.setBalanceAmount(invObj.getDouble("balance"));
                            invoices.add(inv);
                        }
                    }

                    ArrayList<CustomerProduct> customerProducts = new ArrayList<>();

                    if (!shopObj.isNull("Customer_Products")) {

                        JSONArray productArray = shopObj.getJSONArray("Customer_Products");

                        for (int j = 0; j < productArray.length(); j++) {
                            JSONObject invObj = productArray.getJSONObject(j);
                            CustomerProduct p = new CustomerProduct();
                            p.setProductId(invObj.getInt("product_id"));
                            p.setCustomerId(custId);
                            p.setPrice(invObj.getDouble("selling_rate"));

                            customerProducts.add(p);

                        }
                    }


                    shop.setVisit(true);
                    shop.setInvoices(invoices);
                    shop.setProducts(customerProducts);
                    boolean b = myDatabase.insertUnRegisteredCustomer(shop);

                }

            } catch (JSONException e) {

                e.getMessage();

            }

            return "";
        }

        @Override
        protected void onPreExecute() {
            //show progress dialog while image is loading
            progress = new ProgressDialog(getContext());
            progress.setMessage("Please Wait....");
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progress.dismiss();
                }
            }, 1000);

            Toast.makeText(getContext(), "data updated", Toast.LENGTH_SHORT).show();
        }
    }


}






