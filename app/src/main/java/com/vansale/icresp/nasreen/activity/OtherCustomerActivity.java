package com.vansale.icresp.nasreen.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.fragment.ShopDashBoardFragment;
import com.vansale.icresp.nasreen.fragment.ShopSearchFragment;
import com.vansale.icresp.nasreen.listener.ShopClickListener;
import com.vansale.icresp.nasreen.model.Shop;

import static com.vansale.icresp.nasreen.config.ConfigKey.FRAGMENT_SHOP_DASHBOARD;
import static com.vansale.icresp.nasreen.config.ConfigKey.FRAGMENT_SHOP_SEARCH;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;


public class OtherCustomerActivity extends AppCompatActivity implements ShopClickListener {

    String TAG = "OtherCustomerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_customer);


        final ShopSearchFragment shopSearchFragment = new ShopSearchFragment();

//        Removing all fragment from fragment Manager
        getSupportFragmentManager().popBackStack();

// Let's first dynamically add a fragment into a frame container
        getSupportFragmentManager().beginTransaction().
                replace(R.id.container_other_customer, shopSearchFragment, FRAGMENT_SHOP_SEARCH)
                .commit();
    }


    @Override
    public void onShopClick(Shop shop) {


        // Let's first dynamically add a fragment into a frame container
        ShopDashBoardFragment shopDashBoardFragment = new ShopDashBoardFragment().newInstance(shop);

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.container_other_customer, shopDashBoardFragment, FRAGMENT_SHOP_DASHBOARD);
        ft.addToBackStack(null);
        ft.commit();


    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            printLog(TAG, "if : " + getSupportFragmentManager().getBackStackEntryCount());
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {

            super.onBackPressed();

        }
    }


}
