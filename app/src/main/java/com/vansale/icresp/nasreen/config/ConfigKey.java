package com.vansale.icresp.nasreen.config;

/**
 * Created by mentor on 24/10/17.
 */

public class ConfigKey {

    /***
     * Session key
     */


    // Shared pref mode
    public static final int PRIVATE_MODE = 0;

    // Sharedpref file name
    public static final String PREF_NAME = "nasreen_AndroidPref";
    public static final String CODE_PREF_NAME = "icresp_codePref";


    // All Shared Preferences Keys
    public static final String PREF_IS_LOGIN = "IsLoggedIn_pref";


    // User name (make variable public to access from outside)
    public static final String PREF_KEY_NAME = "user_name_prf";

    // Password (make variable public to access from outside)
    public static final String PREF_KEY_PASSWORD = "password_pref";

    // registrationId (make variable public to access from outside)
    public static final String PREF_KEY_ID = "regi_id_pref";

    // group registration (make variable public to access from outside)
    public static final String PREF_GROUP_IS_REGISTERED = "group_regi_key_pref";


    /***
     * fragments key
     */
    public static final String FRAGMENT_DASHBOARD_LEFT = "key_dash_left_fragment";
    public static final String FRAGMENT_SHOPFRAGMENT = "key_shop_fragment";
    public static final String FRAGMENT_SHOP_DASHBOARD = "key_shop_dash_board_fragment";
    public static final String FRAGMENT_SHOP_SEARCH = "key_shop_search_fragment";


    /***
     * value passing key
     */

    public static final String SHOP_KEY = "key_shop"; //ShopClass
    public static final int WRITE_REQUEST_CODE = 1;

    /***
     * webservice key
     */

    public static final String EXECUTIVE_KEY = "executive_id"; //String
    public static final String DAY_REGISTER_KEY = "day_register_id"; //String

    public static final String USERNAME_KEY = "user_name"; //String
    public static final String PASSWORD_KEY = "password";  //String

    public static final String CUSTOMER_KEY = "customer_id";  //String


    public static final String PRODUCT_TYPE_KEY = "product_type_id";  //String

    public static final String PRODUCT_BRAND_KEY = "brand_id";  //String


    public static final String INVOICE_NO_KEY = "invoice_no";  //String


    //    SQLLite action type
    public static final int REQ_SALE_TYPE = 1;
    public static final int REQ_QUOTATION_TYPE = 2;
    public static final int REQ_RETURN_TYPE = 3;
    public static final int REQ_RECEIPT_TYPE = 4;
    public static final int REQ_NO_SALE_TYPE = 5;
    public static final int REQ_CUSTOMER_TYPE = 6;
    public static final int REQ_ANY_TYPE = 0;

    /**
     * LAYOUT VISIBILITIES
     */

    public static final int VIEW_PROGRESSBAR = 1;
    public static final int VIEW_RECYCLERVIEW = 2;
    public static final int VIEW_ERRORVIEW = 3;
    public static final int VIEW_DATAVIEW = 4;
}
