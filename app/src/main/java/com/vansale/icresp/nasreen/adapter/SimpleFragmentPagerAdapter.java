package com.vansale.icresp.nasreen.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vansale.icresp.nasreen.fragment.InvoiceReceiptHistoryFragment;
import com.vansale.icresp.nasreen.fragment.OpeningReceiptHistoryFragment;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new InvoiceReceiptHistoryFragment();
            case 1:
                return new OpeningReceiptHistoryFragment();
            default:
                return null;
        }


    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Invoice Receipt";
            case 1:
                return "Opening Balance Receipt";

            default:
                return null;
        }
    }
}
