package com.vansale.icresp.nasreen.listener;


import com.vansale.icresp.nasreen.model.Invoice;

/**
 * Created by mentor on 30/10/17.
 */

public interface InvoiceClickListener {
    void onInvoiceClick(Invoice invoice, int position);
}
