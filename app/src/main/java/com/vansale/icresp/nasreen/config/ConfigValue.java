package com.vansale.icresp.nasreen.config;

/**
 * Created by mentor on 14/8/17.
 */

public class ConfigValue {


    public static final String PRODUCT_UNIT_PIECE = "Piece";
    public static final String PRODUCT_UNIT_CASE = "Case";

    public static final String SALE_WHOLESALE = "WholeSale";
    public static final String SALE_RETAIL = "Retail";


    public static final String FRAGMENT_INVOICE = "key_invoiceFragment";
    public static final String FRAGMENT_WITOUTINVOICE = "key_withoutInvoiceFragment";


    public static final String CALLING_ACTIVITY_KEY = "calling_activity";


    public static final String SALE_ID_KEY = "sale_id_key";
    public static final String INVOICE_LIST_KEY = "invoice_list_key";
    public static final String OPENING_BALANCE_KEY = "opening_balance_key";

    public static final String SALES_VALUE_KEY = "sales_data_key";

    public static final String RETURN_VALUE_KEY = "sales_data_key";


    public static final String SHOP_VALUE_KEY = "shop_key";

    public static final String PAID_AMOUNT_VALUE_KEY = "paid_amount_key";

    public static final String RECEIPT_NO_KEY = "receipt_no_key";

    public static final String INVOICE_RETURN_VALUE_KEY = "invoice_return_key";





    //    tax type

    static final String TAX_EXCLUSIVE = "exclusive";

    static final String TAX_INCLUSIVE = "inclusive";


}