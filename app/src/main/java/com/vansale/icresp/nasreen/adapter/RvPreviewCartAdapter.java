package com.vansale.icresp.nasreen.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.model.CartItem;

import java.util.ArrayList;

import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_CASE;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;


/**
 * Created by mentor on 30/10/17.
 */

public class RvPreviewCartAdapter extends RecyclerView.Adapter<RvPreviewCartAdapter.RvPreviewCartHolder> {

    private ArrayList<CartItem> cartItems;
    private Context context;

    public RvPreviewCartAdapter(ArrayList<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    @Override
    public RvPreviewCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview_cart, parent, false);
        this.context = parent.getContext();

        return new RvPreviewCartHolder(view);
    }

    @Override
    public void onBindViewHolder(RvPreviewCartHolder holder, int position) {

        final CartItem cartItem = cartItems.get(position);

        int s = position + 1;

        holder.tvCode.setText(cartItem.getProductCode());

        holder.tvSlNo.setText(String.valueOf(s));
        holder.tvProductName.setText(cartItem.getProductName());

        String  qty = "0/" + String.valueOf(cartItem.getTypeQuantity());

        double netPrice = cartItem.getNetPrice();
        if (cartItem.getOrderType().equals(PRODUCT_UNIT_CASE)) {
            netPrice = netPrice * cartItem.getPiecepercart();
            qty = cartItem.getTypeQuantity() + "/0";
        }

        holder.tvQty.setText(qty);

        holder.tvUnitPrice.setText(String.valueOf(getAmount(netPrice) + " " + context.getString(R.string.currency)));
        holder.tvTotal.setText(String.valueOf(getAmount(netPrice * cartItem.getTypeQuantity()) + " " + context.getString(R.string.currency)));

    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }


    public class RvPreviewCartHolder extends RecyclerView.ViewHolder {

        TextView tvSlNo, tvProductName, tvQty, tvUnitPrice, tvTotal, tvCode;

        public RvPreviewCartHolder(View itemView) {
            super(itemView);

            tvSlNo = (TextView) itemView.findViewById(R.id.textView_item_previewCart_no);
            tvProductName = (TextView) itemView.findViewById(R.id.textView_item_previewCart_productName);
            tvQty = (TextView) itemView.findViewById(R.id.textView_item_previewCart_qty);
            tvUnitPrice = (TextView) itemView.findViewById(R.id.textView_item_previewCart_unitPrice);
            tvTotal = (TextView) itemView.findViewById(R.id.textView_item_previewCart_total);

            tvCode = (TextView) itemView.findViewById(R.id.textView_item_previewCart_code);

        }
    }
}
