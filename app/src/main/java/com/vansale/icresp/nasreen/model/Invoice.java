package com.vansale.icresp.nasreen.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mentor on 19/5/17.
 */

public class Invoice implements Serializable {


    private int localId;
    private String invoiceId;
    private String invoiceNo;
    private double balanceAmount;
    private double totalAmount;

    private ArrayList<Transaction> transactions = new ArrayList<>();

    public Invoice() {
    }


    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }


    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }


    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return String.valueOf(getInvoiceNo());
    }

}
