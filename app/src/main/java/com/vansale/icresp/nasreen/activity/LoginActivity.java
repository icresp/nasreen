package com.vansale.icresp.nasreen.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.webservice.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import static com.vansale.icresp.nasreen.config.ConfigKey.PASSWORD_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.USERNAME_KEY;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;

public class LoginActivity extends AppCompatActivity {

    int SPLASH_TIME_OUT = 3000;
    private TextInputEditText etUsername, etPassword;
    private TextInputLayout tilUsername, tilPassword;
    private FloatingActionButton fabLogin;
    private String TAG = "LoginActivity";
    private ProgressDialog progressDialog;

    private SessionAuth sessionAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tilUsername = (TextInputLayout) findViewById(R.id.til_login_username);
        tilPassword = (TextInputLayout) findViewById(R.id.til_login_password);
        etUsername = (TextInputEditText) findViewById(R.id.editText_login_username);
        etPassword = (TextInputEditText) findViewById(R.id.editText_login_password);

        fabLogin = (FloatingActionButton) findViewById(R.id.fab_login);

        sessionAuth = new SessionAuth(LoginActivity.this);

        sessionAuth.checkLogin();
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.setCancelable(false);

        //        show input keyboard
        etUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });


        fabLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              // postLogin();

                CheckVersion();


            }
        });
    }


    private void CheckVersion() {

        if (!isValidate())
            return;

        hideKeyboard();

        progressDialog.show();

        JSONObject object = new JSONObject();

        try {
            object.put("app_version", "2.1");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        fabLogin.setEnabled(false);
        Log.e("Version object", ""+object);

        WebService.webVersionCheck(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                    Log.e("Version check", ""+response);
                try {
                    if (response.getString("status").equals("True")) {
                        progressDialog.dismiss();

                        postLogin();

                    } else{
                        showSnackBar("This version of app is currently unsupported now.");
                }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                fabLogin.setEnabled(true);
                progressDialog.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                showSnackBar(error);

            }
        }, object);
    }

    private void postLogin() {


        if (!isValidate())
            return;

        hideKeyboard();

        progressDialog.show();

        final String userName = etUsername.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        JSONObject object = new JSONObject();

        try {
            object.put(USERNAME_KEY, userName);
            object.put(PASSWORD_KEY, password);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        fabLogin.setEnabled(false);


        WebService.webLogin(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("EXE ID", ""+response);

                try {
                    if (response.getString("status").equals("Login Successful")) {
                        progressDialog.dismiss();
                        sessionAuth.createLoginSession(response.getString("name"), password, response.getString("id"));

                    } else
                        showSnackBar(response.getString("status"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                fabLogin.setEnabled(true);
                progressDialog.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                showSnackBar(error);

            }
        }, object);
    }

    //    validation
    private boolean isValidate() {

        boolean status = false;

        String userName = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        tilUsername.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);

        if (TextUtils.isEmpty(userName)) {

            tilUsername.setError("Invalid Username");
            status = false;

        } else if (TextUtils.isEmpty(password)) {
            tilPassword.setError("Invalid Password");
            status = false;
        } else
            status = true;

        return status;
    }

    private void showSnackBar(String text) {
        progressDialog.dismiss();
        // The SMS quota for the project has been exceeded
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
        fabLogin.setEnabled(true);
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
