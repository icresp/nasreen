package com.vansale.icresp.nasreen.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.ReturnPreviewActivity;
import com.vansale.icresp.nasreen.adapter.RvReturnWithoutInvoiceAdapter;
import com.vansale.icresp.nasreen.dialog.CartSpinnerDialog;
import com.vansale.icresp.nasreen.dialog.ReturnTypeDialog;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.listener.OnSpinerItemClick;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getPercentageValue;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getSalePrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getTaxPrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getWithoutTaxPrice;
import static com.vansale.icresp.nasreen.config.ConfigKey.CUSTOMER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RETURN_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.INVOICE_RETURN_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_CASE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SALE_WHOLESALE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dbDateFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.webservice.WebService.webSaleReturn;


public class WithoutInvoiceFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_CUSTOMER_NAME = "customer_key_arg";
    private static final String ARG_SALETYPE = "saleType";
    String TAG = "WithoutInvoiceFragment";
    private Shop SELECTED_SHOP = null;
    private String SALE_TYPE = null;
    private TextView tvProductSpinner, tvCodeSpinner, tvUnitPrice, tvTotalRefund;
    private EditText etReturnQuantity;
    private Button btnReturn;
    private ProgressBar progressBar;
    private ErrorView errorView;

    private AppCompatSpinner spinnerCartUnit;
    String str_Latitude = "0", str_Longitude = "0";
    private String provider;
    LocationManager locationManager;

    private Button btnFinish;

    private SessionAuth sessionAuth;

    private RecyclerView recyclerView;

    private ViewGroup viewLayout;

    private RvReturnWithoutInvoiceAdapter withoutInvoiceAdapter;

    private ArrayList<CartItem> cartItems = new ArrayList<>();


    private CartItem SELECTED_CART = null;


    private String EXECUTIVE_ID = "";

    private MyDatabase myDatabase;


    // TODO: Rename and change types and number of parameters
    public static WithoutInvoiceFragment newInstance(Shop cus, String type) {
        WithoutInvoiceFragment fragment = new WithoutInvoiceFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CUSTOMER_NAME, cus);
        args.putString(ARG_SALETYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {

                SELECTED_SHOP = (Shop) getArguments().getSerializable(ARG_CUSTOMER_NAME);
                SALE_TYPE = getArguments().getString(ARG_SALETYPE);
            } catch (ClassCastException e) {
                e.getMessage();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_without_invoice, container, false);


        tvProductSpinner = (TextView) view.findViewById(R.id.textView_sales_return_withoutInvoice_product);
        tvCodeSpinner = (TextView) view.findViewById(R.id.textView_sales_return_withoutInvoice_code);
        viewLayout = (ViewGroup) view.findViewById(R.id.layout_sales_return_withoutInvoice);
        tvUnitPrice = (TextView) view.findViewById(R.id.textView_sales_return_withoutInvoice_unitPrice);

        tvTotalRefund = (TextView) view.findViewById(R.id.textView_sales_return_withoutInvoice_refundTotal);
        etReturnQuantity = (EditText) view.findViewById(R.id.editText_sales_return_withoutInvoice_returnQty);
        btnReturn = (Button) view.findViewById(R.id.button_sales_return_withoutInvoice_addCart);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_sales_return_withoutInvoice_product);

        spinnerCartUnit = (AppCompatSpinner) view.findViewById(R.id.spinner_sales_return_withoutInvoice_orderUnit);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        errorView = (ErrorView) view.findViewById(R.id.errorView);
        btnFinish = (Button) view.findViewById(R.id.button_sales_return_withoutInvoice_finish);


        sessionAuth = new SessionAuth(getContext());

        myDatabase = new MyDatabase(getContext());

        try {
            EXECUTIVE_ID = sessionAuth.getExecutiveId();
        } catch (Exception e) {
            e.getMessage();
        }


        withoutInvoiceAdapter = new RvReturnWithoutInvoiceAdapter(cartItems, new OnNotifyListener() {
            @Override
            public void onNotified() {


                String net = String.valueOf("TOTAL : " + getAmount(withoutInvoiceAdapter.getNetTotal()) + " " + getString(R.string.currency));
                String vat = "VAT  : " + getAmount(withoutInvoiceAdapter.getTaxTotal()) + " " + getString(R.string.currency);

                String grandTotal = String.valueOf("REFUND TOTAL : " + getAmount(withoutInvoiceAdapter.getGrandTotal()) + " " + getString(R.string.currency));
                tvTotalRefund.setText(String.valueOf(net + ",\t\t" + vat + ",\t\t" + grandTotal));

                tvTotalRefund.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                tvTotalRefund.setSelected(true);

            }
        });


        btnFinish.setOnClickListener(this);

        btnReturn.setOnClickListener(this);

        initView();


        getVanStockList();
        setShopTypeSpinner();

        cartItems.clear();
        withoutInvoiceAdapter.notifyDataSetChanged();

        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Requires Permission", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                LocationListener mlocListener = new MyLocationListener();
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                provider = locationManager.getBestProvider(criteria, true);
                locationManager.requestLocationUpdates(provider, 61000, 250,
                        mlocListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Error with location manager", Toast.LENGTH_SHORT).show();
        }



        return view;
    }


    private void initView() {

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

//


        //       return Product Recycler View
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(itemAnimator);

        //        Item Divider in recyclerView
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext())
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(withoutInvoiceAdapter);
//        productAdapter.notifyDataSetChanged();


    }

    private void setShopTypeSpinner() {

//order type spinner
        ArrayAdapter<CharSequence> orderTypeAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.cart_type, R.layout.spinner_background_dark);

        orderTypeAdapter.setDropDownViewResource(R.layout.spinner_list);

        spinnerCartUnit.setAdapter(orderTypeAdapter);

        spinnerCartUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setCartProduct();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //    Load Stock  List from Local
    private void getVanStockList() {

        final ArrayList<CartItem> list = new MyDatabase(getContext()).getAllStock();

        if (list.isEmpty())
            setErrorView("No Stock", "", false);
        else
            setProductList(list);

        setProgressBar(false);

    }


    private void setProductList(ArrayList<CartItem> list) {


        final CartSpinnerDialog spinnerCart = new CartSpinnerDialog(getActivity(), list, "Select Product", R.style.DialogAnimations_SmileWindow);// With 	Animation

        tvProductSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showSpinerDialog();
            }
        });

        tvCodeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showCodeSpinerDialog();
            }
        });

        spinnerCart.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(Object item, int position) {

                SELECTED_CART = (CartItem) item;


                if (myDatabase.isExistCustomerProduct(SELECTED_SHOP.getShopId(), SELECTED_CART.getProductId())) {
                    double p = myDatabase.getCustomerProductPrice(SELECTED_SHOP.getShopId(), SELECTED_CART.getProductId());
                    SELECTED_CART.setRetailPrice(p);
                    SELECTED_CART.setWholeSalePrice(p);
                }
                setCartProduct();
            }


        });


        setProgressBar(false);
        viewLayout.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }


    private void setCartProduct() {

        if (SELECTED_CART == null)
            return;

        etReturnQuantity.setText("");

        etReturnQuantity.requestFocus();
        etReturnQuantity.setFocusableInTouchMode(true);

        showSoftKeyboard(etReturnQuantity);

        double normalPrice = SELECTED_CART.getRetailPrice();

        if (SALE_TYPE.equals(SALE_WHOLESALE))
            normalPrice = SELECTED_CART.getWholeSalePrice();

        int quantity = TextUtils.isEmpty(etReturnQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etReturnQuantity.getText().toString().trim());

        if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
            quantity = quantity * SELECTED_CART.getPiecepercart();

        double netPrice = getWithoutTaxPrice(normalPrice, SELECTED_CART.getTax());

        double discountedPrice=  getPercentageValue(netPrice,SELECTED_CART.getDiscountPerce());
        discountedPrice=netPrice-discountedPrice;

        double salePrice = getSalePrice(discountedPrice, SELECTED_CART.getTax());

        Log.e("Net Price ", ""+netPrice);

        SELECTED_CART.setNetPrice(netPrice);
        SELECTED_CART.setSalePrice(salePrice);
        SELECTED_CART.setProductPrice(discountedPrice);

        ///////////////////

        tvUnitPrice.setText(getAmount(SELECTED_CART.getNetPrice()));

        tvProductSpinner.setTag(SELECTED_CART);
        tvCodeSpinner.setTag(SELECTED_CART);

        tvProductSpinner.setText(SELECTED_CART.getProductName());
        tvCodeSpinner.setText(SELECTED_CART.getProductCode());

        etReturnQuantity.requestFocus();
        etReturnQuantity.setFocusableInTouchMode(true);

    }


    private void addToCart() {

        if (SELECTED_CART != null) {

            CartItem cart = SELECTED_CART;


            ////////////


            int returnQuantity = TextUtils.isEmpty(etReturnQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etReturnQuantity.getText().toString().trim());

            cart.setTypeQuantity(returnQuantity);


            if (spinnerCartUnit.getSelectedItem().toString().equals(PRODUCT_UNIT_CASE))
                returnQuantity = returnQuantity * cart.getPiecepercart();



            cart.setDiscountPerce(SELECTED_SHOP.getProductDiscount());
            double normalPrice = cart.getRetailPrice();

            if (SALE_TYPE.equals(SALE_WHOLESALE))
                normalPrice = cart.getWholeSalePrice();



            double netPrice=getWithoutTaxPrice(normalPrice, cart.getTax());
            cart.setNetPrice(netPrice);

            double discountedPrice=  getPercentageValue(netPrice,cart.getDiscountPerce());
            discountedPrice=netPrice-discountedPrice;

            double salePrice = getSalePrice(discountedPrice, cart.getTax());

            cart.setProductPrice(discountedPrice);
            cart.setTaxValue(getTaxPrice(discountedPrice, cart.getTax()));

            cart.setSalePrice(salePrice);

            Log.e("Tax Value", ""+getTaxPrice(discountedPrice, cart.getTax()));
            Log.e("return qnty", ""+returnQuantity);
            cart.setReturnQuantity(returnQuantity);

            cart.setTotalPrice(salePrice * returnQuantity);

            cart.setOrderType(spinnerCartUnit.getSelectedItem().toString());

            withoutInvoiceAdapter.returnItem(cart);

            clearProductView();

        }

    }

    private void clearProductView() {
        tvCodeSpinner.setText("");
        tvProductSpinner.setText("");
        etReturnQuantity.setText("");
        tvUnitPrice.setText("");
        SELECTED_CART = null;

        etReturnQuantity.setFocusable(false);
        tvTotalRefund.setFocusable(true);

        hideSoftKeyboard();

    }

    private boolean returnValidate() {

        boolean status = false;

        String s = tvProductSpinner.getText().toString().trim();
        int returnQuantity = TextUtils.isEmpty(etReturnQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etReturnQuantity.getText().toString().trim());

        tvProductSpinner.setError(null);
        etReturnQuantity.setError(null);


        if (SELECTED_CART == null) {
            status = false;
            tvProductSpinner.setError("Select Product");

        } else if (TextUtils.isEmpty(s)) {
            status = false;
            tvProductSpinner.setError("Select Product");

        } else if (TextUtils.isEmpty(etReturnQuantity.getText().toString().trim())) {
            etReturnQuantity.setError("Enter Quantity");
            status = false;

        } else
            status = true;

        return status;
    }

    //    change sale type

    public void changeSaleType(String type) {

        SALE_TYPE = type;
        ArrayList<CartItem> list = withoutInvoiceAdapter.getReturnItems();

        if (!list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {

                CartItem c = list.get(i);

                double salePrice = c.getRetailPrice();

                if (SALE_TYPE.equals(SALE_WHOLESALE))
                    salePrice = c.getWholeSalePrice();

                c.setSalePrice(salePrice);

                c.setTotalPrice(salePrice * c.getReturnQuantity());

                withoutInvoiceAdapter.updateItem(c, i);
            }
        }
    }

    private void postSaleReturn(final String returnType)  {

        if (withoutInvoiceAdapter.getReturnItems().isEmpty()) {
            Toast.makeText(getContext(), "Return Products is Empty..!", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Please wait...", false, false);

        final ArrayList<CartItem> list = withoutInvoiceAdapter.getReturnItems();

        JSONObject object = new JSONObject();
        JSONObject returnObj = new JSONObject();

        JSONArray productArray = new JSONArray();

        try {
            returnObj.put("invoice_id", "0");
            returnObj.put(CUSTOMER_KEY, SELECTED_SHOP.getShopId());
            returnObj.put("total", withoutInvoiceAdapter.getNetTotal());
            returnObj.put("grand_total", withoutInvoiceAdapter.getGrandTotal());
            returnObj.put("return_type", returnType);
            returnObj.put("latitude", str_Latitude);
            returnObj.put("longitude", str_Longitude);

            for (CartItem c : list) {

                JSONObject obj = new JSONObject();

                obj.put("product_id", c.getProductId());
                obj.put("unit_price", c.getSalePrice());
                obj.put("tax", c.getTax());
                obj.put("return_quantity", c.getReturnQuantity());
                obj.put("refund_Amount", c.getTotalPrice());
                productArray.put(obj);
            }

            returnObj.put("ReturnedProduct", productArray);
            object.put(EXECUTIVE_KEY, EXECUTIVE_ID);
            object.put("SalesReturn", returnObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("return object", ""+object.toString());

        final String strDate = getDateTime();
        final Sales saleReturn = new Sales();

        saleReturn.setCustomerId(SELECTED_SHOP.getShopId());
        saleReturn.setDate(strDate);
        saleReturn.setTotal(withoutInvoiceAdapter.getGrandTotal());
        saleReturn.setSaleType(returnType);
        saleReturn.setPaid(withoutInvoiceAdapter.getGrandTotal());
        saleReturn.setSalePo("");
        saleReturn.setInvoiceCode("");
        saleReturn.setCartItems(list);
        saleReturn.setDiscountTotal(0);

        webSaleReturn(new WebService.webObjectCallback() {

            @Override
            public void onResponse(JSONObject response) {

                try {

                    Log.e("return response", ""+response.toString());

                    if (response.getString("status").equals("Success")) {

                        myDatabase.updateVisitStatus(SELECTED_SHOP.getShopId(), REQ_RETURN_TYPE, "", "", "");  // update sales return status to local db


                        if (returnType.equals(getActivity().getString(R.string.refund)))
                            for (CartItem c : list) {
                            myDatabase.updateStock(c, REQ_RETURN_TYPE);
                        }


                        Toast.makeText(getContext(), "Successfully", Toast.LENGTH_SHORT).show();

//                        print activity

                        Intent intent = new Intent(getActivity(), ReturnPreviewActivity.class);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_WITHOUT_INVOICE_RETURN);
                        intent.putExtra(INVOICE_RETURN_VALUE_KEY, saleReturn);
                        intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);

                        pd.dismiss();

                        startActivity(intent);
                        getActivity().finish();

                        // Reload current fragment
                       /* Fragment frg = getActivity().getSupportFragmentManager().findFragmentByTag(FRAGMENT_WITOUTINVOICE);
                        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.detach(frg);
                        ft.attach(frg);
                        ft.commit();*/
                    } else {
                        Toast.makeText(getContext(), "Error! " + response.getString("result"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();


            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        }, object);


    }

    //     ProgressBar
    private void setProgressBar(boolean isVisible) {

        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            viewLayout.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);

        } else {
            progressBar.setVisibility(View.GONE);
        }

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        viewLayout.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);

        setProgressBar(false);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(false)
                .build());

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                getVanStockList();

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_sales_return_withoutInvoice_addCart:


                if (returnValidate()) {

                    Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.anim_alpha);
                    v.setAnimation(anim);

                    addToCart();
                }

                break;
            case R.id.button_sales_return_withoutInvoice_finish:

               // String type = "Refund";
               //   postSaleReturn(type);

                final ReturnTypeDialog paymentDialog = new ReturnTypeDialog(getActivity(), new ReturnTypeDialog.ReturnTypeClickListener() {
                    @Override
                    public void onReturnTypeClick(String type) {

                        Log.e("Return type click", ""+type);

                            postSaleReturn(type);
                    }
                });

                paymentDialog.show();

                break;
        }
    }

    /**
     * Hides the soft keyboard
     *
     */
    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        if (inputMethodManager != null) {
//            inputMethodManager.showSoftInput(view, 0);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    private static String getDateTime() {

        Date date = new Date();
        return dbDateFormat.format(date);
    }

    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();
            String Text = "My current location is: " + "Latitude = "
                    + loc.getLatitude() + "\nLongitude = " + loc.getLongitude();

            str_Latitude = "" + loc.getLatitude();
            str_Longitude = "" + loc.getLongitude();

            //  tvNetTotal.setText("Lat : "+str_Latitude+" / Long : "+str_Longitude);
            /*Toast.makeText(getActivity(), Text, Toast.LENGTH_SHORT)
                    .show();*/
            Log.d("TAG", "Starting..");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
}
