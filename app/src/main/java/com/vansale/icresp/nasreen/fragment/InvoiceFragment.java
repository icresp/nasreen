package com.vansale.icresp.nasreen.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.vansale.icresp.nasreen.R;
import com.vansale.icresp.nasreen.activity.printpreview.ReturnPreviewActivity;
import com.vansale.icresp.nasreen.adapter.InvoiceAdapter;
import com.vansale.icresp.nasreen.adapter.RvReturnInvoiceAdapter;
import com.vansale.icresp.nasreen.controller.ConnectivityReceiver;
import com.vansale.icresp.nasreen.dialog.CartSpinnerDialog;
import com.vansale.icresp.nasreen.dialog.CartSpinnerDialogNew;
import com.vansale.icresp.nasreen.dialog.ReturnTypeDialog;
import com.vansale.icresp.nasreen.listener.ActivityConstants;
import com.vansale.icresp.nasreen.listener.InvoiceClickListener;
import com.vansale.icresp.nasreen.listener.OnNotifyListener;
import com.vansale.icresp.nasreen.listener.OnSpinerItemClick;
import com.vansale.icresp.nasreen.localdb.MyDatabase;
import com.vansale.icresp.nasreen.model.CartItem;
import com.vansale.icresp.nasreen.model.CartItemCode;
import com.vansale.icresp.nasreen.model.Invoice;
import com.vansale.icresp.nasreen.model.Sales;
import com.vansale.icresp.nasreen.model.Shop;
import com.vansale.icresp.nasreen.session.SessionAuth;
import com.vansale.icresp.nasreen.session.SessionValue;
import com.vansale.icresp.nasreen.view.ErrorView;
import com.vansale.icresp.nasreen.webservice.WebService;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getSalePrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getTaxPrice;
import static com.vansale.icresp.nasreen.config.AmountCalculator.getWithoutTaxPrice;
import static com.vansale.icresp.nasreen.config.ConfigKey.CUSTOMER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.DAY_REGISTER_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.EXECUTIVE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.INVOICE_NO_KEY;
import static com.vansale.icresp.nasreen.config.ConfigKey.REQ_RETURN_TYPE;
import static com.vansale.icresp.nasreen.config.ConfigValue.CALLING_ACTIVITY_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.INVOICE_RETURN_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.ConfigValue.PRODUCT_UNIT_PIECE;
import static com.vansale.icresp.nasreen.config.ConfigValue.SHOP_VALUE_KEY;
import static com.vansale.icresp.nasreen.config.Generic.dbDateFormat;
import static com.vansale.icresp.nasreen.config.Generic.getAmount;
import static com.vansale.icresp.nasreen.config.PrintConsole.printLog;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetAllReceipt;
import static com.vansale.icresp.nasreen.webservice.WebService.webGetReturnInvoiceDetails;
import static com.vansale.icresp.nasreen.webservice.WebService.webSaleReturn;

public class InvoiceFragment extends Fragment implements View.OnClickListener {


    public static final String ARG_CUSTOMER = "customer_key_arg";

    String TAG = "InvoiceFragment";

    private TextView tvProductSpinner, tvCodeSpinner, tvInvoiceTitle, tvUnitPrice, tvActualQuantity, tvTotalRefund;
    private EditText etReturnQuantity;
    private ErrorView evMain, evInvoiceDetals;

    private InvoiceAdapter adapter;

    private ProgressBar pbMain, pbInvoiceDetals;

    private ViewGroup viewLayout, viewInvoiceDetails;

    private Button btnAddToCart,btnFinish;

    private RecyclerView rvProduct;

    private RvReturnInvoiceAdapter returnProductAdapter;

    private ArrayList<CartItem> selectedItems = new ArrayList<>();

    private CartItem SELECTED_CART = null;

    private Invoice SELECTED_INVOICE = null;

    private Shop SELECTED_SHOP;

    private String dayRegId;

    private ListView invoicesListView;
    private ArrayList<Invoice> invoices = new ArrayList<>();

    private MyDatabase myDatabase;
    String str_Latitude = "0", str_Longitude = "0";
    private String provider;
    LocationManager locationManager;

    public static InvoiceFragment newInstance(Shop shop) {
        InvoiceFragment fragment = new InvoiceFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CUSTOMER, shop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {

                SELECTED_SHOP = (Shop) getArguments().getSerializable(ARG_CUSTOMER);
            } catch (ClassCastException e) {
                e.getMessage();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);

        tvProductSpinner = (TextView) view.findViewById(R.id.textView_sales_return_invoice_product);
        tvCodeSpinner = (TextView) view.findViewById(R.id.textView_sales_return_invoice_code);
        viewLayout = (ViewGroup) view.findViewById(R.id.layout_sales_return_invoice);
        viewInvoiceDetails = (ViewGroup) view.findViewById(R.id.view_invoice_details);

        tvInvoiceTitle = (TextView) view.findViewById(R.id.textView_sales_return_invoice_title);
        tvUnitPrice = (TextView) view.findViewById(R.id.textView_sales_return_invoice_unitPrice);
        tvActualQuantity = (TextView) view.findViewById(R.id.textView_sales_return_invoice_actualQty);
        tvTotalRefund = (TextView) view.findViewById(R.id.textView_sales_return_invoice_refundTotal);

        invoicesListView = (ListView) view.findViewById(R.id.listView_drawer_sales_return_invoice);

        etReturnQuantity = (EditText) view.findViewById(R.id.editText_sales_return_invoice_returnQty);
        btnAddToCart = (Button) view.findViewById(R.id.button_sales_return_invoice_addCart);
        rvProduct = (RecyclerView) view.findViewById(R.id.recyclerView_sales_return_invoice_product);


        pbInvoiceDetals = (ProgressBar) view.findViewById(R.id.progressView_invoice_details);
        pbMain = (ProgressBar) view.findViewById(R.id.progressView_fragment_invoice);
        btnFinish = (Button) view.findViewById(R.id.button_sales_return_invoice_finish);
        evMain = (ErrorView) view.findViewById(R.id.errorView_fragment_invoice);
        evInvoiceDetals = (ErrorView) view.findViewById(R.id.errorView_invoice_details);

        myDatabase = new MyDatabase(getContext());


        this.dayRegId = new SessionValue(getContext()).getDayRegisterId();

        adapter = new InvoiceAdapter(getContext(), R.layout.item_textview, invoices, new InvoiceClickListener() {
            @Override
            public void onInvoiceClick(final Invoice invoice, int position) {
                if (invoice != null) {

                    SELECTED_INVOICE = invoice;

                    onInvoiceChange(invoice);

                    invoicesListView.setItemChecked(position, true);
                    invoicesListView.setSelection(position);


                } else {
                    printLog(TAG, "Error in creating fragment");
                }
            }
        });

        invoicesListView.setAdapter(adapter);


        returnProductAdapter = new RvReturnInvoiceAdapter(selectedItems, new OnNotifyListener() {
            @Override
            public void onNotified() {


                String net = String.valueOf("TOTAL : " + getAmount(returnProductAdapter.getNetTotal()) + " " + getActivity().getString(R.string.currency));
                String vat = "VAT  : " + getAmount(returnProductAdapter.getTaxTotal()) + " " + getActivity().getString(R.string.currency);

                String grandTotal = String.valueOf("REFUND TOTAL : " + getAmount(returnProductAdapter.getGrandTotal()) + " " + getActivity().getString(R.string.currency));
                tvTotalRefund.setText(String.valueOf(net + ",\t\t" + vat + ",\t\t" + grandTotal));

                tvTotalRefund.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                tvTotalRefund.setSelected(true);
        }
        });


        initView();

        if (SELECTED_SHOP != null) {
            getInvoiceList(SELECTED_SHOP.getShopId());

        }


        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Requires Permission", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                LocationListener mlocListener = new MyLocationListener();
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                provider = locationManager.getBestProvider(criteria, true);
                locationManager.requestLocationUpdates(provider, 61000, 250,
                        mlocListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Error with location manager", Toast.LENGTH_SHORT).show();
        }


        btnFinish.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);


        return view;
    }


    private void initView() {

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);


        //       return Product Recycler View
        rvProduct.setHasFixedSize(true);
        rvProduct.setItemAnimator(itemAnimator);

        //        Item Divider in recyclerView
        rvProduct.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext())
                .showLastDivider()
//                .color(getResources().getColor(R.color.divider))
                .build());

        rvProduct.setLayoutManager(new LinearLayoutManager(getContext()));

        rvProduct.setAdapter(returnProductAdapter);

    }


    private void getInvoiceList(int customerId) {


        setProgressBarMain(true);

        invoices.clear();

        if (!checkConnection()) {
            setErrorView(this.getString(R.string.no_internet), "", false);
            return;
        }

        final JSONObject object = new JSONObject();
        try {
            object.put(CUSTOMER_KEY, customerId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        invoices.clear();

        webGetAllReceipt(new WebService.webObjectCallback() {
            @Override
            public void onResponse(JSONObject response) {


                try {

                    Log.e("Return invoice", ""+response.toString());

                    if (response.getString("status").equals("Success")) {
                        JSONObject receiptObj = response.getJSONObject("receipt");
                        JSONArray array = receiptObj.getJSONArray("Sale");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject obj = array.getJSONObject(i);

                            Invoice invoice = new Invoice();

                            invoice.setInvoiceId(obj.getString("sale_id")); ////both same
                            invoice.setInvoiceNo(obj.getString("invoice_no")); //both same
                            invoice.setBalanceAmount(0.0f);
                            invoice.setTotalAmount(0.0f);

                            invoices.add(invoice);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (invoices.isEmpty())
                    setErrorView("No Invoices", "", false);
                else {
                    SELECTED_INVOICE = invoices.get(0);


                    if (SELECTED_INVOICE != null) {

                        onInvoiceChange(SELECTED_INVOICE);
                        tvInvoiceTitle.setText(String.valueOf(SELECTED_INVOICE.getInvoiceNo()));

                    }

                    setProgressBarInvoice(false);
                    evMain.setVisibility(View.GONE);
                    viewLayout.setVisibility(View.VISIBLE);

                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onErrorResponse(String error) {

                setErrorView(error, "", true);

            }
        }, object);


    }


    //get invoice details from webServer
    private void getInvoiceDetails(String invoiceNo) {


        setProgressBarInvoice(true);

        selectedItems.clear();
        returnProductAdapter.notifyDataSetChanged();

        clearProductView();


        tvTotalRefund.setText(String.valueOf("REFUND TOTAL : " + getAmount(returnProductAdapter.getNetTotal()) + " " + getActivity().getString(R.string.currency)));


        JSONObject object = new JSONObject();
        try {
            object.put(INVOICE_NO_KEY, invoiceNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayList<CartItem> cartItems = new ArrayList<>();
        final ArrayList<CartItemCode> productCodeList = new ArrayList<>();


        cartItems.clear();
        productCodeList.clear();


        webGetReturnInvoiceDetails(new WebService.webObjectCallback() {

            @Override
            public void onResponse(JSONObject response) {

//                printLog(TAG, "getInvoiceDetails response " + response.toString());

                Log.e("getInvoiceDetails", ""+response.toString());

                try {

                    if (response.getString("status").equals("success") && !response.isNull("Sale")) {

                        JSONArray array = response.getJSONArray("Sale");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject cartObj = array.getJSONObject(i);

                            CartItem cartItem = new CartItem();
                            CartItemCode cartiemcode = new CartItemCode();

//                            printLog(TAG, "getInvoiceDetails cartObj " + cartObj);

                            cartItem.setProductId(cartObj.getInt("id"));
                            double unitPrice =  cartObj.getDouble("unit_price");
                            double taxPrice =  cartObj.getDouble("tax_amount");
                            cartItem.setProductName(cartObj.getString("name"));
                            cartItem.setProductCode(cartObj.getString("code"));
                            cartItem.setReturnQuantity(0);
                            cartItem.setPieceQuantity(cartObj.getInt("quantity"));
                            cartItem.setCost( cartObj.getDouble("cost"));
                            cartItem.setTotalPrice( cartObj.getDouble("total"));

                            float vat = (float) cartObj.getDouble("tax");

                            cartItem.setTax(vat);
                            /////

                            double salePrice = getSalePrice(unitPrice, cartItem.getTax());
                            cartItem.setProductPrice(unitPrice);
                            cartItem.setNetPrice(getWithoutTaxPrice(unitPrice, cartItem.getTax()));
                            cartItem.setTaxValue(getTaxPrice(unitPrice, cartItem.getTax()));
                            cartItem.setSalePrice(salePrice);
                            cartItem.setOrderType(PRODUCT_UNIT_PIECE);

                            /////

                            cartItems.add(cartItem);

             /* ******************8CartitemCode array adding*/

                            cartiemcode.setProductId(cartObj.getInt("id"));
                            double unitPricec =  cartObj.getDouble("unit_price");
                            double taxPricec =  cartObj.getDouble("tax_amount");

                            cartiemcode.setProductName(cartObj.getString("name"));
                            cartiemcode.setProductCode(cartObj.getString("code"));
                            cartiemcode.setReturnQuantity(0);
                            cartiemcode.setPieceQuantity(cartObj.getInt("quantity"));
                            cartiemcode.setCost( cartObj.getDouble("cost"));
                            cartiemcode.setTotalPrice( cartObj.getDouble("total"));

                            float vatc = (float) cartObj.getDouble("tax");

                            cartiemcode.setTax(vatc);

                            /////

                            double salePricec = getSalePrice(unitPricec, cartItem.getTax());
                            cartiemcode.setProductPrice(unitPricec);
                            cartiemcode.setNetPrice(getWithoutTaxPrice(unitPricec, cartItem.getTax()));
                            cartiemcode.setTaxValue(getTaxPrice(unitPricec, cartItem.getTax()));
                            cartiemcode.setSalePrice(salePricec);
                            cartiemcode.setOrderType(PRODUCT_UNIT_PIECE);


                            /* ******************* */
                            productCodeList.add(cartiemcode);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (cartItems.isEmpty())
                    setErrorViewInvoiceDetails("No Data", "", false);
                else
                    setSearchableCart(cartItems, productCodeList);


            }

            @Override
            public void onErrorResponse(String error) {

                setErrorViewInvoiceDetails(error, getString(R.string.error_subtitle_failed_one_more_time), true);


            }
        }, object);


    }


    /*private void setSearchableCart(ArrayList<CartItem> list) {


        final CartSpinnerDialog spinnerCart = new CartSpinnerDialog(getActivity(), list, "Select Product", R.style.DialogAnimations_SmileWindow);// With 	Animation

        tvProductSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showSpinerDialog();
            }
        });

        tvCodeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showCodeSpinerDialog();
            }
        });

        spinnerCart.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(Object item, int position) {

                SELECTED_CART = (CartItem) item;

                setCartValue();
            }


        });

        setProgressBarInvoice(false);

        viewInvoiceDetails.setVisibility(View.VISIBLE);
        evInvoiceDetals.setVisibility(View.GONE);
    }*/

    private void setSearchableCart(ArrayList<CartItem> list, final ArrayList<CartItemCode> listCode) {

        final CartSpinnerDialogNew spinnerCart = new CartSpinnerDialogNew(getActivity(), list, listCode, "Select Product", R.style.DialogAnimations_SmileWindow);// With 	Animation

        tvProductSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showSpinerDialog();
            }
        });

        tvCodeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerCart.showCodeSpinerDialog();
            }
        });

        spinnerCart.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(Object item, int position) {

                SELECTED_CART = (CartItem) item;

                setCartValue();
            }
        });

        setProgressBarInvoice(false);

        viewInvoiceDetails.setVisibility(View.VISIBLE);
        evInvoiceDetals.setVisibility(View.GONE);
    }

    private void setCartValue() {

        etReturnQuantity.setText("");
        tvUnitPrice.setText(getAmount(SELECTED_CART.getNetPrice()));
        tvActualQuantity.setText(String.valueOf(SELECTED_CART.getPieceQuantity()));
        tvProductSpinner.setText(SELECTED_CART.getProductName());
        tvCodeSpinner.setText(SELECTED_CART.getProductCode());

        etReturnQuantity.requestFocus();
        etReturnQuantity.setFocusableInTouchMode(true);

        showSoftKeyboard(etReturnQuantity);

    }

    private void clearProductView() {
        tvProductSpinner.setText("");
        tvCodeSpinner.setText("");
        etReturnQuantity.setText("");
        tvUnitPrice.setText("");
        tvActualQuantity.setText("");
        SELECTED_CART = null;


        tvTotalRefund.setFocusable(true);

        hideSoftKeyboard();

    }

    private boolean returnValidate() {

        boolean status = false;

        String s = tvProductSpinner.getText().toString().trim();
        int returnQuantity = TextUtils.isEmpty(etReturnQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etReturnQuantity.getText().toString().trim());

        tvProductSpinner.setError(null);
        etReturnQuantity.setError(null);

        if (SELECTED_CART == null) {
            status = false;
            tvProductSpinner.setError("Select Product");

        } else if (TextUtils.isEmpty(s)) {
            status = false;
            tvProductSpinner.setError("Select Product");

        }else if (TextUtils.isEmpty(etReturnQuantity.getText().toString().trim())) {
            etReturnQuantity.setError("Enter Quantity");
            status = false;

        } else if (returnQuantity > SELECTED_CART.getPieceQuantity()) {
            etReturnQuantity.setError("Actual Quantity is " + SELECTED_CART.getPieceQuantity());
            status = false;

        } else if (returnQuantity <= 0) {
            etReturnQuantity.setError("Product already returned");
            status = false;

        } else
            status = true;

        return status;
     }

    private boolean isValidate() {

        boolean status = false;
        if (SELECTED_INVOICE == null) {
            Toast.makeText(getContext(), "Please Select Invoice", Toast.LENGTH_SHORT).show();
            status = false;

        } else if (returnProductAdapter.getReturnItems().isEmpty()) {
            Toast.makeText(getContext(), "Return Products is Empty..!", Toast.LENGTH_SHORT).show();
            status = false;
        } else if (SELECTED_SHOP == null) {
            Toast.makeText(getContext(), "Please Select Customer", Toast.LENGTH_SHORT).show();
            status = false;
        } else
            status = true;


        return status;
    }

    private void postSaleReturn(final String returnType) {

        if (!isValidate())
            return;

        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Please wait...", false, false);

        final ArrayList<CartItem> list = returnProductAdapter.getReturnItems();

        JSONObject object = new JSONObject();
        JSONObject returnObj = new JSONObject();
        final JSONArray productArray = new JSONArray();

        Log.e("calling", "1");

        try {

            returnObj.put("invoice_id", SELECTED_INVOICE.getInvoiceNo());
            returnObj.put(CUSTOMER_KEY, SELECTED_SHOP.getShopId());
            returnObj.put("total", returnProductAdapter.getNetTotal());
            returnObj.put("grand_total", returnProductAdapter.getGrandTotal());
            returnObj.put("return_type", returnType);
            returnObj.put("latitude", str_Latitude);
            returnObj.put("longitude", str_Longitude);

            for (CartItem c : list) {

                JSONObject obj = new JSONObject();
                obj.put("product_id", c.getProductId());
                obj.put("unit_price", c.getProductPrice());
                obj.put("tax", c.getTax());
                obj.put("return_quantity", c.getReturnQuantity());
                obj.put("refund_Amount", c.getTotalPrice());
                productArray.put(obj);
            }

            returnObj.put("ReturnedProduct", productArray);

            object.put(EXECUTIVE_KEY, new SessionAuth(getContext()).getExecutiveId());
            object.put(DAY_REGISTER_KEY, dayRegId);
            object.put("SalesReturn", returnObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String strDate = getDateTime();
        final Sales saleReturn = new Sales();

        saleReturn.setCustomerId(SELECTED_SHOP.getShopId());
        saleReturn.setDate(strDate);
        saleReturn.setTotal(returnProductAdapter.getGrandTotal());
        saleReturn.setSaleType(returnType);
        saleReturn.setPaid(returnProductAdapter.getGrandTotal());
        saleReturn.setSalePo("");
        saleReturn.setInvoiceCode(SELECTED_INVOICE.getInvoiceNo());
        saleReturn.setCartItems(list);
        saleReturn.setDiscountTotal(0);

      //  Log.e("calling", "2");

        Log.e("return data", ""+object);

      //  printLog(TAG,"return data "+object);

        webSaleReturn(new WebService.webObjectCallback() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("return response", ""+response.toString());

                try {
                    if (response.getString("status").equals("Success")) {
                        myDatabase.updateVisitStatus(SELECTED_SHOP.getShopId(), REQ_RETURN_TYPE, "","", "");  // update sales return status to local db

                        if (returnType.equals(getActivity().getString(R.string.refund)))
                        for (CartItem c : list) {
                            myDatabase.updateStock(c, REQ_RETURN_TYPE);
                        }

                        Toast.makeText(getContext(), "Successfully", Toast.LENGTH_SHORT).show();

//                        print
                        Intent intent = new Intent(getActivity(), ReturnPreviewActivity.class);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ActivityConstants.ACTIVITY_INVOICE_RETURN);

                        intent.putExtra(INVOICE_RETURN_VALUE_KEY, saleReturn);
                        intent.putExtra(SHOP_VALUE_KEY, SELECTED_SHOP);

                        pd.dismiss();

                        startActivity(intent);
                        getActivity().finish();

                        // Reload current fragment
//                        Fragment frg = getActivity().getSupportFragmentManager().findFragmentByTag(FRAGMENT_INVOICE);
//                        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                        ft.detach(frg);
//                        ft.attach(frg);
//                        ft.commit();


                    } else
                        Toast.makeText(getContext(), response.getString("status"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();

            }

            @Override
            public void onErrorResponse(String error) {

                pd.dismiss();
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        }, object);

     }

    //     ProgressBar Main
    private void setProgressBarMain(boolean isVisible) {

        if (isVisible) {
            pbMain.setVisibility(View.VISIBLE);
            viewLayout.setVisibility(View.GONE);
            evMain.setVisibility(View.GONE);

        } else {
            pbMain.setVisibility(View.GONE);

        }
      }

    private void setProgressBarInvoice(boolean isVisible) {

        if (isVisible) {
            pbInvoiceDetals.setVisibility(View.VISIBLE);
            viewInvoiceDetails.setVisibility(View.GONE);
            evInvoiceDetals.setVisibility(View.GONE);

        } else {
            pbInvoiceDetals.setVisibility(View.GONE);

        }
    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {

        viewLayout.setVisibility(View.GONE);
        evMain.setVisibility(View.VISIBLE);

        setProgressBarMain(false);
        evMain.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());

        evMain.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                if (SELECTED_SHOP != null)
                    getInvoiceList(SELECTED_SHOP.getShopId());
            }
        });
    }


    //set ErrorView
    private void setErrorViewInvoiceDetails(final String title, final String subTitle, boolean isRetry) {

        viewInvoiceDetails.setVisibility(View.GONE);
        evInvoiceDetals.setVisibility(View.VISIBLE);

        setProgressBarInvoice(false);
        evInvoiceDetals.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());

        evInvoiceDetals.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                if (SELECTED_INVOICE != null)
                    getInvoiceDetails(SELECTED_INVOICE.getInvoiceNo());
              }
          });
      }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_sales_return_invoice_addCart:

                if (returnValidate()) {

                    Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.anim_alpha);
                    v.setAnimation(anim);
                    int returnQuantity = TextUtils.isEmpty(etReturnQuantity.getText().toString().trim()) ? 0 : Integer.valueOf(etReturnQuantity.getText().toString().trim());

                    int quantity = SELECTED_CART.getTypeQuantity() - returnQuantity;

                    SELECTED_CART.setReturnQuantity(returnQuantity);
                //  SELECTED_CART.setPieceQuantity(returnQuantity);
                    SELECTED_CART.setTypeQuantity(returnQuantity);
                    SELECTED_CART.setPiecepercart(1);
                    SELECTED_CART.setTotalPrice(SELECTED_CART.getSalePrice() * returnQuantity);

                    returnProductAdapter.returnItem(SELECTED_CART);

                    clearProductView();

                }
                break;
            case R.id.button_sales_return_invoice_finish:

//                String type = "Refund";
//
//                postSaleReturn(type);

                final ReturnTypeDialog paymentDialog = new ReturnTypeDialog(getActivity(), new ReturnTypeDialog.ReturnTypeClickListener() {
                    @Override
                    public void onReturnTypeClick(String type) {

                            postSaleReturn(type);

                    }
                });

                paymentDialog.show();

                break;

        }
    }


    public void onInvoiceChange(Invoice invoice) {
        this.SELECTED_INVOICE = invoice;
        getInvoiceDetails(SELECTED_INVOICE.getInvoiceNo());
        tvInvoiceTitle.setText(String.valueOf(SELECTED_INVOICE.getInvoiceNo()));

    }


    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        try {


            if (getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
            }

        } catch (NullPointerException e) {
            e.getMessage();
        }
    }


    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        if (inputMethodManager != null) {
//            inputMethodManager.showSoftInput(view, 0);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    /**
     * Check InterNet
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }



    private static String getDateTime() {

        Date date = new Date();
        return dbDateFormat.format(date);
    }

    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();
            String Text = "My current location is: " + "Latitude = "
                    + loc.getLatitude() + "\nLongitude = " + loc.getLongitude();

            str_Latitude = "" + loc.getLatitude();
            str_Longitude = "" + loc.getLongitude();

            //  tvNetTotal.setText("Lat : "+str_Latitude+" / Long : "+str_Longitude);
            /*Toast.makeText(getActivity(), Text, Toast.LENGTH_SHORT)
                    .show();*/
            Log.d("TAG", "Starting..");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

}
