package com.vansale.icresp.nasreen.listener;


/**
 * Created by mentor on 21/10/17.
 */

public interface ReceiptTypeClickListener {
    void onReceiptClick(String type, String cheque, String bankName, String remark, String issueDate, String clearDate);
}
